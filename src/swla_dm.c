/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE
#include <stdio.h>

#include "swla/swla_commonLib.h"
#include "swla/swla_dm.h"

#define ME "swlDmEv"

/*
 * @brief return string of full datamodel parameter path
 *
 * @param param amx parameter context
 * @param indexed wether returned path is in indexed format
 *
 * @return string with full path (to be freed by caller) or NULL in case of error
 *
 */
char* swl_dm_getParamPath(amxd_param_t* param, bool indexed) {
    char* opath = amxd_object_get_path(amxd_param_get_owner(param), (indexed ? AMXD_OBJECT_INDEXED : AMXD_OBJECT_NAMED));
    ASSERTS_NOT_NULL(opath, NULL, ME, "NULL");
    char* ppath = NULL;
    int ret = asprintf(&ppath, "%s.%s", opath, amxd_param_get_name(param));
    free(opath);
    ASSERT_FALSE(ret < 0, NULL, ME, "fail to build path");
    return ppath;
}

/*
 * @brief logging function: dump new parameter value
 */
static void s_dumpParam(amxd_param_t* param, const amxc_var_t* const newValue) {
    ASSERTS_FALSE(amxc_var_is_null(newValue), , ME, "NULL");
    char* ppath = swl_dm_getParamPath(param, false);
    ASSERTS_NOT_NULL(ppath, , ME, "NULL");
    char* pvalue = swl_typeCharPtr_fromVariantDef((amxc_var_t*) newValue, NULL);
    if(pvalue != NULL) {
        SAH_TRACEZ_INFO(ME, "%s = %s", ppath, pvalue);
        free(pvalue);
    }
    free(ppath);
}

/*
 * @brief logging function: dump new object's parameter values
 */
static void s_dumpObjParams(amxd_object_t* obj, amxc_var_t* params) {
    amxc_var_for_each(elt, params) {
        const char* key = amxc_var_key(elt);
        s_dumpParam(amxd_object_get_param_def(obj, key), elt);
    }
}

/*
 * @brief filter out passive parameters:
 * 1) non-configurable parameters: when read only, volatile, or instance counter.
 * 2) optionally, remove parameters with empty/null values
 *
 * @param (in) object updated object (singleton, instance)
 * @param (in/out) params htable of object parameters (updated or initiated)
 * @param (in) filterEmpty optional flag to remove parameters with empty/null values
 */
static void s_filterOutPassiveParams(amxd_object_t* object, amxc_var_t* params, bool filterEmpty) {
    ASSERTS_NOT_NULL(object, , ME, "NULL");
    ASSERTS_NOT_NULL(params, , ME, "NULL");
    amxc_var_t emptyParam;
    amxc_var_init(&emptyParam);
    amxc_var_for_each(elt, params) {
        if(filterEmpty) {
            int result = -1;
            amxc_var_set_type(&emptyParam, amxc_var_type_of(elt));
            if(!amxc_var_compare(elt, &emptyParam, &result) && !result) {
                amxc_var_delete(&elt);
                continue;
            }
        }
        const char* key = amxc_var_key(elt);
        amxd_param_t* param = amxd_object_get_param_def(object, key);
        if((param == NULL) ||
           (param->attr.read_only) ||
           (param->attr.variable) ||
           (param->attr.counter)) {
            amxc_var_delete(&elt);
            continue;
        }
    }
    amxc_var_clean(&emptyParam);
}

/*
 * @brief process object related received event:
 * - call instance addition handler with all configured parameters
 * - call object addition handler with all configured parameters
 * - fetch watched params in list of those changed, and call their relative handlers
 *
 * @param dm amx datamodel context
 * @param hdlrs struct including the event handlers
 * @param sig_name signal name
 * @param eventData event data including the altered object (and eventually list of changed parameters)
 * @param priv private user data
 *
 * @return SWL_RC_OK on success, error code otherwise
 */
swl_rc_ne swla_dm_procObjEvt(amxd_dm_t* dm, swla_dm_objEvtHdlrs_t* hdlrs, const char* const sig_name, const amxc_var_t* const eventData, void* priv) {
    ASSERTI_NOT_NULL(eventData, SWL_RC_INVALID_PARAM, ME, "no data");
    const char* eventName = sig_name;
    if(!swl_str_startsWith(eventName, "dm:")) {
        eventName = GET_CHAR(eventData, "notification");
    }
    ASSERTI_TRUE(swl_str_startsWith(eventName, "dm:"), SWL_RC_OK, ME, "not dm event");
    ASSERTI_NOT_NULL(hdlrs, SWL_RC_OK, ME, "NULL");

    amxd_object_t* object = amxd_dm_signal_get_object(dm, eventData);
    ASSERTS_NOT_NULL(object, SWL_RC_OK, ME, "Not object event");
    const char* path = amxd_dm_signal_get_path(dm, eventData);
    amxc_var_t params;
    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxd_object_type_t objType = amxd_object_get_type(object);
    if(swl_str_matches(eventName, "dm:object-changed")) {
        amxc_var_for_each(elt, GET_ARG(eventData, "parameters")) {
            amxc_var_set_key(&params, amxc_var_key(elt), GET_ARG(elt, "to"), AMXC_VAR_FLAG_COPY);
        }
        s_filterOutPassiveParams(object, &params, false);
    } else if((swl_str_matches(eventName, "dm:object-added")) && (objType == amxd_object_singleton)) {
        /*
         * when singleton obj is added, event does not include the default param values from definition
         * That's why we get the configurable params, and handle them as changed (ie with initial values).
         */
        amxd_object_get_params_filtered(object, &params,
                                        "attributes.volatile==false && "
                                        "attributes.read-only==false && "
                                        "attributes.counter==false",
                                        amxd_dm_access_private);
        //filter out non-configurable params and those with initial empty/null values
        s_filterOutPassiveParams(object, &params, true);
    } else if(swl_str_matches(eventName, "dm:instance-added")) {
        /*
         * when new obj instance is added from odl save/defaults with included param values,
         * the action write handlers are only called for params which values are different than in definition.
         * So, we call here all registered obj's param handlers, to get both configured and native param values
         * and report them in relative internal ctx.
         */
        uint32_t index = GET_UINT32(eventData, "index");
        object = amxd_object_get_instance(object, NULL, index);
        if(object == NULL) {
            SAH_TRACEZ_ERROR(ME, "No instance[%d] in templ obj (%s)", index, path);
            amxc_var_clean(&params);
            return SWL_RC_ERROR;
        }
        amxc_var_copy(&params, GET_ARG(eventData, "parameters"));
        //filter out non-configurable params and those with initial empty/null values
        s_filterOutPassiveParams(object, &params, true);
        if(hdlrs->instAddedCb != NULL) {
            SAH_TRACEZ_INFO(ME, "handle new instance");
            s_dumpObjParams(object, &params);
            hdlrs->instAddedCb(priv, object, &params);
        }
        /*
         * propagate initial instance's param values in relative handlers
         */
    } else {
        SAH_TRACEZ_INFO(ME, "unhandled event(%s)/path(%s)/oType(%d)", eventName, path, objType);
        amxc_var_clean(&params);
        return SWL_RC_OK;
    }

    //notify when having modified configurable parameters
    if(amxc_var_get_first(&params) != NULL) {
        if(hdlrs->objChangedCb != NULL) {
            SAH_TRACEZ_INFO(ME, "handle object update");
            s_dumpObjParams(object, &params);
            hdlrs->objChangedCb(priv, object, &params);
        }
        for(uint32_t i = 0; i < hdlrs->nrParamHdlrs; i++) {
            const char* key = hdlrs->paramHdlrs[i].paramName;
            amxc_var_t* val = GET_ARG(&params, key);
            if(amxc_var_is_null(val)) {
                continue;
            }
            amxd_param_t* param = amxd_object_get_param_def(object, key);
            if(param == NULL) {
                continue;
            }
            if(hdlrs->paramHdlrs[i].paramChangedCb != NULL) {
                SAH_TRACEZ_INFO(ME, "handle param update");
                s_dumpParam(param, val);
                hdlrs->paramHdlrs[i].paramChangedCb(priv, object, param, val);
            }
        }
    }

    amxc_var_clean(&params);
    return SWL_RC_OK;
}

/*
 * @brief process object related received events, of local dm context
 * (dm retrieved from registered amxb_bus_ctx_t set with swl_lib_initialize)
 *
 * @param hdlrs struct including the event handlers
 * @param sig_name signal name
 * @param eventData event data including the altered object (and eventually list of changed parameters)
 * @param priv private user data
 *
 * @return SWL_RC_OK on success, error code otherwise
 */
swl_rc_ne swla_dm_procObjEvtOfLocalDm(swla_dm_objEvtHdlrs_t* hdlrs, const char* const sig_name, const amxc_var_t* const eventData, void* priv) {
    amxd_dm_t* dm = swl_lib_getLocalDm();
    ASSERT_NOT_NULL(dm, SWL_RC_ERROR, ME, "local dm is null");
    return swla_dm_procObjEvt(dm, hdlrs, sig_name, eventData, priv);
}

/*
 * @brief get all parameter values of an object.
 * If the objUpdate parameter is not NULL, the object's parameters value update,
 * triggered by the read handler, will be blocked:
 * the current values of the object's parameters will be returned.
 *
 * @param object Pointer to a data model object.
 * @param retval An initialized variant which contains the values of all parameters.
 * @param objUpdate Pointer to a data structure to Blok/Allow the call of the object's read handler. This pointer can be NULL.
 *
 * return amxd_status_ok on success.
 */
amxd_status_t swla_dm_getObjectParams(amxd_object_t* const object, amxc_var_t* const retval, swla_dm_objActionReadCtx_t* onActionReadCtx) {
    SWLA_DM_OBJ_BLOCK_READ_HDLR_CALL(onActionReadCtx);
    amxd_status_t status = amxd_object_get_params(object, retval, amxd_dm_access_private);
    SWLA_DM_OBJ_ALLOW_READ_HDLR_CALL(onActionReadCtx);
    return status;
}

bool s_isReadHdlrCallAllowed(swla_dm_objActionReadCtx_t* onActionReadCtx) {
    bool allowReadHdlrCall = true;
    ASSERTS_NOT_NULL(onActionReadCtx, allowReadHdlrCall, ME, "onActionReadCtx is NULL");
    if(onActionReadCtx->blockReadHdlrCall) {
        allowReadHdlrCall = false;
    } else if(!swl_timespec_isZero(&onActionReadCtx->lastReadHdlrCallTime)) {
        swl_timeSpecMono_t now;
        swl_timespec_getMono(&now);
        uint32_t minRefreshPeriod = (onActionReadCtx->minRefreshPeriodMs > 0) ? onActionReadCtx->minRefreshPeriodMs : DEFAULT_REFRESH_PERIOD_MS;
        allowReadHdlrCall = swl_timespec_diffToMillisec(&onActionReadCtx->lastReadHdlrCallTime, &now) > minRefreshPeriod;
    }

    return allowReadHdlrCall;
}

/*
 * @brief process object read action.
 * when objUpdate is not null, verify whether invoking the readHdlr function
 * is necessary to update the values of the object's.
 * if the updte is not necessary, then get only the current values of the object's parameters.
 *
 * @param object The data model object on which the action is invoked.
 * @param param This is always NULL for the object read action.
 * @param reason The action reason: action_object_read.
 * @param args A variant containing the action arguments.
 * @param retval A variant that must be filled with the action return data.
 * @param priv An opaque pointer (user data), that is added when the action callback was set.
 * @param objUpdate Pointer to a data structure containing the object's read handler last call
 *                  and indicating whether the call of the read handler should be blocked. This pointer can be NULL.
 * @param readHdlr Handler of the object read action.
 *
 * return amxd_status_ok if successful.
 */
amxd_status_t swla_dm_procObjActionRead(amxd_object_t* const object,
                                        amxd_param_t* const param,
                                        amxd_action_t reason,
                                        const amxc_var_t* const args,
                                        amxc_var_t* const retval,
                                        void* priv,
                                        swla_dm_objActionReadCtx_t* onActionReadCtx, objActionReadHandler_f readHdlr) {
    ASSERT_NOT_NULL(object, amxd_status_object_not_found, ME, "The object is null");
    ASSERT_EQUALS(reason, action_object_read, amxd_status_function_not_implemented, ME, "The action reason differs to action_object_read");
    ASSERT_NOT_NULL(readHdlr, amxd_status_function_not_implemented, ME, "The read handler is null");

    bool allowReadHdlrCall = s_isReadHdlrCallAllowed(onActionReadCtx);

    if(allowReadHdlrCall) {
        swl_rc_ne retVal = readHdlr(object);

        if(retVal < SWL_RC_OK) {
            return amxd_status_unknown_error;
        } else if(retVal == SWL_RC_CONTINUE) {
            return amxd_status_deferred;
        }

        if(onActionReadCtx != NULL) {
            swl_timespec_getMono(&onActionReadCtx->lastReadHdlrCallTime);
        }
    }

    return amxd_action_object_read(object, param, reason, args, retval, priv);
}
