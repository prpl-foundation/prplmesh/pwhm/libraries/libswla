/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/


#include "swla/swla_circTable.h"

#include "swla/swla_commonLib.h"

#define ME "swlCTbl"

/**
 * This module implements a circular table, i.e. a circular buffer of tuples,
 * which belong to a given tupleType.
 */


/**
 * Writes a given column to a map. Oldest values will always go first.
 * The name of the column will be the value of names at type index if names is not zero, and the name at that index is not zero
 * otherwise it will be the index stringified.
 *
 * @param map: the map to which the column must be written.
 * @param table: the table to dump
 * @param typeIndex: the index of the type in the tupleType
 */
void swl_circTable_mapAddColumn(amxc_var_t* map, swl_circTable_t* table, size_t typeIndex) {
    ASSERT_NOT_NULL(table, , ME, "NULL");
    ASSERT_NOT_NULL(map, , ME, "NULL");

    char* name = (table->names != NULL ? table->names[typeIndex] : NULL);
    if(table->table.curNrTuples < table->table.nrTuples) {
        swl_table_mapAddColumnCharOffset(map, name, &table->table, typeIndex, 0, table->table.curNrTuples);
    } else {
        swl_table_mapAddColumnCharOffset(map, name, &table->table, typeIndex, table->nextFill, table->table.curNrTuples);
    }
}

/**
 * Writes all the columns of the table to the map. Oldest values will always go first.
 * The name of each column will be the value of names at type index if names is not zero, and the name at that index is not zero
 * otherwise it will be the index stringified.
 *
 * @param map: the map to which the column must be written.
 * @param table: the table to dump
 */
void swl_circTable_mapAdd(amxc_var_t* map, swl_circTable_t* table) {
    ASSERT_NOT_NULL(table, , ME, "NULL");
    ASSERT_NOT_NULL(map, , ME, "NULL");
    for(size_t i = 0; i < table->table.tupleType->nrTypes; i++) {
        swl_circTable_mapAddColumn(map, table, i);
    }
}

/**
 * Dump the contents of a table in the given string.
 * @param table: the table to dump
 * @param string: the string in which to write the dump
 */
void swl_circTable_toString(swl_circTable_t* table, amxc_string_t* string) {
    ASSERT_NOT_NULL(table, , ME, "NULL");
    ASSERT_NOT_NULL(string, , ME, "NULL");
    size_t offset = (swl_circTable_isFilled(table) ? table->nextFill : 0);
    swl_table_toStringOffset(&table->table, string, table->names, offset, table->table.curNrTuples);
}
