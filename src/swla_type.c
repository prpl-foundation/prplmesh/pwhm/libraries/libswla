/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "swla/swla_commonLib.h"

#define ME "swlType"


/**
 * Convert the srcData of type type to a variant. Variant type depends on swl type.
 */
bool swl_type_toVariant(swl_type_t* type, amxc_var_t* tgt, const swl_typeData_t* srcData) {
    ASSERT_NOT_NULL(type, false, ME, "NULL");
    ASSERT_NOT_NULL(tgt, false, ME, "NULL");
    if(type->typeFun->toVariant != NULL) {
        return type->typeFun->toVariant(type, tgt, srcData);
    } else {
        char buffer[SWL_TYPE_BUF_SIZE] = {0};
        size_t nrBytes = type->typeFun->toChar(type, buffer, sizeof(buffer), srcData);

        if((nrBytes == 0) || (nrBytes >= sizeof(buffer))) {
            SAH_TRACEZ_ERROR(ME, "toChar error");
            return false;
        }

        return ( amxc_var_set_cstring_t(tgt, buffer) == 0);
    }
}

/**
 * Convert the src variant to a data entry of type type. This entry will be filled in with data.
 */
bool swl_type_fromVariant(swl_type_t* type, swl_typeEl_t* tgtData, const amxc_var_t* src) {
    ASSERT_NOT_NULL(type, false, ME, "NULL");
    ASSERT_NOT_NULL(tgtData, false, ME, "NULL");
    if(type->typeFun->fromVariant != NULL) {
        return type->typeFun->fromVariant(type, tgtData, src);
    } else if(amxc_var_type_of(src) == AMXC_VAR_ID_CSTRING) {
        const char* data = amxc_var_constcast(cstring_t, src);
        return type->typeFun->fromChar(type, tgtData, data);
    } else {
        char* data = amxc_var_dyncast(cstring_t, src);
        bool retVal = type->typeFun->fromChar(type, tgtData, data);
        free(data);
        return retVal;
    }
}

bool swl_type_toObjectParam(swl_type_t* type, amxd_object_t* object, const char* param, swl_typeData_t* srcData) {
    ASSERT_NOT_NULL(type, false, ME, "NULL");
    ASSERT_NOT_NULL(param, false, ME, "NULL");
    ASSERT_NOT_NULL(object, false, ME, "NULL object for %s", param);
    amxc_var_t myVar;
    amxc_var_init(&myVar);
    ASSERT_TRUE(swl_type_toVariant(type, &myVar, srcData), false, ME, "convert fail");
    char* data = amxc_var_get_cstring_t(&myVar);
    free(data);
    amxd_status_t status = amxd_object_set_param(object, param, &myVar);
    amxc_var_clean(&myVar);
    const char* oname = amxd_object_get_name(object, AMXD_OBJECT_NAMED);
    ASSERT_EQUALS(status, amxd_status_ok, false, ME, "fail to set obj(%s) param(%s) status(%s)", oname, param, amxd_status_string(status));
    return true;
}

bool swl_type_toTransParam(swl_type_t* type, amxd_trans_t* const trans, const char* param, swl_typeData_t* srcData) {
    ASSERT_NOT_NULL(type, false, ME, "NULL");
    ASSERT_NOT_NULL(trans, false, ME, "NULL");
    amxc_var_t myVar;
    amxc_var_init(&myVar);
    ASSERT_TRUE(swl_type_toVariant(type, &myVar, srcData), false, ME, "convert fail");
    amxd_status_t status = amxd_trans_set_param(trans, param, &myVar);
    amxc_var_clean(&myVar);
    ASSERT_EQUALS(status, amxd_status_ok, false, ME, "fail to set trans param(%s) status(%s)", param, amxd_status_string(status));
    return true;
}

/*
 * @brief commit object's parameter value on local datamodel with dedicated transaction
 */
bool swl_type_commitObjectParam(swl_type_t* type, amxd_object_t* const object, const char* param, swl_typeData_t* srcData) {
    ASSERT_NOT_NULL(type, false, ME, "NULL");
    ASSERT_NOT_NULL(param, false, ME, "NULL");
    ASSERT_NOT_NULL(object, false, ME, "NULL object for %s", param);

    amxd_trans_t trans;
    const char* oname = amxd_object_get_name(object, AMXD_OBJECT_NAMED);
    ASSERT_STR(oname, false, ME, "Invalid object");
    amxd_status_t status = swl_object_prepareTransaction(&trans, object);
    ASSERT_EQUALS(status, amxd_status_ok, false, ME, "fail to prepare transaction obj(%s) param(%s) status(%s)", oname, param, amxd_status_string(status));

    if(!swl_type_toTransParam(type, &trans, param, srcData)) {
        SAH_TRACEZ_ERROR(ME, "fail to set obj(%s) param(%s)", oname, param);
        amxd_trans_clean(&trans);
        return false;
    }

    status = swl_object_finalizeTransactionOnLocalDm(&trans);
    ASSERT_EQUALS(status, amxd_status_ok, false, ME, "fail to finalize transaction obj(%s) param(%s) status(%s)", oname, param, amxd_status_string(status));
    return true;
}

bool swl_type_fromObjectParam(swl_type_t* type, amxd_object_t* object, const char* param, swl_typeEl_t* tgtData) {
    ASSERT_NOT_NULL(type, false, ME, "NULL");
    ASSERT_NOT_NULL(tgtData, false, ME, "NULL");
    const amxc_var_t* tmpVar = amxd_object_get_param_value(object, param);
    ASSERTS_NOT_NULL(tmpVar, false, ME, "NULL");
    return swl_type_fromVariant(type, tgtData, tmpVar);
}

bool swl_type_addToMap(swl_type_t* type, amxc_var_t* map, const char* param, swl_typeData_t* srcData) {
    ASSERT_NOT_NULL(type, false, ME, "NULL");
    ASSERT_NOT_NULL(map, false, ME, "NULL");
    amxc_var_t* myVar = amxc_var_add_new_key(map, param);
    ASSERT_TRUE(swl_type_toVariant(type, myVar, srcData), false, ME, "convert fail");
    return true;
}

bool swl_type_findInMap(swl_type_t* type, const amxc_var_t* map, const char* param, swl_typeEl_t* tgtData) {
    ASSERT_NOT_NULL(type, false, ME, "NULL");
    ASSERT_NOT_NULL(tgtData, false, ME, "NULL");
    const amxc_var_t* tmpVar = amxc_var_get_key(map, param, AMXC_VAR_FLAG_DEFAULT);
    ASSERTS_NOT_NULL(tmpVar, false, ME, "NULL");
    return swl_type_fromVariant(type, tgtData, tmpVar);
}



/**
 * Convert the srcData of type type to a variant. Variant type depends on swl type.
 */
bool swl_type_toVariantString(swl_type_t* type, amxc_var_t* tgt, const swl_typeData_t* srcData) {
    ASSERT_NOT_NULL(type, false, ME, "NULL");
    ASSERT_NOT_NULL(tgt, false, ME, "NULL");

    char* data = swl_type_toCStringExt(type, srcData, &g_swl_print_csv);
    ASSERT_NOT_NULL(data, false, ME, "toChar error");

    int ret = amxc_var_set_cstring_t(tgt, data);
    free(data);
    return ret == 0;

}

/**
 * Convert the src variant to a data entry of type type. This entry will be filled in with data.
 */
bool swl_type_fromVariantString(swl_type_t* type, swl_typeEl_t* tgtData, const amxc_var_t* src) {
    ASSERT_NOT_NULL(type, false, ME, "NULL");
    ASSERT_NOT_NULL(tgtData, false, ME, "NULL");
    if(amxc_var_type_of(src) == AMXC_VAR_ID_CSTRING) {
        const char* data = amxc_var_get_const_cstring_t(src);
        return swl_type_fromCharExt(type, tgtData, data, &g_swl_print_csv);
    } else {
        char* data = amxc_var_get_cstring_t(src);
        bool retVal = swl_type_fromCharExt(type, tgtData, data, &g_swl_print_csv);
        free(data);
        return retVal;
    }
}

bool swl_type_toObjectParamString(swl_type_t* type, amxd_object_t* object, const char* param, swl_typeData_t* srcData) {
    ASSERT_NOT_NULL(type, false, ME, "NULL");
    ASSERT_NOT_NULL(param, false, ME, "NULL");
    ASSERT_NOT_NULL(object, false, ME, "NULL object for %s", param);
    amxc_var_t myVar;
    amxc_var_init(&myVar);
    ASSERT_TRUE(swl_type_toVariantString(type, &myVar, srcData), false, ME, "convert fail");
    amxd_status_t success = amxd_object_set_param(object, param, &myVar);
    amxc_var_clean(&myVar);
    const char* oname = amxd_object_get_name(object, AMXD_OBJECT_NAMED);
    ASSERT_EQUALS(success, amxd_status_ok, false, ME, "fail to set obj(%s) param(%s) status(%s)", oname, param, amxd_status_string(success));
    return true;
}

bool swl_type_toTransParamString(swl_type_t* type, amxd_trans_t* const trans, const char* param, swl_typeData_t* srcData) {
    ASSERT_NOT_NULL(type, false, ME, "NULL");
    ASSERT_NOT_NULL(trans, false, ME, "NULL");
    amxc_var_t myVar;
    amxc_var_init(&myVar);
    ASSERT_TRUE(swl_type_toVariantString(type, &myVar, srcData), false, ME, "convert fail");
    amxd_status_t status = amxd_trans_set_param(trans, param, &myVar);
    amxc_var_clean(&myVar);
    ASSERT_EQUALS(status, amxd_status_ok, false, ME, "fail to set trans param(%s) status(%s)", param, amxd_status_string(status));
    return true;
}

bool swl_type_commitObjectParamString(swl_type_t* type, amxd_object_t* object, const char* param, swl_typeData_t* srcData) {
    ASSERT_NOT_NULL(type, false, ME, "NULL");
    ASSERT_NOT_NULL(param, false, ME, "NULL");
    ASSERT_NOT_NULL(object, false, ME, "NULL object for %s", param);

    amxd_trans_t trans;
    const char* oname = amxd_object_get_name(object, AMXD_OBJECT_NAMED);
    amxd_status_t status = swl_object_prepareTransaction(&trans, object);
    ASSERT_EQUALS(status, amxd_status_ok, false, ME, "fail to prepare transaction obj(%s) param(%s) status(%s)", oname, param, amxd_status_string(status));

    if(!swl_type_toTransParamString(type, &trans, param, srcData)) {
        SAH_TRACEZ_ERROR(ME, "fail to set obj(%s) param(%s)", oname, param);
        amxd_trans_clean(&trans);
        return false;
    }

    status = swl_object_finalizeTransactionOnLocalDm(&trans);
    ASSERT_EQUALS(status, amxd_status_ok, false, ME, "fail to finalize transaction obj(%s) param(%s) status(%s)", oname, param, amxd_status_string(status));
    return true;
}

bool swl_type_fromObjectParamString(swl_type_t* type, amxd_object_t* object, const char* param, swl_typeEl_t* tgtData) {
    ASSERT_NOT_NULL(type, false, ME, "NULL");
    ASSERT_NOT_NULL(tgtData, false, ME, "NULL");
    const amxc_var_t* tmpVar = amxd_object_get_param_value(object, param);
    ASSERTS_NOT_NULL(tmpVar, false, ME, "NULL");
    return swl_type_fromVariantString(type, tgtData, tmpVar);
}

bool swl_type_addToMapString(swl_type_t* type, amxc_var_t* map, const char* param, swl_typeData_t* srcData) {
    ASSERT_NOT_NULL(type, false, ME, "NULL");
    ASSERT_NOT_NULL(map, false, ME, "NULL");
    amxc_var_t* myVar = amxc_var_add_new_key(map, param);
    ASSERT_TRUE(swl_type_toVariantString(type, myVar, srcData), false, ME, "convert fail");
    return true;
}
bool swl_type_findInMapString(swl_type_t* type, const amxc_var_t* map, const char* param, swl_typeEl_t* tgtData) {
    ASSERT_NOT_NULL(type, false, ME, "NULL");
    ASSERT_NOT_NULL(tgtData, false, ME, "NULL");
    const amxc_var_t* tmpVar = amxc_var_get_key(map, param, AMXC_VAR_FLAG_DEFAULT);
    ASSERTS_NOT_NULL(tmpVar, false, ME, "NULL");
    return swl_type_fromVariantString(type, tgtData, tmpVar);
}

/**
 * Convert an array to a #amxc_llist_t.
 *
 * @param type: type of elements in the array
 * @param tgtVarList: variant list to which elements are added. Must be initialized.
 *   If not empty, the elements are added at the end.
 * @param srcArray: array of elements that will be converted to elements in `tgtVarList`
 * @param srcArraySize: number of elements in `srcArray`
 *
 * @return True if conversion succeeded, false otherwise. In the latter case, `tgtVarList` can
 *   contain some added elements.
 */
bool swl_type_arrayToVariantList(swl_type_t* type, amxc_var_t* tgtVarList, swl_typeEl_t* srcArray, size_t srcArraySize) {
    ASSERT_NOT_NULL(type, false, ME, "NULL");
    ASSERT_NOT_NULL(tgtVarList, false, ME, "NULL");
    ASSERT_NOT_NULL(srcArray, false, ME, "NULL");

    swl_typeEl_t* currentSrcElement = srcArray;
    for(size_t i = 0; i < srcArraySize; i++) {
        amxc_var_t* tgtElementVariant = amxc_var_add_new(tgtVarList);
        bool ok = type->typeFun->toVariant(type, tgtElementVariant, currentSrcElement);
        if(!ok) {
            SAH_TRACEZ_ERROR(ME, "Error converting element");
            return false;
        }
        ASSERT_TRUE(ok, false, ME, "Error adding to variant list");

        currentSrcElement += type->size;
    }

    return true;
}

/**
 * Convert a #amxc_llist_t to an array.
 *
 * @param type: type of elements we will put in the array
 * @param tgtArray: array to which elements are added.
 * @param tgtArraySize: number of elements in `tgtArray` we can at most write
 * @param srcVarList: elements that will be converted to elements in `tgtArray`
 * @param tgtElementsWritten: Written to how many elements of `tgtArray` are successfully written to.
 *   Can be NULL.
 *
 * @return True if conversion succeeded, false otherwise.
 */
bool swl_type_arrayFromVariantList(swl_type_t* type, swl_typeEl_t* tgtArray, size_t tgtArraySize, amxc_var_t* srcVarList, size_t* tgtElementsWritten) {
    size_t elementsWrittenStack = 0;
    size_t* i = NULL;
    if(tgtElementsWritten != NULL) {
        i = tgtElementsWritten;
        *i = 0;
    } else {
        i = &elementsWrittenStack;
    }

    ASSERT_NOT_NULL(type, false, ME, "NULL");
    ASSERT_NOT_NULL(srcVarList, false, ME, "NULL");
    ASSERT_NOT_NULL(tgtArray, false, ME, "NULL");

    const amxc_llist_t* list = amxc_var_get_const_amxc_llist_t(srcVarList);

    swl_typeEl_t* currentTgtElement = tgtArray;
    amxc_llist_it_t* it = amxc_llist_get_first(list);
    while(it != NULL) {
        ASSERTS_TRUE(*i < tgtArraySize, false, ME, "Array too short");
        amxc_var_t* currentSrcElementVariant = amxc_var_from_llist_it(it);

        bool ok = type->typeFun->fromVariant(type, currentTgtElement, currentSrcElementVariant);
        ASSERT_TRUE(ok, false, ME, "Error converting element from variant");

        currentTgtElement += type->size;

        it = amxc_llist_it_get_next(it);
        (*i)++;
    }

    return true;
}

/**
 * Attempt to write the string value of the pointer of given type to the given object char parameter.
 * returns false if write to object fails, true otherwise.
 */
bool swl_type_arrayObjectParamSetChar(amxd_object_t* object, const char* name, swl_type_t* type, swl_typeEl_t* array, size_t arraySize) {
    ASSERT_NOT_NULL(name, false, ME, "NULL");
    ASSERT_NOT_NULL(object, false, ME, "NULL object for %s", name);
    ASSERT_NOT_NULL(type, false, ME, "NULL");
    ASSERT_NOT_NULL(array, false, ME, "NULL");
    char buffer[SWL_TYPE_BUF_SIZE] = {0};
    swl_type_arrayToChar(type, buffer, sizeof(buffer), array, arraySize);
    return amxd_object_set_value(cstring_t, object, name, buffer);
}

/**
 * Attempt to write the string value of the pointer of given type to the given object char parameter.
 * returns false if write to object fails, true otherwise.
 */
bool swl_type_arrayTransParamSetChar(amxd_trans_t* trans, const char* name, swl_type_t* type, swl_typeEl_t* array, size_t arraySize) {
    ASSERT_NOT_NULL(trans, false, ME, "NULL");
    ASSERT_NOT_NULL(name, false, ME, "NULL");
    ASSERT_NOT_NULL(type, false, ME, "NULL");
    ASSERT_NOT_NULL(array, false, ME, "NULL");
    char buffer[SWL_TYPE_BUF_SIZE] = {0};
    swl_type_arrayToChar(type, buffer, sizeof(buffer), array, arraySize);
    return amxd_trans_set_value(cstring_t, trans, name, buffer);
}


/**
 * Attempt to read a type array from the given object char parameter.
 * Returns zero upon failure (empty mask), else the number of elements parsed that matches the parameter.
 * Expect comma separated value format.
 */
size_t swl_type_arrayObjectParamChar(amxd_object_t* object, const char* name, swl_type_t* type, swl_typeEl_t* tgtArray, size_t tgtArraySize) {
    ASSERT_NOT_NULL(object, 0, ME, "NULL");
    ASSERT_NOT_NULL(name, 0, ME, "NULL");
    ASSERT_NOT_NULL(type, 0, ME, "NULL");
    ASSERT_NOT_NULL(tgtArray, 0, ME, "NULL");
    const char* param = amxd_object_get_value(cstring_t, object, name, NULL);
    ASSERT_NOT_NULL(param, 0, ME, "NULL param %s", name);
    size_t s = swl_type_arrayFromChar(type, tgtArray, tgtArraySize, param);
    return s;
}

/**
 * Serializes array to string and add that string to map.
 *
 * Serializes given `array` (containing of elements of type `type`) to
 * a string and adds that string as value with key `name` to `map`.
 *
 * @return false if a detected error occurs, otherwise true.
 *         On error during serialization, true can still be returned.
 */

bool swl_type_arrayMapAddChar(amxc_var_t* map, const char* name, swl_type_t* type, swl_typeEl_t* array, size_t arraySize) {
    ASSERT_NOT_NULL(map, false, ME, "NULL");
    ASSERT_NOT_NULL(name, false, ME, "NULL");
    ASSERT_NOT_NULL(type, false, ME, "NULL");
    ASSERT_NOT_NULL(array, false, ME, "NULL");
    char buffer[SWL_TYPE_BUF_SIZE] = {0};
    swl_type_arrayToChar(type, buffer, sizeof(buffer), array, arraySize);

    amxc_var_t* tmp = amxc_var_add_new_key_cstring_t(map, name, buffer);
    return tmp != NULL? true : false;
}

/**
 * Tries to read a string from map, and deserializes it to array
 *
 * Searches for a string with name `name` in the map. If that string exists, it will
 * attempt to deserialize that string into the `tgtArray`. It wil only write `tgtArraySize` entries
 * in the array at most.
 *
 * @return the number of values written in tgtArray
 */

size_t swl_type_arrayMapFindChar(amxc_var_t* map, const char* name, swl_type_t* type, swl_typeEl_t* tgtArray, size_t tgtArraySize) {
    ASSERT_NOT_NULL(map, 0, ME, "NULL");
    ASSERT_NOT_NULL(name, 0, ME, "NULL");
    ASSERT_NOT_NULL(type, 0, ME, "NULL");
    ASSERT_NOT_NULL(tgtArray, 0, ME, "NULL");
    const char* param = amxc_var_constcast(cstring_t, amxc_var_get_key(map, name, AMXC_VAR_FLAG_DEFAULT));
    ASSERT_NOT_NULL(param, 0, ME, "NULL param %s", name);
    return swl_type_arrayFromChar(type, tgtArray, tgtArraySize, param);
}
