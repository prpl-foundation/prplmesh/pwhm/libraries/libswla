/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "swla/swla_commonLib.h"

#define ME "swlTEnm"

static bool s_enum_toVariant_cb(swl_type_t* type, amxc_var_t* tgt, const void* srcData) {
    ASSERTS_NOT_NULL(tgt, false, ME, "NULL");
    ASSERT_NOT_NULL(type, false, ME, "NULL");
    uint32_t val = *(uint32_t*) srcData;
    swl_type_enum_t* enumType = (swl_type_enum_t*) type;

    ASSERT_TRUE(val < enumType->maxEnumVal, 0, ME, "Excess value %u / %zu", val, enumType->maxEnumVal);

    return amxc_var_set(cstring_t, tgt, enumType->enumStr[val]);
}

static bool s_enum_fromVariant_cb(swl_type_t* type _UNUSED, uint32_t* tgtData, const amxc_var_t* var) {
    ASSERTS_NOT_NULL(tgtData, false, ME, "NULL");
    ASSERT_NOT_NULL(type, false, ME, "NULL");

    const char* data = amxc_var_constcast(cstring_t, var);
    swl_type_enum_t* enumType = (swl_type_enum_t*) type;
    bool result = false;
    *tgtData = swl_conv_charToEnum_ext(data, enumType->enumStr, enumType->maxEnumVal, enumType->defaultEnumVal, NULL, 0, &result);

    return result;
}

SWL_CONSTRUCTOR static void s_enum_varInit(void) {
    swl_type_enum_fun.toVariant = ((swl_type_toVariant_cb) s_enum_toVariant_cb);
    swl_type_enum_fun.fromVariant = ((swl_type_fromVariant_cb) s_enum_fromVariant_cb);
}
