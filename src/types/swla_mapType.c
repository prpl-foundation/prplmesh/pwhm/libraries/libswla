/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "swl/swl_common.h"
#include "swla/swla_type.h"
#include "swl/swl_map.h"
#include "swl/types/swl_mapType.h"

#define ME "swlMapT"

static bool s_toVariant_cb(swl_type_t* type _UNUSED, amxc_var_t* tgt, const swl_typeData_t* srcData) {
    ASSERTS_NOT_NULL(tgt, false, ME, "NULL");
    swl_map_t* map = (swl_map_t*) srcData;

    if(amxc_var_type_of(tgt) != AMXC_VAR_ID_HTABLE) {
        amxc_var_init(tgt);
        amxc_var_set_type(tgt, AMXC_VAR_ID_HTABLE);
    }

    swl_mapIt_t mapIt;
    SAH_TRACEZ_ERROR(ME, "%u %p", swl_map_isInitialized(map), map);
    swl_map_for_each(mapIt, map) {
        char* keyChar = swl_type_toCStringExt(map->keyType, swl_map_itKey(&mapIt), &g_swl_print_dm);
        ASSERT_NOT_NULL(keyChar, false, ME, "KEY ERROR");
        amxc_var_t* tmpVar = amxc_var_add_new_key(tgt, keyChar);

        bool success = swl_type_toVariant(map->valueType, tmpVar, swl_map_itValue(&mapIt));

        if(!success) {
            SAH_TRACEZ_ERROR(ME, "VALUE ERROR -%s-", keyChar);
            free(keyChar);
            return false;
        }
        free(keyChar);
    }

    return true;
}

static bool s_fromVariant_cb(swl_type_t* type _UNUSED, swl_typeEl_t* tgtData, const amxc_var_t* var) {
    swl_map_t* map = (swl_map_t*) tgtData;
    swl_mapType_t* mapType = (swl_mapType_t*) type;
    if(!swl_map_isInitialized(map)) {
        swl_map_init(map, mapType->keyType, mapType->valueType);
    }

    const amxc_htable_t* htable = &var->data.vm;
    amxc_htable_for_each(it, htable) {
        amxc_var_t* item = amxc_var_from_htable_it(it);
        swl_mapEntry_t* entry = swl_map_alloc(map);
        ASSERT_NOT_NULL(entry, false, ME, "ALLOC FAIL");
        bool ret = swl_type_fromChar(map->keyType, swl_map_getEntryKeyRef(map, entry), it->key);
        ret &= swl_type_fromVariant(map->valueType, swl_map_getEntryValueRef(map, entry), item);
        if(!ret) {
            swl_map_deleteEntry(map, entry);
            return false;
        }
    }
    return true;
}

SWL_CONSTRUCTOR static void s_mapType_var_init(void) {
    swl_map_fun.toVariant = (swl_type_toVariant_cb) s_toVariant_cb;
    swl_map_fun.fromVariant = (swl_type_fromVariant_cb) s_fromVariant_cb;
}
