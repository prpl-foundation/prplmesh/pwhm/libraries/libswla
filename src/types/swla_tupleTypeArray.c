/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "swla/swla_commonLib.h"
#include "swla/swla_table.h"
#include "swla/types/swla_tupleTypeArray.h"

#define ME "swlTta"


/**
 * Write a column of this table to a map, starting at a given offset, limiting to a certain number of elements to add.
 *
 * @param map: the map to which to write
 * @param table: the table of which to set the tuple. Must be dynamic.
 * @param typeIndex: the index of the type to copy
 * @param offSet: the offset from which to start. Will cycle if around to 0 if more elements can be put. Will put max table.size entries.
 * @param nrToAdd: max number of entries to add.
 */
void swl_tta_mapAddColumnCharOffset(amxc_var_t* map, char* name, swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize, size_t index, size_t offset, size_t nrToAdd) {
    ASSERT_NOT_NULL(map, , ME, "NULL");
    ASSERT_NOT_NULL(type, , ME, "NULL");
    ASSERT_NOT_NULL(array, , ME, "NULL");


    swl_type_t* targetType = type->types[index];
    char myData[arraySize * targetType->size];
    size_t nrVals = SWL_MIN(arraySize, nrToAdd);

    swl_tta_columnToArrayOffset(type, myData, nrVals, array, arraySize, index, offset);

    char* tmpName;
    char nameBuf[16];
    if(name != NULL) {
        tmpName = name;
    } else {
        snprintf(nameBuf, sizeof(nameBuf), "%zu", index);
        tmpName = nameBuf;
    }

    swl_type_arrayMapAddChar(map, tmpName, targetType, myData, nrVals);
}

/**
 * Write a column of this table to a map
 *
 * @param map: the map to which to write
 * @param table: the table of which to set the tuple. Must be dynamic.
 * @param typeIndex: the index of the type to copy
 */
void swl_tta_mapAddColumnChar(amxc_var_t* map, char* name, swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize, size_t index) {
    swl_tta_mapAddColumnCharOffset(map, name, type, array, arraySize, index, 0, -1);
}

/**
 * Fill the data from a column by find a char array in the table with the given name.
 *
 * @param map: the map from which to read the data
 * @param name: the name of the map parameter to read
 * @param table: the table for which to fill the column
 * @param index: the index of the type, in which column to put the data
 */
static size_t s_fillDataColumnFromMapParam(amxc_var_t* map, char* name, swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize, size_t index) {
    ASSERT_NOT_NULL(map, 0, ME, "NULL");
    ASSERT_NOT_NULL(type, 0, ME, "NULL");
    ASSERT_NOT_NULL(array, 0, ME, "NULL");

    swl_type_t* targetType = type->types[index];
    size_t offSet = swl_tupleType_getOffset(type, index);

    size_t myDataSize = SWL_MAX((size_t) 1, arraySize) * targetType->size;
    char myData[myDataSize];
    memset(&myData, 0, myDataSize);

    char* tmpName;
    char nameBuf[16];
    if(name != NULL) {
        tmpName = name;
    } else {
        snprintf(nameBuf, sizeof(nameBuf), "%zu", index);
        tmpName = nameBuf;
    }

    size_t nrVals = swl_type_arrayMapFindChar(map, tmpName, targetType, myData, arraySize);

    swl_typeEl_t* tgtDataPtr = array + offSet;
    swl_typeEl_t* srcDataPtr = myData;
    for(size_t i = 0; i < SWL_MIN(arraySize, nrVals); i++) {
        memcpy(tgtDataPtr, srcDataPtr, targetType->size);
        tgtDataPtr += swl_tupleType_size(type);
        srcDataPtr += targetType->size;
    }

    return SWL_MIN(nrVals, arraySize);
}

/**
 * Write table to a map of comma separated arrays
 *
 * @param map: the map to which to write
 * @param names: the names to use when writing to map
 * @param table: the table to write.
 */
void swl_tta_toMapOfChar(amxc_var_t* map, char** names, swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize) {

    for(size_t i = 0; i < type->nrTypes; i++) {
        swl_tta_mapAddColumnChar(map, names != NULL ? names[i] : NULL, type, array, arraySize, i);
    }
}

/**
 * Write table to a map of comma separated arrays, starting with a given offset, limiting to nrToAdd tuples
 *
 * @param map: the map to which to write
 * @param names: the names to use when writing to map
 * @param table: the table to write.
 * @param offSet: the offset from which to start. Will cycle if around to 0 if more elements can be put. Will put max table.size entries.
 * @param nrToAdd: max number of entries to add.
 */
void swl_tta_toMapOfCharOffset(amxc_var_t* map, char** names, swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize, size_t offset, size_t nrToAdd) {
    for(size_t i = 0; i < type->nrTypes; i++) {
        swl_tta_mapAddColumnCharOffset(map, (names != NULL ? names[i] : NULL), type, array, arraySize, i, offset, nrToAdd);
    }
}

/**
 * Read table from a map of comma separated arrays
 *
 * @param map: the map from which to read
 * @param names: the names used
 * @param table: the table to which to read.
 */
void swl_tta_fromMapOfChar(amxc_var_t* map, char** names, swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize) {
    for(size_t i = 0; i < type->nrTypes; i++) {
        s_fillDataColumnFromMapParam(map, (names != NULL ? names[i] : NULL), type, array, arraySize, i);
    }
}

/**
 * Write table to a variant list of variant maps. Names of map entries to be provided
 *
 * @param list: the list to which to write
 * @param names: the names to use when writing to map
 * @param table: the table to write.
 */
void swl_tta_toListOfMaps(amxc_var_t* list, char** names, swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize) {
    for(size_t i = 0; i < arraySize; i++) {
        amxc_var_t* tmpMap = amxc_var_add(amxc_htable_t, list, NULL);
        swl_tupleType_toMap(tmpMap, names, type, swl_tta_getTuple(type, array, arraySize, i));
    }
}

/**
 * Read table from a variant list of variant maps. Name of map entries to be provided
 *
 * @param map: the list from which to read
 * @param names: the names used
 * @param table: the table to which to read.
 */
void swl_tta_fromListOfMaps(amxc_var_t* list, char** names, swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize) {
    size_t maxEntries = SWL_MIN(amxc_llist_size(amxc_var_get_const_amxc_llist_t(list)), arraySize);

    for(size_t i = 0; i < maxEntries; i++) {
        amxc_var_t* listVar = amxc_var_from_llist_it(amxc_llist_get_at(amxc_var_get_const_amxc_llist_t(list), i));
        swl_tupleType_fromMap(listVar, names, type, swl_tta_getTuple(type, array, arraySize, i));
    }
}

/**
 * Dump the contents of a table in the given string, starting at given offset, limited to the nrToAdd elements
 *
 * @param table: the table to dump
 * @param string: the string to write to
 * @param names: the names to use for the columns when writing string. Can be NULL. If not null, must have at least table.tuple.nrTypes entries
 * @param offset: the offset from which to start
 * @param nrToAdd: the number of tuples to add to the string, starting at offset.
 *
 * The dump will cycle back to 0, but will not dump more that table.nrTuple values.
 */
void swl_tta_toStringOffset(swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize, amxc_string_t* string, char** names, size_t offset, size_t nrToAdd) {
    amxc_string_clean(string);

    amxc_var_t tmp;
    amxc_var_init(&tmp);
    amxc_var_set_type(&tmp, AMXC_VAR_ID_HTABLE);

    swl_tta_toMapOfCharOffset(&tmp, names, type, array, arraySize, offset, nrToAdd);
    amxc_var_t targetStr;
    amxc_var_init(&targetStr);
    amxc_var_convert(&targetStr, &tmp, AMXC_VAR_ID_CSTRING);
    const char* dump = amxc_var_constcast(cstring_t, &targetStr);
    amxc_string_append(string, dump, swl_str_len(dump));
    amxc_var_clean(&targetStr);

    amxc_var_clean(&tmp);
}

/**
 * Dump the contents of a table in the given string
 * @param table: the table to dump
 * @param string: the string to write to
 * @param names: the names to use for the columns when writing string. Can be NULL. If not null, must have at least table.tuple.nrTypes entries
 */
void swl_tta_toString(swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize, amxc_string_t* string, char** names) {
    swl_tta_toStringOffset(type, array, arraySize, string, names, 0, -1);
}
