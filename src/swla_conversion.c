/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "swla/swla_commonLib.h"

#define ME "swlConv"

#define SWL_CONV_BUF_SIZE 128

/*
 * @return True on success, false if buffer is too small or invalid arguments.
 */
bool swl_conv_maskToCharSep(char* tgtStr, uint32_t tgtStrSize, uint32_t srcMask, const char* const* srcEnumStrList, uint32_t maxVal, char separator) {
    ASSERT_NOT_NULL(srcEnumStrList, false, ME, "NULL");
    ASSERT_NOT_NULL(tgtStr, false, ME, "NULL");
    ASSERT_FALSE(tgtStrSize == 0, false, ME, "NULL");
    ASSERT_NOT_EQUALS(separator, '\0', false, ME, "Zero separator");
    ASSERT_NOT_EQUALS(maxVal, 0, false, ME, "No entries");
    uint32_t i = 0;
    uint32_t index = 0;
    int printed = 0;
    tgtStr[0] = '\0';

    for(i = 0; i < maxVal; i++) {
        if(srcMask & (1 << i)) {
            if(index) {
                printed = snprintf(&tgtStr[index], tgtStrSize - index, "%c%s", separator, srcEnumStrList[i]);
            } else {
                printed = snprintf(&tgtStr[index], tgtStrSize - index, "%s", srcEnumStrList[i]);
            }
            index += printed;

            ASSERT_TRUE(index < tgtStrSize, false, ME, "Buff too small");
        }
    }
    return true;
}

bool swl_conv_maskToChar(char* tgtStr, uint32_t tgtStrSize, uint32_t srcMask, const char* const* srcEnumStrList, uint32_t maxVal) {
    return swl_conv_maskToCharSep(tgtStr, tgtStrSize, srcMask, srcEnumStrList, maxVal, ',');
}

/**
 * Attempt to write the string value of the enum to the given object char parameter.
 * returns false if enumVal is out of range, or write fails, true otherwise.
 */
bool swl_conv_objectParamSetEnum(amxd_object_t* object, const char* name, uint32_t enumVal, const char* const* srcEnumStrList, uint32_t maxVal) {
    ASSERT_NOT_NULL(name, false, ME, "NULL");
    ASSERT_NOT_NULL(object, false, ME, "NULL object for %s", name);
    ASSERT_NOT_NULL(srcEnumStrList, false, ME, "NULL");
    ASSERT_TRUE(enumVal < maxVal, false, ME, "Invalid %s", name);
    amxd_status_t status = amxd_object_set_cstring_t(object, name, srcEnumStrList[enumVal]);
    return status == amxd_status_ok;
}

/**
 * Attempt to write the string value of the mask to the given object char parameter.
 * returns false if mask does not fit in buffer, or write fails, true otherwise.
 * Will format mask in comma separated value format
 */
bool swl_conv_objectParamSetMask(amxd_object_t* object, const char* name, uint32_t srcMask, const char* const* srcEnumStrList, uint32_t maxVal) {
    ASSERT_NOT_NULL(name, false, ME, "NULL");
    ASSERT_NOT_NULL(object, false, ME, "NULL object for %s", name);
    ASSERT_NOT_NULL(srcEnumStrList, false, ME, "NULL");
    char buffer[SWL_CONV_BUF_SIZE] = {0};
    ASSERT_TRUE(swl_conv_maskToChar(buffer, sizeof(buffer), srcMask, srcEnumStrList, maxVal),
                false, ME, "FAIL");
    return amxd_object_set_value(cstring_t, object, name, buffer) == amxd_status_ok;
}


/**
 * Attempt to write the string value of the enum to the given object char parameter.
 * returns false if enumVal is out of range, or write fails, true otherwise.
 */
bool swl_conv_transParamSetEnum(amxd_trans_t* trans, const char* name, uint32_t enumVal, const char* const* srcEnumStrList, uint32_t maxVal) {
    ASSERT_NOT_NULL(trans, false, ME, "NULL");
    ASSERT_NOT_NULL(name, false, ME, "NULL");
    ASSERT_NOT_NULL(srcEnumStrList, false, ME, "NULL");
    ASSERT_TRUE(enumVal < maxVal, false, ME, "Invalid %s", name);
    amxd_status_t status = amxd_trans_set_cstring_t(trans, name, srcEnumStrList[enumVal]);
    return status == amxd_status_ok;
}

/**
 * Attempt to write the string value of the mask to the given object char parameter.
 * returns false if mask does not fit in buffer, or write fails, true otherwise.
 * Will format mask in comma separated value format
 */
bool swl_conv_transParamSetMask(amxd_trans_t* trans, const char* name, uint32_t srcMask, const char* const* srcEnumStrList, uint32_t maxVal) {
    ASSERT_NOT_NULL(trans, false, ME, "NULL");
    ASSERT_NOT_NULL(name, false, ME, "NULL");
    ASSERT_NOT_NULL(srcEnumStrList, false, ME, "NULL");
    char buffer[SWL_CONV_BUF_SIZE] = {0};
    ASSERT_TRUE(swl_conv_maskToChar(buffer, sizeof(buffer), srcMask, srcEnumStrList, maxVal),
                false, ME, "FAIL");
    return amxd_trans_set_value(cstring_t, trans, name, buffer) == amxd_status_ok;
}


/**
 * Attempt to read an enum from the given object char parameter.
 * Returns default val upon failure, else the enum that matches the parameter.
 */
uint32_t swl_conv_objectParamEnum(amxd_object_t* object, const char* name, const char* const* srcEnumStrList, uint32_t maxVal, uint32_t defaultVal) {
    ASSERT_NOT_NULL(object, defaultVal, ME, "NULL");
    ASSERT_NOT_NULL(name, defaultVal, ME, "NULL");
    ASSERT_NOT_NULL(srcEnumStrList, defaultVal, ME, "NULL");

    char* param = amxd_object_get_cstring_t(object, name, NULL);
    ASSERT_NOT_NULL(param, defaultVal, ME, "NULL param %s", name);
    uint32_t ret = swl_conv_charToEnum(param, srcEnumStrList, maxVal, defaultVal);
    free(param);
    return ret;
}

/**
 * Attempt to read a mask from the given object char parameter.
 * Returns zero upon failure (empty mask), else the mask that matches the parameter.
 * Expect comma separated value format.
 */
uint32_t swl_conv_objectParamMask(amxd_object_t* object, const char* name, const char* const* srcEnumStrList, uint32_t maxVal) {
    ASSERT_NOT_NULL(object, 0, ME, "NULL");
    ASSERT_NOT_NULL(name, 0, ME, "NULL");
    ASSERT_NOT_NULL(srcEnumStrList, 0, ME, "NULL");
    char* param = amxd_object_get_value(cstring_t, object, name, NULL);
    ASSERT_NOT_NULL(param, 0, ME, "NULL param %s", name);
    uint32_t ret = swl_conv_charToMask(param, srcEnumStrList, maxVal);
    free(param);
    return ret;
}

/**
 * Add an enum to a map as a cstring. It will ensure the enum val does not exceed the string list.
 * returns true if enum is successfully added to the map as string, false otherwise.
 */
bool swl_conv_addEnumToMap(amxc_var_t* map, const char* name, uint32_t enumVal, const char* const* srcEnumStrList, uint32_t maxVal) {
    ASSERT_NOT_NULL(map, false, ME, "NULL");
    ASSERT_NOT_NULL(name, false, ME, "NULL");
    ASSERT_NOT_NULL(srcEnumStrList, false, ME, "NULL");
    ASSERT_TRUE(enumVal < maxVal, false, ME, "Invalid %s", name);
    return (amxc_var_add_new_key_cstring_t(map, name, srcEnumStrList[enumVal]) != NULL? true : false);
}

/**
 * Add a mask to a map as a cstring.
 * returns true if mask is successfully added to the map as string, false otherwise.
 */
bool swl_conv_addMaskToMap(amxc_var_t* map, const char* name, uint32_t srcMask, const char* const* srcEnumStrList, uint32_t maxVal) {
    ASSERT_NOT_NULL(map, false, ME, "NULL");
    ASSERT_NOT_NULL(name, false, ME, "NULL");
    ASSERT_NOT_NULL(srcEnumStrList, false, ME, "NULL");
    char buffer[SWL_CONV_BUF_SIZE] = {0};
    ASSERT_TRUE(swl_conv_maskToChar(buffer, sizeof(buffer), srcMask, srcEnumStrList, maxVal),
                false, ME, "FAIL");
    return (amxc_var_add_key(cstring_t, map, name, buffer) != NULL? true : false);
}

/**
 * Find the enum in the map.
 * It will return defaultVal in case the enum parameter is not found, or in case of errors.
 * It will also print error message in case of problems.
 */
uint32_t swl_conv_findEnumInMap(const amxc_var_t* map, const char* name, const char* const* srcEnumStrList, uint32_t maxVal, uint32_t defaultVal) {
    ASSERT_NOT_NULL(map, defaultVal, ME, "NULL");
    ASSERT_NOT_NULL(name, defaultVal, ME, "NULL");
    ASSERT_NOT_NULL(srcEnumStrList, defaultVal, ME, "NULL");

    const char* param = amxc_var_constcast(cstring_t, amxc_var_get_key(map, name, AMXC_VAR_FLAG_DEFAULT));

    ASSERT_NOT_NULL(param, defaultVal, ME, "NULL param %s", name);
    return swl_conv_charToEnum(param, srcEnumStrList, maxVal, defaultVal);
}

/**
 * Read an enum from a map if possible
 * It shall be silent when the value is not found, to allow "update if present" scenarios.
 * It shall still print errors if other expectations are not met.
 * It shall return
 * * SWL_RC_OK if parameter is successfully read, and written into tgtEnumVal.
 * * SWL_RC_ERROR if param could not be found
 * * SWL_RC_INVALID_PARAM if arguements are somehow invalid
 * In case of error tgtEnumVal shall not be written to.
 */
swl_rc_ne swl_conv_readEnumFromMap(uint32_t* tgtEnumVal, const amxc_var_t* map, const char* name, const char* const* srcEnumStrList, uint32_t maxVal) {
    ASSERT_NOT_NULL(tgtEnumVal, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(map, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(name, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(srcEnumStrList, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_TRUE(maxVal > 0, SWL_RC_INVALID_PARAM, ME, "NULL");

    const char* param = amxc_var_constcast(cstring_t, amxc_var_get_key(map, name, AMXC_VAR_FLAG_DEFAULT));

    ASSERTI_NOT_NULL(param, SWL_RC_ERROR, ME, "NULL param %s", name);
    uint32_t tmpVal = swl_conv_charToEnum(param, srcEnumStrList, maxVal, maxVal);
    if(tmpVal == maxVal) {
        return SWL_RC_ERROR;
    }
    *tgtEnumVal = tmpVal;
    return SWL_RC_OK;
}

/**
 * Find the mask in the map.
 * It will return 0 in case the parameter is not found, or in case of errors.
 * It will also print error message in case of all problems.
 */
uint32_t swl_conv_findMaskInMap(const amxc_var_t* map, const char* name, const char* const* srcEnumStrList, uint32_t maxVal) {
    ASSERT_NOT_NULL(map, 0, ME, "NULL");
    ASSERT_NOT_NULL(name, 0, ME, "NULL");
    ASSERT_NOT_NULL(srcEnumStrList, 0, ME, "NULL");

    const char* param = amxc_var_constcast(cstring_t, amxc_var_get_key(map, name, AMXC_VAR_FLAG_DEFAULT));

    ASSERT_NOT_NULL(param, 0, ME, "NULL param %s", name);
    return swl_conv_charToMask(param, srcEnumStrList, maxVal);
}


/**
 * Read a mask from a map if possible
 * It shall be silent when the value is not found, to allow "update if present" scenarios.
 * It shall return
 * * SWL_RC_OK if parameter is successfully read in tgtMaskVal.
 * * SWL_RC_ERROR if param could not be found
 * * SWL_RC_INVALID_PARAM if arguements are somehow invalid
 * In case of error tgtMaskVal shall not be written to.
 */
swl_rc_ne swl_conv_readMaskFromMap(uint32_t* tgtMaskVal, const amxc_var_t* map, const char* name, const char* const* srcEnumStrList, uint32_t maxVal) {
    ASSERT_NOT_NULL(tgtMaskVal, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(map, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(name, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(srcEnumStrList, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_TRUE(maxVal > 0, SWL_RC_INVALID_PARAM, ME, "NULL");

    const char* param = amxc_var_constcast(cstring_t, amxc_var_get_key(map, name, AMXC_VAR_FLAG_DEFAULT));

    ASSERTI_NOT_NULL(param, SWL_RC_ERROR, ME, "NULL param %s", name);
    *tgtMaskVal = swl_conv_charToMask(param, srcEnumStrList, maxVal);
    return SWL_RC_OK;
}
