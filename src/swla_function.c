/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "swl/swl_common.h"
#include "swla/swla_function.h"


#define ME "swlaFun"


static void s_cancelHandler_cb(uint64_t call_id, void* const priv) {
    swl_function_deferredInfo_t* info = (swl_function_deferredInfo_t*) priv;
    ASSERT_NOT_NULL(info, , ME, "NULL callback");
    ASSERT_TRUE(call_id == info->callId, , ME, "Invalid callId");
    ASSERT_TRUE(info->status == SWL_FUNCTION_DEFERRED_STATUS_STARTED, , ME, "Invalid state");


    swl_function_deferredCancel_cb callFun = info->cancelFun;
    info->status = SWL_FUNCTION_DEFERRED_STATUS_CANCELLED;
    info->callId = 0;
    info->cancelFun = NULL;

    if(callFun != NULL) {
        callFun(info, info->priv);
    }
}

/**
 * Initialize a defer info
 */
amxd_status_t swl_function_deferInit(swl_function_deferredInfo_t* info) {
    ASSERT_NOT_NULL(info, amxd_status_invalid_arg, ME, "NULL");
    memset(info, 0, sizeof(swl_function_deferredInfo_t));
    return amxd_status_ok;
}

/**
 * Defer a function without adding a cancel handler.
 * @param info The deferred info object
 * @param func The function call that is being deferred
 * @param retval The retval of the amx function. This sends back the callId. Other data shall be overwritten !!
 *
 * @return amxd_status_deferred if the function was successfully deferred, and error return otherwise
 * note that this differs from amxd_function_defer, so that the return of this can be immediately returned to calling function
 */
amxd_status_t swl_function_defer(swl_function_deferredInfo_t* info, const amxd_function_t* const func, amxc_var_t* retval) {
    ASSERT_NOT_NULL(info, amxd_status_invalid_arg, ME, "NULL");
    ASSERT_NOT_NULL(func, amxd_status_invalid_arg, ME, "NULL");
    ASSERT_FALSE(swl_function_deferIsActive(info), amxd_status_invalid_action, ME, "DEFER BUSY");

    swl_function_deferInit(info);

    amxd_status_t status = amxd_function_defer(func, &info->callId, retval, s_cancelHandler_cb, info);
    if((status == amxd_status_ok) || (status == amxd_status_deferred)) {
        info->status = SWL_FUNCTION_DEFERRED_STATUS_STARTED;
        return amxd_status_deferred;
    } else {
        info->status = SWL_FUNCTION_DEFERRED_STATUS_ERROR;
        return status;
    }
}

/**
 * Defer a function, providing a callback and private data to be returned on callback call
 * @param info The deferred info object
 * @param func The function call that is being deferred
 * @param retval The retval of the amx function. This sends back the callId. Other data shall be overwritten !!
 * @param cancelFun The function that shall be called upon cancellation. The cancel function is allowed to
 * free the deferredInfo if so desired upon cancellation.
 * @param priv The private data to be provided upon calling the cancel fun
 *
 *
 * @return amxd_status_deferred if the function was successfully deferred, and error return otherwise
 * note that this differs from amxd_function_defer, so that the return of this can be immediately returned to calling function
 */
amxd_status_t swl_function_deferCb(swl_function_deferredInfo_t* info, const amxd_function_t* const func,
                                   amxc_var_t* retval, swl_function_deferredCancel_cb cancelFun, void* priv) {
    // input checking done in defer function.
    amxd_status_t status = swl_function_defer(info, func, retval);
    if(status == amxd_status_deferred) {
        info->priv = priv;
        info->cancelFun = cancelFun;
    }
    return status;
}

/**
 * Notify that a deferred function is done.
 *
 * @param info The deferred info object
 * @param status The final status of the RPC method
 * @param out_args Out arguments if any (can be NULL)
 * @param ret The return value of the RPC method
 *
 * @return
 * amxd_status_ok if the reply was send or another status when an error has
   occured.
 */
amxd_status_t swl_function_deferDone(swl_function_deferredInfo_t* info,
                                     amxd_status_t status,
                                     amxc_var_t* out_args,
                                     amxc_var_t* ret) {
    ASSERT_NOT_NULL(info, amxd_status_invalid_arg, ME, "NULL");
    if(info->status != SWL_FUNCTION_DEFERRED_STATUS_STARTED) {
        return amxd_status_invalid_action;
    }

    amxd_status_t outStatus = amxd_function_deferred_done(info->callId, status, out_args, ret);

    if(outStatus == amxd_status_ok) {
        info->status = SWL_FUNCTION_DEFERRED_STATUS_DONE;
    } else {
        info->status = SWL_FUNCTION_DEFERRED_STATUS_ERROR;
    }

    info->priv = NULL;
    info->cancelFun = NULL;
    info->callId = 0;
    return outStatus;
}

/**
 * Check whether the defer info is currently active, i.e. a function is currently being deferred.
 */
bool swl_function_deferIsActive(swl_function_deferredInfo_t* info) {
    return info->status == SWL_FUNCTION_DEFERRED_STATUS_STARTED;
}
