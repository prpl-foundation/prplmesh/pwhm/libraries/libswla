/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "swla/swla_commonLib.h"

#define ME "swlTDef"

static bool s_int8_toVariant_cb(swl_type_t* type _UNUSED, amxc_var_t* tgt, const void* srcData) {
    ASSERTS_NOT_NULL(tgt, false, ME, "NULL");
    return amxc_var_set(int8_t, tgt, *(int8_t*) srcData) == 0;
}

static bool s_int8_fromVariant_cb(swl_type_t* type _UNUSED, int8_t* tgtData, const amxc_var_t* var) {
    ASSERTS_NOT_NULL(tgtData, false, ME, "NULL");
    amxc_var_t variant;
    amxc_var_init(&variant);
    int retVal = amxc_var_convert(&variant, var, AMXC_VAR_ID_INT8);
    if(retVal < 0) {
        return false;
    }
    *tgtData = variant.data.i8;

    return true;
}

SWL_CONSTRUCTOR static void s_int8_var_init(void) {
    swl_type_int8_impl.typeFun->toVariant = ((swl_type_toVariant_cb) s_int8_toVariant_cb);
    swl_type_int8_impl.typeFun->fromVariant = ((swl_type_fromVariant_cb) s_int8_fromVariant_cb);
}


/**
 * int16_t handlers
 */
static bool s_int16_toVariant_cb(swl_type_t* type _UNUSED, amxc_var_t* tgt, const void* srcData) {
    ASSERTS_NOT_NULL(tgt, false, ME, "NULL");
    return amxc_var_set(int16_t, tgt, *(int16_t*) srcData) == 0;
}

static bool s_int16_fromVariant_cb(swl_type_t* type _UNUSED, int16_t* tgtData, const amxc_var_t* var) {
    ASSERTS_NOT_NULL(tgtData, false, ME, "NULL");
    amxc_var_t variant;
    amxc_var_init(&variant);
    int retVal = amxc_var_convert(&variant, var, AMXC_VAR_ID_INT16);
    if(retVal < 0) {
        return false;
    }
    *tgtData = variant.data.i16;

    return true;
}

SWL_CONSTRUCTOR static void s_int16_var_init(void) {
    swl_type_int16_impl.typeFun->toVariant = (swl_type_toVariant_cb) s_int16_toVariant_cb;
    swl_type_int16_impl.typeFun->fromVariant = (swl_type_fromVariant_cb) s_int16_fromVariant_cb;
}

/**
 * int32_t handlers
 */
static bool s_int32_toVariant_cb(swl_type_t* type _UNUSED, amxc_var_t* tgt, const void* srcData) {
    ASSERTS_NOT_NULL(tgt, false, ME, "NULL");
    return amxc_var_set(int32_t, tgt, *(int32_t*) srcData) == 0;
}

static bool s_int32_fromVariant_cb(swl_type_t* type _UNUSED, int32_t* tgtData, const amxc_var_t* var) {
    ASSERTS_NOT_NULL(tgtData, false, ME, "NULL");

    amxc_var_t variant;
    amxc_var_init(&variant);
    int retVal = amxc_var_convert(&variant, var, AMXC_VAR_ID_INT32);
    if(retVal < 0) {
        return false;
    }
    *tgtData = variant.data.i32;

    return true;
}

SWL_CONSTRUCTOR static void s_int32_var_init(void) {
    swl_type_int32_impl.typeFun->toVariant = (swl_type_toVariant_cb) s_int32_toVariant_cb;
    swl_type_int32_impl.typeFun->fromVariant = (swl_type_fromVariant_cb) s_int32_fromVariant_cb;
}

/**
 * int64_t handlers
 */
static bool s_int64_toVariant_cb(swl_type_t* type _UNUSED, amxc_var_t* tgt, const void* srcData) {
    ASSERTS_NOT_NULL(tgt, false, ME, "NULL");
    return amxc_var_set(int64_t, tgt, *(int64_t*) srcData) == 0;
}

static bool s_int64_fromVariant_cb(swl_type_t* type _UNUSED, int64_t* tgtData, const amxc_var_t* var) {
    ASSERTS_NOT_NULL(tgtData, false, ME, "NULL");
    amxc_var_t variant;
    amxc_var_init(&variant);
    int retVal = amxc_var_convert(&variant, var, AMXC_VAR_ID_INT64);
    if(retVal < 0) {
        return false;
    }
    *tgtData = variant.data.i64;

    return true;
}

SWL_CONSTRUCTOR static void s_int64_var_init(void) {
    swl_type_int64_impl.typeFun->toVariant = (swl_type_toVariant_cb) s_int64_toVariant_cb;
    swl_type_int64_impl.typeFun->fromVariant = (swl_type_fromVariant_cb) s_int64_fromVariant_cb;
}

/**
 * uint8_t handler
 */
static bool s_uint8_toVariant_cb(swl_type_t* type _UNUSED, amxc_var_t* tgt, const void* srcData) {
    ASSERTS_NOT_NULL(tgt, false, ME, "NULL");
    return amxc_var_set(uint8_t, tgt, *(uint8_t*) srcData) == 0;
}

static bool s_uint8_fromVariant_cb(swl_type_t* type _UNUSED, uint8_t* tgtData, const amxc_var_t* var) {
    ASSERTS_NOT_NULL(tgtData, false, ME, "NULL");
    amxc_var_t variant;
    amxc_var_init(&variant);
    int retVal = amxc_var_convert(&variant, var, AMXC_VAR_ID_UINT8);
    if(retVal < 0) {
        return false;
    }
    *tgtData = variant.data.ui8;

    return true;
}

SWL_CONSTRUCTOR static void s_uint8_var_init(void) {
    swl_type_uint8_impl.typeFun->toVariant = (swl_type_toVariant_cb) s_uint8_toVariant_cb;
    swl_type_uint8_impl.typeFun->fromVariant = (swl_type_fromVariant_cb) s_uint8_fromVariant_cb;
}

/**
 * int16_t handlers
 */
static bool s_uint16_toVariant_cb(swl_type_t* type _UNUSED, amxc_var_t* tgt, const void* srcData) {
    ASSERTS_NOT_NULL(tgt, false, ME, "NULL");
    return amxc_var_set(uint16_t, tgt, *(uint16_t*) srcData) == 0;
}

static bool s_uint16_fromVariant_cb(swl_type_t* type _UNUSED, uint16_t* tgtData, const amxc_var_t* var) {
    ASSERTS_NOT_NULL(tgtData, false, ME, "NULL");
    amxc_var_t variant;
    amxc_var_init(&variant);
    int retVal = amxc_var_convert(&variant, var, AMXC_VAR_ID_UINT16);
    if(retVal < 0) {
        return false;
    }
    *tgtData = variant.data.ui16;
    return true;
}

SWL_CONSTRUCTOR static void s_uint16_var_init(void) {
    swl_type_uint16_impl.typeFun->toVariant = (swl_type_toVariant_cb) s_uint16_toVariant_cb;
    swl_type_uint16_impl.typeFun->fromVariant = (swl_type_fromVariant_cb) s_uint16_fromVariant_cb;
}

/**
 * int32_t handlers
 */
static bool s_uint32_toVariant_cb(swl_type_t* type _UNUSED, amxc_var_t* tgt, const void* srcData) {
    ASSERTS_NOT_NULL(tgt, false, ME, "NULL");
    return amxc_var_set(uint32_t, tgt, *(uint32_t*) srcData) == 0;
}

static bool s_uint32_fromVariant_cb(swl_type_t* type _UNUSED, uint32_t* tgtData, const amxc_var_t* var) {
    ASSERTS_NOT_NULL(tgtData, false, ME, "NULL");
    amxc_var_t variant;
    amxc_var_init(&variant);
    int retVal = amxc_var_convert(&variant, var, AMXC_VAR_ID_UINT32);
    if(retVal < 0) {
        return false;
    }
    *tgtData = variant.data.ui32;
    return true;
}

SWL_CONSTRUCTOR static void s_uint32_var_init(void) {
    swl_type_uint32_impl.typeFun->toVariant = (swl_type_toVariant_cb) s_uint32_toVariant_cb;
    swl_type_uint32_impl.typeFun->fromVariant = (swl_type_fromVariant_cb) s_uint32_fromVariant_cb;
}

/**
 * int64_t handlers
 */
static bool s_uint64_toVariant_cb(swl_type_t* type _UNUSED, amxc_var_t* tgt, const void* srcData) {
    ASSERTS_NOT_NULL(tgt, false, ME, "NULL");
    return amxc_var_set(uint64_t, tgt, *(uint64_t*) srcData) == 0;
}

static bool s_uint64_fromVariant_cb(swl_type_t* type _UNUSED, uint64_t* tgtData, const amxc_var_t* var) {
    ASSERTS_NOT_NULL(tgtData, false, ME, "NULL");
    amxc_var_t variant;
    amxc_var_init(&variant);
    int retVal = amxc_var_convert(&variant, var, AMXC_VAR_ID_UINT64);
    if(retVal < 0) {
        return false;
    }
    *tgtData = variant.data.ui64;
    return true;
}

SWL_CONSTRUCTOR static void s_uint64_var_init(void) {
    swl_type_uint64_impl.typeFun->toVariant = (swl_type_toVariant_cb) s_uint64_toVariant_cb;
    swl_type_uint64_impl.typeFun->fromVariant = (swl_type_fromVariant_cb) s_uint64_fromVariant_cb;
}

/**
 * double handlers
 */
static bool s_double_toVariant_cb(swl_type_t* type _UNUSED, amxc_var_t* tgt, const void* srcData) {
    ASSERTS_NOT_NULL(tgt, false, ME, "NULL");
    return amxc_var_set(double, tgt, *(double*) srcData) == 0;
}

static bool s_double_fromVariant_cb(swl_type_t* type _UNUSED, double* tgtData, const amxc_var_t* var) {
    ASSERTS_NOT_NULL(tgtData, false, ME, "NULL");
    *tgtData = amxc_var_constcast(double, var);
    return true;
}

SWL_CONSTRUCTOR static void s_double_var_init(void) {
    swl_type_double_impl.typeFun->toVariant = (swl_type_toVariant_cb) s_double_toVariant_cb;
    swl_type_double_impl.typeFun->fromVariant = (swl_type_fromVariant_cb) s_double_fromVariant_cb;
}

/**
 * Char ptr handlers
 */
bool swl_typeDef_charPtr_toVariant_cb(swl_type_t* type _UNUSED, amxc_var_t* tgt, const char* srcData) {
    return amxc_var_set(cstring_t, tgt, srcData) == 0;
}

bool swl_typeDef_charPtr_fromVariant_cb(swl_type_t* type _UNUSED, char** data, const amxc_var_t* var) {
    ASSERTS_NOT_NULL(data, false, ME, "NULL");
    *data = amxc_var_dyncast(cstring_t, var);
    return (*data != NULL);
}

SWL_CONSTRUCTOR static void s_charPtr_var_init(void) {
    swl_type_charPtr_impl.typeFun->toVariant = (swl_type_toVariant_cb) swl_typeDef_charPtr_toVariant_cb;
    swl_type_charPtr_impl.typeFun->fromVariant = (swl_type_fromVariant_cb) swl_typeDef_charPtr_fromVariant_cb;
}

bool swl_typeDef_charBuf_fromChar_cb(swl_type_t* type, char* tgtData, const char* srcStr) {
    ASSERTS_NOT_NULL(tgtData, false, ME, "NULL");
    ASSERTS_NOT_NULL(srcStr, false, ME, "NULL");

    swl_str_copy(tgtData, type->size, srcStr);
    return true;
}

bool swl_typeDef_charBuf_fromVariant_cb(swl_type_t* type, char* data, const amxc_var_t* var) {
    ASSERTS_NOT_NULL(data, false, ME, "NULL");
    swl_str_copy(data, type->size, amxc_var_constcast(cstring_t, var));
    return true;
}

SWL_CONSTRUCTOR static void s_charBuf_var_init(void) {
    swl_type_charBuf4_impl.typeFun->toVariant = ((swl_type_toVariant_cb) swl_typeDef_charPtr_toVariant_cb);
    swl_type_charBuf4_impl.typeFun->fromVariant = ((swl_type_fromVariant_cb) swl_typeDef_charBuf_fromVariant_cb);
}
