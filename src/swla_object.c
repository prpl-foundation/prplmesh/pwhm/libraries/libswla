/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "swla/swla_commonLib.h"

#define ME "swlObj"

/**
 * @brief fetch a reference object, with provided reference path
 *
 * @param callerObj (in): caller object, used to get common local root object name
 * @param referencePath (in): datamodel path to referenced object
 * @return obj (out): reference object if found, NULL otherwise
 */
amxd_object_t* swla_object_getReferenceObject(amxd_object_t* callerObj, const char* referencePath) {
    ASSERTS_STR(referencePath, NULL, ME, "Empty");
    char* callerPath = amxd_object_get_path(callerObj, AMXD_OBJECT_NAMED);
    ASSERT_NOT_NULL(callerPath, NULL, ME, "NULL");
    char* localReferencePath = NULL;
    const char* commonRootObjName = strtok(callerPath, ".");
    if(commonRootObjName != NULL) {
        localReferencePath = strstr(referencePath, commonRootObjName);
    }
    free(callerPath);
    ASSERT_STR(localReferencePath, NULL, ME, "Reference (%s) unmatch DM root object!", referencePath);
    return amxd_object_findf(amxd_object_get_root(callerObj), "%s", localReferencePath);
}

/**
 * @brief fetch a reference object, with provided reference path
 * and return included private data context
 *
 * @param callerObj (in): caller object, used to get common local root object name
 * @param referencePath (in): datamodel path to referenced object
 * @return void* (out): reference object's private data
 */
void* swla_object_getReferenceObjectPriv(amxd_object_t* callerObj, const char* referencePath) {
    amxd_object_t* refObj = swla_object_getReferenceObject(callerObj, referencePath);
    ASSERTS_NOT_NULL(refObj, NULL, ME, "NULL");
    return refObj->priv;
}

/**
 * @brief dump object's parameters into variant map
 *
 * @param map (out): output variant map
 * @param obj (in): source object
 * @return amxd_status_ok if dump is successful, error code otherwise
 */
amxd_status_t swla_object_paramsToMap(amxc_var_t* map, amxd_object_t* obj) {
    ASSERTS_NOT_NULL(map, amxd_status_unknown_error, ME, "NULL");
    ASSERTS_NOT_NULL(obj, amxd_status_unknown_error, ME, "NULL");
    return amxd_object_get_params(obj, map, amxd_dm_access_private);
}

/**
 * @brief dump child object's parameters into variant sub map, having same child object name
 *
 * @param map (out): output variant map, will include new sub map with child object parameters
 * @param parentObj (in): source parent object
 * @param childObjName (in): child object name
 * @return amxd_status_ok if dump is successful, error code otherwise
 */
amxd_status_t swla_object_addChildParamsToMap(amxc_var_t* map, amxd_object_t* parentObj, const char* childObjName) {
    amxd_object_t* childObject = amxd_object_get(parentObj, childObjName);
    ASSERT_NOT_NULL(childObject, amxd_status_unknown_error, ME, "No sub object named <%s>", childObjName);
    amxc_var_t* childMap = amxc_var_add_key(amxc_htable_t, map, childObjName, NULL);
    return swla_object_paramsToMap(childMap, childObject);
}

/**
 * @brief fetch in template object, the first available index to be used
 * when creating new instance
 *
 * @param templateObj (in): template object in which the new instance will be added
 * @return the found available index, 0 if templateObj is not a template object
 */
uint32_t swla_object_getFirstAvailableIndex(amxd_object_t* templateObj) {
    ASSERT_EQUALS(amxd_object_get_type(templateObj), amxd_object_template, 0, ME, "Invalid object");
    uint32_t instanceCount = amxd_object_get_instance_count(templateObj);
    if(instanceCount == 0) {
        return 1;
    }
    for(uint32_t i = 1; i <= instanceCount; i++) {
        if(amxd_object_get_instance(templateObj, NULL, i) == NULL) {
            SAH_TRACEZ_INFO(ME, "Found available index %u", i);
            return i;
        }
    }
    return (instanceCount + 1);
}

/*
 * @brief prepare transaction to set parameters of selected object
 * (including read-only parameters)
 *
 * @param trans pointer to transaction context to be initialized
 * @param object object to be altered and committed
 *
 * @return amxd_status_ok in case of success, error code otherwise
 */
amxd_status_t swl_object_prepareTransaction(amxd_trans_t* const trans, const amxd_object_t* object) {
    ASSERT_NOT_NULL(object, amxd_status_object_not_found, ME, "NULL");
    amxd_status_t status = amxd_status_object_not_found;
    const char* oname = amxd_object_get_name(object, AMXD_OBJECT_NAMED);
    ASSERT_STR(oname, status, ME, "Invalid object");
    status = amxd_trans_init(trans);
    ASSERT_EQUALS(status, amxd_status_ok, status, ME, "failed init trans of object (%s) status(%s)", oname, amxd_status_string(status));
    //allow setting read-only parameters on local datamodel (private access)
    status = amxd_trans_set_attr(trans, amxd_tattr_change_ro, true);
    ASSERT_EQUALS(status, amxd_status_ok, status, ME, "failed allow RO settings on object (%s) status(%s)", oname, amxd_status_string(status));
    status = amxd_trans_select_object(trans, object);
    ASSERT_EQUALS(status, amxd_status_ok, status, ME, "failed select trans object (%s) status(%s)", oname, amxd_status_string(status));
    return status;
}

/*
 * @brief finalize transaction (commit and clean it up)
 *
 * @param dm datamodel context on which transaction must be applied
 * @param trans transaction context to be applied and cleaned up
 *
 * @return amxd_status_ok on success, error code otherwise
 */
amxd_status_t swl_object_finalizeTransaction(amxd_trans_t* trans, amxd_dm_t* dm) {
    amxd_status_t status = amxd_trans_apply(trans, dm);
    amxd_trans_clean(trans);
    ASSERT_EQUALS(status, amxd_status_ok, status, ME, "failed to apply transaction, status(%s)", amxd_status_string(status));
    return status;
}

/*
 * @brief finalize transaction (commit and clean it up) on local dm context
 * (dm retrieved from registered amxb_bus_ctx_t set with swl_lib_initialize)
 *
 * @param trans transaction context to be applied and cleaned up
 *
 * @return amxd_status_ok on success, error code otherwise
 */
amxd_status_t swl_object_finalizeTransactionOnLocalDm(amxd_trans_t* trans) {
    return swl_object_finalizeTransaction(trans, swl_lib_getLocalDm());
}

/*
 * @brief delete object instance using transaction
 *
 * @param instance object instance to be deleted
 * @param dm datamodel context on which transaction must be applied
 *
 * @return amxd_status_ok on success, error code otherwise
 */
amxd_status_t swl_object_delInstWithTrans(const amxd_object_t* instance, amxd_dm_t* dm) {
    amxd_status_t status = amxd_status_invalid_value;
    ASSERT_NOT_NULL(dm, status, ME, "NULL");
    ASSERT_EQUALS(amxd_object_get_type(instance), amxd_object_instance, status, ME, "obj is not instance");
    uint32_t index = amxd_object_get_index(instance);
    ASSERT_TRUE(index > 0, status, ME, "instance has invalid index");
    amxd_object_t* tmpl = amxd_object_get_parent(instance);
    ASSERT_NOT_NULL(tmpl, status, ME, "No parent template");
    const char* iname = amxd_object_get_name(instance, AMXD_OBJECT_NAMED);
    const char* pname = amxd_object_get_name(tmpl, AMXD_OBJECT_NAMED);
    amxd_trans_t trans;
    status = swl_object_prepareTransaction(&trans, tmpl);
    ASSERT_EQUALS(status, amxd_status_ok, status, ME, "fail to prepare del-inst %s.%s", pname, iname);
    amxd_trans_del_inst(&trans, index, NULL);
    status = swl_object_finalizeTransaction(&trans, dm);
    ASSERT_EQUALS(status, amxd_status_ok, status, ME, "fail to finalize del-inst %s.%s", pname, iname);
    return status;
}

/*
 * @brief delete object instance using transaction on local dm context
 * (dm retrieved from registered amxb_bus_ctx_t set with swl_lib_initialize)
 *
 * @param instance object instance to be deleted
 *
 * @return amxd_status_ok on success, error code otherwise
 */
amxd_status_t swl_object_delInstWithTransOnLocalDm(const amxd_object_t* instance) {
    return swl_object_delInstWithTrans(instance, swl_lib_getLocalDm());
}

