/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "swl/swl_common.h"
#include "swl/swl_unLiList.h"
#include "amxc/amxc_llist.h"
#include "amxp/amxp_timer.h"
#include "swla/swla_delayExec.h"

#define ME "dlyExec"

typedef struct {
    amxp_timer_t* mainTimer;
    swl_unLiList_t triggerList;
    bool initialized;
} swla_delayExec_t;

typedef struct {
    amxp_timer_t* timer;
    void* userData;
    swla_delayExecFun_cbf fun;
} swla_delayExecEntry_t;

swla_delayExec_t s_myData;

static void s_callbackFun(amxp_timer_t* timer _UNUSED, void* priv _UNUSED) {
    swl_unLiListIt_t it;
    swl_unLiList_for_each(it, &s_myData.triggerList) {
        swla_delayExecEntry_t* data = it.data;
        data->fun(data->userData);
        swl_unLiList_delIt(&it);
    }
}

static void s_callbackFunTimeout(amxp_timer_t* timer _UNUSED, void* priv) {
    ASSERTS_NOT_NULL(priv, , ME, "NULL");
    swla_delayExecEntry_t* data = (swla_delayExecEntry_t*) priv;
    data->fun(data->userData);
    amxp_timer_delete(&data->timer);
    free(data);
}

/**
 * Initialize the delay execute module, preparing internal structures.
 */
void swla_delayExec_init() {
    amxp_timer_new(&s_myData.mainTimer, s_callbackFun, NULL);
    swl_unLiList_init(&s_myData.triggerList, sizeof(swla_delayExecEntry_t));
    s_myData.initialized = true;
}

/**
 * Cleanup the delay execute module, destroying internal structures.
 * Any pending triggers will NOT be called.
 */
void swla_delayExec_cleanup() {
    s_myData.initialized = false;
    amxp_timer_delete(&s_myData.mainTimer);
    swl_unLiList_destroy(&s_myData.triggerList);
}

void swla_delayExec_addTimeout(swla_delayExecFun_cbf fun, void* data, uint32_t timeout) {
    ASSERT_TRUE(s_myData.initialized, , ME, "Callback request while not initialized");
    if(timeout == 0) {
        swla_delayExecEntry_t myData = {.userData = data, .fun = fun};
        swl_unLiList_add(&s_myData.triggerList, &myData);
        amxp_timer_state_t state = amxp_timer_get_state(s_myData.mainTimer);
        if((state != amxp_timer_started) && (state != amxp_timer_running)) {
            amxp_timer_start(s_myData.mainTimer, timeout);
        }
    } else {
        swla_delayExecEntry_t* myData = calloc(1, sizeof(swla_delayExecEntry_t));
        ASSERTS_NOT_NULL(myData, , ME, "NULL");
        myData->userData = data;
        myData->fun = fun;
        amxp_timer_new(&myData->timer, s_callbackFunTimeout, myData);
        amxp_timer_start(myData->timer, timeout);
    }
}

/**
 * Add a new delayed execution trigger. The provided function and data will be called
 * as soon as the timer handlers are handled next.
 * Functions are called in the order they were added.
 */
void swla_delayExec_add(swla_delayExecFun_cbf fun, void* data) {
    swla_delayExec_addTimeout(fun, data, 0);
}
