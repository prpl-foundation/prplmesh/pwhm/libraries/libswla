/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "swl/swl_common.h"
#include "swla/swla_trans.h"
#include "swla/swla_object.h"
#include "swla/swla_lib.h"

#define ME "swlaTrn"

/**
 * Initialize an swla transaction. The swla transaction allows to "stack transactions", meaning that it
 * provides an easy way to use an existing transaction, if one exists. In that case
 * the existing transaction will be targeted on targetObject.
 * If none exists, a new transaction will be initialized on the target object.
 *
 *
 * @param swlaTrans: an uninitialized swla trans, which will be used to fill
 * @param existingTransaction: optional amxd_trans_t* which will be used if not NULL
 * @param targetObject: the object on which the following actions shall be done
 *
 * @return a transaction that can be used.
 */
amxd_trans_t* swla_trans_init(swla_trans_t* swlaTrans, amxd_trans_t* externalTrans, amxd_object_t* targetObject) {
    memset(swlaTrans, 0, sizeof(swla_trans_t));
    if(externalTrans != NULL) {
        swlaTrans->externalTrans = externalTrans;
        amxd_status_t status = amxd_trans_select_object(externalTrans, targetObject);
        ASSERT_TRUE(status == amxd_status_ok, NULL, ME, "Fail to select target object %s", amxd_status_string(status));

        return swlaTrans->externalTrans;
    } else {
        amxd_status_t status = swl_object_prepareTransaction(&swlaTrans->localTrans, targetObject);
        ASSERT_TRUE(status == amxd_status_ok, NULL, ME, "Fail to prepare trans %s", amxd_status_string(status));
        return &swlaTrans->localTrans;
    }

}

/**
 * Finalize the swla transaction.
 * If no top transaction was given on init, the transaction will entirely be finalized.
 * If a top transaction was given, and a topObject was given, that will be selected.
 *
 * @param swlaTrans: the swla transaction to finalize if it's a local one
 * @param dm : the data model on which to finalize. If NULL the local data model shall be used
 *
 * @return
 * * SWL_RC_OK if transaction actions was finalized successfully, i.e. there was no existing transaction
 * * SWL_RC_ERROR if there was no existing transaction, and the transaction failed to finalize.
 * * SWL_RC_CONTINUE if there was an existing transaction. This transaction must still be finalized separately.
 */
swl_rc_ne swla_trans_finalize(swla_trans_t* swlaTrans, amxd_dm_t* dm) {
    if(swlaTrans->externalTrans != NULL) {
        return SWL_RC_CONTINUE;
    } else {
        amxd_status_t status = amxd_status_ok;
        status = swl_object_finalizeTransaction(&swlaTrans->localTrans, ((dm != NULL) ? dm : swl_lib_getLocalDm()));
        if(status != amxd_status_ok) {
            SAH_TRACEZ_ERROR(ME, "Fail to finalize transaction %s", amxd_status_string(status));
            return SWL_RC_ERROR;
        } else {
            return SWL_RC_OK;
        }
    }
}
