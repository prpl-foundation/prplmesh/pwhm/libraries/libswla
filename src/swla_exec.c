/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**
 * Helper functions to streamline executing other programs or scripts.
 */

#define _GNU_SOURCE
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <libgen.h>
#include "swla/swla_commonLib.h"
#include "swla/swla_exec.h"
#include "swla/swla_lib.h"

#define ME "swlExec"

#define SUBPROC_WAIT_TIMEOUT 30000 /* in milliseconds */

typedef struct {
    const char* file;
    uint32_t line;
    amxc_string_t argBuffer;
    amxp_subproc_t* subProc;
} swl_exec_debugInfo_t;

typedef struct {
    swl_exec_debugInfo_t dbgInfo;
    swl_exec_result_t info;
} swl_exec_fdinfo_t;

typedef struct {
    swl_exec_debugInfo_t dbgInfo;
    swl_exec_doneHandler_f fDoneCb;
    swl_exec_doneBufHandler_f fDoneBufCb;
    void* userData;
    int fdOut;
    int fdErr;
    uint32_t maxStdOutBufSize;
    uint32_t maxStdErrBufSize;
} swl_exec_cbInfo_t;


/**
 * dump stream content into buffer
 * If maxReallocDstBufSize is larger than dstBufSize, the given buffer will be reallocated up to
 * maxReallocDstBufSize bytes. In this case, the given buffer may be NULL, it will be allocated
 * by s_dumpFd, but must be freed by the caller. If the buffer is NULL, dstBufSize should be 0.
 */

static void s_dumpFd(int fd, char** dstBuf, uint32_t dstBufSize, uint32_t maxReallocDstBufSize) {
    ASSERTS_NOT_EQUALS(fd, -1, , TRACEZ_IO_OUT, "INVALID");
    if(*dstBuf && dstBufSize) {
        *dstBuf[0] = '\0';
    }
    char localBuf[128] = {0};
    //just read out the data if not enabled, so we are where we need to be next time
    int readRet = 0;
    uint32_t totalRead = 0;
    do {
        readRet = read(fd, localBuf, sizeof(localBuf) - 1);
        if(readRet < 0) {
            //flag O_NONBLOCK was set on fd_out
            if((errno == EAGAIN) || (errno == EWOULDBLOCK)) {
                continue;
            }
            SAH_TRACEZ_ERROR(ME, "fail to read fd(%d): %d:%s", fd, errno, strerror(errno));
            break;
        }
        localBuf[readRet] = 0;
        totalRead += readRet;
        if(totalRead >= dstBufSize) {
            uint32_t newDstBufSize = dstBufSize;
            while(totalRead >= newDstBufSize) {
                newDstBufSize = newDstBufSize ? newDstBufSize * 2 : 128;
            }
            if(newDstBufSize > maxReallocDstBufSize) {
                newDstBufSize = maxReallocDstBufSize;
            }
            if(newDstBufSize > dstBufSize) {
                char* newDstBuf = realloc(*dstBuf, newDstBufSize);
                if(newDstBuf == NULL) {
                    SAH_TRACEZ_WARNING(ME, "realloc failed");
                } else {
                    if(*dstBuf == NULL) {
                        newDstBuf[dstBufSize] = '\0';
                    }
                    *dstBuf = newDstBuf;
                    dstBufSize = newDstBufSize;
                }
            }
        }
        bool appended = swl_str_cat(*dstBuf, dstBufSize, localBuf);
        ASSERTW_TRUE(appended, , ME, "Proc output larger than %u bytes, truncating", dstBufSize - 1);
    } while(readRet > 0);
}


/**
 * Redirect the given output stream (stdOut or stdErr) to a tempfile to
 * allow more data to be written.
 */
static swl_rc_ne s_redirOutstreamToTmpFile(amxp_subproc_t* subProc, int stdRedirRequested) {
    ASSERT_NOT_NULL(subProc, SWL_RC_INVALID_PARAM, ME, "NULL");
    /*
     * use temporary files instead of pipes to redirect std streams
     * as pipes transmits at most pipe-max-size (1MB), and by default 16 mempages( 16x4KB=64KB)
     * which may be not enough to retrieve all child's output
     */
    int stdFd = -1;
    FILE* fTemp = tmpfile();
    ASSERT_NOT_NULL(fTemp, SWL_RC_ERROR, ME, "fail to create temp redir stream (%d:%s)", errno, strerror(errno));
    // duplicate stream fd, for the parent to keep file open (kernel level) after closing the stream
    stdFd = dup(fileno(fTemp));
    // close useless stream: to free allocated stream mem;
    //tmp file will remain opened as fd are already duplicated
    fclose(fTemp);
    ASSERT_NOT_EQUALS(stdFd, -1, SWL_RC_ERROR, ME, "fail to create redir (%d:%s)", errno, strerror(errno));

    subProc->fd[stdRedirRequested][0] = stdFd;
    /*
     * temporary file are not mapped to filesystem and will be naturally removed when
     * all file descriptors (references) are closed (parent and child)
     */
    // create another fd dup for the child usage
    subProc->fd[stdRedirRequested][1] = dup(stdFd);
    return SWL_RC_OK;
}

/**
 * Get the file handler of the temp file a given output stream (stdOut or stdErr)
 * This requires s_redirOutstreamToTmpFile to be called before process start for the given
 * file handler.
 */
static void s_getOutStreamFileHandler(amxp_subproc_t* subProc, int stdRedirRequested, int* pFd) {
    ASSERTS_NOT_NULL(pFd, , ME, "NULL");
    ASSERT_NOT_NULL(subProc, , ME, "NULL");
    int* pBufFd = &subProc->fd[stdRedirRequested][0];
    *pFd = *pBufFd;
    ASSERTS_NOT_EQUALS(*pBufFd, -1, , ME, "no redir fd");
    //rewind buffer fd to let caller read from the beginning
    ASSERT_NOT_EQUALS(lseek(*pBufFd, 0L, SEEK_SET), (off_t) -1, , ME, "fail to rewind %d:%s", errno, strerror(errno));
    //reset fd to avoid closing tmp buf fd (and then removing tmp file) when cleaning redirection ctx.
    //let the caller read the fd, then close it (and trigger tmp file deletion)
    *pBufFd = -1;
}

static void s_fillExitInfo(swl_exec_procExitInfo_t* pExitInfo, const amxc_var_t* const pEventData, swl_exec_debugInfo_t* pDbgInfo) {
    swl_exec_procExitInfo_t exitInfo;
    memset(&exitInfo, 0, sizeof(exitInfo));
    const char* cmdLine = ((pDbgInfo != NULL) ? amxc_string_get(&pDbgInfo->argBuffer, 0) : "");
    amxp_subproc_t* subProc = ((pDbgInfo != NULL) ? pDbgInfo->subProc : NULL);
    pid_t childPid = 0;

    if(pEventData) {
        childPid = GET_INT32(pEventData, "PID");
        exitInfo.isExited = (GET_ARG(pEventData, "ExitCode") != NULL);
        if(exitInfo.isExited) {
            exitInfo.exitStatus = GET_INT32(pEventData, "ExitCode");
        }
        exitInfo.isSignaled = (GET_ARG(pEventData, "Signalled") != NULL);
        if(exitInfo.isSignaled) {
            exitInfo.termSignal = GET_INT32(pEventData, "Signal");
        }
    }
    if((subProc != NULL) &&
       ((pEventData == NULL) || (!exitInfo.isExited && !exitInfo.isSignaled))) {
        childPid = amxp_subproc_get_pid(subProc);
        exitInfo.isExited = (amxp_subproc_ifexited(subProc) != 0);
        if(exitInfo.isExited) {
            exitInfo.exitStatus = amxp_subproc_get_exitstatus(subProc);
        }
        exitInfo.isSignaled = (amxp_subproc_ifsignaled(subProc) != 0);
        if(exitInfo.isSignaled) {
            exitInfo.termSignal = amxp_subproc_get_termsig(subProc);
        }
    }
    if(exitInfo.isSignaled) {
        SAH_TRACEZ_INFO(ME, "exec \"%s\" %p pid(%d) signaled (sig=%s)",
                        cmdLine, subProc, childPid, strsignal(exitInfo.termSignal));
    } else if(exitInfo.isExited) {
        SAH_TRACEZ_INFO(ME, "exec \"%s\" %p terminated (ret=%d)",
                        cmdLine, subProc, exitInfo.exitStatus);
    }
    if(pExitInfo != NULL) {
        memcpy(pExitInfo, &exitInfo, sizeof(*pExitInfo));
    }
}

/**
 * Callback handler on process completion for background execution
 */
static void s_procExitHandlerBgCb(const char* const event_name,
                                  const amxc_var_t* const pEventData,
                                  void* const priv) {

    SAH_TRACEZ_INFO(ME, "AMX Signal = %s", event_name);
    swl_exec_cbInfo_t* pExecInfo = (swl_exec_cbInfo_t*) priv;
    swl_exec_debugInfo_t* pDbgInfo = ((pExecInfo != NULL) ? &pExecInfo->dbgInfo : NULL);
    swl_exec_result_t result = {0};
    s_fillExitInfo(&result.exitInfo, pEventData, pDbgInfo);

    ASSERTW_NOT_NULL(pExecInfo, , ME, "NULL");
    if(pExecInfo->fDoneCb != NULL) {
        pExecInfo->fDoneCb(pDbgInfo->subProc, &result.exitInfo, pExecInfo->userData);
    } else if(pExecInfo->fDoneBufCb != NULL) {
        s_getOutStreamFileHandler(pDbgInfo->subProc, STDOUT_FILENO, &pExecInfo->fdOut);
        s_getOutStreamFileHandler(pDbgInfo->subProc, STDERR_FILENO, &pExecInfo->fdErr);
        if(pExecInfo->fdOut > 0) {
            s_dumpFd(pExecInfo->fdOut, &result.outBuf, 0, pExecInfo->maxStdOutBufSize);
            close(pExecInfo->fdOut);
        }
        if(pExecInfo->fdErr > 0) {
            s_dumpFd(pExecInfo->fdErr, &result.errBuf, 0, pExecInfo->maxStdErrBufSize);
            close(pExecInfo->fdErr);
        }
        pExecInfo->fDoneBufCb(pDbgInfo->subProc, &result, pExecInfo->userData);
        free(result.outBuf);
        free(result.errBuf);
    }

    amxc_string_clean(&pDbgInfo->argBuffer);
    amxp_subproc_delete(&pDbgInfo->subProc);
    free(pExecInfo);
}

/**
 * Common execution functionality.
 */
static amxp_subproc_t* s_exec_common(swl_exec_debugInfo_t* dbgInfo, const char* prg, const char* str,
                                     int* pFdOut, int* pFdErr, amxp_slot_fn_t handler, void* userData) {
    ASSERT_STR(prg, NULL, ME, "Empty");
    amxp_subproc_t* subProc = NULL;

    char prgCopy[swl_str_len(prg) + 1];
    swl_str_copy(prgCopy, sizeof(prgCopy), prg);
    if(basename(prgCopy) != prgCopy) {
        // full path given
        ASSERT_EQUALS(access(prg, F_OK | X_OK), 0, NULL, ME, "prg (%s) can not be executed", prg);
    }

    if(swl_str_isEmpty(str)) {
        amxc_string_set(&dbgInfo->argBuffer, prg);
    } else {
        ASSERT_NULL(strchr(str, '>'), NULL, ME, "Don't run task with redirection -\"%s %s\" aborted", prg, str);
        ASSERT_NULL(strchr(str, '|'), NULL, ME, "Don't run task with pipes -\"%s %s\" aborted", prg, str);
        ASSERT_NULL(strchr(str, '&'), NULL, ME, "Don't run task in background -\"%s %s\" aborted", prg, str);
        amxc_string_setf(&dbgInfo->argBuffer, "%s %s", prg, str);
    }
    const char* cmdLine = amxc_string_get(&dbgInfo->argBuffer, 0);

    int ret = 0;
    ret = amxp_subproc_new(&subProc);
    if(ret != 0) {
        SAH_TRACEZ_ERROR(ME, "fail to create process");
        goto error;
    }
    if((pFdOut) && (s_redirOutstreamToTmpFile(subProc, STDOUT_FILENO) != SWL_RC_OK)) {
        SAH_TRACEZ_ERROR(ME, "fail to create stdout redirection");
        goto error;
    }
    if((pFdErr) && (s_redirOutstreamToTmpFile(subProc, STDERR_FILENO) != SWL_RC_OK)) {
        SAH_TRACEZ_ERROR(ME, "fail to create stderr redirection");
        goto error;
    }

    amxc_array_t aArgs;
    amxc_array_init(&aArgs, 10);
    amxc_llist_t lArgs;
    amxc_llist_init(&lArgs);
    amxc_string_split_to_llist(&dbgInfo->argBuffer, &lArgs, ' ');
    amxc_llist_for_each(it, &lArgs) {
        amxc_string_t* arg = amxc_container_of(it, amxc_string_t, it);
        if(!amxc_string_is_empty(arg)) {
            amxc_array_append_data(&aArgs, (void*) amxc_string_get(arg, 0));
        }
    }

    amxp_signal_mngr_t* sigMngr = amxp_subproc_get_sigmngr(subProc);
    if(handler != NULL) {
        amxp_slot_connect(sigMngr, "stop", NULL, handler, userData);
    }

    ret = amxp_subproc_astart(subProc, &aArgs);
    amxc_array_clean(&aArgs, NULL);
    amxc_llist_clean(&lArgs, amxc_string_list_it_free);
    if(ret != 0) {
        SAH_TRACEZ_ERROR(ME, "could not start %s", cmdLine);
        goto error;
    }
    dbgInfo->subProc = subProc;
    SAH_TRACEZ_INFO(ME, "exec -%s- %p @ %s:%u", prg, subProc, dbgInfo->file, dbgInfo->line);
    SAH_TRACEZ_INFO(ME, " * cmd: %s", cmdLine);

    return subProc;
error:
    amxc_string_clean(&dbgInfo->argBuffer);
    amxp_subproc_delete(&subProc);
    return NULL;
}

/**
 * Common functionality for file descriptor execution
 */
static swl_rc_ne s_common_fd(const char* file, uint32_t line,
                             int* pFdOut, int* pFdErr, swl_exec_result_t* pResult,
                             const char* prg, char* str) {

    swl_rc_ne ret = SWL_RC_ERROR;
    swl_exec_fdinfo_t execInfo;
    memset(&execInfo, 0, sizeof(swl_exec_fdinfo_t));
    execInfo.dbgInfo.file = file;
    execInfo.dbgInfo.line = line;
    amxc_string_init(&execInfo.dbgInfo.argBuffer, 0);

    amxp_subproc_t* subProc = s_exec_common(&execInfo.dbgInfo, prg, str,
                                            pFdOut, pFdErr, NULL, NULL);
    ASSERT_NOT_NULL(subProc, SWL_RC_ERROR, ME, "fail to create process");

    int waitRet = amxp_subproc_wait(subProc, SUBPROC_WAIT_TIMEOUT);
    if(waitRet == 0) {
        swl_exec_procExitInfo_t* pProcExitInfo = &execInfo.info.exitInfo;
        s_fillExitInfo(pProcExitInfo, NULL, &execInfo.dbgInfo);
        //check here if child has been really executed
        if(((pProcExitInfo->isExited) && (pProcExitInfo->exitStatus == EXIT_FAILURE)) ||
           (pProcExitInfo->isSignaled)) {
            SAH_TRACEZ_ERROR(ME, "(%s)(pid:%d) execution stops abnormally", prg, amxp_subproc_get_pid(subProc));
        }
        if(pResult) {
            memcpy(&pResult->exitInfo, pProcExitInfo, sizeof(pResult->exitInfo));
        }

        s_getOutStreamFileHandler(subProc, STDOUT_FILENO, pFdOut);
        s_getOutStreamFileHandler(subProc, STDERR_FILENO, pFdErr);
        ret = SWL_RC_OK;
    } else if(waitRet == 1) {
        SAH_TRACEZ_ERROR(ME, "timeout : %s %s", prg, str);
        amxp_subproc_kill(subProc, SIGKILL);
    } else {
        SAH_TRACEZ_ERROR(ME, "error %d : %s %s", waitRet, prg, str);
    }

    amxc_string_clean(&execInfo.dbgInfo.argBuffer);
    amxp_subproc_delete(&subProc);
    return ret;
}

static amxp_subproc_t* s_exec_common_bg(const char* file, uint32_t line,
                                        swl_exec_doneHandler_f doneHandler, swl_exec_doneBufHandler_f doneBufHandler,
                                        uint32_t maxStdOutBufSize, uint32_t maxStdErrBufSize,
                                        void* userData, const char* prg, const char* str) {
    swl_exec_cbInfo_t* cbInfo = calloc(1, sizeof(swl_exec_cbInfo_t));
    ASSERT_NOT_NULL(cbInfo, NULL, ME, "alloc failed");
    cbInfo->fDoneCb = doneHandler;
    cbInfo->fDoneBufCb = doneBufHandler;
    cbInfo->maxStdOutBufSize = maxStdOutBufSize;
    cbInfo->maxStdErrBufSize = maxStdErrBufSize;
    cbInfo->userData = userData;
    cbInfo->dbgInfo.file = file;
    cbInfo->dbgInfo.line = line;
    amxc_string_init(&cbInfo->dbgInfo.argBuffer, 0);

    int* pFdOut = NULL;
    int* pFdErr = NULL;
    if(doneBufHandler) {
        pFdOut = &cbInfo->fdOut;
        pFdErr = &cbInfo->fdErr;
    }

    return s_exec_common(&cbInfo->dbgInfo, prg, str, pFdOut, pFdErr, s_procExitHandlerBgCb, cbInfo);
}

/**
 * @brief execute a program in background, and call doneHandler when done
 *
 * @param file: the file from where call is made, for debugging purposes
 * @param line: the line from where call is made, for debugging purposes
 * @param doneHandler: the callback handler. Shall be called when task finishes. Can be NULL
 * @param userData: the userdata to be passed to the callback handler
 * @param prg: the program to be called to execute
 * @param format: the arguements to be added, as list of arguements
 * @return new subProc context if program correctly started, NULL otherwise
 */
amxp_subproc_t* swl_exec_common_bg(const char* file, uint32_t line,
                                   swl_exec_doneHandler_f doneHandler, void* userData,
                                   const char* prg, char* format, ...) {

    va_list args;
    amxp_subproc_t* subProc = NULL;
    char* str = NULL;
    va_start(args, format);
    int retVal = vasprintf(&str, format, args);
    va_end(args);
    ASSERT_NOT_EQUALS(retVal, -1, NULL, ME, "fail to format command");
    subProc = s_exec_common_bg(file, line, doneHandler, NULL, 0, 0, userData, prg, str);
    free(str);
    return subProc;
}

/**
 * @brief execute a program in background, and call doneHandler with stdout and stderr when done
 *
 * @param file: the file from where call is made, for debugging purposes
 * @param line: the line from where call is made, for debugging purposes
 * @param doneHandler: the callback handler. Shall be called when task finishes. Can be NULL
 * @param userData: the userdata to be passed to the callback handler
 * @param maxStdOutBufSize: maximum number of bytes to allocate for the output buffer passed to doneHandler
 * @param maxStdErrBufSize: maximum number of bytes to allocate for the error buffer passed to doneHandler
 * @param prg: the program to be called to execute
 * @param format: the arguements to be added, as list of arguements
 * @return new subProc context if program correctly started, NULL otherwise
 */

amxp_subproc_t* swl_exec_common_buf_bg(const char* file, uint32_t line,
                                       swl_exec_doneBufHandler_f doneHandler, void* userData,
                                       uint32_t maxStdOutBufSize, uint32_t maxStdErrBufSize,
                                       const char* prg, const char* format, ...) {
    va_list args;
    amxp_subproc_t* subProc = NULL;
    char* str = NULL;
    va_start(args, format);
    int retVal = vasprintf(&str, format, args);
    va_end(args);
    ASSERT_NOT_EQUALS(retVal, -1, NULL, ME, "fail to format command");
    subProc = s_exec_common_bg(file, line, NULL, doneHandler, maxStdOutBufSize, maxStdErrBufSize, userData, prg, str);
    free(str);
    return subProc;
}

/**
 * @brief execute a program and put output in provided file descriptors
 *
 * @param file: the file from where call is made, for debugging purposes
 * @param line: the line from where call is made, for debugging purposes
 * @param pFdOut: the output file descriptor
 * @param pFdErr: the error file descriptor
 * @param prg: the program to be called to execute
 * @param format: the arguements to be added, as list of arguements
 * @return SWL_RC_OK if program correctly started, SWL_RC_ERROR otherwise. Note that
 *   it is possible the program failed during run, but this shall not be reflected in return value.
 */
swl_rc_ne swl_exec_common_fd(const char* file, uint32_t line,
                             int* pFdOut, int* pFdErr,
                             const char* prg, char* format, ...) {
    va_list args;
    char* str = NULL;
    swl_rc_ne ret = SWL_RC_ERROR;
    /* Create the command */
    va_start(args, format);
    int retVal = vasprintf(&str, format, args);
    va_end(args);
    ASSERT_NOT_EQUALS(retVal, -1, SWL_RC_ERROR, ME, "fail to format command");
    ret = s_common_fd(file, line, pFdOut, pFdErr, NULL, prg, str);
    free(str);
    return ret;
}

/**
 * @brief execute a program and write stdOut into given buffer
 *
 * @param file: the file from where call is made, for debugging purposes
 * @param line: the line from where call is made, for debugging purposes
 * @param buf: the buffer to write output to
 * @param bufSize: bufSize
 * @param prg: the program to be called to execute
 * @param format: the arguements to be added, as list of arguements
 * @return SWL_RC_OK if program correctly started, SWL_RC_ERROR otherwise. Note that
 *   it is possible the program failed during run, but this shall not be reflected in return value.
 */
swl_rc_ne swl_exec_common_buf(const char* file, uint32_t line,
                              char* buf, size_t bufSize,
                              const char* prg, char* format, ...) {
    va_list args;
    char* str = NULL;
    int fd = -1;
    swl_rc_ne ret = SWL_RC_ERROR;
    /* Create the command */
    va_start(args, format);
    ret = vasprintf(&str, format, args);
    va_end(args);
    ASSERT_NOT_EQUALS(ret, -1, SWL_RC_ERROR, ME, "fail to format command");
    ret = s_common_fd(file, line, &fd, NULL, NULL, prg, str);
    free(str);

    if(ret < 0) {
        return ret;
    }
    s_dumpFd(fd, &buf, bufSize, 0);
    close(fd);
    return ret;
}

/**
 * @brief execute a program and write stdOut stdErr (buffers), exitCode and exitSignal into given structure.
 *
 * @param retResult: pointer to struct to be filled with all execution results
 * @param prg: the program to be called to execute
 * @param format: the arguements to be added, as list of arguements
 * @return SWL_RC_OK if program correctly started, SWL_RC_ERROR otherwise. Note that
 *   it is possible the program failed during run, but this shall not be reflected in return value.
 *   Output and error buffers are filled only if not null and sized.
 */
swl_rc_ne swl_exec_common_buf_ext(const char* file, uint32_t line,
                                  swl_exec_result_t* pResult,
                                  const char* prg, char* format, ...) {
    va_list args;
    char* str = NULL;
    int fdOut = -1;
    int* pFdOut = NULL;
    int fdErr = -1;
    int* pFdErr = NULL;

    if(pResult) {
        if(pResult->outBuf && pResult->outBufSize) {
            pResult->outBuf[0] = 0;
            pFdOut = &fdOut;
        }
        if(pResult->errBuf && pResult->errBufSize) {
            pResult->errBuf[0] = 0;
            pFdErr = &fdErr;
        }
    }

    swl_rc_ne ret = SWL_RC_ERROR;
    /* Create the command */
    va_start(args, format);
    ret = vasprintf(&str, format, args);
    va_end(args);
    ASSERT_NOT_EQUALS(ret, -1, SWL_RC_ERROR, ME, "fail to format command");
    ret = s_common_fd(file, line, pFdOut, pFdErr, pResult, prg, str);
    free(str);

    if(ret < 0) {
        return ret;
    }
    if(pFdOut && (*pFdOut != -1)) {
        s_dumpFd(*pFdOut, &pResult->outBuf, pResult->outBufSize, 0);
        close(*pFdOut);
    }
    if(pFdErr && (*pFdErr != -1)) {
        s_dumpFd(*pFdErr, &pResult->errBuf, pResult->errBufSize, 0);
        close(*pFdErr);
    }
    return ret;
}
