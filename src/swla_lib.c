/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
/**
 * Central swl library. Current functionality is just storing amx pointer for
 * other parts of library that may require it.
 */
#include "swla/swla_commonLib.h"
#include "swla/swla_delayExec.h"

#define ME "swlLib"

static amxb_bus_ctx_t* s_amx;

/**
 * Initialize the swl library module
 *
 * @param amx: pointer to the amx object.
 * @return true if successfully initialized, false otherwise.
 */
bool swl_lib_initialize(amxb_bus_ctx_t* amx) {
    ASSERT_NOT_NULL(amx, false, ME, "NULL");
    s_amx = amx;
    swla_delayExec_init();
    return true;
}

/**
 * Clean up the swl library module
 */
void swl_lib_cleanup() {
    SAH_TRACEZ_INFO(ME, "Cleanup swl");
    s_amx = NULL;
    swla_delayExec_cleanup();
}

/**
 * Returns the amx object.
 */
amxb_bus_ctx_t* swl_lib_getBus() {
    return s_amx;
}

/*
 * @brief return local dm context
 * (dm retrieved from registered amxb_bus_ctx_t set with swl_lib_initialize)
 *
 * @return return local dm context, NULL when not bus ctx is registered (ie missing init)
 */
amxd_dm_t* swl_lib_getLocalDm() {
    amxb_bus_ctx_t* busCtx = swl_lib_getBus();
    ASSERT_NOT_NULL(busCtx, NULL, ME, "fail to get local dm: Local bus not registered");
    return busCtx->dm;
}

