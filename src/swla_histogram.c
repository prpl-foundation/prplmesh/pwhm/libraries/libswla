/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "swla/swla_time.h"
#include "swla/swla_histogram.h"

#define ME "swlHstG"


static void s_addFlatWindowToMap(swl_histogram_t* hist, amxc_var_t* varMap, swl_histogramWindow_t* window, char* timePrefix) {
    char tmpBuffer[32];
    char dataBuffer[256];

    for(uint32_t i = 0; i < hist->nrValues; i++) {
        const char* name = NULL;
        if(hist->names != NULL) {
            name = hist->names[i];
        }
        if(name == NULL) {
            name = tmpBuffer;
            snprintf(tmpBuffer, sizeof(tmpBuffer), "%u", i);
        }

        uint16_t* targetBuckets = &window->buckets[hist->offsetList[i]];
        swl_typeUInt16_arrayToChar(dataBuffer, sizeof(dataBuffer), targetBuckets, hist->nrBucketsPerValue[i]);
        amxc_var_add_new_key_cstring_t(varMap, name, dataBuffer);
    }

    if(timePrefix != NULL) {
        char timeNameBuffer[64];
        snprintf(timeNameBuffer, sizeof(timeNameBuffer), "%sStartTime", timePrefix);
        swl_time_mapAddMono(varMap, timeNameBuffer, window->startTime);
        snprintf(timeNameBuffer, sizeof(timeNameBuffer), "%sEndTime", timePrefix);
        swl_time_mapAddMono(varMap, timeNameBuffer, window->endTime);
    }
}

/**
 * Add the given histogram window to the varmap in a flat fashion
 * if timePrefix is NULL, no time will be added. If it's empty string, it will be added with StartTime and EndTime.
 * If timePrefix contains an actual string, it will be prefixed to StartTime and EndTime.
 */
swl_rc_ne swl_histogram_addFlatWindowToMap(swl_histogram_t* hist, amxc_var_t* varMap, swl_histogram_windowId_e histogramIndex, char* timePrefix) {
    ASSERT_NOT_NULL(hist, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(varMap, SWL_RC_INVALID_PARAM, ME, "NULL");

    swl_histogramWindow_t* tgtWindow = swl_histogram_getWindow(hist, histogramIndex);
    ASSERT_NOT_NULL(tgtWindow, SWL_RC_INVALID_PARAM, ME, "Window not found");

    s_addFlatWindowToMap(hist, varMap, tgtWindow, timePrefix);

    return SWL_RC_OK;
}

static void s_addMap(swl_histogram_t* hist, amxc_var_t* varMap, const char* windowName, swl_histogramWindow_t* window) {
    amxc_var_t* tmpMap = amxc_var_add_key(amxc_htable_t, varMap, windowName, NULL);
    char tmpBuffer[32];
    char dataBuffer[256];

    for(uint32_t i = 0; i < hist->nrValues; i++) {
        const char* name = NULL;
        if(hist->names != NULL) {
            name = hist->names[i];
        }
        if(name == NULL) {
            name = tmpBuffer;
            snprintf(tmpBuffer, sizeof(tmpBuffer), "%u", i);
        }

        uint16_t* targetBuckets = &window->buckets[hist->offsetList[i]];
        swl_typeUInt16_arrayToChar(dataBuffer, sizeof(dataBuffer), targetBuckets, hist->nrBucketsPerValue[i]);
        amxc_var_add_key(cstring_t, tmpMap, name, dataBuffer);
    }

    swl_time_mapAddMono(tmpMap, "StartTime", window->startTime);
    swl_time_mapAddMono(tmpMap, "EndTime", window->endTime);
}

/**
 * Get all the histograms, meaning Current, Total, and Last histograms.
 */
swl_rc_ne swl_histogram_getAllHistograms(swl_histogram_t* hist, amxc_var_t* varMap) {
    ASSERT_NOT_NULL(hist, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(varMap, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(hist->offsetList, SWL_RC_INVALID_STATE, ME, "INIT NOT DONE");

    swl_histogram_windowData_t* hgData = &hist->histogramData;
    s_addMap(hist, varMap, "Current", &hgData->currentWindow);
    s_addMap(hist, varMap, "Total", &hgData->totalWindow);

    uint32_t lastWindowIndex = (hgData->nextWindowIndex + hist->nrWindows - 1 ) % hist->nrWindows;
    s_addMap(hist, varMap, "Last", &hgData->lastWindows[lastWindowIndex]);

    return SWL_RC_OK;
}

/**
 * get histogram map of a given table
 */
swl_rc_ne swl_histogram_mapAddHistogramWindow(swl_histogram_t* hist, amxc_var_t* varMap, char* name, swl_histogram_windowId_e histogramIndex) {
    swl_histogramWindow_t* tgtWindow = swl_histogram_getWindow(hist, histogramIndex);
    const char* tgtName = (name != NULL ? name : swl_histogram_tableNames_str[histogramIndex]);
    if(tgtWindow != NULL) {
        s_addMap(hist, varMap, tgtName, tgtWindow);
    }

    return SWL_RC_OK;
}

/**
 * Get the history in a variant map, for all values.
 */
swl_rc_ne swl_histogram_getHistory(swl_histogram_t* hist, amxc_var_t* varMap) {
    ASSERT_NOT_NULL(hist, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(varMap, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(hist->offsetList, SWL_RC_INVALID_STATE, ME, "INIT NOT DONE");

    amxc_var_t* tmpMap = amxc_var_add_key(amxc_htable_t, varMap, "History", NULL);

    char tmpBuffer[32] = {0};
    char dataBuffer[256] = {0};

    swl_histogram_historyData_t* hrData = &hist->historyData;

    for(uint32_t i = 0; i < hist->nrValues; i++) {
        const char* name = NULL;
        if(hist->names != NULL) {
            name = hist->names[i];
        }
        if(name == NULL) {
            name = tmpBuffer;
            snprintf(tmpBuffer, sizeof(tmpBuffer), "%u", i);
        }

        if(hist->historyLen == 0) {
            amxc_var_add_key(cstring_t, tmpMap, name, "");
        } else {
            uint8_t* targetBuckets = &hrData->history[i * hist->historyLen];

            if((hrData->filledHistorySize < hist->historyLen) || (hrData->nextHistoryIndex == 0)) {
                swl_typeUInt8_arrayToChar(dataBuffer, sizeof(dataBuffer), targetBuckets, hrData->filledHistorySize);
            } else {
                uint8_t finalData[hrData->filledHistorySize];
                uint32_t nrEntryAfter = hist->historyLen - hrData->nextHistoryIndex;
                memcpy(finalData, &targetBuckets[hrData->nextHistoryIndex], nrEntryAfter);
                memcpy(&finalData[nrEntryAfter], targetBuckets, hrData->nextHistoryIndex);
                swl_typeUInt8_arrayToChar(dataBuffer, sizeof(dataBuffer), finalData, hist->historyLen);
            }
            amxc_var_add_key(cstring_t, tmpMap, name, dataBuffer);
        }


    }
    return SWL_RC_OK;
}
