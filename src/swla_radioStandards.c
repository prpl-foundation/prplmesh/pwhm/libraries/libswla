/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/


/** @file This .c file is for representation of and basic operations on radio standards such as 802.11a.
 *
 * A **Radio Standard** is a standard of low-level Wi-Fi communication defined by IEEE.
 * A Radio Standard exists regardless of whether the chip supports it or whether it is enabled
 * or used.
 *
 * *Operating Standards* is the set of radio standards that are enabled for a radio.
 * Operating Standards can be written in different formats:
 * - *Standard Format*: comma-separated of standards, e.g. "b,g,n". NOT: "bgn".
 * - *Legacy Format*: value without comma, e.g. "bgn", "ac" or "b". NOT: "b,g,n" or "g,ac".
 *    "ac" and "ax" mean including lower supported standards. This is the most backwards-compatible.
 *    If it cannot be expressed like this, fallback to writing it in Standard Format.
 *
 * *Supported Standards* is the set of radio standards that can be enabled (but not necessarily are)
 * for a radio.
 * Supported Standards can be written in different formats:
 * - *Standard Format*: comma-separated of standards, e.g. "b,g,n". NOT: "bgn".
 * - *Legacy Format*: comma-separated of standards and of sets of standards, e.g. "b,g,n,bg,bgn".
 *
 * Note that it is preferred to not use strings to represent data (use #swl_radioStandard_m
 * instead), it is needed only when parsing/formatting from/to users and for externally defined
 * APIs that require strings.
 */

#define ME "swlRStd"

#include "swla/swla_commonLib.h"

/**
 * Formats a radio standard bitmask into a string.
 *
 * The order is lower bits first.
 *
 * Example:
 * - `format`: legacy, `radioStandards`: "a,ac", supportedStandards: "a,ac". Result: "ac".
 * - `format`: legacy, `radioStandards`: "a,ac", supportedStandards: "a,n,ac". Result: "n,ac".
 * - `format`: standard, `radioStandards`: "a,ac". Result: "a,ac".
 * - `format`: legacy, `radioStandards`: "a,b". Result: "ab".
 * - `format`: legacy, `radioStandards`: "a,g". Result: "a,g".
 * - `format`: standard, `radioStandards`: "a,b". Result: "a,b".
 *
 * The caller is responsible for cleanup of the result (`amxc_string_clean(&theString)`)
 *
 * For invalid `radioStandards`, an empty string is returned.
 *
 * @param supportedStandards: used when dealing with legacy format, as explained above.
 *   Ignored if format is standard.
 */
amxc_string_t swl_radStd_toStr(
    swl_radioStandard_m radioStandards,
    swl_radStd_format_e format,
    swl_radioStandard_m supportedStandards) {

    char buffer[64];
    bool ok = swl_radStd_toChar(buffer, sizeof(buffer), radioStandards, format, supportedStandards);
    if(!ok) {
        buffer[0] = '\0';
    }

    amxc_string_t radioStandardsText;
    amxc_string_init(&radioStandardsText, sizeof(buffer));
    amxc_string_set(&radioStandardsText, buffer);

    return radioStandardsText;
}

