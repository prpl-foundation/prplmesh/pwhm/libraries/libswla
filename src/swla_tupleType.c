/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "swla/swla_commonLib.h"
#include "swla/swla_tupleType.h"

#define ME "swlTpl"


/**
 * Convert a tuple to a map, with names as provided by arguement.
 */
void swl_tupleType_toMap(amxc_var_t* map, char** names, swl_tupleType_t* tplType, swl_tuple_t* tuple) {
    ASSERTI_TRUE(swl_tupleType_check(tplType), , ME, "NULL");

    for(size_t i = 0; i < tplType->nrTypes; i++) {
        swl_type_t* type = tplType->types[i];
        swl_typeData_t* tmpValue = swl_tupleType_getValue(tplType, tuple, i);
        amxc_var_t* tmpVar = amxc_var_add_new_key(map, names[i]);
        bool success = swl_type_toVariant(type, tmpVar, tmpValue);
        ASSERT_TRUE(success, , ME, "ERR %zu", i);
    }
}

/**
 * Read a tuple from a map, with names as provided.
 */
void swl_tupleType_fromMap(amxc_var_t* map, char** names, swl_tupleType_t* tplType, swl_tuple_t* tuple) {
    ASSERTI_TRUE(swl_tupleType_check(tplType), , ME, "NULL");

    for(size_t i = 0; i < tplType->nrTypes; i++) {
        swl_type_t* type = tplType->types[i];
        swl_typeEl_t* tmpValue = swl_tupleType_getReference(tplType, tuple, i);
        amxc_var_t* var = amxc_var_get_key(map, names[i], AMXC_VAR_FLAG_DEFAULT);
        bool success = swl_type_fromVariant(type, tmpValue, var);
        ASSERT_TRUE(success, , ME, "ERR %zu", i);
    }
}


/**
 * Write a tuple of type tplType to an object, with the names of parameters as provided.
 */
bool swl_tupleType_toObject(amxd_object_t* object, char** names, swl_tupleType_t* tplType, swl_tuple_t* tuple) {
    ASSERTI_TRUE(swl_tupleType_check(tplType), false, ME, "NULL");

    for(size_t i = 0; i < tplType->nrTypes; i++) {
        swl_type_t* type = tplType->types[i];
        swl_typeData_t* tmpValue = swl_tupleType_getValue(tplType, tuple, i);

        bool retVal = swl_type_toObjectParam(type, object, names[i], tmpValue);
        ASSERTI_TRUE(retVal, false, ME, "Error");
    }
    return true;
}

/**
 * Read a tuple of type tplType from an object, with the names of the parameters as provided.
 */
bool swl_tupleType_fromObject(amxd_object_t* object, char** names, swl_tupleType_t* tplType, swl_tuple_t* tuple) {
    ASSERTI_TRUE(swl_tupleType_check(tplType), false, ME, "NULL");

    for(size_t i = 0; i < tplType->nrTypes; i++) {
        swl_type_t* type = tplType->types[i];
        swl_typeEl_t* tmpValue = swl_tupleType_getReference(tplType, tuple, i);

        bool retVal = swl_type_fromObjectParam(type, object, names[i], tmpValue);
        ASSERTI_TRUE(retVal, false, ME, "Error");
    }
    return true;
}

/**
 * Reads values from object with given parameter name, compares it with the values currently in tuple.
 * If value i is changed, then the mask wil have bit i set. Note that this only works up to 64 bits.
 *
 * This function requires updatedField to be not NULL. If you updatedField is required, please use swl_tupleType_fromObject as
 * this is much more efficient avoiding some clean-up
 */
bool swl_tupleType_fromObjectUpdate(amxd_object_t* object, char** names, swl_tupleType_t* tplType, swl_tuple_t* tuple, swl_mask64_m* updatedField) {
    ASSERTI_TRUE(swl_tupleType_check(tplType), false, ME, "NULL");
    ASSERT_NOT_NULL(updatedField, false, ME, "NULL");
    *updatedField = 0;
    swl_mask64_m tmpMask = 0;

    for(size_t i = 0; i < tplType->nrTypes; i++) {
        swl_type_t* type = tplType->types[i];
        swl_typeEl_t* tmpValue = swl_tupleType_getReference(tplType, tuple, i);
        char buffer[type->size];
        memset(buffer, 0, type->size);
        swl_typeEl_t* localValue = &buffer;
        bool retVal = swl_type_fromObjectParam(type, object, names[i], localValue);
        ASSERTI_TRUE(retVal, false, ME, "Error");

        if(swl_type_equals(type, SWL_TYPE_TO_PTR(type, localValue), SWL_TYPE_TO_PTR(type, tmpValue))) {
            swl_type_cleanup(type, localValue);
        } else {
            tmpMask = SWL_BIT_SET(tmpMask, i);
            swl_type_cleanup(type, tmpValue);
            memcpy(tmpValue, localValue, type->size);
        }
    }
    *updatedField = tmpMask;
    return true;
}

/**
 * Writes values to object with given parameter name, compares it with the values currently in tuple.
 * If value i is changed, then the mask wil have bit i set. Note that this only works up to 64 bits.
 *
 * This function requires updatedField to be not NULL. If you updatedField is required, please use swl_tupleType_fromObject as
 * this is much more efficient avoiding some clean-up
 */
bool swl_tupleType_toObjectUpdate(amxd_object_t* object, char** names, swl_tupleType_t* tplType, swl_tuple_t* tuple, swl_mask64_m* updatedField) {
    ASSERTI_TRUE(swl_tupleType_check(tplType), false, ME, "NULL");
    ASSERT_NOT_NULL(updatedField, false, ME, "NULL");
    *updatedField = 0;

    swl_mask64_m tmpMask = 0;

    for(size_t i = 0; i < tplType->nrTypes; i++) {
        swl_type_t* type = tplType->types[i];
        swl_typeEl_t* tmpValue = swl_tupleType_getReference(tplType, tuple, i);
        char buffer[type->size];
        memset(buffer, 0, type->size);
        swl_typeEl_t* localValue = &buffer;
        bool retVal = swl_type_fromObjectParam(type, object, names[i], localValue);
        ASSERTI_TRUE(retVal, false, ME, "Error");

        if(!swl_type_equals(type, SWL_TYPE_TO_PTR(type, localValue), SWL_TYPE_TO_PTR(type, tmpValue))) {

            tmpMask = SWL_BIT_SET(tmpMask, i);
            bool retVal = swl_type_toObjectParam(type, object, names[i], tmpValue);
            ASSERTI_TRUE(retVal, false, ME, "Error");
        }
        swl_type_cleanup(type, localValue);
    }
    *updatedField = tmpMask;
    return true;
}

/**
 * Read a tuple of type tplType from the argument_value_list_t args.
 * Returns
 * * SWL_TRL_FALSE if some problem with arguments, or parsing of an argument failed
 * * SWL_TRL_UNKNOWN if not all types were able to be parsed. See updatedField for which are available.
 * * SWL_TRL_TRUE if all types were properly parsed.
 *
 */
swl_trl_e swl_tupleType_fromArgs(amxc_var_t* args, char** names, swl_tupleType_t* tplType, swl_tuple_t* tuple, swl_mask64_m* updatedField) {
    ASSERTI_TRUE(swl_tupleType_check(tplType), SWL_TRL_FALSE, ME, "NULL");
    swl_trl_e retVal = SWL_TRL_TRUE;


    swl_mask64_m tmpMask = 0;

    for(size_t i = 0; i < tplType->nrTypes; i++) {
        swl_type_t* type = tplType->types[i];
        swl_typeEl_t* tmpValue = swl_tupleType_getReference(tplType, tuple, i);
        amxc_var_t* var = amxc_var_get_key(args, names[i], AMXC_VAR_FLAG_DEFAULT);
        if(var == NULL) {
            continue;
        }
        tmpMask = SWL_BIT_SET(tmpMask, i);

        bool success = swl_type_fromVariant(type, tmpValue, var);
        ASSERT_TRUE(success, SWL_TRL_FALSE, ME, "ERR %zu", i);
    }
    if(updatedField != NULL) {
        *updatedField = tmpMask;
    }
    return retVal;
}

bool swl_tupleType_toVariant(swl_tupleType_t* tplType, amxc_var_t* tgt, const swl_tuple_t* tuple) {

    ASSERTI_TRUE(swl_tupleType_check(tplType), false, ME, "NULL");
    if(tgt->type_id != AMXC_VAR_ID_LIST) {
        amxc_var_set_type(tgt, AMXC_VAR_ID_LIST);
    }


    for(size_t i = 0; i < tplType->nrTypes; i++) {
        swl_type_t* type = tplType->types[i];
        swl_typeData_t* tmpValue = swl_tupleType_getValue(tplType, tuple, i);
        amxc_var_t* myVar = amxc_var_add_new(tgt);
        bool retVal = swl_type_toVariant(type, myVar, tmpValue);
        ASSERTI_TRUE(retVal, false, ME, "Error");
    }

    return true;
}

bool swl_tupleType_toVariantNamed(swl_tupleType_t* tplType, amxc_var_t* tgt, const swl_tuple_t* tuple, char** names) {
    ASSERTI_TRUE(swl_tupleType_check(tplType), false, ME, "NULL");
    if(tgt->type_id != AMXC_VAR_ID_HTABLE) {
        amxc_var_set_type(tgt, AMXC_VAR_ID_HTABLE);
    }


    for(size_t i = 0; i < tplType->nrTypes; i++) {
        swl_type_t* type = tplType->types[i];
        swl_typeData_t* tmpValue = swl_tupleType_getValue(tplType, tuple, i);
        amxc_var_t* myVar = amxc_var_add_new_key(tgt, names[i]);
        bool retVal = swl_type_toVariant(type, myVar, tmpValue);

        ASSERTI_TRUE(retVal, false, ME, "Error");
    }

    return true;
}

/**
 * Read a tuple of type tplType from a variant list.
 */
bool swl_tupleType_fromVariant(swl_tupleType_t* tplType, swl_tuple_t* tuple, const amxc_var_t* var) {
    ASSERTI_TRUE(swl_tupleType_check(tplType), false, ME, "NULL");
    ASSERT_EQUALS(var->type_id, AMXC_VAR_ID_LIST, false, "ME", "Invalid var type");

    const amxc_llist_t* varlist = amxc_var_get_const_amxc_llist_t(var);
    ASSERT_EQUALS(amxc_llist_size(varlist), tplType->nrTypes, false, "ME", "VarList not right size");

    size_t i = 0;
    amxc_llist_for_each(it, varlist) {
        swl_type_t* type = tplType->types[i];
        swl_typeEl_t* tmpValue = swl_tupleType_getReference(tplType, tuple, i);
        amxc_var_t* tmpVar = amxc_llist_it_get_data(it, amxc_var_t, lit);
        bool retVal = swl_type_fromVariant(type, tmpValue, tmpVar);
        ASSERTI_TRUE(retVal, false, ME, "Error");

        i++;
    }

    return true;
}

/**
 * Read a tuple of type tplType from a variant map, with the names of the parameters as provided.
 */
bool swl_tupleType_fromVariantNamed(swl_tupleType_t* tplType, swl_tuple_t* tuple, const amxc_var_t* var, char** names) {
    ASSERTI_TRUE(swl_tupleType_check(tplType), false, ME, "NULL");
    ASSERT_EQUALS(var->type_id, AMXC_VAR_ID_HTABLE, false, "ME", "Invalid var type");

    const amxc_htable_t* varMap = amxc_var_get_const_amxc_htable_t(var);
    for(size_t i = 0; i < tplType->nrTypes; i++) {
        swl_type_t* type = tplType->types[i];
        swl_typeEl_t* tmpValue = swl_tupleType_getReference(tplType, tuple, i);

        amxc_htable_it_t* it = amxc_htable_get(varMap, names[i]);
        amxc_var_t* tmpVar = amxc_htable_it_get_data(it, amxc_var_t, hit);
        ASSERTI_NOT_NULL(var, false, ME, "NULL %s", names[i]);

        bool retVal = swl_type_fromVariant(type, tmpValue, tmpVar);
        ASSERTI_TRUE(retVal, false, ME, "Error");
    }
    return true;
}

