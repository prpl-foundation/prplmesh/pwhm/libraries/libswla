/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "swla/swla_time_spec.h"
#include "swla/swla_conversion.h"

#include "swla/swla_commonLib.h"

#define ME "swlTime"

static bool s_timespecMono_toVariant_cb(swl_type_t* type _UNUSED, amxc_var_t* tgt, const swl_timeSpecMono_t* srcData) {
    ASSERTS_NOT_NULL(tgt, false, ME, "NULL");
    swl_datetime_t swl_dt;
    memset(&swl_dt, 0, sizeof(swl_datetime_t));
    swl_timespec_monoToDatetime(&swl_dt, srcData);
    amxc_ts_t dt;
    amxc_ts_from_tm(&dt, &swl_dt.datetime);
    dt.nsec = swl_dt.nanoseconds;
    return amxc_var_set(amxc_ts_t, tgt, &dt) == amxd_status_ok;
}

static bool s_timespecMono_fromVariant_cb(swl_type_t* type _UNUSED, swl_timeSpecMono_t* tgtData, const amxc_var_t* var) {
    ASSERTS_NOT_NULL(tgtData, false, ME, "NULL");
    swl_datetime_t swl_dt;
    memset(&swl_dt, 0, sizeof(swl_datetime_t));
    amxc_ts_t* time = amxc_var_dyncast(amxc_ts_t, var);
    amxc_ts_to_tm_utc(time, &swl_dt.datetime);
    swl_dt.nanoseconds = time->nsec;
    swl_timespec_datetimeToMono(tgtData, &swl_dt);
    free(time);
    return true;
}

SWL_CONSTRUCTOR static void s_timeSpecMono_var_init(void) {
    swl_type_timeSpecMono_impl.typeFun->toVariant = ((swl_type_toVariant_cb) s_timespecMono_toVariant_cb);
    swl_type_timeSpecMono_impl.typeFun->fromVariant = ((swl_type_fromVariant_cb) s_timespecMono_fromVariant_cb);
}


static bool s_timespecReal_toVariant_cb(swl_type_t* type _UNUSED, amxc_var_t* tgt, const swl_timeSpecReal_t* srcData) {
    ASSERTS_NOT_NULL(tgt, false, ME, "NULL");
    ASSERTS_NOT_NULL(srcData, false, ME, "NULL");

    if(swl_timespec_isZero(srcData)) {
        amxc_ts_t time;
        memset(&time, 0, sizeof(amxc_ts_t));
        amxc_ts_parse(&time, SWL_DATETIME_TR098_EPOCH_STR, strlen(SWL_DATETIME_TR098_EPOCH_STR));
        return amxc_var_set(amxc_ts_t, tgt, &time);
    }

    amxc_ts_t time = {
        .sec = srcData->tv_sec,
        .nsec = (int32_t) srcData->tv_nsec,
        .offset = 0
    };
    return amxc_var_set(amxc_ts_t, tgt, &time) == amxd_status_ok;
}

static bool s_timespecReal_fromVariant_cb(swl_type_t* type _UNUSED, swl_timeSpecReal_t* tgtData, const amxc_var_t* var) {
    ASSERTS_NOT_NULL(tgtData, false, ME, "NULL");
    swl_datetime_t swl_dt;
    memset(&swl_dt, 0, sizeof(swl_datetime_t));
    amxc_ts_t* time = amxc_var_dyncast(amxc_ts_t, var);
    amxc_ts_to_tm_utc(time, &swl_dt.datetime);
    swl_dt.nanoseconds = time->nsec;
    swl_timespec_datetimeToReal(tgtData, &swl_dt);
    free(time);
    return true;
}

SWL_CONSTRUCTOR static void s_timeSpecReal_var_init(void) {
    swl_type_timeSpecReal_impl.typeFun->toVariant = ((swl_type_toVariant_cb) s_timespecReal_toVariant_cb);
    swl_type_timeSpecReal_impl.typeFun->fromVariant = ((swl_type_fromVariant_cb) s_timespecReal_fromVariant_cb);
}

