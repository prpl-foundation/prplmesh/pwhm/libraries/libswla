/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include "swl/swl_datetime.h"
#include "swla/swla_time.h"

#define ME "swlTime"

/**
 * Adds monotonic time to map as realtime.
 */
void swl_time_mapAddMono(amxc_var_t* map, const char* name, swl_timeMono_t time) {
    struct tm timeval;
    memset(&timeval, 0, sizeof(struct tm));
    amxc_ts_t ts;
    memset(&ts, 0, sizeof(amxc_ts_t));
    swl_time_monoToTm(&timeval, time);
    amxc_ts_from_tm(&ts, &timeval);
    amxc_var_add_key(amxc_ts_t, map, name, &ts);
}

/**
 * Adds realtime to map.
 */
void swl_time_mapAddReal(amxc_var_t* map, const char* name, swl_timeReal_t time) {
    struct tm timeval;
    memset(&timeval, 0, sizeof(struct tm));
    amxc_ts_t ts;
    memset(&ts, 0, sizeof(amxc_ts_t));
    swl_time_realToTm(&timeval, time);
    amxc_ts_from_tm(&ts, &timeval);
    amxc_var_add_key(amxc_ts_t, map, name, &ts);
}

/**
 * Returns monotonic time from given UTC datetime in a variant map.
 */
swl_timeMono_t swl_time_mapFindMono(amxc_var_t* map, const char* name) {
    return swl_time_realToMono(swl_time_mapFindReal(map, name));
}

/**
 * Returns realtime from given UTC datetime in a variant map.
 */
swl_timeReal_t swl_time_mapFindReal(amxc_var_t* map, const char* name) {
    const amxc_ts_t* time = amxc_var_constcast(amxc_ts_t, amxc_var_get_key(map, name, AMXC_VAR_FLAG_DEFAULT));
    struct tm timeStamp;
    memset(&timeStamp, 0, sizeof(struct tm));
    amxc_ts_to_tm_utc(time, &timeStamp);
    if(!memcmp(&timeStamp, &swl_time_nullTimeTm, sizeof(struct tm))) {
        return 0;
    }
    time_t gmFromTime = timegm(&timeStamp);
    return gmFromTime;
}

/**
 * Sets monotonic time to object parameter as UTC datetime.
 */
void swl_time_objectParamSetMono(amxd_object_t* object, const char* name, swl_timeMono_t time) {
    struct tm timeval;
    memset(&timeval, 0, sizeof(struct tm));
    amxc_ts_t value;
    memset(&value, 0, sizeof(amxc_ts_t));
    swl_time_monoToTm(&timeval, time);
    amxc_ts_from_tm(&value, &timeval);
    amxd_status_t status = amxd_object_set_amxc_ts_t(object, name, &value);
    ASSERT_EQUALS(status, amxd_status_ok, , ME, "Err %s", amxd_status_string(status));
}

/**
 * Sets realtime to object parameter as UTC datetime.
 */
void swl_time_objectParamSetReal(amxd_object_t* object, const char* name, swl_timeReal_t time) {
    struct tm timeval;
    memset(&timeval, 0, sizeof(struct tm));
    amxc_ts_t value;
    memset(&value, 0, sizeof(amxc_ts_t));
    swl_time_realToTm(&timeval, time);
    amxc_ts_from_tm(&value, &timeval);
    amxd_status_t status = amxd_object_set_amxc_ts_t(object, name, &value);
    ASSERT_EQUALS(status, amxd_status_ok, , ME, "Err %s", amxd_status_string(status));
}

/**
 * Loads object parameter as UTC datetime and converts it to realtime
 * @param object
 *  pointer to amx object
 * @param name
 *  amx parameter name
 * @return -1 in case of error
 */
swl_timeReal_t swl_time_objectParamReal(amxd_object_t* object, const char* name) {
    swl_timeReal_t realtime = ((time_t) -1);
    amxc_ts_t* ts = amxd_object_get_value(amxc_ts_t, object, name, NULL);
    struct tm p_tmTime;
    memset(&p_tmTime, 0, sizeof(struct tm));
    amxc_ts_to_tm_utc(ts, &p_tmTime);
    free(ts);
    ASSERT_NOT_NULL(&p_tmTime, realtime, ME, "fail to parse datetime: %s", name);
    realtime = swl_time_tmToReal(&p_tmTime);
    ASSERT_NOT_EQUALS(realtime, ((time_t) -1), realtime, ME, "fail to convert datetime: %s", strerror(errno));
    return realtime;
}

/**
 * Loads object parameter as UTC datetime and converts it to monotonic time
 * @param object
 *  pointer to amx object
 * @param name
 *  amx parameter name
 * @return -1 in case of error
 */
swl_timeMono_t swl_time_objectParamMono(amxd_object_t* object, const char* name) {
    swl_timeReal_t realtime = swl_time_objectParamReal(object, name);
    ASSERT_NOT_EQUALS(realtime, ((time_t) -1), realtime, ME, "invalid");
    return swl_time_realToMono(realtime);
}

static bool s_timeMono_toVariant_cb(swl_type_t* type _UNUSED, amxc_var_t* tgt, const swl_timeMono_t* srcData) {
    ASSERTS_NOT_NULL(tgt, false, ME, "NULL");
    ASSERTS_NOT_NULL(srcData, false, ME, "NULL");
    amxc_ts_t time;
    memset(&time, 0, sizeof(amxc_ts_t));
    char buffer[64] = {0};
    swl_time_monoToDate(buffer, sizeof(buffer), *srcData);
    amxc_ts_parse(&time, buffer, strlen(buffer));
    amxc_var_set(amxc_ts_t, tgt, &time);
    return true;
}

static bool s_timeMono_fromVariant_cb(swl_type_t* type _UNUSED, swl_timeMono_t* tgtData, const amxc_var_t* var) {
    ASSERTS_NOT_NULL(tgtData, false, ME, "NULL");
    amxc_ts_t* time = amxc_var_get_amxc_ts_t(var);
    *tgtData = swl_time_realToMono(time->sec);
    free(time);
    return true;
}

SWL_CONSTRUCTOR static void s_timeMono_var_init(void) {
    swl_type_timeMono_impl.typeFun->toVariant = ((swl_type_toVariant_cb) s_timeMono_toVariant_cb);
    swl_type_timeMono_impl.typeFun->fromVariant = ((swl_type_fromVariant_cb) s_timeMono_fromVariant_cb);
}

static bool s_timeReal_toVariant_cb(swl_type_t* type _UNUSED, amxc_var_t* tgt, const swl_timeReal_t* srcData) {
    ASSERTS_NOT_NULL(tgt, false, ME, "NULL");
    ASSERTS_NOT_NULL(srcData, false, ME, "NULL");
    swl_timeReal_t timeData = *srcData;
    if(timeData == 0) {
        amxc_ts_t time;
        memset(&time, 0, sizeof(amxc_ts_t));
        amxc_ts_parse(&time, SWL_DATETIME_TR098_EPOCH_STR, strlen(SWL_DATETIME_TR098_EPOCH_STR));
        return amxc_var_set(amxc_ts_t, tgt, &time);
    }

    amxc_ts_t time = {
        .sec = timeData,
        .nsec = 0,
        .offset = 0
    };
    return amxc_var_set(amxc_ts_t, tgt, &time) == amxd_status_ok;
}

static bool s_timeReal_fromVariant_cb(swl_type_t* type _UNUSED, swl_timeReal_t* tgtData, const amxc_var_t* var) {
    ASSERTS_NOT_NULL(tgtData, false, ME, "NULL");
    amxc_ts_t* time = amxc_var_dyncast(amxc_ts_t, var);
    *tgtData = time->sec;
    free(time);
    return true;
}

SWL_CONSTRUCTOR static void s_timeReal_var_init(void) {
    swl_type_timeReal_impl.typeFun->toVariant = ((swl_type_toVariant_cb) s_timeReal_toVariant_cb);
    swl_type_timeReal_impl.typeFun->fromVariant = ((swl_type_fromVariant_cb) s_timeReal_fromVariant_cb);
}
