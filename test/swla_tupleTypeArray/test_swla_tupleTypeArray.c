/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "swla/ttb/swla_ttbVariant.h"
#include "swla/swla_tupleType.h"
#include "swla/types/swla_tupleTypeArray.h"
#include "swl/swl_common.h"
#include "swla/swla_conversion.h"
#include "swla/swla_commonLib.h"
#include "swla/swla_type.h"
#include "test-toolbox/ttb_assert.h"

#define NR_TYPES 3
#define NR_VALUES 4

#define MY_TEST_TABLE_VAR(X, Y) \
    X(Y, gtSwl_type_int32, index) \
    X(Y, gtSwl_type_int64, key) \
    X(Y, gtSwl_type_charPtr, val)
SWL_TT(myTestTTType, swl_myTestTT_t, MY_TEST_TABLE_VAR, structMod);


swl_myTestTT_t myTestTable[NR_VALUES] = {
    {1, 100, "test1"},
    {2, -200, "test2"},
    {-8852, 289870, "foobar"},
    {8851, -289869, "barfoo"},
};

swl_myTestTT_t myTestTableDup[NR_VALUES] = {
    {1, 100, "test1"},
    {1, -200, "test2"},
    {2, -200, "test3"},
    {3, -300, "test3"},
};

swl_myTestTT_t zeroValues = {0, 0, NULL};


char* names[NR_TYPES] = {"SmallInt", "BigInt", "String"};

const char* columnArrays[NR_TYPES] = {
    "1,2,-8852,8851",
    "100,-200,289870,-289869",
    "test1,test2,foobar,barfoo"
};

/**
 * Strings with one offset, for offset testing
 */
const char* columnArraysOffset[NR_TYPES] = {
    "2,-8852,8851,1",
    "-200,289870,-289869,100",
    "test2,foobar,barfoo,test1"
};

#define NR_OTHER_DATA 2

swl_myTestTT_t otherData[NR_OTHER_DATA] = {
    {5, 500, "notHere"},
    {-5, -500, "alsoNotHere"},
};


static void test_swl_tta_toMapOfChar(void** state _UNUSED) {

    amxc_var_t myVar;

    char nameBuffer[32] = {0};
    for(size_t i = 0; i <= NR_VALUES; i++) {
        amxc_var_init(&myVar);
        amxc_var_set_type(&myVar, AMXC_VAR_ID_HTABLE);
        swl_tta_toMapOfChar(&myVar, names, &myTestTTType, myTestTable, i);
        snprintf(nameBuffer, sizeof(nameBuffer), "toMap/test_%zu.txt", i);
        swl_ttbVariant_assertToFileMatchesFile(&myVar, nameBuffer);
        amxc_var_clean(&myVar);
    }
}


static void test_swl_tta_fromMapOfChar(void** state _UNUSED) {

    amxc_var_t myVar;
    amxc_var_init(&myVar);
    amxc_var_set_type(&myVar, AMXC_VAR_ID_HTABLE);


    swl_tta_toMapOfChar(&myVar, names, &myTestTTType, myTestTable, NR_VALUES);


    swl_myTestTT_t testData[NR_VALUES];
    memset(testData, 0, sizeof(testData));

    swl_tta_fromMapOfChar(&myVar, names, &myTestTTType, testData, NR_VALUES);

    swl_ttb_assertBigTypeArrayEquals(&myTestTTType.type, testData, NR_VALUES, myTestTable, NR_VALUES);

    swl_type_arrayCleanup(&myTestTTType.type, testData, NR_VALUES);
    amxc_var_clean(&myVar);
}

static void test_swl_tta_mapAddColumn(void** state _UNUSED) {

    amxc_var_t myVar;

    char nameBuffer[32] = {0};
    for(size_t i = 0; i < NR_TYPES; i++) {
        amxc_var_init(&myVar);
        amxc_var_set_type(&myVar, AMXC_VAR_ID_HTABLE);
        swl_tta_mapAddColumnChar(&myVar, names[i], &myTestTTType, myTestTable, NR_VALUES, i);
        snprintf(nameBuffer, sizeof(nameBuffer), "mapAddColumn/test_%zu.txt", i);
        swl_ttbVariant_assertToFileMatchesFile(&myVar, nameBuffer);
        amxc_var_clean(&myVar);

    }

    for(size_t i = 0; i < NR_TYPES; i++) {
        amxc_var_init(&myVar);
        amxc_var_set_type(&myVar, AMXC_VAR_ID_HTABLE);
        swl_tta_mapAddColumnCharOffset(&myVar, names[i], &myTestTTType, myTestTable, NR_VALUES, i, 1, -1);
        snprintf(nameBuffer, sizeof(nameBuffer), "mapAddColumn/testOffset_%zu.txt", i);
        swl_ttbVariant_assertToFileMatchesFile(&myVar, nameBuffer);
        amxc_var_clean(&myVar);
    }
}

static void test_swl_tta_toListOfMaps(void** state _UNUSED) {

    amxc_var_t myVar;

    char nameBuffer[32] = {0};
    for(size_t i = 0; i <= NR_VALUES; i++) {
        amxc_var_init(&myVar);
        amxc_var_set_type(&myVar, AMXC_VAR_ID_LIST);

        swl_tta_toListOfMaps(&myVar, names, &myTestTTType, myTestTable, i);
        snprintf(nameBuffer, sizeof(nameBuffer), "toList/test_%zu.txt", i);
        swl_ttbVariant_assertToFileMatchesFile(&myVar, nameBuffer);
        amxc_var_clean(&myVar);
    }
}


static void test_swl_tta_fromListOfMaps(void** state _UNUSED) {

    amxc_var_t myVar;
    amxc_var_init(&myVar);
    amxc_var_set_type(&myVar, AMXC_VAR_ID_LIST);



    swl_tta_toListOfMaps(&myVar, names, &myTestTTType, myTestTable, NR_VALUES);


    swl_myTestTT_t testData[NR_VALUES];
    memset(testData, 0, sizeof(testData));


    swl_tta_fromListOfMaps(&myVar, names, &myTestTTType, testData, NR_VALUES);
    swl_ttb_assertBigTypeArrayEquals(&myTestTTType.type, testData, NR_VALUES, myTestTable, NR_VALUES);


    swl_type_arrayCleanup(&myTestTTType.type, testData, NR_VALUES);
    amxc_var_clean(&myVar);

}

static int setup_suite(void** state _UNUSED) {
    return 0;
}

static int teardown_suite(void** state _UNUSED) {
    return 0;
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlConv");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_tta_toMapOfChar),
        cmocka_unit_test(test_swl_tta_fromMapOfChar),
        cmocka_unit_test(test_swl_tta_mapAddColumn),
        cmocka_unit_test(test_swl_tta_toListOfMaps),
        cmocka_unit_test(test_swl_tta_fromListOfMaps),
    };
    int rc = 0;
    ttb_util_setFilter();
    rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);
    sahTraceClose();
    return rc;
}

