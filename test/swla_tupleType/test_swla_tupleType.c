/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>

#include "swla/swla_common_amx.h"
#include "swla/swla_commonLib.h"
#include "swla/swla_type.h"
#include "swla/swla_table.h"

#define NR_TYPES 3
#define NR_VALUES 4

/**
 * Defining a tuple type using a table
 */
SWL_TABLE(myTestTable,
          ARR(int32_t index; int64_t key; char* val; ),
          ARR(swl_type_int32, swl_type_int64, swl_type_charPtr),
          ARR(
              {1, 100, "test1"},
              {2, -200, "test2"},
              {-8852, 289870, "foobar"},
              {8851, -289869, "barfoo"},
              )
          );

/**
 * Example of defining a tuple type
 */
SWL_TUPLE_TYPE(myTestTuple, ARR(swl_type_int32, swl_type_int64, swl_type_charPtr));

#define NR_OTHER_DATA 2

myTestTableStruct_t otherData[NR_OTHER_DATA] = {
    {5, 500, "notHere"},
    {-5, -500, "alsoNotHere"},
};

char* names[NR_TYPES] = {"SmallInt", "BigInt", "String"};

static void test_swl_tupleType_getValFromTuple(void** state _UNUSED) {
    for(size_t i = 0; i < myTestTable.nrTuples; i++) {
        void* testData = swl_tupleType_getValue(&myTestTableTupleType, &myTestTableValues[i], 0);
        assert_ptr_equal(&myTestTableValues[i].index, testData);

        testData = swl_tupleType_getValue(&myTestTableTupleType, &myTestTableValues[i], 1);
        assert_ptr_equal(&myTestTableValues[i].key, testData);

        //ptr logic => returned data is pointer itself
        testData = swl_tupleType_getValue(&myTestTableTupleType, &myTestTableValues[i], 2);
        assert_ptr_equal(myTestTableValues[i].val, testData);


        assert_null(swl_tupleType_getValue(&myTestTableTupleType, &myTestTableValues[i], myTestTable.nrTuples));
        assert_null(swl_tupleType_getValue(&myTestTableTupleType, &myTestTableValues[i], -1));
    }
}

static void test_swl_tupleType_getRefFromTuple(void** state _UNUSED) {
    for(size_t i = 0; i < myTestTable.nrTuples; i++) {
        void* testData = swl_tupleType_getReference(&myTestTableTupleType, &myTestTableValues[i], 0);
        assert_ptr_equal(&myTestTableValues[i].index, testData);

        testData = swl_tupleType_getReference(&myTestTableTupleType, &myTestTableValues[i], 1);
        assert_ptr_equal(&myTestTableValues[i].key, testData);

        //ptr logic => returned data is reference still
        testData = swl_tupleType_getReference(&myTestTableTupleType, &myTestTableValues[i], 2);
        assert_ptr_equal(&myTestTableValues[i].val, testData);


        assert_null(swl_tupleType_getReference(&myTestTableTupleType, &myTestTableValues[i], myTestTable.nrTuples));
        assert_null(swl_tupleType_getReference(&myTestTableTupleType, &myTestTableValues[i], -1));
    }
}

static void test_swl_tupleType_offsetAndSize(void** state _UNUSED) {

    size_t counter = 0;
    for(size_t i = 0; i < NR_TYPES; i++) {
        assert_int_equal(counter, swl_tupleType_getOffset(&myTestTableTupleType, i));
        counter += myTestTableTypes[i]->size;
    }

    assert_int_equal(counter, swl_tupleType_size(&myTestTableTupleType));
}

static void test_swl_tupleType_check(void** state _UNUSED) {
    assert_true(swl_tupleType_check(&myTestTableTupleType));
    assert_int_not_equal(0, myTestTableTupleType.type.size);

    assert_false(swl_tupleType_check(NULL));


    assert_int_equal(0, myTestTupleTupleType.type.size);
    assert_true(swl_tupleType_check(&myTestTupleTupleType));
    assert_int_not_equal(0, myTestTupleTupleType.type.size);

}

#define NR_MASK_VALUES (2 << (NR_TYPES - 1))
#define ALL_MASK (NR_MASK_VALUES - 1)

/**
 * Defining a tuple type using a table
 */
SWL_TABLE(myTestTableMasked,
          ARR(int32_t index; int64_t key; char* val; ),
          ARR(swl_type_int32, swl_type_int64, swl_type_charPtr),
          ARR(
              {2, 200, "test2"},
              {1, 200, "test2"},
              {2, 100, "test2"},
              {1, 100, "test2"},
              {2, 200, "test1"},
              {1, 200, "test1"},
              {2, 100, "test1"},
              {1, 100, "test1"},
              )
          );

myTestTableMaskedStruct_t myBaseVal = {.index = 1, .key = 100, .val = "test1"};

static amxd_dm_t s_myDm;
amxd_object_t* s_myObj;


static void test_swl_tupleType_equalsByMask(void** state _UNUSED) {
    for(size_t i = 0; i < (2 << (NR_TYPES - 1)); i++) {
        for(size_t j = 0; j < (2 << (NR_TYPES - 1)); j++) {
            assert_int_equal(((j & i) == i)
                             , swl_tupleType_equalsByMask(&myTestTableMaskedTupleType, &myBaseVal, &myTestTableMaskedValues[j], i));
        }
    }
}


static void test_swl_tupleType_copyClean(void** state _UNUSED) {
    size_t typeSize = swl_tupleType_size(&myTestTableTupleType);
    char buffer[typeSize];
    char zeroBuffer[typeSize];
    memset(zeroBuffer, 0, sizeof(zeroBuffer));
    for(size_t i = 0; i < NR_MASK_VALUES; i++) {
        memset(buffer, 0, sizeof(buffer));
        swl_tupleType_copyByMask(buffer, &myTestTableMaskedTupleType, &myTestTableMaskedValues[i], i);

        assert_true(swl_tupleType_equalsByMask(&myTestTableMaskedTupleType, buffer, &myBaseVal, i));

        size_t oppositeMask = ALL_MASK & (~i);
        assert_true(swl_tupleType_equalsByMask(&myTestTableMaskedTupleType, buffer, zeroBuffer, oppositeMask));

        for(size_t j = 0; j < NR_MASK_VALUES; j++) {
            assert_int_equal(((j & i) == i)
                             , swl_tupleType_equalsByMask(&myTestTableMaskedTupleType, &myBaseVal, &myTestTableMaskedValues[j], i));
        }

        swl_tupleType_cleanupByMask(&myTestTableMaskedTupleType, buffer, i);
    }
}


void test_swl_tupleType_toFromMap(void** state _UNUSED) {

    for(size_t i = 0; i < NR_VALUES; i++) {
        amxc_var_t tmpMap;
        amxc_var_init(&tmpMap);
        amxc_var_set_type(&tmpMap, AMXC_VAR_ID_HTABLE);

        swl_tupleType_toMap(&tmpMap, names, &myTestTableTupleType, &myTestTableValues[i]);

        for(size_t j = 0; j < NR_TYPES; j++) {
            amxc_var_t tmpVar;
            amxc_var_init(&tmpVar);
            swl_type_t* tmpType = myTestTableTupleType.types[j];
            swl_type_toVariant(tmpType, &tmpVar, swl_tupleType_getReference(&myTestTableTupleType, &myTestTableValues[i], j));
            int diff = 0;
            amxc_var_t* mapVar = amxc_var_get_key(&tmpMap, names[j], AMXC_VAR_FLAG_DEFAULT);
            assert_int_equal(amxc_var_compare(&tmpVar, mapVar, &diff), 0);
            amxc_var_clean(&tmpVar);
        }


        myTestTableStruct_t val;
        memset(&val, 0, sizeof(myTestTableStruct_t));

        swl_tupleType_fromMap(&tmpMap, names, &myTestTableTupleType, &val);

        assert_true(swl_tupleType_equals(&myTestTableTupleType, &val, &myTestTableValues[i]));

        swl_tupleType_cleanup(&myTestTableTupleType, &val);


        amxc_var_clean(&tmpMap);
    }
}


static void test_swl_tupleType_toFromObject(void** state _UNUSED) {
    for(size_t i = 0; i < NR_VALUES; i++) {

        assert_true(swl_tupleType_toObject(s_myObj, names, &myTestTableTupleType, &myTestTableValues[i]));

        for(size_t j = 0; j < NR_TYPES; j++) {
            amxc_var_t tmpVar;
            amxc_var_init(&tmpVar);

            swl_type_t* tmpType = myTestTableTupleType.types[j];
            swl_type_toVariant(tmpType, &tmpVar, swl_tupleType_getValue(&myTestTableTupleType, &myTestTableValues[i], j));
            int diff = 0;

            amxc_var_t mapVar;
            amxc_var_init(&mapVar);
            amxd_status_t status = amxd_object_get_param(s_myObj, names[j], &mapVar);
            assert_int_equal(status, amxd_status_ok);


            assert_int_equal(amxc_var_compare(&tmpVar, &mapVar, &diff), 0);
            assert_true(diff == 0);


            amxc_var_clean(&mapVar);
            amxc_var_clean(&tmpVar);
        }

        myTestTableStruct_t val;
        memset(&val, 0, sizeof(myTestTableStruct_t));

        swl_tupleType_fromObject(s_myObj, names, &myTestTableTupleType, &val);

        assert_true(swl_tupleType_equals(&myTestTableTupleType, &val, &myTestTableValues[i]));

        swl_tupleType_cleanup(&myTestTableTupleType, &val);
    }
}


static void test_swl_tupleType_toFromObjectChanged(void** state _UNUSED) {
    for(size_t i = 0; i < (1 << NR_TYPES); i++) {
        assert_true(swl_tupleType_toObject(s_myObj, names, &myTestTableTupleType, &myTestTableValues[0]));

        myTestTableStruct_t tmpData;
        memset(&tmpData, 0, sizeof(myTestTableStruct_t));
        swl_tupleType_copyByMask(&tmpData, &myTestTableTupleType, &myTestTableValues[0], ~i);
        swl_tupleType_copyByMask(&tmpData, &myTestTableTupleType, &otherData[0], i);


        swl_mask64_m tmpMask = 0;
        assert_true(swl_tupleType_toObjectUpdate(s_myObj, names, &myTestTableTupleType, &tmpData, &tmpMask));
        assert_int_equal(tmpMask, i);

        assert_true(swl_tupleType_toObject(s_myObj, names, &myTestTableTupleType, &myTestTableValues[0]));
        tmpMask = 0;
        assert_true(swl_tupleType_fromObjectUpdate(s_myObj, names, &myTestTableTupleType, &tmpData, &tmpMask));
        assert_int_equal(tmpMask, i);


        swl_tupleType_cleanup(&myTestTableTupleType, &tmpData);
    }
}

static void test_swl_tupleType_fromArgs(void** state _UNUSED) {
    for(size_t i = 0; i < NR_VALUES; i++) {
        amxc_var_t argList;
        amxc_var_init(&argList);
        amxc_var_set_type(&argList, AMXC_VAR_ID_HTABLE);

        for(size_t j = 0; j < NR_TYPES; j++) {
            amxc_var_t* tmpVar = amxc_var_add_new_key(&argList, names[j]);
            swl_type_t* tmpType = myTestTableTupleType.types[j];
            swl_type_toVariant(tmpType, tmpVar, swl_tupleType_getValue(&myTestTableTupleType, &myTestTableValues[i], j));
        }

        const amxc_htable_t* myTable = amxc_var_get_const_amxc_htable_t(&argList);

        assert_true(amxc_htable_size(myTable) == NR_TYPES);

        myTestTableStruct_t val;
        memset(&val, 0, sizeof(myTestTableStruct_t));

        swl_tupleType_fromArgs(&argList, names, &myTestTableTupleType, &val, NULL);

        assert_true(swl_tupleType_equals(&myTestTableTupleType, &val, &myTestTableValues[i]));

        swl_tupleType_cleanup(&myTestTableTupleType, &val);

        amxc_var_clean(&argList);
    }
}

static void test_swl_tupleType_toFromVar(void** state _UNUSED) {
    for(size_t i = 0; i < NR_VALUES; i++) {
        amxc_var_t var;
        amxc_var_init(&var);

        assert_true(swl_tupleType_toVariant(&myTestTableTupleType, &var, &myTestTableValues[i]));

        assert_true(var.type_id == AMXC_VAR_ID_LIST);
        const amxc_llist_t* varList = amxc_var_get_const_amxc_llist_t(&var);

        for(size_t j = 0; j < NR_TYPES; j++) {
            amxc_var_t tmpVar;
            amxc_var_init(&tmpVar);

            swl_type_t* tmpType = myTestTableTupleType.types[j];
            swl_type_toVariant(tmpType, &tmpVar, swl_tupleType_getValue(&myTestTableTupleType, &myTestTableValues[i], j));
            int diff = 0;


            amxc_llist_it_t* it = amxc_llist_get_at(varList, j);
            amxc_var_t* mapVar = amxc_llist_it_get_data(it, amxc_var_t, lit);

            assert_int_equal(amxc_var_compare(&tmpVar, mapVar, &diff), 0);
            assert_true(diff == 0);

            amxc_var_clean(&tmpVar);
        }

        myTestTableStruct_t val;
        memset(&val, 0, sizeof(myTestTableStruct_t));

        swl_tupleType_fromVariant(&myTestTableTupleType, &val, &var);

        assert_true(swl_tupleType_equals(&myTestTableTupleType, &val, &myTestTableValues[i]));

        swl_tupleType_cleanup(&myTestTableTupleType, &val);
        amxc_var_clean(&var);
    }
}


static void test_swl_tupleType_toFromVarNamed(void** state _UNUSED) {
    for(size_t i = 0; i < NR_VALUES; i++) {
        amxc_var_t var;
        amxc_var_init(&var);

        assert_true(swl_tupleType_toVariantNamed(&myTestTableTupleType, &var, &myTestTableValues[i], names));

        assert_true(var.type_id == AMXC_VAR_ID_HTABLE);
        const amxc_htable_t* varMap = amxc_var_get_const_amxc_htable_t(&var);

        for(size_t j = 0; j < NR_TYPES; j++) {
            amxc_var_t tmpVar;
            amxc_var_init(&tmpVar);

            swl_type_t* tmpType = myTestTableTupleType.types[j];
            swl_type_toVariant(tmpType, &tmpVar, swl_tupleType_getValue(&myTestTableTupleType, &myTestTableValues[i], j));
            int diff = 0;


            amxc_htable_it_t* it = amxc_htable_get(varMap, names[j]);
            amxc_var_t* mapVar = amxc_htable_it_get_data(it, amxc_var_t, hit);

            assert_int_equal(amxc_var_compare(&tmpVar, mapVar, &diff), 0);
            assert_true(diff == 0);

            amxc_var_clean(&tmpVar);
        }

        myTestTableStruct_t val;
        memset(&val, 0, sizeof(myTestTableStruct_t));

        swl_tupleType_fromVariantNamed(&myTestTableTupleType, &val, &var, names);

        assert_true(swl_tupleType_equals(&myTestTableTupleType, &val, &myTestTableValues[i]));

        swl_tupleType_cleanup(&myTestTableTupleType, &val);
        amxc_var_clean(&var);
    }
}



static int s_setupSuite(void** state _UNUSED) {

    amxd_param_t* param = NULL;




    assert_int_equal(amxd_dm_init(&s_myDm), 0);

    assert_int_equal(amxd_object_new(&s_myObj, amxd_object_singleton, "parent"), 0);
    assert_int_equal(amxd_dm_add_root_object(&s_myDm, s_myObj), 0);



    assert_non_null(s_myObj);


    assert_int_equal(amxd_param_new(&param, "SmallInt", AMXC_VAR_ID_INT32), 0);
    assert_int_equal(amxd_object_add_param(s_myObj, param), amxd_status_ok);
    assert_int_equal(amxd_param_new(&param, "BigInt", AMXC_VAR_ID_INT64), 0);
    assert_int_equal(amxd_object_add_param(s_myObj, param), amxd_status_ok);
    assert_int_equal(amxd_param_new(&param, "String", AMXC_VAR_ID_CSTRING), 0);
    assert_int_equal(amxd_object_add_param(s_myObj, param), amxd_status_ok);
    assert_true(amxd_object_get_param_value(s_myObj, "SmallInt") != NULL);

    return 0;
}

static int s_teardownSuite(void** state _UNUSED) {


    amxd_dm_clean(&s_myDm);

    return 0;
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlConv");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_tupleType_getValFromTuple),
        cmocka_unit_test(test_swl_tupleType_getRefFromTuple),
        cmocka_unit_test(test_swl_tupleType_offsetAndSize),
        cmocka_unit_test(test_swl_tupleType_check),
        cmocka_unit_test(test_swl_tupleType_copyClean),
        cmocka_unit_test(test_swl_tupleType_equalsByMask),
        cmocka_unit_test(test_swl_tupleType_toFromMap),
        cmocka_unit_test(test_swl_tupleType_toFromObject),
        cmocka_unit_test(test_swl_tupleType_toFromObjectChanged),
        cmocka_unit_test(test_swl_tupleType_fromArgs),
        cmocka_unit_test(test_swl_tupleType_toFromVar),
        cmocka_unit_test(test_swl_tupleType_toFromVarNamed),
    };
    int rc = 0;
    rc = cmocka_run_group_tests(tests, s_setupSuite, s_teardownSuite);
    sahTraceClose();
    return rc;
}
