/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>
#include <libgen.h>
#include <debug/sahtrace.h>
#include "test-toolbox/ttb_amx.h"
#include "test-toolbox/ttb_mockClock.h"
#include "test-toolbox/ttb_mockTimer.h"
#include "test-toolbox/ttb_timespec.h"
#include "test-toolbox/ttb_object.h"

#include "swla/swla_delayExec.h"


/**
 * @file Test for testing that we can call a pcb function from a unittest.
 *
 * Can also be used as an example of a unittest that calls a pcb function.
 */

#ifndef _UNUSED
#define _UNUSED __attribute__((unused))
#endif

#define NR_ENTRY 10

int s_data[NR_ENTRY];

bool s_marked0[NR_ENTRY];
bool s_marked1[NR_ENTRY];

static ssize_t s_getDataIndex(int* data) {
    for(size_t i = 0; i < NR_ENTRY; i++) {
        if(data == &s_data[i]) {
            return i;
        }
    }
    return -1;
}

static void s_callBackFun0(void* data) {
    ssize_t index = s_getDataIndex(data);
    assert_true(index >= 0);
    s_marked0[index] = true;
}


static void s_callBackFun1(void* data) {
    ssize_t index = s_getDataIndex(data);
    assert_true(index >= 0);
    s_marked1[index] = true;
}

static void s_resetMarks() {
    for(size_t i = 0; i < NR_ENTRY; i++) {
        s_marked0[i] = false;
        s_marked1[i] = false;
    }
}

static void test_swla_delayExec(void** state _UNUSED) {
    for(size_t i = 0; i < NR_ENTRY; i++) {
        swla_delayExec_add(s_callBackFun0, &s_data[i]);
        if(i % 2 == 0) {
            swla_delayExec_add(s_callBackFun1, &s_data[i]);
        }
    }
    for(size_t i = 0; i < NR_ENTRY; i++) {
        assert_false(s_marked0[i]);
        assert_false(s_marked1[i]);
    }

    ttb_mockTimer_goToFutureMs(1);
    for(size_t i = 0; i < NR_ENTRY; i++) {
        assert_true(s_marked0[i]);
        if(i % 2 == 0) {
            assert_true(s_marked1[i]);
        } else {
            assert_false(s_marked1[i]);
        }
    }


    s_resetMarks();
}

static void test_swla_delayExecTimeout(void** state _UNUSED) {
    for(size_t i = 0; i < NR_ENTRY; i++) {
        swla_delayExec_add(s_callBackFun0, &s_data[i]);
        if(i % 2 == 0) {
            swla_delayExec_addTimeout(s_callBackFun1, &s_data[i], 100);
        }
    }
    for(size_t i = 0; i < NR_ENTRY; i++) {
        assert_false(s_marked0[i]);
        assert_false(s_marked1[i]);
    }

    ttb_mockTimer_goToFutureMs(1);
    for(size_t i = 0; i < NR_ENTRY; i++) {
        assert_true(s_marked0[i]);
        if(i % 2 == 0) {
            assert_false(s_marked1[i]);
        } else {
            assert_false(s_marked1[i]);
        }
    }

    s_resetMarks();

    ttb_mockTimer_goToFutureMs(100);
    for(size_t i = 0; i < NR_ENTRY; i++) {
        assert_false(s_marked0[i]);
        if(i % 2 == 0) {
            assert_true(s_marked1[i]);
        } else {
            assert_false(s_marked1[i]);
        }
    }


    s_resetMarks();
}

static int setup_suite(void** state _UNUSED) {
    swla_delayExec_init();
    return 0;
}

static int teardown_suite(void** state _UNUSED) {
    swla_delayExec_cleanup();
    return 0;
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);

    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swla_delayExec),
        cmocka_unit_test(test_swla_delayExecTimeout),

    };

    int rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);
    sahTraceClose();
    return rc;
}
