/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/


/**
 * @file Tests for swl_type. They are run "normally", i.e. just once and not repeated for each type.
 */

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>

#include "swla/swla_commonLib.h"
#include "swla/swla_type.h"
#include "swla/swla_table.h"


static bool s_oddInt32_toVariant_cb(swl_type_t* type _UNUSED, amxc_var_t* tgt, const int32_t* srcData) {
    ASSERTS_NOT_NULL(tgt, false, ME, "NULL");
    if(*srcData % 2 == 0) {
        return false;
    }
    return amxc_var_set(int32_t, tgt, *srcData);
}

static bool s_oddInt32_fromVariant_cb(swl_type_t* type _UNUSED, int32_t* tgtData, const amxc_var_t* var) {
    ASSERTS_NOT_NULL(tgtData, false, ME, "NULL");
    if(amxc_var_type_of(var) != AMXC_VAR_ID_INT32) {
        return false;
    }
    int32_t intVal = amxc_var_dyncast(int32_t, var);
    if(intVal % 2 == 0) {
        return false;
    }
    *tgtData = intVal;
    return true;
}

static int64_t s_charToInt64(const char* srcStr) {
    return strtoll(srcStr, NULL, 10);
}

static size_t s_int32_toChar_cb(swl_type_t* type _UNUSED, char* tgtStr, size_t tgtStrSize, const void* srcData) {
    return snprintf(tgtStr, tgtStrSize, "%d", *(int32_t*) srcData);
}

static bool s_int32_fromChar_cb(swl_type_t* type _UNUSED, int32_t* tgtData, const char* srcStr) {
    ASSERTS_NOT_NULL(tgtData, false, ME, "NULL");
    ASSERTS_NOT_NULL(srcStr, false, ME, "NULL");
    *tgtData = (int32_t) s_charToInt64(srcStr);
    return true;
}

swl_typeFun_t type_oddInt32_fun = {
    .isValue = true,
    .toChar = (swl_type_toChar_cb) s_int32_toChar_cb,
    .fromChar = (swl_type_fromChar_cb) s_int32_fromChar_cb,
    .equals = NULL,
    .copy = NULL,
    .toVariant = (swl_type_toVariant_cb) s_oddInt32_toVariant_cb,
    .fromVariant = (swl_type_fromVariant_cb) s_oddInt32_fromVariant_cb,
};

swl_type_t type_oddInt32_impl = {
    .typeFun = &type_oddInt32_fun,
    .size = sizeof(int32_t),
};

typedef int32_t oddInt32_t;
extern swl_type_t type_oddInt32_impl;

#define type_oddInt32 &type_oddInt32_impl


SWL_TYPE_DEFINE_FUNCTIONS_VAL(oddInt32, type_oddInt32, oddInt32_t);

static void s_assertVarListType(const amxc_llist_t* varList, uint32_t expectedType) {
    amxc_llist_for_each(it, varList) {
        amxc_var_t* element = amxc_var_from_llist_it(it);
        assert_int_equal(expectedType, amxc_var_type_of(element));
    }
}

static amxc_var_t* s_varListAt(const amxc_llist_t* list, size_t index) {
    return amxc_var_from_llist_it(amxc_llist_get_at(list, index));
}

static void test_swl_type_arrayToVariantList(void** state _UNUSED) {
    // GIVEN an array of values and the type of these values
    int16_t values[6] = {1, -1, 0, 100, 32767, -32768};
    swl_type_t* type = swl_type_int16;

    // WHEN converting to a variant list
    amxc_var_t varList;
    amxc_var_init(&varList);
    amxc_var_set_type(&varList, AMXC_VAR_ID_LIST);

    bool ok = swl_type_arrayToVariantList(type, &varList, values, SWL_ARRAY_SIZE(values));

    const amxc_llist_t* list = amxc_var_get_const_amxc_llist_t(&varList);
    // THEN it succeeded and the variant list contains these values
    assert_true(ok);
    s_assertVarListType(list, AMXC_VAR_ID_INT16);
    assert_int_equal(amxc_var_dyncast(int16_t, s_varListAt(list, 0)), 1);
    assert_int_equal(amxc_var_dyncast(int16_t, s_varListAt(list, 1)), -1);
    assert_int_equal(amxc_var_dyncast(int16_t, s_varListAt(list, 2)), 0);
    assert_int_equal(amxc_var_dyncast(int16_t, s_varListAt(list, 3)), 100);
    assert_int_equal(amxc_var_dyncast(int16_t, s_varListAt(list, 4)), 32767);
    assert_int_equal(amxc_var_dyncast(int16_t, s_varListAt(list, 5)), -32768);
    assert_int_equal(SWL_ARRAY_SIZE(values), amxc_llist_size(list)); \



    amxc_var_clean(&varList);
}


static void test_swl_type_arrayToVariantList_conversionError(void** state _UNUSED) {
    // GIVEN an array of values and the type of these values, where an element cannot be converted to variant.
    oddInt32_t values[3] = {1, 2, 3};
    swl_type_t* type = type_oddInt32;
    amxc_var_t variant;
    amxc_var_init(&variant);
    bool canConvert = swl_type_toVariant(type, &variant, &values[1]);
    amxc_var_clean(&variant);
    assert_false(canConvert);

    // WHEN converting to a variant list
    amxc_var_t varList;
    amxc_var_init(&varList);
    amxc_var_set_type(&varList, AMXC_VAR_ID_LIST);

    bool ok = swl_type_arrayToVariantList(type, &varList, values, SWL_ARRAY_SIZE(values));

    // THEN it failed because the second element cannot be converted
    assert_false(ok);

    amxc_var_clean(&varList);
}

static void test_swl_arrayFromVariantList(void** state _UNUSED) {
    // GIVEN a variant_list with elements of a type corresponding to an swl type:
    amxc_var_t varList;
    amxc_var_init(&varList);
    amxc_var_set_type(&varList, AMXC_VAR_ID_LIST);
    amxc_var_add(uint64_t, &varList, 0);
    amxc_var_add(uint64_t, &varList, 1);
    amxc_var_add(uint64_t, &varList, UINT64_C(18446744073709551493));
    amxc_var_add(uint64_t, &varList, 100);
    swl_type_t* type = swl_type_uint64;

    // WHEN converting that list to an array:
    uint64_t buffer[4] = {999, 999, 999, 999};
    memset(buffer, 0, sizeof(buffer));
    size_t nbWritten = 0;
    bool ok = swl_type_arrayFromVariantList(type, buffer, 4, &varList, &nbWritten);

    // THEN the array contains the same elements:
    assert_true(ok);
    assert_int_equal(buffer[0], 0);
    assert_int_equal(buffer[1], 1);
    assert_int_equal(buffer[2], UINT64_C(18446744073709551493));
    assert_int_equal(buffer[3], 100);
    assert_int_equal(4, nbWritten);

    amxc_var_clean(&varList);
}

static void test_swl_arrayFromVariantList_conversionError(void** state _UNUSED) {
    // GIVEN a type and a variant_list where one element cannot be converted to that type
    amxc_var_t varList;
    amxc_var_init(&varList);
    amxc_var_set_type(&varList, AMXC_VAR_ID_LIST);
    amxc_var_add(int32_t, &varList, 4);
    amxc_var_add(int32_t, &varList, 5);
    amxc_var_add(int32_t, &varList, 6);
    swl_type_t* type = type_oddInt32;

    // WHEN converting that list to an array:
    int8_t buffer[3] = {9, 9, 9};
    size_t nbWritten = 0;
    bool ok = swl_type_arrayFromVariantList(type, buffer, 3, &varList, &nbWritten);

    // THEN conversion failed
    assert_false(ok);
    assert_true(nbWritten < 3);

    amxc_var_clean(&varList);
}

static void test_swl_arrayFromVariantList_nullNbWritten(void** state _UNUSED) {
    // GIVEN a variant_list with elements of a type corresponding to an swl type:
    amxc_var_t varList;
    amxc_var_init(&varList);
    amxc_var_set_type(&varList, AMXC_VAR_ID_LIST);
    amxc_var_add(uint64_t, &varList, 0);
    amxc_var_add(uint64_t, &varList, 1);
    amxc_var_add(uint64_t, &varList, UINT64_C(18446744073709551493));
    amxc_var_add(uint64_t, &varList, 100);
    swl_type_t* type = swl_type_uint64;

    // WHEN converting that list to an array, without asking how many elements were written to
    uint64_t buffer[4] = {999, 999, 999, 999};
    memset(buffer, 0, sizeof(buffer));
    bool ok = swl_type_arrayFromVariantList(type, buffer, 4, &varList, NULL);

    // THEN the array contains the same elements:
    assert_true(ok);
    assert_int_equal(buffer[0], 0);
    assert_int_equal(buffer[1], 1);
    assert_int_equal(buffer[2], UINT64_C(18446744073709551493));
    assert_int_equal(buffer[3], 100);

    amxc_var_clean(&varList);
}

static void test_swl_type_arrayFromVariantList_tooSmallTargetArray(void** state _UNUSED) {
    // GIVEN an array of size 1 and a variant_list of size 2
    uint8_t buffer[1] = {0};
    amxc_var_t varList;
    amxc_var_init(&varList);
    amxc_var_set_type(&varList, AMXC_VAR_ID_LIST);
    amxc_var_add(uint8_t, &varList, 7);
    amxc_var_add(uint8_t, &varList, 6);
    swl_type_t* type = swl_type_uint8;

    // WHEN serializing the variant list into that array
    size_t nbWritten = 100000;
    bool ok = swl_type_arrayFromVariantList(type, buffer, 1, &varList, &nbWritten);

    // THEN serializing failed (and did not crash), and 1 element was still written.
    assert_false(ok);
    assert_int_equal(1, nbWritten);
    assert_int_equal(buffer[0], 7);

    amxc_var_clean(&varList);
}


int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlType");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_type_arrayToVariantList),
        cmocka_unit_test(test_swl_type_arrayToVariantList_conversionError),
        cmocka_unit_test(test_swl_arrayFromVariantList),
        cmocka_unit_test(test_swl_arrayFromVariantList_conversionError),
        cmocka_unit_test(test_swl_arrayFromVariantList_nullNbWritten),
        cmocka_unit_test(test_swl_type_arrayFromVariantList_tooSmallTargetArray),
    };
    int rc = 0;
    rc = cmocka_run_group_tests(tests, NULL, NULL);
    sahTraceClose();
    return rc;
}
