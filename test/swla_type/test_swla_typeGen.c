/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>

#include "swla/swla_commonLib.h"
#include "swla/swla_type.h"

#include <amxd/amxd_dm.h>

typedef struct {
    const char* serialStr;
    const char** strValues;
    void* baseValues;
    size_t* valueCount;
    void* equalsValues;
    void* notContains;
    uint32_t nrTestValues;
} testData_t;



#define NR_INT_TEST_VALUES 7

int32_t int32_base[NR_INT_TEST_VALUES] = {0, 1, 2, -3, 2201, 2, 4};
size_t int32_count[NR_INT_TEST_VALUES] = {1, 1, 2, 1, 1, 2, 1};
int32_t int32_notContains[NR_INT_TEST_VALUES] = {-1, 5, 12, -2201, 1014, -2, 8};
#define testStrInt32 "0,1,2,-3,2201,2,4"
const char* testStrValuesInt32[NR_INT_TEST_VALUES] = {
    "0", "1", "2", "-3", "2201", "2", "4"
};

#define NR_CHAR_TEST_VALUES 5
char* char_base[NR_CHAR_TEST_VALUES] = {"abc", "def", "xyz", "abc", "4ava"};
size_t monoCount[NR_CHAR_TEST_VALUES] = {2, 1, 1, 2, 1};
char* monoNotContains[NR_CHAR_TEST_VALUES] = {"abd", "3ava", "zyx", "456", "1234"};
#define monoStrChar "abc,def,xyz,abc,4ava"
const char* testStrValuesChar[NR_CHAR_TEST_VALUES] = {
    "abc", "def", "xyz", "abc", "4ava"
};


static amxd_dm_t myDm;
static amxd_object_t* myObj;
static int s_setupSuite(void** state) {
    (void) state;
    amxd_param_t* param = NULL;

    assert_int_equal(amxd_dm_init(&myDm), 0);

    assert_int_equal(amxd_object_new(&myObj, amxd_object_singleton, "parent"), 0);
    assert_int_equal(amxd_dm_add_root_object(&myDm, myObj), 0);

    assert_non_null(myObj);


    assert_int_equal(amxd_param_new(&param, "TestParam", AMXC_VAR_ID_CSTRING), 0);
    assert_int_equal(amxd_object_add_param(myObj, param), amxd_status_ok);

    return 0;
}

static int s_teardownSuite(void** state) {
    (void) state;
    amxd_dm_clean(&myDm);
    return 0;
}


static void test_swl_typeInt32_default(void** state _UNUSED) {
    assert_true(swl_typeInt32_equals(1, 1));
    assert_false(swl_typeInt32_equals(1, -1));
    assert_int_equal(1, swl_typeInt32_copy(1));
    assert_int_equal(-1, swl_typeInt32_copy(-1));
    int32_t testArray[NR_INT_TEST_VALUES + 1] = {0};
    assert_int_equal(swl_typeInt32_arrayFromChar(testArray, NR_INT_TEST_VALUES + 1, testStrInt32), NR_INT_TEST_VALUES);
    assert_true(swl_typeInt32_arrayEquals(testArray, NR_INT_TEST_VALUES, int32_base, NR_INT_TEST_VALUES));
    char buffer[128] = {0};
    size_t len = swl_typeInt32_arrayToChar(buffer, sizeof(buffer), int32_base, NR_INT_TEST_VALUES);
    assert_int_equal(strlen(testStrInt32), len);
    assert_string_equal(testStrInt32, buffer);

    for(int i = 0; i < NR_INT_TEST_VALUES; i++) {
        assert_int_equal(swl_typeInt32_arrayCount(int32_base, NR_INT_TEST_VALUES, int32_base[i]), int32_count[i]);
    }
    for(int i = 0; i < NR_INT_TEST_VALUES; i++) {
        assert_int_equal(swl_typeInt32_arrayCount(int32_base, NR_INT_TEST_VALUES, int32_notContains[i]), 0);
    }

    for(int i = 0; i < NR_INT_TEST_VALUES; i++) {
        assert_true(swl_typeInt32_arrayContains(int32_base, NR_INT_TEST_VALUES, int32_base[i]));
        assert_false(swl_typeInt32_arrayContains(int32_base, NR_INT_TEST_VALUES, int32_notContains[i]));
    }
}

static void test_swl_typeInt32_valMatches(void** state _UNUSED) {
    int32_t copyArr[NR_INT_TEST_VALUES] = {0};
    size_t copyArrSize = NR_INT_TEST_VALUES * sizeof(int32_t);
    memcpy(&copyArr, &int32_base, copyArrSize);
    assert_true(swl_typeInt32_arrayMatches(int32_base, NR_INT_TEST_VALUES, copyArr, NR_INT_TEST_VALUES));
    assert_false(swl_typeInt32_arrayMatches(int32_base, NR_INT_TEST_VALUES, copyArr, NR_INT_TEST_VALUES - 1));
    assert_false(swl_typeInt32_arrayMatches(int32_base, NR_INT_TEST_VALUES, int32_notContains, NR_INT_TEST_VALUES));

    for(int i = 0; i < NR_INT_TEST_VALUES; i++) {
        assert_true(swl_typeInt32_arrayMatches(int32_base, NR_INT_TEST_VALUES, copyArr, NR_INT_TEST_VALUES));
        copyArr[i] = int32_notContains[i];
        assert_false(swl_typeInt32_arrayMatches(int32_base, NR_INT_TEST_VALUES, copyArr, NR_INT_TEST_VALUES));
        copyArr[i] = int32_base[i];
    }

    for(int i = 0; i < NR_INT_TEST_VALUES; i++) {
        for(int j = 0; j < NR_INT_TEST_VALUES; j++) {
            int32_t tmpData = copyArr[i];
            copyArr[i] = copyArr[j];
            copyArr[j] = tmpData;
            assert_true(swl_typeInt32_arrayMatches(int32_base, NR_INT_TEST_VALUES, copyArr, NR_INT_TEST_VALUES));
            copyArr[j] = copyArr[i];
            copyArr[i] = tmpData;
        }
    }
}

static void test_swl_typeInt32_arrayIsSuperset(void** state _UNUSED) {
    int32_t copyArr[NR_INT_TEST_VALUES] = {0};
    size_t copyArrSize = NR_INT_TEST_VALUES * sizeof(int32_t);
    memcpy(&copyArr, &int32_base, copyArrSize);

    for(int i = 0; i < NR_INT_TEST_VALUES; i++) {
        assert_true(swl_typeInt32_arrayIsSuperset(int32_base, NR_INT_TEST_VALUES,
                                                  copyArr, NR_INT_TEST_VALUES - i));
    }
    for(int i = 0; i < NR_INT_TEST_VALUES - 1; i++) {
        assert_false(swl_typeInt32_arrayIsSuperset(int32_base, NR_INT_TEST_VALUES,
                                                   int32_notContains, NR_INT_TEST_VALUES - i));
    }

    for(int i = 0; i < NR_INT_TEST_VALUES; i++) {
        memcpy(copyArr, int32_base, copyArrSize);
        assert_true(swl_typeInt32_arrayIsSuperset(int32_base, NR_INT_TEST_VALUES, copyArr, NR_INT_TEST_VALUES));
        copyArr[i] = int32_notContains[i];
        assert_false(swl_typeInt32_arrayIsSuperset(int32_base, NR_INT_TEST_VALUES, copyArr, NR_INT_TEST_VALUES));
    }

    for(int i = 0; i < NR_INT_TEST_VALUES; i++) {
        for(int j = 0; j < NR_INT_TEST_VALUES; j++) {
            memcpy(copyArr, int32_base, copyArrSize);
            int32_t tmpData = copyArr[i];
            copyArr[i] = copyArr[j];
            copyArr[j] = tmpData;
            for(int k = 0; k < NR_INT_TEST_VALUES - 1; k++) {
                assert_true(swl_typeInt32_arrayIsSuperset(int32_base, NR_INT_TEST_VALUES, copyArr, NR_INT_TEST_VALUES - k));
            }
        }
    }
}

static void test_swl_typeInt32_toFromChar(void** state _UNUSED) {
    char buf[128];
    int32_t testVal = 0;
    for(int i = 0; i < NR_INT_TEST_VALUES; i++) {
        memset(buf, 0, sizeof(buf));
        swl_typeInt32_toChar(buf, sizeof(buf), int32_base[i]);
        assert_string_equal(buf, testStrValuesInt32[i]);

        testVal = 0;
        swl_typeInt32_fromChar(&testVal, testStrValuesInt32[i]);
        assert_int_equal(testVal, int32_base[i]);

        testVal = swl_typeInt32_fromCharDef(testStrValuesInt32[i], 0);
        assert_int_equal(testVal, int32_base[i]);
    }

    testVal = -1;
    testVal = swl_typeInt32_fromCharDef("", 0);
    assert_int_equal(testVal, 0);

    assert_false(swl_typeInt32_fromChar(&testVal, ""));
}

void test_swl_typeInt32_toFromVar(void** state _UNUSED) {
    amxc_var_t myVar;
    int32_t testVal = 0;
    for(int i = 0; i < NR_INT_TEST_VALUES; i++) {
        amxc_var_init(&myVar);
        amxc_var_set_type(&myVar, AMXC_VAR_ID_INT32);
        swl_typeInt32_toVariant(&myVar, int32_base[i]);
        assert_int_equal(amxc_var_type_of(&myVar), AMXC_VAR_ID_INT32);
        assert_int_equal(amxc_var_dyncast(int32_t, &myVar), int32_base[i]);

        amxc_var_clean(&myVar);
        amxc_var_init(&myVar);
        amxc_var_set_type(&myVar, AMXC_VAR_ID_INT32);
        amxc_var_set(int32_t, &myVar, int32_base[i]);


        testVal = 0;
        assert_true(swl_typeInt32_fromVariant(&testVal, &myVar));
        assert_int_equal(testVal, int32_base[i]);

        testVal = 0;
        testVal = swl_typeInt32_fromVariantDef(&myVar, 0);
        assert_int_equal(testVal, int32_base[i]);
        amxc_var_clean(&myVar);
    }


    amxc_var_init(&myVar);
    amxc_var_set_type(&myVar, AMXC_VAR_ID_CSTRING);
    amxc_var_set(cstring_t, &myVar, "");

    testVal = -1;
    testVal = swl_typeInt32_fromVariantDef(&myVar, 100);
    assert_int_equal(testVal, 100);

    assert_false(swl_typeInt32_fromVariant(&testVal, &myVar));
    amxc_var_clean(&myVar);
}


void test_swl_typeInt32_toFromObj(void** state _UNUSED) {
    for(int i = 0; i < NR_INT_TEST_VALUES; i++) {
        assert_true(swl_typeInt32_toObjectParam(myObj, "TestParam", int32_base[i]));
        printf("%i %i \n", int32_base[i], amxd_object_get_int32_t(myObj, "TestParam", NULL));

        assert_int_equal(amxd_object_get_int32_t(myObj, "TestParam", NULL), int32_base[i]);
    }

    int32_t testVal;

    for(int i = 0; i < NR_INT_TEST_VALUES; i++) {
        testVal = 0;
        amxd_object_set_value(int32_t, myObj, "TestParam", int32_base[i]);
        assert_true(swl_typeInt32_fromObjectParam(myObj, "TestParam", &testVal));
        assert_int_equal(testVal, int32_base[i]);

        testVal = 0;
        testVal = swl_typeInt32_fromObjectParamDef(myObj, "TestParam", 0);
        assert_int_equal(testVal, int32_base[i]);
    }

    amxd_object_set_value(cstring_t, myObj, "TestParam", "");

    testVal = -1;
    testVal = swl_typeInt32_fromObjectParamDef(myObj, "TestParam", 0);
    assert_int_equal(testVal, 0);

    testVal = -1;
    assert_false(swl_typeInt32_fromObjectParam(myObj, "TestParam", &testVal));
}

static void test_swl_typeInt32_toFromMap(void** state _UNUSED) {
    amxc_var_t myMap;
    amxc_var_init(&myMap);
    amxc_var_set_type(&myMap, AMXC_VAR_ID_HTABLE);

    char buffer[50];

    for(int i = 0; i < NR_INT_TEST_VALUES; i++) {
        snprintf(buffer, sizeof(buffer), "TestParam_%u", i);
        swl_typeInt32_addToMap(&myMap, buffer, int32_base[i]);
    }

    for(int i = 0; i < NR_INT_TEST_VALUES; i++) {
        snprintf(buffer, sizeof(buffer), "TestParam_%u", i);
        assert_int_equal(amxc_var_constcast(int32_t, amxc_var_get_key(&myMap, buffer, AMXC_VAR_FLAG_DEFAULT)), int32_base[i]);
    }

    int32_t testVal;

    for(int i = 0; i < NR_INT_TEST_VALUES; i++) {
        testVal = 0;
        snprintf(buffer, sizeof(buffer), "TestParam_%u", i);
        assert_true(swl_typeInt32_findInMap(&myMap, buffer, &testVal));
        assert_int_equal(testVal, int32_base[i]);

        assert_int_equal(swl_typeInt32_findInMapDef(&myMap, buffer, 0), int32_base[i]);
    }


    testVal = -1;
    testVal = swl_typeInt32_findInMapDef(&myMap, "TestParam", 0);
    assert_int_equal(testVal, 0);

    testVal = -1;
    assert_false(swl_typeInt32_findInMap(&myMap, "TestParam", &testVal));

    amxc_var_clean(&myMap);
}

static void test_swl_typeCharPtr_default(void** state _UNUSED) {
    assert_true(swl_typeCharPtr_equals("a", "a"));
    assert_false(swl_typeCharPtr_equals("a", "b"));

    char* tmpPtr = swl_typeCharPtr_copy("test");
    assert_string_equal(tmpPtr, "test");
    free(tmpPtr);
    tmpPtr = swl_typeCharPtr_copy("foobar");
    assert_string_equal(tmpPtr, "foobar");
    free(tmpPtr);

    char* testArray[NR_CHAR_TEST_VALUES + 1] = {0};
    assert_int_equal(swl_typeCharPtr_arrayFromChar(testArray, NR_CHAR_TEST_VALUES + 1, monoStrChar), NR_CHAR_TEST_VALUES);
    assert_true(swl_typeCharPtr_arrayEquals(testArray, NR_CHAR_TEST_VALUES, char_base, NR_CHAR_TEST_VALUES));


    char buffer[128] = {0};
    size_t len = swl_typeCharPtr_arrayToChar(buffer, sizeof(buffer), char_base, NR_CHAR_TEST_VALUES);
    assert_int_equal(strlen(monoStrChar), len);
    assert_string_equal(monoStrChar, buffer);

    for(int i = 0; i < NR_CHAR_TEST_VALUES; i++) {
        assert_int_equal(swl_typeCharPtr_arrayCount(char_base, NR_CHAR_TEST_VALUES, testArray[i]), monoCount[i]);
    }
    for(int i = 0; i < NR_CHAR_TEST_VALUES; i++) {
        assert_int_equal(swl_typeCharPtr_arrayCount(char_base, NR_CHAR_TEST_VALUES, monoNotContains[i]), 0);
    }

    for(int i = 0; i < NR_CHAR_TEST_VALUES; i++) {
        assert_true(swl_typeCharPtr_arrayContains(char_base, NR_CHAR_TEST_VALUES, testArray[i]));
        assert_false(swl_typeCharPtr_arrayContains(char_base, NR_CHAR_TEST_VALUES, monoNotContains[i]));
    }

    for(int i = 0; i < NR_CHAR_TEST_VALUES; i++) {
        free(testArray[i]);
    }
}

static void test_swl_typeCharPtr_valMatches(void** state _UNUSED) {
    char* copyArr[NR_CHAR_TEST_VALUES] = {0};
    assert_int_equal(swl_typeCharPtr_arrayFromChar(copyArr, NR_CHAR_TEST_VALUES, monoStrChar), NR_CHAR_TEST_VALUES);

    assert_true(swl_typeCharPtr_arrayMatches(char_base, NR_CHAR_TEST_VALUES, copyArr, NR_CHAR_TEST_VALUES));
    assert_false(swl_typeCharPtr_arrayMatches(char_base, NR_CHAR_TEST_VALUES, copyArr, NR_CHAR_TEST_VALUES - 1));
    assert_false(swl_typeCharPtr_arrayMatches(char_base, NR_CHAR_TEST_VALUES, monoNotContains, NR_CHAR_TEST_VALUES));

    for(int i = 0; i < NR_CHAR_TEST_VALUES; i++) {
        char* tmpChar = copyArr[i];
        assert_true(swl_typeCharPtr_arrayMatches(char_base, NR_CHAR_TEST_VALUES, copyArr, NR_CHAR_TEST_VALUES));
        copyArr[i] = monoNotContains[i];
        assert_false(swl_typeCharPtr_arrayMatches(char_base, NR_CHAR_TEST_VALUES, copyArr, NR_CHAR_TEST_VALUES));
        copyArr[i] = tmpChar;
    }

    for(int i = 0; i < NR_CHAR_TEST_VALUES; i++) {
        for(int j = 0; j < NR_CHAR_TEST_VALUES; j++) {
            char* tmpData = copyArr[i];
            copyArr[i] = copyArr[j];
            copyArr[j] = tmpData;
            assert_true(swl_typeCharPtr_arrayMatches(char_base, NR_CHAR_TEST_VALUES, copyArr, NR_CHAR_TEST_VALUES));
            copyArr[j] = copyArr[i];
            copyArr[i] = tmpData;
        }
    }
    for(int i = 0; i < NR_CHAR_TEST_VALUES; i++) {
        free(copyArr[i]);
    }
}


static void test_swl_typeCharPtr_arrayIsSuperset(void** state _UNUSED) {
    char* copyArr[NR_CHAR_TEST_VALUES] = {0};
    assert_int_equal(swl_typeCharPtr_arrayFromChar(copyArr, NR_CHAR_TEST_VALUES, monoStrChar), NR_CHAR_TEST_VALUES);

    for(int i = 0; i < NR_CHAR_TEST_VALUES; i++) {
        assert_true(swl_typeCharPtr_arrayIsSuperset(char_base, NR_CHAR_TEST_VALUES,
                                                    copyArr, NR_CHAR_TEST_VALUES - i));
    }
    for(int i = 0; i < NR_CHAR_TEST_VALUES - 1; i++) {
        assert_false(swl_typeCharPtr_arrayIsSuperset(char_base, NR_CHAR_TEST_VALUES,
                                                     monoNotContains, NR_CHAR_TEST_VALUES - i));
    }

    for(int i = 0; i < NR_CHAR_TEST_VALUES; i++) {
        assert_true(swl_typeCharPtr_arrayIsSuperset(char_base, NR_CHAR_TEST_VALUES, copyArr, NR_CHAR_TEST_VALUES));
        char* tmpVal = copyArr[i];
        copyArr[i] = monoNotContains[i];
        assert_false(swl_typeCharPtr_arrayIsSuperset(char_base, NR_CHAR_TEST_VALUES, copyArr, NR_CHAR_TEST_VALUES));
        copyArr[i] = tmpVal;
    }

    for(int i = 0; i < NR_CHAR_TEST_VALUES; i++) {
        for(int j = 0; j < NR_CHAR_TEST_VALUES; j++) {
            char* tmpVal = copyArr[i];
            copyArr[i] = copyArr[j];
            copyArr[j] = tmpVal;
            for(int k = 0; k < NR_CHAR_TEST_VALUES - 1; k++) {
                assert_true(swl_typeCharPtr_arrayIsSuperset(char_base, NR_CHAR_TEST_VALUES, copyArr, NR_CHAR_TEST_VALUES - k));
            }
            copyArr[j] = copyArr[i];
            copyArr[i] = tmpVal;
        }
    }

    for(int i = 0; i < NR_CHAR_TEST_VALUES; i++) {
        free(copyArr[i]);
    }
}

#define FREE_AND_NULL(str) \
    if(str != NULL) { \
        free(str); \
        str = NULL; \
    }

static void test_swl_typeCharPtr_toFromChar(void** state _UNUSED) {
    char buf[128];
    char* testVal = NULL;
    for(int i = 0; i < NR_CHAR_TEST_VALUES; i++) {
        memset(buf, 0, sizeof(buf));
        swl_typeCharPtr_toChar(buf, sizeof(buf), char_base[i]);
        assert_string_equal(buf, testStrValuesChar[i]);
        FREE_AND_NULL(testVal);

        swl_typeCharPtr_fromChar(&testVal, testStrValuesChar[i]);
        assert_string_equal(testVal, char_base[i]);
        FREE_AND_NULL(testVal);

        testVal = swl_typeCharPtr_fromCharDef(testStrValuesChar[i], NULL);
        assert_string_equal(testVal, char_base[i]);
        FREE_AND_NULL(testVal);
    }


    testVal = swl_typeCharPtr_fromCharDef(NULL, "Invalid");
    assert_string_equal(testVal, "Invalid");
    FREE_AND_NULL(testVal);

    assert_false(swl_typeCharPtr_fromChar(&testVal, NULL));

}

static void test_swl_typeCharPtr_toFromVar(void** state _UNUSED) {
    amxc_var_t myVar;
    char* testVal = NULL;
    for(int i = 0; i < NR_CHAR_TEST_VALUES; i++) {
        amxc_var_init(&myVar);
        swl_typeCharPtr_toVariant(&myVar, char_base[i]);
        assert_int_equal(amxc_var_type_of(&myVar), AMXC_VAR_ID_CSTRING);
        assert_string_equal(amxc_var_constcast(cstring_t, &myVar), char_base[i]);

        amxc_var_clean(&myVar);
        amxc_var_init(&myVar);
        amxc_var_set_type(&myVar, AMXC_VAR_ID_CSTRING);
        amxc_var_set(cstring_t, &myVar, char_base[i]);



        FREE_AND_NULL(testVal);
        assert_true(swl_typeCharPtr_fromVariant(&testVal, &myVar));
        assert_string_equal(testVal, char_base[i]);


        FREE_AND_NULL(testVal);
        testVal = swl_typeCharPtr_fromVariantDef(&myVar, NULL);
        assert_string_equal(testVal, char_base[i]);
        amxc_var_clean(&myVar);

        FREE_AND_NULL(testVal);
    }




    FREE_AND_NULL(testVal);
    testVal = swl_typeCharPtr_fromVariantDef(NULL, "Invalid");
    assert_string_equal(testVal, "Invalid");
    FREE_AND_NULL(testVal);

    assert_false(swl_typeCharPtr_fromVariant(&testVal, NULL));

}


void test_swl_typeCharPtr_toFromObj(void** state _UNUSED) {
    for(int i = 0; i < NR_CHAR_TEST_VALUES; i++) {
        swl_typeCharPtr_toObjectParam(myObj, "TestParam", char_base[i]);
        assert_string_equal(amxc_var_constcast(cstring_t, amxd_object_get_param_value(myObj, "TestParam")), char_base[i]);
    }

    char* testVal = NULL;

    for(int i = 0; i < NR_CHAR_TEST_VALUES; i++) {

        FREE_AND_NULL(testVal);
        amxd_object_set_value(cstring_t, myObj, "TestParam", char_base[i]);
        assert_true(swl_typeCharPtr_fromObjectParam(myObj, "TestParam", &testVal));
        assert_string_equal(testVal, char_base[i]);


        FREE_AND_NULL(testVal);
        testVal = swl_typeCharPtr_fromObjectParamDef(myObj, "TestParam", 0);
        assert_string_equal(testVal, char_base[i]);
    }

    amxd_object_set_value(cstring_t, myObj, "TestParam", "");


    FREE_AND_NULL(testVal);
    testVal = swl_typeCharPtr_fromObjectParamDef(myObj, "NotAParam", "Invalid");
    assert_string_equal(testVal, "Invalid");


    FREE_AND_NULL(testVal);
    assert_false(swl_typeCharPtr_fromObjectParam(myObj, "NotAParam", &testVal));
    assert_null(testVal);
}

static void test_swl_typeCharPtr_toFromMap(void** state _UNUSED) {
    amxc_var_t myMap;
    amxc_var_init(&myMap);
    amxc_var_set_type(&myMap, AMXC_VAR_ID_HTABLE);

    char buffer[50];

    for(int i = 0; i < NR_CHAR_TEST_VALUES; i++) {

        snprintf(buffer, sizeof(buffer), "TestParam_%u", i);
        swl_typeCharPtr_addToMap(&myMap, buffer, char_base[i]);
    }
    for(int i = 0; i < NR_CHAR_TEST_VALUES; i++) {
        snprintf(buffer, sizeof(buffer), "TestParam_%u", i);
        assert_string_equal(amxc_var_constcast(cstring_t, amxc_var_get_key(&myMap, buffer, AMXC_VAR_FLAG_DEFAULT)), char_base[i]);
    }

    char* testVal = NULL;

    for(int i = 0; i < NR_CHAR_TEST_VALUES; i++) {

        snprintf(buffer, sizeof(buffer), "TestParam_%u", i);
        FREE_AND_NULL(testVal);
        assert_true(swl_typeCharPtr_findInMap(&myMap, buffer, &testVal));
        assert_string_equal(testVal, char_base[i]);


        FREE_AND_NULL(testVal);
        testVal = swl_typeCharPtr_findInMapDef(&myMap, buffer, NULL);
        assert_string_equal(testVal, char_base[i]);
    }


    FREE_AND_NULL(testVal);
    testVal = swl_typeCharPtr_findInMapDef(&myMap, "NotAParam", "Invalid");
    assert_string_equal(testVal, "Invalid");


    FREE_AND_NULL(testVal);
    assert_false(swl_typeCharPtr_findInMap(&myMap, "NotAParam", &testVal));
    assert_null(testVal);

    amxc_var_clean(&myMap);
}

typedef struct {
    uint16_t val1;
    uint32_t val2;
    char* val3;
    uint8_t val4[2];
} test_swl_type_extract;

#define NR_TEST_VALUES 4

test_swl_type_extract extractTest[NR_TEST_VALUES] = {
    {1, 2, "3", {8, 9}},
    {2, 3, "5", {18, 19}},
    {100, 200, "abc", {28, 29}},
    {222, 11, "a1b2c3", {38, 39}}
};

uint16_t extract16_1[NR_TEST_VALUES] = {1, 2, 100, 222};
uint16_t extract16_2[NR_TEST_VALUES] = {100, 222, 1, 2};

uint32_t extract32_1[NR_TEST_VALUES] = {2, 3, 200, 11};
uint32_t extract32_2[NR_TEST_VALUES] = {200, 11, 2, 3};

char* extractChar_1[NR_TEST_VALUES] = {"3", "5", "abc", "a1b2c3"};
char* extractChar_2[NR_TEST_VALUES] = {"abc", "a1b2c3", "3", "5"};

uint8_t extract8_1[2][NR_TEST_VALUES] = {{8, 18, 28, 38}, {9, 19, 29, 39}};
uint8_t extract8_2[2][NR_TEST_VALUES] = {{28, 38, 8, 18}, {29, 39, 9, 19}};

static void test_swl_type_extractValueArrayFromStructArray(void** state _UNUSED) {
    char buffer[NR_TEST_VALUES * 16];

    //uint16
    swl_type_extractValueArrayFromStructArray(swl_type_uint16, buffer, extractTest,
                                              offsetof(test_swl_type_extract, val1), sizeof(test_swl_type_extract), 0, NR_TEST_VALUES);
    assert_true(swl_type_arrayEquals(swl_type_uint16, buffer, NR_TEST_VALUES, extract16_1, NR_TEST_VALUES));

    swl_type_extractValueArrayFromStructArray(swl_type_uint16, buffer, extractTest,
                                              offsetof(test_swl_type_extract, val1), sizeof(test_swl_type_extract), 2, NR_TEST_VALUES);
    assert_true(swl_type_arrayEquals(swl_type_uint16, buffer, NR_TEST_VALUES, extract16_2, NR_TEST_VALUES));

    //uint32
    swl_type_extractValueArrayFromStructArray(swl_type_uint32, buffer, extractTest,
                                              offsetof(test_swl_type_extract, val2), sizeof(test_swl_type_extract), 0, NR_TEST_VALUES);
    assert_true(swl_type_arrayEquals(swl_type_uint32, buffer, NR_TEST_VALUES, extract32_1, NR_TEST_VALUES));

    swl_type_extractValueArrayFromStructArray(swl_type_uint32, buffer, extractTest,
                                              offsetof(test_swl_type_extract, val2), sizeof(test_swl_type_extract), 2, NR_TEST_VALUES);
    assert_true(swl_type_arrayEquals(swl_type_uint32, buffer, NR_TEST_VALUES, extract32_2, NR_TEST_VALUES));

    //char
    swl_type_extractValueArrayFromStructArray(swl_type_charPtr, buffer, extractTest,
                                              offsetof(test_swl_type_extract, val3), sizeof(test_swl_type_extract), 0, NR_TEST_VALUES);
    assert_true(swl_type_arrayEquals(swl_type_charPtr, buffer, NR_TEST_VALUES, extractChar_1, NR_TEST_VALUES));

    swl_type_extractValueArrayFromStructArray(swl_type_charPtr, buffer, extractTest,
                                              offsetof(test_swl_type_extract, val3), sizeof(test_swl_type_extract), 2, NR_TEST_VALUES);
    assert_true(swl_type_arrayEquals(swl_type_charPtr, buffer, NR_TEST_VALUES, extractChar_2, NR_TEST_VALUES));

    //uint8_t
    for(int i = 0; i < 2; i++) {
        swl_type_extractValueArrayFromStructArray(swl_type_uint8, buffer, extractTest,
                                                  offsetof(test_swl_type_extract, val4[i]), sizeof(test_swl_type_extract), 0, NR_TEST_VALUES);
        assert_true(swl_type_arrayEquals(swl_type_uint8, buffer, NR_TEST_VALUES, extract8_1[i], NR_TEST_VALUES));

        swl_type_extractValueArrayFromStructArray(swl_type_uint8, buffer, extractTest,
                                                  offsetof(test_swl_type_extract, val4[i]), sizeof(test_swl_type_extract), 2, NR_TEST_VALUES);
        assert_true(swl_type_arrayEquals(swl_type_uint8, buffer, NR_TEST_VALUES, extract8_2[i], NR_TEST_VALUES));
    }
}

typedef enum {
    SWL_TYPE_TEST_0,
    SWL_TYPE_TEST_1,
    SWL_TYPE_TEST_2,
    SWL_TYPE_TEST_3,
    SWL_TYPE_TEST_4,
    SWL_TYPE_TEST_MAX,
} swl_typeTest_e;

const char* swl_typeTest_str[SWL_TYPE_TEST_MAX] = {"Test0", "Test1", "Test2", "Test3", "Test4"};

SWL_TYPE_ENUM(swl_type_enumTest_impl, swl_typeTest_e, SWL_TYPE_TEST_0, SWL_TYPE_TEST_MAX, swl_typeTest_str);
swl_type_t* swl_type_enumTest = (swl_type_t*) &swl_type_enumTest_impl;

static void test_swl_type_enum(void** state _UNUSED) {
    swl_typeTest_e testVal;
    bool result;
    for(uint32_t i = 0; i < SWL_TYPE_TEST_MAX; i++) {
        result = swl_type_fromChar(swl_type_enumTest, &testVal, swl_typeTest_str[i]);
        assert_true(result);
        assert_int_equal(testVal, i);
    }

    testVal = SWL_TYPE_TEST_MAX;
    result = swl_type_fromChar(swl_type_enumTest, &testVal, NULL);
    assert_false(result);
    assert_int_equal(testVal, SWL_TYPE_TEST_0);

    testVal = SWL_TYPE_TEST_MAX;
    result = swl_type_fromChar(swl_type_enumTest, &testVal, "Test5");
    assert_false(result);
    assert_int_equal(testVal, SWL_TYPE_TEST_0);

}

int g_testVar;
const int g_constVar;
static int s_testVar;
static void s_testFunc(void) {
}
static void test_swl_type_voidPtr(void** state _UNUSED) {
    int localVar;
    char* strOnStack = "dummy";
    char* strOnHeap = strdup("dummy");
    void* sampleVoidPtr[] = {
        &g_testVar,
        (void*) &g_constVar,
        &s_testVar,
        s_testFunc,
        &localVar,
        NULL,
        &errno,
        strOnStack,
        strOnHeap,
    };
    free(strOnHeap);
    int ret;
    char buffer[128];
    void* result;
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(sampleVoidPtr); i++) {
        ret = swl_typeVoidPtr_toChar(buffer, sizeof(buffer), sampleVoidPtr[i]);
        assert_true(ret > 0);
        ret = swl_typeVoidPtr_fromChar(&result, buffer);
        // de-serialization not allowed
        assert_false(ret);
        result = swl_typeVoidPtr_copy(sampleVoidPtr[i]);
        assert_ptr_equal(result, sampleVoidPtr[i]);
        ret = swl_typeVoidPtr_equals(result, sampleVoidPtr[i]);
        assert_true(ret);
    }
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlConv");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_typeInt32_default),
        cmocka_unit_test(test_swl_typeInt32_valMatches),
        cmocka_unit_test(test_swl_typeInt32_arrayIsSuperset),
        cmocka_unit_test(test_swl_typeInt32_toFromChar),
        cmocka_unit_test(test_swl_typeInt32_toFromVar),
        cmocka_unit_test(test_swl_typeInt32_toFromObj),
        cmocka_unit_test(test_swl_typeInt32_toFromMap),
        cmocka_unit_test(test_swl_typeCharPtr_default),
        cmocka_unit_test(test_swl_typeCharPtr_valMatches),
        cmocka_unit_test(test_swl_typeCharPtr_arrayIsSuperset),
        cmocka_unit_test(test_swl_typeCharPtr_toFromChar),
        cmocka_unit_test(test_swl_typeCharPtr_toFromVar),
        cmocka_unit_test(test_swl_typeCharPtr_toFromObj),
        cmocka_unit_test(test_swl_typeCharPtr_toFromMap),
        cmocka_unit_test(test_swl_type_extractValueArrayFromStructArray),
        cmocka_unit_test(test_swl_type_enum),
        cmocka_unit_test(test_swl_type_voidPtr),
    };
    int rc = 0;

    rc = cmocka_run_group_tests(tests, s_setupSuite, s_teardownSuite);

    sahTraceClose();
    return rc;
}
