/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "test_swla_type_testCommon.h"

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>

#include "swla/swla_commonLib.h"
#include "swla/swla_type.h"
#include "swl/swl_math.h"
#include "swla/swla_time_spec.h"

#include <amxd/amxd_dm.h>

testData_t* curData = NULL;
swl_type_t* curType = NULL;

static void* s_valToPtr(void* ptr) {
    if(curType->typeFun->isValue) {
        return ptr;
    } else {
        return *(void**) ptr;
    }
}

static void s_freePtr(void* ptr) {
    if(!curType->typeFun->isValue) {
        free(s_valToPtr(ptr));
    }
}

static void* s_arrVal(void* array, uint32_t index) {
    return s_valToPtr(array + curType->size * index);
}

static amxd_dm_t myDm;
static amxd_object_t* myObj;
static int s_setupSuite(void** state) {
    (void) state;
    // Need to duplicate the values for base in equals, otherwise tests may not be valid.
    // Note that we cannot depend on just writing multiple lines of the same string, as compiler may optimize.
    curData->equalsValues = (void**) calloc(curType->size, curData->nrTestValues);
    if(curType->typeFun->isValue) {
        memcpy(curData->equalsValues, curData->baseValues, curType->size * curData->nrTestValues);
    } else {
        for(uint32_t i = 0; i < curData->nrTestValues; i++) {
            *((void**) curData->equalsValues + i) = swl_type_copy(curType, s_arrVal(curData->baseValues, i));
        }
    }

    amxd_param_t* param = NULL;

    assert_int_equal(amxd_dm_init(&myDm), 0);

    assert_int_equal(amxd_object_new(&myObj, amxd_object_singleton, "parent"), 0);
    assert_int_equal(amxd_dm_add_root_object(&myDm, myObj), 0);

    assert_non_null(myObj);


    assert_int_equal(amxd_param_new(&param, "TestParam", curData->targetType), 0);
    assert_int_equal(amxd_object_add_param(myObj, param), amxd_status_ok);

    return 0;
}

static int s_teardownSuite(void** state) {
    (void) state;
    if(!curType->typeFun->isValue) {
        for(uint32_t i = 0; i < curData->nrTestValues; i++) {
            free(*((void**) curData->equalsValues + i));
        }
    }
    free(curData->equalsValues);

    amxd_dm_clean(&myDm);
    return 0;
}


static void test_valueSetup(void** state _UNUSED) {
    // Test whether the values in base are actually different from the values in equals.
    if(curType->typeFun->isValue) {
        assert_ptr_not_equal(curData->baseValues, curData->equalsValues);
    } else {
        for(uint32_t i = 0; i < curData->nrTestValues; i++) {
            assert_ptr_not_equal(s_arrVal(curData->baseValues, i), s_arrVal(curData->equalsValues, i));
        }
    }

}

static void test_swl_type_equals(void** state _UNUSED) {
    size_t i = 0;
    for(i = 0; i < curData->nrTestValues; i++) {
        assert_true(swl_type_equals(curType, s_arrVal(curData->baseValues, i), s_arrVal(curData->equalsValues, i)));
        assert_false(swl_type_equals(curType, s_arrVal(curData->baseValues, i), s_arrVal(curData->notContains, i)));
        assert_false(swl_type_equals(curType, s_arrVal(curData->equalsValues, i), s_arrVal(curData->notContains, i)));
    }
}

static void test_swl_type_copy(void** state _UNUSED) {
    size_t i = 0;
    void* curBase = curData->baseValues;
    for(i = 0; i < curData->nrTestValues; i++) {
        void* duplicate = swl_type_copy(curType, curBase);
        assert_true(swl_type_equals(curType, curBase, duplicate));
        assert_true(duplicate != curBase);
        free(duplicate);
    }
}

static void test_swl_type_copyCleanup(void** state _UNUSED) {
    size_t i = 0;
    void* firstDataEl = swl_type_toPtr(curType, curData->baseValues);
    for(i = 0; i < curData->nrTestValues; i++) {
        char data[curType->size];
        void* dataPtr = &data;
        memset(data, 0, curType->size);

        swl_type_copyTo(curType, dataPtr, firstDataEl);

        assert_true(swl_type_equals(curType, firstDataEl, swl_type_toPtr(curType, dataPtr)));

        swl_type_cleanup(curType, data);

        char zeroData[curType->size];
        memset(zeroData, 0, curType->size);
        assert_memory_equal(data, zeroData, curType->size);
    }
}


static void test_swl_type_tofromChar(void** state _UNUSED) {
    size_t i = 0;
    void* curPtr = curData->baseValues;
    for(i = 0; i < curData->nrTestValues; i++) {
        char buffer[128] = {0};
        memset(buffer, 0, sizeof(buffer));
        swl_type_toChar(curType, buffer, sizeof(buffer), s_valToPtr(curPtr));
        assert_string_equal(buffer, curData->strValues[i]);

        char bufDataBuffer[curType->size];
        memset(bufDataBuffer, 0, curType->size);
        void* bufData = &bufDataBuffer;

        swl_type_fromChar(curType, bufData, curData->strValues[i]);
        if(!curData->noDeSerial) {
            assert_true(swl_type_equals(curType, s_valToPtr(bufData), s_valToPtr(curPtr)));
        }

        s_freePtr(bufData);

        curPtr += curType->size;
    }
}

static void s_runToFromVarTest(size_t i, void* curPtr, bool checkVarType) {
    amxc_var_t testVariant = {0};
    swl_type_toVariant(curType, &testVariant, s_valToPtr(curPtr));

    if(checkVarType) {
        assert_int_equal(testVariant.type_id, curData->targetType);
    }

    if(amxc_var_type_of(&testVariant) == AMXC_VAR_ID_CSTRING) {
        //Compare variant output with to string values, as some types may only convert to string.
        assert_string_equal(amxc_var_constcast(cstring_t, &testVariant), curData->strValues[i]);
    } else if(checkVarType) {
        if(curData->varToData == NULL) {
            assert_true(swl_type_equals(curType, &testVariant.data, curPtr));
        } else {
            void* data = curData->varToData(&testVariant);
            printf("%s - %s\n", swl_type_toBuf32(curType, data).buf, swl_type_toBuf32(curType, curPtr).buf);

            assert_true(swl_type_equals(curType, data, curPtr));
            free(data);
        }


    } else {
        assert_true(false);
    }

    char bufDataBuffer[curType->size];
    memset(bufDataBuffer, 0, curType->size);
    void* bufData = &bufDataBuffer;
    swl_type_fromVariant(curType, bufData, &testVariant);
    if(!curData->noDeSerial) {
        assert_true(swl_type_equals(curType, s_valToPtr(bufData), s_valToPtr(curPtr)));
    }
    s_freePtr(bufData);
    amxc_var_clean(&testVariant);
}

static void test_swl_type_tofromVariant(void** state _UNUSED) {
    uint32_t i = 0;
    void* curPtr = curData->baseValues;
    for(i = 0; i < curData->nrTestValues; i++) {
        s_runToFromVarTest(i, curPtr, true);

        if((curType->typeFun->toChar != NULL) && (curType->typeFun->toVariant != NULL) && (curType->typeFun->fromChar != NULL) && (curType->typeFun->fromVariant != NULL)) {
            swl_type_toVariant_cb tmpToVar = curType->typeFun->toVariant;
            swl_type_fromVariant_cb tmpFromVar = curType->typeFun->fromVariant;
            curType->typeFun->toVariant = NULL;
            curType->typeFun->fromVariant = NULL;

            s_runToFromVarTest(i, curPtr, false);

            curType->typeFun->fromVariant = tmpFromVar;
            curType->typeFun->toVariant = tmpToVar;
        }
        curPtr += curType->size;
    }
}

static void test_swl_type_toFromObject(void** state _UNUSED) {
    for(uint32_t i = 0; i < curData->nrTestValues; i++) {
        assert_true(swl_type_toObjectParam(curType, myObj, "TestParam", s_arrVal(curData->baseValues, i)));
        const char* cmpStr = curData->variantVals != NULL ? curData->variantVals[i] : curData->strValues[i];
        const amxc_var_t* amxVar = amxd_object_get_param_value(myObj, "TestParam");
        char* data = amxc_var_get_cstring_t(amxVar);
        assert_string_equal(data, cmpStr);
        free(data);
    }


    for(uint32_t i = 0; i < curData->nrTestValues; i++) {
        char dataBuf[curType->size];
        memset(dataBuf, 0, curType->size);
        void* data = &dataBuf;
        amxd_object_set_value(cstring_t, myObj, "TestParam", curData->strValues[i]);
        swl_type_fromObjectParam(curType, myObj, "TestParam", data);
        if(!curData->noDeSerial) {
            assert_true(swl_type_equals(curType, s_valToPtr(data), s_arrVal(curData->baseValues, i)));
        }
        s_freePtr(data);
    }
}

static void test_swl_type_arrayToChar(void** state _UNUSED) {
    char buffer[128];
    swl_type_arrayToChar(curType, buffer, sizeof(buffer), curData->baseValues, curData->nrTestValues);
    assert_string_equal(buffer, curData->serialStr);

    swl_type_arrayToCharSep(curType, buffer, sizeof(buffer), curData->baseValues, curData->nrTestValues, ";;");
    assert_string_equal(buffer, curData->serialStr2);
}

static void test_swl_type_arrayFromChar(void** state _UNUSED) {
    char buffer[curType->size * curData->nrTestValues];

    size_t nrValues = swl_type_arrayFromChar(curType, buffer, curData->nrTestValues, curData->serialStr);
    if(!curData->noDeSerial) {
        assert_int_equal(nrValues, curData->nrTestValues);
    }

    void* tgtData = buffer;
    void* srcData = curData->baseValues;

    for(uint32_t i = 0; i < nrValues; i++) {
        assert_true(swl_type_equals(curType, s_valToPtr(tgtData), s_valToPtr(srcData)));
        s_freePtr(tgtData);
        tgtData += curType->size;
        srcData += curType->size;
    }
}

static void test_swl_type_arrayFromCharSep(void** state _UNUSED) {
    char buffer[curType->size * curData->nrTestValues];

    size_t nrValues = swl_type_arrayFromCharSep(curType, buffer, curData->nrTestValues, curData->serialStr2, ";;");
    if(!curData->noDeSerial) {
        assert_int_equal(nrValues, curData->nrTestValues);
    }

    void* tgtData = buffer;
    void* srcData = curData->baseValues;

    for(uint32_t i = 0; i < nrValues; i++) {
        assert_true(swl_type_equals(curType, s_valToPtr(tgtData), s_valToPtr(srcData)));
        s_freePtr(tgtData);
        tgtData += curType->size;
        srcData += curType->size;
    }
}

static void test_swl_type_arrayEquals(void** state _UNUSED) {
    assert_true(swl_type_arrayEquals(curType, curData->baseValues, curData->nrTestValues, curData->equalsValues, curData->nrTestValues));
    assert_false(swl_type_arrayEquals(curType, curData->baseValues, curData->nrTestValues, curData->equalsValues, curData->nrTestValues - 1));
    assert_false(swl_type_arrayEquals(curType, curData->baseValues, curData->nrTestValues, curData->notContains, curData->nrTestValues));

    size_t arraySize = curType->size * curData->nrTestValues;
    unsigned char data[arraySize];
    for(uint32_t i = 0; i < curData->nrTestValues; i++) {
        memcpy(data, curData->equalsValues, arraySize);
        assert_true(swl_type_arrayEquals(curType, curData->baseValues, curData->nrTestValues, data, curData->nrTestValues));
        size_t offset = i * curType->size;
        memcpy(data + offset, curData->notContains, curType->size);
        assert_false(swl_type_arrayEquals(curType, curData->baseValues, curData->nrTestValues, data, curData->nrTestValues));
    }
}

static void test_swl_type_arrayCount(void** state _UNUSED) {

    for(uint32_t i = 0; i < curData->nrTestValues; i++) {
        assert_int_equal(swl_type_arrayCount(curType, curData->baseValues, curData->nrTestValues, s_arrVal(curData->equalsValues, i)),
                         curData->valueCount[i]);
    }
    for(uint32_t i = 0; i < curData->nrTestValues; i++) {
        assert_int_equal(swl_type_arrayCount(curType, curData->baseValues, curData->nrTestValues, s_arrVal(curData->notContains, i)),
                         0);
    }
}

static bool s_permutate(void* data, size_t index1, size_t index2) {
    size_t offset1 = curType->size * index1;
    size_t offset2 = curType->size * index2;
    if(swl_type_equals(curType, data + offset1, data + offset2)) {
        return true;
    }
    unsigned char buffer[curType->size];
    memcpy(buffer, data + offset1, curType->size);
    memcpy(data + offset1, data + offset2, curType->size);
    memcpy(data + offset2, buffer, curType->size);

    return false;
}

static void test_swl_type_arrayMatches(void** state _UNUSED) {
    assert_true(swl_type_arrayMatches(curType, curData->baseValues, curData->nrTestValues, curData->equalsValues, curData->nrTestValues));
    assert_false(swl_type_arrayMatches(curType, curData->baseValues, curData->nrTestValues, curData->equalsValues, curData->nrTestValues - 1));
    assert_false(swl_type_arrayMatches(curType, curData->baseValues, curData->nrTestValues, curData->notContains, curData->nrTestValues));

    size_t arraySize = curType->size * curData->nrTestValues;
    unsigned char data[arraySize];
    for(uint32_t i = 0; i < curData->nrTestValues; i++) {
        memcpy(data, curData->equalsValues, arraySize);
        assert_true(swl_type_arrayMatches(curType, curData->baseValues, curData->nrTestValues, data, curData->nrTestValues));
        size_t offset = i * curType->size;
        memcpy(data + offset, curData->notContains, curType->size);
        assert_false(swl_type_arrayMatches(curType, curData->baseValues, curData->nrTestValues, data, curData->nrTestValues));
    }

    for(uint32_t i = 0; i < curData->nrTestValues; i++) {
        for(uint32_t j = 0; j < curData->nrTestValues; j++) {
            memcpy(data, curData->equalsValues, arraySize);
            s_permutate(data, i, j);
            assert_true(swl_type_arrayMatches(curType, curData->baseValues, curData->nrTestValues, data, curData->nrTestValues));
        }
    }
}

static void test_swl_type_arrayFindOffset(void** state _UNUSED) {
    for(ssize_t i = 0; i < curData->nrTestValues; i++) {
        for(ssize_t j = -1 * (ssize_t) curData->nrTestValues; j < (ssize_t) curData->nrTestValues; j++) {
            ssize_t index = swl_type_arrayFindOffset(curType, curData->baseValues, curData->nrTestValues,
                                                     s_arrVal(curData->equalsValues, i), j);

            if((j < -1 * (ssize_t) curData->nrTestValues) || (j >= curData->nrTestValues)) {
                assert_true(index == (ssize_t) -1);
            } else if(j < 0) {
                ssize_t offset = j + curData->nrTestValues;

                if(offset >= i) {
                    assert_true(index >= 0);
                }
                if(index < 0) {
                    assert_true(offset < i);
                } else if(index == i) {
                    assert_true(offset >= i);
                } else {
                    assert_true(curData->valueCount[i] > 1);
                    assert_true(curData->valueCount[index] > 1);
                    assert_true(swl_type_equals(curType,
                                                swl_type_arrayGetValue(curType, curData->baseValues, curData->nrTestValues, index),
                                                swl_type_arrayGetValue(curType, curData->baseValues, curData->nrTestValues, i)));
                }
            } else { // j >= 0
                if(j <= i) {
                    assert_true(index >= 0);
                }
                if(index < 0) {
                    assert_true(j > i);
                } else if(index == i) {
                    assert_true(j <= i);
                } else {
                    assert_true(curData->valueCount[i] > 1);
                    assert_true(curData->valueCount[index] > 1);
                    assert_true(swl_type_equals(curType,
                                                swl_type_arrayGetValue(curType, curData->baseValues, curData->nrTestValues, index),
                                                swl_type_arrayGetValue(curType, curData->baseValues, curData->nrTestValues, i)));
                }
            }
        }
    }

    for(ssize_t i = 0; i < curData->nrTestValues; i++) {
        for(ssize_t j = -2 * curData->nrTestValues; j < 2 * curData->nrTestValues; j++) {
            ssize_t index = swl_type_arrayFindOffset(curType, curData->baseValues, curData->nrTestValues,
                                                     s_arrVal(curData->notContains, i), j);
            assert_true(index == -1);
        }


    }
}

static void test_swl_type_arrayFind(void** state _UNUSED) {
    for(ssize_t i = 0; i < curData->nrTestValues; i++) {
        ssize_t index = swl_type_arrayFind(curType, curData->baseValues, curData->nrTestValues,
                                           s_arrVal(curData->equalsValues, i));
        assert_true(index >= 0);
        if(index != i) {
            assert_true(curData->valueCount[i] > 1);
            assert_true(curData->valueCount[index] > 1);
            assert_true(swl_type_equals(curType,
                                        swl_type_arrayGetValue(curType, curData->baseValues, curData->nrTestValues, index),
                                        swl_type_arrayGetValue(curType, curData->baseValues, curData->nrTestValues, i)));
        }

    }

    for(ssize_t i = 0; i < curData->nrTestValues; i++) {
        ssize_t index = swl_type_arrayFind(curType, curData->baseValues, curData->nrTestValues,
                                           s_arrVal(curData->notContains, i));
        assert_true(index == -1);
    }
}

static void test_swl_type_arrayContains(void** state _UNUSED) {
    for(uint32_t i = 0; i < curData->nrTestValues; i++) {
        assert_true(swl_type_arrayContains(curType, curData->baseValues, curData->nrTestValues,
                                           s_arrVal(curData->equalsValues, i)));
        assert_false(swl_type_arrayContains(curType, curData->baseValues, curData->nrTestValues,
                                            s_arrVal(curData->notContains, i)));
    }
}

static void test_swl_type_arrayIsSuperset(void** state _UNUSED) {
    for(uint32_t i = 0; i < curData->nrTestValues; i++) {
        assert_true(swl_type_arrayIsSuperset(curType, curData->baseValues, curData->nrTestValues,
                                             curData->equalsValues, curData->nrTestValues - i));
    }
    for(uint32_t i = 0; i < curData->nrTestValues - 1; i++) {
        assert_false(swl_type_arrayIsSuperset(curType, curData->baseValues, curData->nrTestValues,
                                              curData->notContains, curData->nrTestValues - i));
    }

    size_t arraySize = curType->size * curData->nrTestValues;
    unsigned char data[arraySize];
    for(uint32_t i = 0; i < curData->nrTestValues; i++) {
        memcpy(data, curData->equalsValues, arraySize);
        assert_true(swl_type_arrayIsSuperset(curType, curData->baseValues, curData->nrTestValues, data, curData->nrTestValues));
        size_t offset = i * curType->size;
        memcpy(data + offset, curData->notContains, curType->size);
        assert_false(swl_type_arrayIsSuperset(curType, curData->baseValues, curData->nrTestValues, data, curData->nrTestValues));
    }

    for(uint32_t i = 0; i < curData->nrTestValues; i++) {
        for(uint32_t j = 0; j < curData->nrTestValues; j++) {
            memcpy(data, curData->equalsValues, arraySize);
            s_permutate(data, i, j);
            for(uint32_t k = 0; k < curData->nrTestValues - 1; k++) {
                assert_true(swl_type_arrayIsSuperset(curType, curData->baseValues, curData->nrTestValues, data, curData->nrTestValues - k));
            }
        }
    }
}



void test_swl_type_arrayToObj(void** state _UNUSED) {
    swl_type_arrayObjectParamSetChar(myObj, "TestParam", curType, curData->baseValues, curData->nrTestValues);
    assert_string_equal(amxc_var_constcast(cstring_t, amxd_object_get_param_value(myObj, "TestParam")), curData->serialStr);
}

void test_swl_type_arrayFromObj(void** state _UNUSED) {
    char buffer[curType->size * curData->nrTestValues];

    amxd_object_set_value(cstring_t, myObj, "TestParam", curData->serialStr);


    size_t nrValues = swl_type_arrayObjectParamChar(myObj, "TestParam", curType, buffer, curData->nrTestValues);
    if(!curData->noDeSerial) {
        assert_int_equal(nrValues, curData->nrTestValues);
    }

    void* tgtData = buffer;
    void* srcData = curData->baseValues;

    for(uint32_t i = 0; i < nrValues; i++) {
        assert_true(swl_type_equals(curType, s_valToPtr(tgtData), s_valToPtr(srcData)));
        s_freePtr(tgtData);
        tgtData += curType->size;
        srcData += curType->size;
    }
}

void test_swl_type_arrayToMap(void** state _UNUSED) {
    amxc_var_t myMap;
    amxc_var_init(&myMap);
    amxc_var_set_type(&myMap, AMXC_VAR_ID_HTABLE);
    swl_type_arrayMapAddChar(&myMap, "TestParam", curType, curData->baseValues, curData->nrTestValues);
    assert_string_equal(amxc_var_constcast(cstring_t, amxc_var_get_key(&myMap, "TestParam", AMXC_VAR_FLAG_DEFAULT)), curData->serialStr);
    amxc_var_clean(&myMap);
}

void test_swl_type_arrayFromMap(void** state _UNUSED) {
    char buffer[curType->size * curData->nrTestValues];
    amxc_var_t myMap;
    amxc_var_init(&myMap);
    amxc_var_set_type(&myMap, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &myMap, "TestParam", curData->serialStr);

    size_t nrValues = swl_type_arrayMapFindChar(&myMap, "TestParam", curType, buffer, curData->nrTestValues);
    if(!curData->noDeSerial) {
        assert_int_equal(nrValues, curData->nrTestValues);
    }

    void* tgtData = buffer;
    void* srcData = curData->baseValues;

    for(uint32_t i = 0; i < nrValues; i++) {
        assert_true(swl_type_equals(curType, s_valToPtr(tgtData), s_valToPtr(srcData)));
        s_freePtr(tgtData);
        tgtData += curType->size;
        srcData += curType->size;
    }
    amxc_var_clean(&myMap);
}


void test_swl_type_arrayAddSetShift(void** state _UNUSED) {
    size_t bufSize = curType->size * curData->nrTestValues;
    size_t arraySize = curData->nrTestValues;

    char testBuffer[bufSize]; // buffer on which functions are run
    char tgtBuffer[bufSize];  // own constructed buffer
    swl_typeEl_t* testArray = &testBuffer;
    swl_typeEl_t* tgtArray = &tgtBuffer;

    char strTest[256];
    char strTgt[256];

    memset(testBuffer, 0, bufSize);
    memset(tgtBuffer, 0, bufSize);

    for(size_t i = 0; i < curData->nrTestValues; i++) {
        swl_type_arraySet(curType, testArray, arraySize, swl_type_toPtr(curType, curData->baseValues + i * curType->size), i);
        memcpy(tgtArray + i * curType->size, curData->baseValues + i * curType->size, curType->size);

        assert_true(swl_type_arrayEquals(curType, testBuffer, arraySize, tgtBuffer, arraySize));
    }
    assert_true(swl_type_arrayEquals(curType, testBuffer, arraySize, curData->baseValues, arraySize));


    for(size_t i = 0; i < curData->nrTestValues; i++) {
        swl_type_arrayPrepend(curType, testBuffer, arraySize, swl_type_toPtr(curType, curData->notContains), 0);
        memmove(tgtBuffer + curType->size, tgtBuffer, (curData->nrTestValues - 1) * curType->size);
        memcpy(tgtBuffer, curData->notContains, curType->size);
        assert_true(swl_type_arrayEquals(curType, testBuffer, arraySize, tgtBuffer, arraySize));
    }

    for(size_t i = 0; i < curData->nrTestValues; i++) {
        assert_true(swl_type_arrayEquals(curType, testBuffer, arraySize, tgtBuffer, arraySize));

        swl_type_arrayAppend(curType, testBuffer, arraySize, swl_type_toPtr(curType, curData->baseValues), -1);
        memcpy(tgtBuffer + (curData->nrTestValues - 1 - i) * curType->size, curData->baseValues, curType->size);
        assert_true(swl_type_arrayEquals(curType, testBuffer, arraySize, tgtBuffer, arraySize));
    }

    ssize_t testTarget = curData->nrTestValues;
    ssize_t testStartTarget = testTarget * -1;
    // shift testing
    for(ssize_t i = testStartTarget; i <= testTarget; i++) {

        for(size_t i = 0; i < curData->nrTestValues; i++) {
            swl_type_arraySet(curType, testArray, arraySize, swl_type_toPtr(curType, curData->baseValues + i * curType->size), i);
        }


        swl_type_arrayShift(curType, testBuffer, arraySize, i);

        size_t firstNrEl = swl_math_fmod(i, arraySize);
        size_t firstSize = firstNrEl * curType->size;
        size_t secondNrEl = arraySize - firstNrEl;
        size_t secondSize = secondNrEl * curType->size;

        memcpy(tgtBuffer, curData->baseValues + firstSize, secondSize);
        memcpy(tgtBuffer + secondSize, curData->baseValues, firstSize);

        assert_true(swl_type_arrayEquals(curType, testBuffer, arraySize, tgtBuffer, arraySize));

        if(firstNrEl == 0) {
            assert_true(swl_type_arrayEquals(curType, testBuffer, arraySize, curData->baseValues, arraySize));
        }
    }

    swl_type_arrayToChar(curType, strTest, sizeof(strTest), testArray, arraySize);
    swl_type_arrayToChar(curType, strTgt, sizeof(strTgt), tgtArray, arraySize);

    swl_type_arrayCleanup(curType, testArray, arraySize);
    memset(tgtBuffer, 0, bufSize);
    assert_memory_equal(testBuffer, tgtBuffer, bufSize);

    assert_string_equal(strTest, strTgt);
}


static void test_swl_type_arrayGet(void** state _UNUSED) {
    for(size_t i = 0; i < curData->nrTestValues; i++) {
        swl_typeEl_t* elData = curData->baseValues + (i * curType->size);
        for(ssize_t j = -1; j < 1; j++) {
            swl_typeData_t* data = swl_type_arrayGetValue(curType, curData->baseValues, curData->nrTestValues, i + j * curData->nrTestValues);
            assert_memory_equal(swl_type_toPtr(curType, elData), data, curType->size);

            swl_typeEl_t* refData = swl_type_arrayGetReference(curType, curData->baseValues, curData->nrTestValues, i + j * curData->nrTestValues);
            assert_ptr_equal(refData, elData);

        }
    }
}

static void test_swl_type_arrayGetCopy(void** state _UNUSED) {
    char buffer[curType->size];
    memset(buffer, 0, curType->size);
    for(size_t i = 0; i < curData->nrTestValues; i++) {

        swl_typeEl_t* elData = swl_type_toPtr(curType, curData->baseValues + (i * curType->size));

        for(ssize_t j = -1; j < 1; j++) {
            memset(buffer, 0, curType->size);

            swl_type_arrayGet(curType, buffer, curData->baseValues, curData->nrTestValues, i + j * curData->nrTestValues);

            if(curType->typeFun->isValue) {
                assert_memory_equal(elData, buffer, curType->size);
            } else {
                assert_memory_not_equal(elData, buffer, curType->size);
                assert_true(swl_type_equals(curType, swl_type_toPtr(curType, buffer), elData));
            }

            swl_type_cleanup(curType, buffer);
        }
    }

}

static void test_swl_type_extractValueArrayFromStructArray(void** state _UNUSED) {
    uint32_t testNrEntries = 4;
    uint32_t entrySize = curType->size * testNrEntries;
    uint32_t bufSize = entrySize * curData->nrTestValues;

    char buffer[bufSize];

    for(uint32_t i = 0; i < testNrEntries; i++) {
        for(uint32_t j = 0; j < curData->nrTestValues; j++) {
            memset(buffer, 0, bufSize);
            for(uint32_t k = 0; k < curData->nrTestValues; k++) {
                uint32_t index = (j + k) % curData->nrTestValues;
                void* tgtLocation = buffer + index * entrySize + i * curType->size;
                void* srcLocation = curData->baseValues + k * curType->size;
                memcpy(tgtLocation, srcLocation, curType->size);
            }
            char targetBuffer[curType->size * curData->nrTestValues];

            swl_type_extractValueArrayFromStructArray(curType, targetBuffer, buffer, i * curType->size,
                                                      testNrEntries * curType->size, j, curData->nrTestValues);
            assert_true(swl_type_arrayEquals(curType, targetBuffer, curData->nrTestValues, curData->baseValues, curData->nrTestValues));
        }
    }
}

static void test_swl_type_badVals(void** state _UNUSED) {
    char buffer[curType->size ];

    for(uint32_t i = 0; i < curData->nrBadVals; i++) {
        memset(buffer, 0, sizeof(buffer));
        assert_false(swl_type_fromChar(curType, buffer, curData->badVals[i]));
    }

    memset(buffer, 0, sizeof(buffer));
    assert_false(swl_type_fromChar(curType, NULL, NULL));
    assert_false(swl_type_fromChar(curType, NULL, curData->strValues[0]));
    assert_false(swl_type_fromChar(NULL, buffer, curData->strValues[0]));
}

static void s_checkStr(const char** data, amxc_string_t* testString, testData_t* testData, const char* delimiter) {
    if(*data != NULL) {
        return;
    }
    amxc_string_init(testString, 0);
    for(uint32_t i = 0; i < testData->nrTestValues; i++) {
        amxc_string_append(testString, testData->strValues[i], strlen(testData->strValues[i]));
        if(i < testData->nrTestValues - 1) {
            amxc_string_append(testString, delimiter, strlen(delimiter));
        }
    }
    *data = (const char*) testString->buffer;
}

static void s_verifyTestData(testData_t* testData) {
    s_checkStr(&testData->serialStr, &testData->serial1, testData, ",");
    s_checkStr(&testData->serialStr2, &testData->serial2, testData, ";;");
}

static void s_cleanTestData(testData_t* testData) {
    if(!amxc_string_is_empty(&testData->serial1)) {
        testData->serialStr = NULL;
        amxc_string_clean(&testData->serial1);
    }
    if(!amxc_string_is_empty(&testData->serial2)) {
        testData->serialStr2 = NULL;
        amxc_string_clean(&testData->serial2);
    }
}

int runCommonTypeTest(testData_t* testData) {
    s_verifyTestData(testData);
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlConv");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_valueSetup),
        cmocka_unit_test(test_swl_type_equals),
        cmocka_unit_test(test_swl_type_copy),
        cmocka_unit_test(test_swl_type_copyCleanup),
        cmocka_unit_test(test_swl_type_tofromChar),
        cmocka_unit_test(test_swl_type_tofromVariant),
        cmocka_unit_test(test_swl_type_toFromObject),
        cmocka_unit_test(test_swl_type_arrayToChar),
        cmocka_unit_test(test_swl_type_arrayFromChar),
        cmocka_unit_test(test_swl_type_arrayFromCharSep),
        cmocka_unit_test(test_swl_type_arrayEquals),
        cmocka_unit_test(test_swl_type_arrayCount),
        cmocka_unit_test(test_swl_type_arrayFindOffset),
        cmocka_unit_test(test_swl_type_arrayFind),
        cmocka_unit_test(test_swl_type_arrayContains),
        cmocka_unit_test(test_swl_type_arrayMatches),
        cmocka_unit_test(test_swl_type_arrayIsSuperset),
        cmocka_unit_test(test_swl_type_extractValueArrayFromStructArray),
        cmocka_unit_test(test_swl_type_arrayGet),
        cmocka_unit_test(test_swl_type_arrayGetCopy),
        cmocka_unit_test(test_swl_type_badVals),

    };
    curType = testData->testType;
    curData = testData;
    printf("Testing %s\n", curData->name);
    int rc = cmocka_run_group_tests(tests, s_setupSuite, s_teardownSuite);

    s_cleanTestData(testData);
    sahTraceClose();
    return rc;
}
