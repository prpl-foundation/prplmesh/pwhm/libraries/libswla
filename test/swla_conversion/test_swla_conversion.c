/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>
#include <debug/sahtrace.h>

#include "swla/swla_commonLib.h"
#include "test-toolbox/ttb.h"

#include <amxd/amxd_types.h>
#include <amxd/amxd_dm.h>

#define NR_ENTRIES 6

static const char* strList[NR_ENTRIES] = {"bla", "bla2", "foo", "bar", "foobar", "barfoo"};
static const char* invalidStrList[NR_ENTRIES] = {"XXbla", "bXXla2", "fooR", "Zbar", "fDFoobar", "barfAoo"};

typedef struct {
    const char* fromStr;
    uint32_t val;
    const char* toStr;
} swl_convTest_t;

#define NR_TESTVAL_ENUM 7

static swl_convTest_t testValMask[NR_TESTVAL_ENUM] = {
    {"", 0x0, ""},
    {"bla,foobar", 0x11, "bla,foobar"},
    {"bla", 0x1, "bla"},
    {"barfoo", 0x20, "barfoo"},
    {"bla,bla2,foo,bar,foobar,barfoo", 0x3f, "bla,bla2,foo,bar,foobar,barfoo"},
    {"abla", 0x0, ""},
    {"abla,rabarblafoo,foo,foobarbar", 0x4, "foo"},
};
static amxd_dm_t s_myDm;
static amxd_object_t* s_myObj;
static int s_setupSuite(void** state) {
    (void) state;
    amxd_param_t* param = NULL;

    assert_int_equal(amxd_dm_init(&s_myDm), 0);

    assert_int_equal(amxd_object_new(&s_myObj, amxd_object_singleton, "parent"), 0);
    assert_int_equal(amxd_dm_add_root_object(&s_myDm, s_myObj), 0);

    assert_non_null(s_myObj);


    assert_int_equal(amxd_param_new(&param, "TestParam", AMXC_VAR_ID_CSTRING), 0);
    assert_int_equal(amxd_object_add_param(s_myObj, param), amxd_status_ok);


    return 0;
}

static int s_teardownSuite(void** state) {
    (void) state;
    amxd_dm_clean(&s_myDm);
    return 0;
}

static void test_conv_maskToCharGo(char* input, uint32_t expectedMask, char* expectedOutput) {
    char buffer[128];
    uint32_t mask = swl_conv_charToMask(input, strList, SWL_ARRAY_SIZE(strList));
    assert_int_equal(mask, expectedMask);
    swl_conv_maskToChar(buffer, sizeof(buffer), mask, strList, SWL_ARRAY_SIZE(strList));
    if(expectedOutput == NULL) {
        assert_string_equal(buffer, input);
    } else {
        assert_string_equal(buffer, expectedOutput);
    }
}

static void test_conv_maskToChar(void** state _UNUSED) {
    test_conv_maskToCharGo("bla,foobar", 0x11, NULL);
    test_conv_maskToCharGo("bla", 0x1, NULL);
    test_conv_maskToCharGo("barfoo", 0x20, NULL);
    test_conv_maskToCharGo("bla,bla2,foo,bar,foobar,barfoo", 0x3f, NULL);
    test_conv_maskToCharGo("abla", 0x0, "");
    test_conv_maskToCharGo("abla,rabarblafoo,foo,foobarbar", 0x4, "foo");
}

static void test_conv_charToMaskGo(uint32_t input, char* expectedChar, uint32_t output) {
    char buffer[128];
    swl_conv_maskToChar(buffer, sizeof(buffer), input, strList, SWL_ARRAY_SIZE(strList));
    assert_string_equal(buffer, expectedChar);
    uint32_t mask = swl_conv_charToMask(buffer, strList, SWL_ARRAY_SIZE(strList));
    assert_int_equal(mask, output);
}

static void test_conv_charToMask(void** state _UNUSED) {
    test_conv_charToMaskGo(0x1, "bla", 0x1);
    test_conv_charToMaskGo(0x25, "bla,foo,barfoo", 0x25);
    test_conv_charToMaskGo(0x3f, "bla,bla2,foo,bar,foobar,barfoo", 0x3f);
    test_conv_charToMaskGo(0xff, "bla,bla2,foo,bar,foobar,barfoo", 0x3f);
}

static void helper_conv_charToMaskSep(
    bool expectAllUsed,
    uint32_t expectedMask,
    const char* str,
    const char** enumStrList,
    uint32_t maxVal,
    char separator) {
    bool resultAllUsed;
    uint32_t resultMask = swl_conv_charToMaskSep(str, enumStrList, maxVal, separator, &resultAllUsed);
    assert_true(expectAllUsed == resultAllUsed);
    assert_int_equal(expectedMask, resultMask);
}

static void test_conv_charToMaskSep(void** state _UNUSED) {
    const char* alphabet[] = {"ab", "cd", "ef", "gh", "ij", "k", "l", "m", 0};
    //                 index: 0    1    2    3    4    5   6   7

    // Normal:
    helper_conv_charToMaskSep(true, 1, "ab", alphabet, UINT32_MAX, ',');
    helper_conv_charToMaskSep(true, 1 | 1 << 2 | 1 << 4, "ab,ef,ij", alphabet, UINT32_MAX, ',');
    helper_conv_charToMaskSep(true, 1 << 4 | 1 << 5 | 1 << 6, "ij,k,l", alphabet, UINT32_MAX, ',');

    // Other order:
    helper_conv_charToMaskSep(true, 1 | 1 << 2 | 1 << 4, "ef,ab,ij", alphabet, UINT32_MAX, ',');

    // One value:
    helper_conv_charToMaskSep(true, 1 << 2, "ef", alphabet, UINT32_MAX, ',');
    helper_conv_charToMaskSep(true, 1 << 6, "l", alphabet, UINT32_MAX, ',');

    const char* overlap[] = {"ab", "b", "bc", "abc", "abcd", "x", "c", "y", 0};
    //                index: 0      1    2     3      4       5    6    7

    // Searched string is substring, with no elements before or after
    helper_conv_charToMaskSep(true, 1, "ab", overlap, UINT32_MAX, ',');
    helper_conv_charToMaskSep(true, 1 << 1, "b", overlap, UINT32_MAX, ',');
    helper_conv_charToMaskSep(true, 1 << 2, "bc", overlap, UINT32_MAX, ',');
    helper_conv_charToMaskSep(false, 0, "bcd", overlap, UINT32_MAX, ',');

    // Searched string is substring, with elements before but not after
    helper_conv_charToMaskSep(true, 1 << 5 | 1 << 1, "x,b", overlap, UINT32_MAX, ',');
    helper_conv_charToMaskSep(true, 1 << 5 | 1 << 6, "x,c", overlap, UINT32_MAX, ',');

    // Searched string is substring, with no elements before but with elements after
    helper_conv_charToMaskSep(true, 1 << 5 | 1 << 1, "b,x", overlap, UINT32_MAX, ',');
    helper_conv_charToMaskSep(true, 1 << 5 | 1 << 6, "c,x", overlap, UINT32_MAX, ',');

    // Searched string is substring, with elements before and after
    helper_conv_charToMaskSep(true, 1 << 5 | 1 << 7 | 1 << 1, "y,b,x", overlap, UINT32_MAX, ',');
    helper_conv_charToMaskSep(true, 1 << 5 | 1 << 7 | 1 << 6, "y,c,x", overlap, UINT32_MAX, ',');

    // One element in the comma-separated list cannot be mapped
    helper_conv_charToMaskSep(false, 1 | 1 << 7, "ab,bx,y", overlap, UINT32_MAX, ',');
}

static void test_conv_charToEnum(void** state _UNUSED) {
    const char* months_lastNull[] = {"January", "February", "March", "April", 0};
    const char* months_lastNotNull[] = {"January", "February", "March", "April", "May"};

    bool retVal;
    // Normal case:
    assert_int_equal(2, SWL_CONV_CHAR_TO_ENUM_RET("March", months_lastNull, 4, 100, &retVal));
    assert_true(retVal);
    assert_int_equal(2, SWL_CONV_CHAR_TO_ENUM_RET("March", months_lastNotNull, 5, 100, &retVal));
    assert_true(retVal);

    // Edges:
    assert_int_equal(0, SWL_CONV_CHAR_TO_ENUM_RET("January", months_lastNull, 4, 100, &retVal));
    assert_true(retVal);
    assert_int_equal(0, SWL_CONV_CHAR_TO_ENUM_RET("January", months_lastNotNull, 5, 100, &retVal));
    assert_true(retVal);
    assert_int_equal(3, SWL_CONV_CHAR_TO_ENUM_RET("April", months_lastNull, 4, 100, &retVal));
    assert_true(retVal);
    assert_int_equal(4, SWL_CONV_CHAR_TO_ENUM_RET("May", months_lastNotNull, 5, 100, &retVal));
    assert_true(retVal);

    // Not found, in bound:
    assert_int_equal(100, SWL_CONV_CHAR_TO_ENUM_RET("Jan", months_lastNull, 4, 100, &retVal));
    assert_false(retVal);
    assert_int_equal(100, SWL_CONV_CHAR_TO_ENUM_RET("Jan", months_lastNotNull, 5, 100, &retVal));
    assert_false(retVal);
    assert_int_equal(100, SWL_CONV_CHAR_TO_ENUM_RET("", months_lastNull, 4, 100, &retVal));
    assert_false(retVal);
    assert_int_equal(100, SWL_CONV_CHAR_TO_ENUM_RET("", months_lastNotNull, 5, 100, &retVal));
    assert_false(retVal);
    assert_int_equal(100, SWL_CONV_CHAR_TO_ENUM_RET(NULL, months_lastNull, 4, 100, &retVal));
    assert_false(retVal);
    assert_int_equal(100, SWL_CONV_CHAR_TO_ENUM_RET(NULL, months_lastNotNull, 5, 100, &retVal));
    assert_false(retVal);

    // Not found, hit null termination:
    assert_int_equal(100, SWL_CONV_CHAR_TO_ENUM_RET("Jan", months_lastNull, 1000, 100, &retVal));
    assert_false(retVal);
    assert_int_equal(100, SWL_CONV_CHAR_TO_ENUM_RET("", months_lastNull, 1000, 100, &retVal));
    assert_false(retVal);
    assert_int_equal(100, SWL_CONV_CHAR_TO_ENUM_RET(NULL, months_lastNull, 1000, 100, &retVal));
    assert_false(retVal);

    // Not found, bound before null:
    assert_int_equal(100, SWL_CONV_CHAR_TO_ENUM_RET("January", months_lastNull, 0, 100, &retVal));
    assert_false(retVal);
    assert_int_equal(100, SWL_CONV_CHAR_TO_ENUM_RET("March", months_lastNull, 1, 100, &retVal));
    assert_false(retVal);
    assert_int_equal(100, SWL_CONV_CHAR_TO_ENUM_RET("March", months_lastNull, 2, 100, &retVal));
    assert_false(retVal);
}

static void test_swl_conv_uint8ArrayToCharGo(char* expectedOutput, int inputlen, uint8_t* input) {
    char buffer[120];
    bool ret;
    if(expectedOutput == NULL) {
        ret = swl_conv_uint8ArrayToChar(buffer, 5, input, inputlen);
        assert_false(ret);
    } else {
        ret = swl_conv_uint8ArrayToChar(buffer, sizeof(buffer), input, inputlen);
        assert_true(ret);
        assert_string_equal(buffer, expectedOutput);
    }
}

static void test_swl_conv_uint8ArrayToChar(void** state _UNUSED) {
    uint8_t testInput[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    test_swl_conv_uint8ArrayToCharGo("1,2", 2, testInput);
    test_swl_conv_uint8ArrayToCharGo("1,2,3,4,5,6,7,8,9,10", 10, testInput);
    test_swl_conv_uint8ArrayToCharGo(NULL, 10, testInput);
    test_swl_conv_uint8ArrayToCharGo("1,2,3,4,5,6", 6, testInput);
}

static void test_swl_conv_objectParamSetEnum(void** state _UNUSED) {
    // Test successful set
    for(int i = 0; i < NR_ENTRIES; i++) {
        assert_true(swl_conv_objectParamSetEnum(s_myObj, "TestParam", i, strList, NR_ENTRIES));
        assert_string_equal(amxc_var_constcast(cstring_t, amxd_object_get_param_value(s_myObj, "TestParam")), strList[i]);
    }

    // Test failed set, due to index too high.
    for(int i = NR_ENTRIES; i < NR_ENTRIES * 2; i++) {
        assert_false(swl_conv_objectParamSetEnum(s_myObj, "TestParam", i, strList, NR_ENTRIES));
        assert_string_equal(amxc_var_constcast(cstring_t, amxd_object_get_param_value(s_myObj, "TestParam")), strList[NR_ENTRIES - 1]);
    }

    // Test failed set, param does not exist.
    assert_false(swl_conv_objectParamSetEnum(s_myObj, "NotParam", 0, strList, NR_ENTRIES));

}
static void test_swl_conv_objectParamSetMask(void** state _UNUSED) {
    // Test successful set
    for(int i = 0; i < NR_TESTVAL_ENUM; i++) {
        assert_true(swl_conv_objectParamSetMask(s_myObj, "TestParam", testValMask[i].val, strList, NR_ENTRIES));
        assert_string_equal(amxc_var_constcast(cstring_t, amxd_object_get_param_value(s_myObj, "TestParam")), testValMask[i].toStr);
    }

    // Test failed set, param does not exist.
    assert_false(swl_conv_objectParamSetEnum(s_myObj, "NotParam", 0, strList, NR_ENTRIES));
}
static void test_swl_conv_objectParamEnum(void** state _UNUSED) {
    for(int i = 0; i < NR_ENTRIES; i++) {
        amxd_object_set_value(cstring_t, s_myObj, "TestParam", strList[i]);
        assert_int_equal(swl_conv_objectParamEnum(s_myObj, "TestParam", strList, NR_ENTRIES, 0), i);
    }

    for(int i = 0; i < NR_ENTRIES; i++) {
        amxd_object_set_value(cstring_t, s_myObj, "TestParam", invalidStrList[i]);
        assert_int_equal(swl_conv_objectParamEnum(s_myObj, "TestParam", strList, NR_ENTRIES, NR_ENTRIES), NR_ENTRIES);
    }
}

static void test_swl_conv_objectParamMask(void** state _UNUSED) {
    for(int i = 0; i < NR_TESTVAL_ENUM; i++) {
        amxd_object_set_value(cstring_t, s_myObj, "TestParam", testValMask[i].fromStr);
        assert_int_equal(swl_conv_objectParamMask(s_myObj, "TestParam", strList, NR_ENTRIES), testValMask[i].val);
    }
}

static void test_swl_conv_refToDbm(void** state _UNUSED) {
    assert_int_equal(swl_conv_refToDbm(0), -110);
    assert_int_equal(swl_conv_refToDbm(240), 10);
    assert_int_equal(swl_conv_refToDbm(100), -60);
}

static void test_swl_conv_addEnumToMap(void** state _UNUSED) {
    amxc_var_t myVar;
    amxc_var_init(&myVar);
    amxc_var_set_type(&myVar, AMXC_VAR_ID_HTABLE);

    char nameBuffer[16] = {0};

    for(uint32_t i = 0; i < NR_ENTRIES / 2; i++) {
        snprintf(nameBuffer, sizeof(nameBuffer), "Val_%i", i);
        bool result = swl_conv_addEnumToMap(&myVar, nameBuffer, i, strList, NR_ENTRIES);
        ttb_assert_true(result);
    }

    for(uint32_t i = 0; i < NR_ENTRIES / 2; i++) {
        snprintf(nameBuffer, sizeof(nameBuffer), "Val_%i", i);
        char* testVal = swl_typeCharPtr_findInMapDef(&myVar, nameBuffer, NULL);
        ttb_assert_str_eq(testVal, strList[i]);
        free(testVal);
    }

    for(uint32_t i = NR_ENTRIES / 2; i < NR_ENTRIES; i++) {
        snprintf(nameBuffer, sizeof(nameBuffer), "Val_%i", i);
        char* testVal = swl_typeCharPtr_findInMapDef(&myVar, nameBuffer, NULL);
        ttb_assert_null(testVal);
    }

    ttb_assert_false(swl_conv_addEnumToMap(NULL, "TestVal", 0, strList, NR_ENTRIES));
    ttb_assert_false(swl_conv_addEnumToMap(&myVar, NULL, 0, strList, NR_ENTRIES));
    ttb_assert_false(swl_conv_addEnumToMap(&myVar, "TestVal", 0, NULL, NR_ENTRIES));
    ttb_assert_false(swl_conv_addEnumToMap(&myVar, "TestVal", 0, strList, 0));
    ttb_assert_true(swl_conv_addEnumToMap(&myVar, "TestVal", 0, strList, NR_ENTRIES));

    amxc_var_clean(&myVar);
}



static void test_swl_conv_findEnumInMap(void** state _UNUSED) {
    amxc_var_t myVar;
    amxc_var_init(&myVar);
    amxc_var_set_type(&myVar, AMXC_VAR_ID_HTABLE);

    char nameBuffer[16] = {0};

    for(uint32_t i = 0; i < NR_ENTRIES; i++) {
        snprintf(nameBuffer, sizeof(nameBuffer), "Val_%i", i);
        amxc_var_add_new_key_cstring_t(&myVar, nameBuffer, strList[i]);
    }

    for(uint32_t i = 0; i < NR_ENTRIES; i++) {
        snprintf(nameBuffer, sizeof(nameBuffer), "Val_%i", i);
        uint32_t enumVal = swl_conv_findEnumInMap(&myVar, nameBuffer, strList, NR_ENTRIES, NR_ENTRIES);
        ttb_assert_int_eq(enumVal, i);
    }

    for(uint32_t i = NR_ENTRIES; i < NR_ENTRIES * 2; i++) {
        snprintf(nameBuffer, sizeof(nameBuffer), "Val_%i", i);
        uint32_t enumVal = swl_conv_findEnumInMap(&myVar, nameBuffer, strList, NR_ENTRIES, NR_ENTRIES);
        ttb_assert_int_eq(enumVal, NR_ENTRIES);
    }

    ttb_assert_int_eq(swl_conv_findEnumInMap(&myVar, "Val_0", strList, NR_ENTRIES, NR_ENTRIES), 0);
    ttb_assert_int_eq(swl_conv_findEnumInMap(NULL, "Val_0", strList, NR_ENTRIES, NR_ENTRIES), NR_ENTRIES);
    ttb_assert_int_eq(swl_conv_findEnumInMap(&myVar, NULL, strList, NR_ENTRIES, NR_ENTRIES), NR_ENTRIES);
    ttb_assert_int_eq(swl_conv_findEnumInMap(&myVar, "Val_0", NULL, NR_ENTRIES, NR_ENTRIES), NR_ENTRIES);
    ttb_assert_int_eq(swl_conv_findEnumInMap(&myVar, "Val_0", strList, 0, NR_ENTRIES), NR_ENTRIES);
    ttb_assert_int_eq(swl_conv_findEnumInMap(&myVar, "Val_0", strList, 0, NR_ENTRIES * 2), NR_ENTRIES * 2);


    amxc_var_clean(&myVar);
}


static void test_swl_conv_readEnumFromMap(void** state _UNUSED) {
    amxc_var_t myVar;
    amxc_var_init(&myVar);
    amxc_var_set_type(&myVar, AMXC_VAR_ID_HTABLE);

    char nameBuffer[16] = {0};

    for(uint32_t i = 0; i < NR_ENTRIES; i++) {
        snprintf(nameBuffer, sizeof(nameBuffer), "Val_%i", i);
        amxc_var_add_new_key_cstring_t(&myVar, nameBuffer, strList[i]);
    }

    for(uint32_t i = 0; i < NR_ENTRIES; i++) {
        snprintf(nameBuffer, sizeof(nameBuffer), "Val_%i", i);
        uint32_t enumVal = NR_ENTRIES;
        ttb_assert_int_eq(swl_conv_readEnumFromMap(&enumVal, &myVar, nameBuffer, strList, NR_ENTRIES), SWL_RC_OK);
        ttb_assert_int_eq(enumVal, i);
    }

    for(uint32_t i = NR_ENTRIES; i < NR_ENTRIES * 2; i++) {
        snprintf(nameBuffer, sizeof(nameBuffer), "Val_%i", i);
        uint32_t enumVal = NR_ENTRIES + i;
        ttb_assert_int_eq(swl_conv_readEnumFromMap(&enumVal, &myVar, nameBuffer, strList, NR_ENTRIES), SWL_RC_ERROR);
        ttb_assert_int_eq(enumVal, NR_ENTRIES + i);
    }

    uint32_t enumVal = NR_ENTRIES;


    ttb_assert_int_eq(swl_conv_readEnumFromMap(NULL, &myVar, "Val_0", strList, NR_ENTRIES), SWL_RC_INVALID_PARAM);
    ttb_assert_int_eq(enumVal, NR_ENTRIES);

    ttb_assert_int_eq(swl_conv_readEnumFromMap(&enumVal, NULL, "Val_0", strList, NR_ENTRIES), SWL_RC_INVALID_PARAM);
    ttb_assert_int_eq(enumVal, NR_ENTRIES);
    ttb_assert_int_eq(swl_conv_readEnumFromMap(&enumVal, &myVar, NULL, strList, NR_ENTRIES), SWL_RC_INVALID_PARAM);
    ttb_assert_int_eq(enumVal, NR_ENTRIES);
    ttb_assert_int_eq(swl_conv_readEnumFromMap(&enumVal, &myVar, "Val_0", NULL, NR_ENTRIES), SWL_RC_INVALID_PARAM);
    ttb_assert_int_eq(enumVal, NR_ENTRIES);
    ttb_assert_int_eq(swl_conv_readEnumFromMap(&enumVal, &myVar, "Val_0", strList, 0), SWL_RC_INVALID_PARAM);
    ttb_assert_int_eq(enumVal, NR_ENTRIES);

    ttb_assert_int_eq(swl_conv_readEnumFromMap(&enumVal, &myVar, "Val_0", strList, NR_ENTRIES), SWL_RC_OK);
    ttb_assert_int_eq(enumVal, 0);



    amxc_var_clean(&myVar);
}

static void test_swl_conv_addMaskToMap(void** state _UNUSED) {
    amxc_var_t myVar;
    amxc_var_init(&myVar);
    amxc_var_set_type(&myVar, AMXC_VAR_ID_HTABLE);

    char nameBuffer[16] = {0};

    for(uint32_t i = 0; i < (1 << NR_ENTRIES / 2); i++) {
        snprintf(nameBuffer, sizeof(nameBuffer), "Val_%i", i);
        bool result = swl_conv_addMaskToMap(&myVar, nameBuffer, i, strList, NR_ENTRIES);
        ttb_assert_true(result);
    }

    for(uint32_t i = 0; i < (1 << NR_ENTRIES / 2); i++) {
        snprintf(nameBuffer, sizeof(nameBuffer), "Val_%i", i);
        char* testVal = swl_typeCharPtr_findInMapDef(&myVar, nameBuffer, NULL);
        char testBuffer[32] = {0};
        swl_conv_maskToChar(testBuffer, sizeof(testBuffer), i, strList, NR_ENTRIES);
        ttb_assert_str_eq(testVal, testBuffer);
        free(testVal);
    }

    ttb_assert_false(swl_conv_addMaskToMap(NULL, "TestVal", 0, strList, NR_ENTRIES));
    ttb_assert_false(swl_conv_addMaskToMap(&myVar, NULL, 0, strList, NR_ENTRIES));
    ttb_assert_false(swl_conv_addMaskToMap(&myVar, "TestVal", 0, NULL, NR_ENTRIES));
    ttb_assert_false(swl_conv_addMaskToMap(&myVar, "TestVal", 0, strList, 0));
    ttb_assert_true(swl_conv_addMaskToMap(&myVar, "TestVal", 0, strList, NR_ENTRIES));


    amxc_var_clean(&myVar);
}


static void test_swl_conv_findMaskInMap(void** state _UNUSED) {
    amxc_var_t myVar;
    amxc_var_init(&myVar);
    amxc_var_set_type(&myVar, AMXC_VAR_ID_HTABLE);

    char nameBuffer[16] = {0};

    for(uint32_t i = 0; i < (1 << NR_ENTRIES / 2); i++) {
        snprintf(nameBuffer, sizeof(nameBuffer), "Val_%i", i);
        char testBuffer[32] = {0};
        swl_conv_maskToChar(testBuffer, sizeof(testBuffer), i, strList, NR_ENTRIES);
        amxc_var_add_new_key_cstring_t(&myVar, nameBuffer, testBuffer);
    }

    for(uint32_t i = 0; i < (1 << NR_ENTRIES / 2); i++) {
        snprintf(nameBuffer, sizeof(nameBuffer), "Val_%i", i);
        uint32_t maskVal = swl_conv_findMaskInMap(&myVar, nameBuffer, strList, NR_ENTRIES);
        ttb_assert_int_eq(maskVal, i);
    }

    ttb_assert_int_eq(swl_conv_findMaskInMap(&myVar, "Val_1", strList, NR_ENTRIES), 1);
    ttb_assert_int_eq(swl_conv_findMaskInMap(NULL, "Val_1", strList, NR_ENTRIES), 0);
    ttb_assert_int_eq(swl_conv_findMaskInMap(&myVar, NULL, strList, NR_ENTRIES), 0);
    ttb_assert_int_eq(swl_conv_findMaskInMap(&myVar, "Val_1", NULL, NR_ENTRIES), 0);
    ttb_assert_int_eq(swl_conv_findMaskInMap(&myVar, "Val_1", strList, 0), 0);


    amxc_var_clean(&myVar);
}


static void test_swl_conv_readMaskFromMap(void** state _UNUSED) {
    amxc_var_t myVar;
    amxc_var_init(&myVar);
    amxc_var_set_type(&myVar, AMXC_VAR_ID_HTABLE);

    char nameBuffer[16] = {0};

    for(uint32_t i = 0; i < (1 << NR_ENTRIES / 2); i++) {
        snprintf(nameBuffer, sizeof(nameBuffer), "Val_%i", i);
        char testBuffer[32] = {0};
        swl_conv_maskToChar(testBuffer, sizeof(testBuffer), i, strList, NR_ENTRIES);
        amxc_var_add_new_key_cstring_t(&myVar, nameBuffer, testBuffer);
    }

    for(uint32_t i = 0; i < (1 << NR_ENTRIES / 2); i++) {
        snprintf(nameBuffer, sizeof(nameBuffer), "Val_%i", i);
        uint32_t maskVal = 1 << NR_ENTRIES;
        ttb_assert_int_eq(swl_conv_readMaskFromMap(&maskVal, &myVar, nameBuffer, strList, NR_ENTRIES), SWL_RC_OK);
        ttb_assert_int_eq(maskVal, i);
    }

    uint32_t maskVal = 1 << NR_ENTRIES;
    ttb_assert_int_eq(swl_conv_readMaskFromMap(NULL, &myVar, "Val_1", strList, NR_ENTRIES), SWL_RC_INVALID_PARAM);
    ttb_assert_int_eq(maskVal, 1 << NR_ENTRIES);
    ttb_assert_int_eq(swl_conv_readMaskFromMap(&maskVal, NULL, "Val_1", strList, NR_ENTRIES), SWL_RC_INVALID_PARAM);
    ttb_assert_int_eq(maskVal, 1 << NR_ENTRIES);
    ttb_assert_int_eq(swl_conv_readMaskFromMap(&maskVal, &myVar, NULL, strList, NR_ENTRIES), SWL_RC_INVALID_PARAM);
    ttb_assert_int_eq(maskVal, 1 << NR_ENTRIES);
    ttb_assert_int_eq(swl_conv_readMaskFromMap(&maskVal, &myVar, "Val_1", NULL, NR_ENTRIES), SWL_RC_INVALID_PARAM);
    ttb_assert_int_eq(maskVal, 1 << NR_ENTRIES);
    ttb_assert_int_eq(swl_conv_readMaskFromMap(&maskVal, &myVar, "Val_1", strList, 0), SWL_RC_INVALID_PARAM);
    ttb_assert_int_eq(maskVal, 1 << NR_ENTRIES);

    ttb_assert_int_eq(swl_conv_readMaskFromMap(&maskVal, &myVar, "NoVal", strList, NR_ENTRIES), SWL_RC_ERROR);
    ttb_assert_int_eq(maskVal, 1 << NR_ENTRIES);


    amxc_var_clean(&myVar);
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlConv");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_conv_maskToChar),
        cmocka_unit_test(test_conv_charToMask),
        cmocka_unit_test(test_conv_charToMaskSep),
        cmocka_unit_test(test_conv_charToEnum),
        cmocka_unit_test(test_swl_conv_uint8ArrayToChar),
        cmocka_unit_test(test_swl_conv_objectParamSetEnum),
        cmocka_unit_test(test_swl_conv_objectParamSetMask),
        cmocka_unit_test(test_swl_conv_objectParamEnum),
        cmocka_unit_test(test_swl_conv_objectParamMask),
        cmocka_unit_test(test_swl_conv_refToDbm),
        cmocka_unit_test(test_swl_conv_addEnumToMap),
        cmocka_unit_test(test_swl_conv_findEnumInMap),
        cmocka_unit_test(test_swl_conv_readEnumFromMap),
        cmocka_unit_test(test_swl_conv_addMaskToMap),
        cmocka_unit_test(test_swl_conv_findMaskInMap),
        cmocka_unit_test(test_swl_conv_readMaskFromMap),
    };
    ttb_util_setFilter();
    int rc = cmocka_run_group_tests(tests, s_setupSuite, s_teardownSuite);
    sahTraceClose();
    return rc;
}
