/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define _GNU_SOURCE // for strptime
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cmocka.h>
#include <time.h>

#include <debug/sahtrace.h>
#include "test-toolbox/ttb_mockClock.h"
#include "test-toolbox/ttb_assert.h"

#include "swla/swla_commonLib.h"
#include "swla/swla_time.h"
#include "swla/swla_time_spec.h"
#include "swla/swla_type.h"

#include <amxd/amxd_dm.h>

#include "test_swla_type_testCommon.h"

// assume system up for 100k seconds
#define MONOTIME_BASE 100000
#define REALTIME_BASE 1588169613
#define REALTIME_STR "2020-04-29T14:13:33Z"

struct tm nullTime;
struct tm curTime;
struct tm timeTest1;
struct tm timeTest2;
struct tm timeTest3;

#define TEST1_NULLTIME_STR "0001-01-01T00:00:00Z"

//test 1 old time still in monotime
#define TEST1_MONOTIME 64654
#define TEST1_REALTIME 1588134267
#define TEST1_REALTIME_STR "2020-04-29T04:24:27Z"

//time in future
#define TEST2_MONOTIME 33804636
#define TEST2_REALTIME 1621874249
#define TEST2_REALTIME_STR "2021-05-24T16:37:29Z"

//very old time, before start of mono
#define TEST3_REALTIME 1564365271
#define TEST3_REALTIME_STR "2019-07-29T01:54:31Z"

typedef struct testVal {
    swl_timeMono_t monoTime;
    swl_timeReal_t realTime;
    const char* str;
    const char* localStr;
    struct tm* timeStamp;
} testVal_t;

//Only first 3 test vals are for mono conversion, as 4th is an error case
#define MONO_TESTS 4
#define REAL_TESTS 5

testVal_t vals[5] = {
    {.monoTime = 0,
        .realTime = 0,
        .str = TEST1_NULLTIME_STR,
        .localStr = "0001-01-01T00:00:00Z",
        .timeStamp = &nullTime},
    {.monoTime = MONOTIME_BASE,
        .realTime = REALTIME_BASE,
        .str = REALTIME_STR,
        .localStr = "2020-04-29T16:13:33Z",
        .timeStamp = &curTime},
    {.monoTime = TEST1_MONOTIME,
        .realTime = TEST1_REALTIME,
        .str = TEST1_REALTIME_STR,
        .localStr = "2020-04-29T06:24:27Z",
        .timeStamp = &timeTest1},
    {.monoTime = TEST2_MONOTIME,
        .realTime = TEST2_REALTIME,
        .str = TEST2_REALTIME_STR,
        .localStr = "2021-05-24T18:37:29Z",
        .timeStamp = &timeTest2},
    {.monoTime = 1,
        .realTime = TEST3_REALTIME,
        .str = TEST3_REALTIME_STR,
        .localStr = "2019-07-29T01:54:31Z",
        .timeStamp = &timeTest3},
};

static amxd_dm_t myDm;
static amxd_object_t* myObj;

struct timespec myMonoTime = {.tv_sec = MONOTIME_BASE, .tv_nsec = 0};
struct timespec myRealTime = {.tv_sec = REALTIME_BASE, .tv_nsec = 0};

static void s_resetTime() {
    myMonoTime.tv_sec = MONOTIME_BASE;
    myMonoTime.tv_nsec = 0;

    myRealTime.tv_sec = REALTIME_BASE;
    myRealTime.tv_nsec = 0;
}

static int s_setupSuite(void** state) {
    (void) state;
    s_resetTime();
    strptime(TEST1_NULLTIME_STR, SWL_DATETIME_ISO8601_DATE_TIME_FMT, &nullTime);
    strptime(REALTIME_STR, SWL_DATETIME_ISO8601_DATE_TIME_FMT, &curTime);
    strptime(TEST1_REALTIME_STR, SWL_DATETIME_ISO8601_DATE_TIME_FMT, &timeTest1);
    strptime(TEST2_REALTIME_STR, SWL_DATETIME_ISO8601_DATE_TIME_FMT, &timeTest2);
    strptime(TEST3_REALTIME_STR, SWL_DATETIME_ISO8601_DATE_TIME_FMT, &timeTest3);


    amxd_param_t* param = NULL;

    assert_int_equal(amxd_dm_init(&myDm), 0);

    assert_int_equal(amxd_object_new(&myObj, amxd_object_singleton, "parent"), 0);
    assert_int_equal(amxd_dm_add_root_object(&myDm, myObj), 0);

    assert_non_null(myObj);


    assert_int_equal(amxd_param_new(&param, "MyTime", AMXC_VAR_ID_TIMESTAMP), 0);
    assert_int_equal(amxd_object_add_param(myObj, param), amxd_status_ok);


    ttb_mockClock_init(&myMonoTime, &myRealTime);

    return 0;
}

static int s_teardownSuite(void** state) {
    (void) state;
    amxd_dm_clean(&myDm);
    return 0;
}

static void s_assert_tm_equal(struct tm* tm1, struct tm* tm2) {
    // only look at the date values, and not the other as they can differ depending on format
    assert_memory_equal(tm1, tm2, offsetof(struct tm, tm_wday));
}

void test_swl_time_mapAddMono(void** state _UNUSED) {
    for(int i = 0; i < MONO_TESTS; i++) {
        amxc_var_t myMap;
        amxc_var_init(&myMap);
        amxc_var_set_type(&myMap, AMXC_VAR_ID_HTABLE);
        swl_time_mapAddMono(&myMap, "MyTime", vals[i].monoTime);
        const amxc_ts_t* time = amxc_var_constcast(amxc_ts_t, amxc_var_get_key(&myMap, "MyTime", AMXC_VAR_FLAG_DEFAULT));
        struct tm timeStamp;
        amxc_ts_to_tm_utc(time, &timeStamp);
        s_assert_tm_equal(&timeStamp, vals[i].timeStamp);

        char* timeStr = amxc_var_get_cstring_t(amxc_var_get_key(&myMap, "MyTime", AMXC_VAR_FLAG_DEFAULT));
        ttb_assert_str_eq(timeStr, vals[i].str);
        free(timeStr);

        amxc_var_clean(&myMap);
    }
}

void test_swl_typetime_mapAddMono(void** state _UNUSED) {
    for(int i = 0; i < MONO_TESTS; i++) {
        amxc_var_t myMap;
        amxc_var_init(&myMap);
        amxc_var_set_type(&myMap, AMXC_VAR_ID_HTABLE);
        swl_typeTimeMono_addToMap(&myMap, "MyTime", vals[i].monoTime);
        const amxc_ts_t* time = amxc_var_constcast(amxc_ts_t, amxc_var_get_key(&myMap, "MyTime", AMXC_VAR_FLAG_DEFAULT));
        struct tm timeStamp;
        amxc_ts_to_tm_utc(time, &timeStamp);
        s_assert_tm_equal(&timeStamp, vals[i].timeStamp);

        char* timeStr = amxc_var_get_cstring_t(amxc_var_get_key(&myMap, "MyTime", AMXC_VAR_FLAG_DEFAULT));
        ttb_assert_str_eq(timeStr, vals[i].str);
        free(timeStr);

        amxc_var_clean(&myMap);
    }
}

static void test_swl_time_mapAddReal(void** state _UNUSED) {
    for(int i = 0; i < REAL_TESTS; i++) {
        amxc_var_t myMap;
        amxc_var_init(&myMap);
        amxc_var_set_type(&myMap, AMXC_VAR_ID_HTABLE);
        swl_time_mapAddReal(&myMap, "MyTime", vals[i].realTime);
        const amxc_ts_t* time = amxc_var_constcast(amxc_ts_t, amxc_var_get_key(&myMap, "MyTime", AMXC_VAR_FLAG_DEFAULT));
        struct tm timeStamp;
        amxc_ts_to_tm_utc(time, &timeStamp);
        s_assert_tm_equal(&timeStamp, vals[i].timeStamp);

        char* timeStr = amxc_var_get_cstring_t(amxc_var_get_key(&myMap, "MyTime", AMXC_VAR_FLAG_DEFAULT));
        ttb_assert_str_eq(timeStr, vals[i].str);
        free(timeStr);

        amxc_var_clean(&myMap);
    }
}


static void test_swl_typetime_mapAddReal(void** state _UNUSED) {
    for(int i = 0; i < REAL_TESTS; i++) {
        amxc_var_t myMap;
        amxc_var_init(&myMap);
        amxc_var_set_type(&myMap, AMXC_VAR_ID_HTABLE);
        swl_typeTimeReal_addToMap(&myMap, "MyTime", vals[i].realTime);
        const amxc_ts_t* time = amxc_var_constcast(amxc_ts_t, amxc_var_get_key(&myMap, "MyTime", AMXC_VAR_FLAG_DEFAULT));
        struct tm timeStamp;
        amxc_ts_to_tm_utc(time, &timeStamp);
        s_assert_tm_equal(&timeStamp, vals[i].timeStamp);

        char* timeStr = amxc_var_get_cstring_t(amxc_var_get_key(&myMap, "MyTime", AMXC_VAR_FLAG_DEFAULT));
        ttb_assert_str_eq(timeStr, vals[i].str);
        free(timeStr);

        amxc_var_clean(&myMap);
    }
}


void test_swl_time_mapFindMono(void** state _UNUSED) {
    for(int i = 0; i < MONO_TESTS; i++) {
        amxc_var_t myMap;
        amxc_var_init(&myMap);
        amxc_var_set_type(&myMap, AMXC_VAR_ID_HTABLE);

        amxc_ts_t time;
        amxc_ts_from_tm(&time, vals[i].timeStamp);
        amxc_var_add_key(amxc_ts_t, &myMap, "MyTime", &time);
        assert_int_equal(vals[i].monoTime, swl_time_mapFindMono(&myMap, "MyTime"));
        amxc_var_clean(&myMap);
    }
}

void test_swl_time_mapFindReal(void** state _UNUSED) {
    for(int i = 0; i < REAL_TESTS; i++) {
        amxc_var_t myMap;
        amxc_var_init(&myMap);
        amxc_var_set_type(&myMap, AMXC_VAR_ID_HTABLE);
        amxc_ts_t time;
        amxc_ts_from_tm(&time, vals[i].timeStamp);
        amxc_var_add_key(amxc_ts_t, &myMap, "MyTime", &time);
        assert_int_equal(vals[i].realTime, swl_time_mapFindReal(&myMap, "MyTime"));
        amxc_var_clean(&myMap);
    }
}

void test_swl_time_objectParamSetMono(void** state _UNUSED) {
    for(int i = 0; i < MONO_TESTS; i++) {
        swl_time_objectParamSetMono(myObj, "MyTime", vals[i].monoTime);
        struct tm timeStamp;
        amxc_ts_t* ts = amxd_object_get_value(amxc_ts_t, myObj, "MyTime", NULL);
        amxc_ts_to_tm_utc(ts, &timeStamp);

        s_assert_tm_equal(&timeStamp, vals[i].timeStamp);
        free(ts);
    }
}

void test_swl_time_objectParamSetReal(void** state _UNUSED) {
    for(int i = 0; i < REAL_TESTS; i++) {
        swl_time_objectParamSetReal(myObj, "MyTime", vals[i].realTime);
        struct tm timeStamp;
        amxc_ts_t* ts = amxd_object_get_value(amxc_ts_t, myObj, "MyTime", NULL);
        amxc_ts_to_tm_utc(ts, &timeStamp);
        s_assert_tm_equal(&timeStamp, vals[i].timeStamp);
        free(ts);
    }
}

void test_swl_time_objectParamReal(void** state _UNUSED) {
    for(int i = 0; i < REAL_TESTS; i++) {
        amxc_ts_t time;
        amxc_ts_from_tm(&time, vals[i].timeStamp);
        amxd_status_t result = amxd_object_set_amxc_ts_t(myObj, "MyTime", &time);
        assert_int_equal(result, amxd_status_ok);
        swl_timeReal_t realtime = swl_time_objectParamReal(myObj, "MyTime");
        assert_int_equal(realtime, vals[i].realTime);
    }
}

void test_swl_time_objectParamMono(void** state _UNUSED) {
    for(int i = 0; i < MONO_TESTS; i++) {
        amxc_ts_t time;
        amxc_ts_from_tm(&time, vals[i].timeStamp);
        amxd_status_t result = amxd_object_set_amxc_ts_t(myObj, "MyTime", &time);
        assert_int_equal(result, amxd_status_ok);
        swl_timeMono_t monotime = swl_time_objectParamMono(myObj, "MyTime");
        assert_int_equal(monotime, vals[i].monoTime);
    }
}

#define NR_MONO_TEST_VALUES 3
swl_timeMono_t monoBase[NR_MONO_TEST_VALUES] = {TEST1_MONOTIME, TEST2_MONOTIME, TEST1_MONOTIME};
size_t monoCount[NR_MONO_TEST_VALUES] = {2, 1, 2};
swl_timeMono_t monoNotContains[NR_MONO_TEST_VALUES] = {TEST1_MONOTIME - 1, TEST1_MONOTIME + 1, TEST2_MONOTIME + 1};
#define monoStrChar TEST1_REALTIME_STR ","TEST2_REALTIME_STR ","TEST1_REALTIME_STR
#define monoStr2Char TEST1_REALTIME_STR ";;"TEST2_REALTIME_STR ";;"TEST1_REALTIME_STR
const char* monotestStrValuesChar[NR_MONO_TEST_VALUES] = {TEST1_REALTIME_STR, TEST2_REALTIME_STR, TEST1_REALTIME_STR};

#define NR_REAL_TEST_VALUES 3
swl_timeMono_t realBase[NR_REAL_TEST_VALUES] = {TEST1_REALTIME, TEST2_REALTIME, TEST1_REALTIME};
size_t realCount[NR_REAL_TEST_VALUES] = {2, 1, 2};
swl_timeMono_t realNotContains[NR_REAL_TEST_VALUES] = {TEST1_REALTIME - 1, TEST1_REALTIME + 1, TEST2_REALTIME + 1};
#define realStrChar TEST1_REALTIME_STR ","TEST2_REALTIME_STR ","TEST1_REALTIME_STR
#define realStr2Char TEST1_REALTIME_STR ";;"TEST2_REALTIME_STR ";;"TEST1_REALTIME_STR
const char* realtestStrValuesChar[NR_REAL_TEST_VALUES] = {TEST1_REALTIME_STR, TEST2_REALTIME_STR, TEST1_REALTIME_STR};

void* s_varToMonoData(amxc_var_t* variant) {

    struct tm time;
    amxc_ts_t* tm = amxc_var_dyncast(amxc_ts_t, variant);
    if(amxc_ts_to_tm_utc(tm, &time) < 0) {
        free(tm);
        return NULL;
    }

    swl_timeMono_t* tgtData = calloc(1, sizeof(swl_timeMono_t));
    *tgtData = swl_time_tmToMono(&time);
    free(tm);
    return tgtData;
}

void* s_varToRealData(amxc_var_t* variant) {

    struct tm time;
    amxc_ts_t* tm = amxc_var_dyncast(amxc_ts_t, variant);
    if(amxc_ts_to_tm_utc(tm, &time) < 0) {
        free(tm);
        return NULL;
    }

    swl_timeMono_t* tgtData = calloc(1, sizeof(swl_timeMono_t));
    *tgtData = swl_time_tmToReal(&time);
    free(tm);
    return tgtData;
}

testData_t timeMonoData = {
    .name = "monoTime",
    .testType = swl_type_timeMono,
    .serialStr = monoStrChar,
    .serialStr2 = monoStr2Char,
    .strValues = monotestStrValuesChar,
    .baseValues = &monoBase,
    .valueCount = monoCount,
    .notContains = &monoNotContains,
    .nrTestValues = NR_MONO_TEST_VALUES,
    .targetType = AMXC_VAR_ID_TIMESTAMP,
    .varToData = s_varToMonoData
};

testData_t timeRealData = {
    .name = "realTime",
    .testType = swl_type_timeReal,
    .serialStr = realStrChar,
    .serialStr2 = realStr2Char,
    .strValues = realtestStrValuesChar,
    .baseValues = &realBase,
    .valueCount = realCount,
    .notContains = &realNotContains,
    .nrTestValues = NR_MONO_TEST_VALUES,
    .targetType = AMXC_VAR_ID_TIMESTAMP,
    .varToData = s_varToRealData
};

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    int rc = -1;
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlRStd");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_time_mapAddMono),
        cmocka_unit_test(test_swl_typetime_mapAddMono),
        cmocka_unit_test(test_swl_time_mapAddReal),
        cmocka_unit_test(test_swl_typetime_mapAddReal),
        cmocka_unit_test(test_swl_time_mapFindMono),
        cmocka_unit_test(test_swl_time_mapFindReal),
        cmocka_unit_test(test_swl_time_objectParamSetMono),
        cmocka_unit_test(test_swl_time_objectParamSetReal),
        cmocka_unit_test(test_swl_time_objectParamReal),
        cmocka_unit_test(test_swl_time_objectParamMono),
    };
    rc = cmocka_run_group_tests(tests, s_setupSuite, s_teardownSuite);
    sahTraceClose();

    // run type tests
    runCommonTypeTest(&timeMonoData);
    runCommonTypeTest(&timeRealData);

    return rc;
}
