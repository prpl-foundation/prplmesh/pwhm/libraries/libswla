/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <unistd.h>
#include <libgen.h>
#include <cmocka.h>

#include "swla/swla_commonLib.h"

#define TEST_FILE "/tmp/test.txt"

static int setup_suite(void** state _UNUSED) {
    return 0;
}

static int teardown_suite(void** state _UNUSED) {
    return 0;
}

static void test_script_path_check(void** state _UNUSED) {
    int ret;
    //wrong script path
    unlink(TEST_FILE);
    ret = SWL_EXEC("./touch", "%s", TEST_FILE);
    assert_int_equal(ret, SWL_RC_ERROR);
}

static void test_execution(void** state _UNUSED) {
    int ret;
    //dirname is stripped, only basename is checked in env path
    unlink(TEST_FILE);
    ret = SWL_EXEC("touch", "%s", TEST_FILE);
    assert_int_equal(ret, SWL_RC_OK);
    //file is created
    assert_int_equal(access(TEST_FILE, F_OK), 0);
}

static void test_stdout_check(void** state _UNUSED) {
    int ret;
    char buf[256] = {0};
    //check content
    ret = SWL_EXEC_BUF(buf, sizeof(buf), "date", "-d 20191212 +%D");
    assert_int_equal(ret, SWL_RC_OK);
    assert_string_equal(buf, "12/12/19\n");
}

static void test_big_output_check(void** state _UNUSED) {
    //content: eqv: 256 of mem pages (256 x 4KB) = 1MB
    int bs = 4096;
    int cnt = 256;
    int fdOut = -1;
    swl_rc_ne ret = SWL_EXEC_FD(&fdOut, NULL, "dd", "if=/dev/urandom bs=%d count=%d", bs, cnt);
    assert_int_equal(ret, SWL_RC_OK);
    assert_int_not_equal(fdOut, -1);
    off_t size = lseek(fdOut, 0L, SEEK_END);
    close(fdOut);
    //check that we get on stdout the expected data amount
    assert_int_equal(size, (bs * cnt));
}

static void test_stderr_check(void** state _UNUSED) {
    int ret;
    int fdErr = -1;
    unlink(TEST_FILE);
    //try to find deleted file
    ret = SWL_EXEC_FD(NULL, &fdErr, "ls", "%s", TEST_FILE);
    assert_int_equal(ret, SWL_RC_OK);
    assert_int_not_equal(fdErr, -1);
    //get shell error message on stderr
    off_t sizeErr = lseek(fdErr, 0L, SEEK_END);
    assert_false(sizeErr == (off_t) -1);
    char bufErr[sizeErr + 1];
    memset(bufErr, 0, sizeof(bufErr));
    lseek(fdErr, 0L, SEEK_SET);
    read(fdErr, bufErr, sizeErr);
    close(fdErr);
    char* msg = strrchr(bufErr, ':');
    assert_non_null(msg);
    //check error msg content
    assert_string_equal(msg, ": No such file or directory\n");
}

static void test_result_check(void** state _UNUSED) {
    int ret;
    char errBuf[256] = {0};
    swl_exec_result_t result = {
        .errBuf = errBuf,
        .errBufSize = sizeof(errBuf),
    };
    unlink(TEST_FILE);
    //try to find deleted file
    ret = SWL_EXEC_BUF_EXT(&result, "ls", "%s", TEST_FILE);
    assert_int_equal(ret, SWL_RC_OK);
    //get shell error message on stderr
    char* msg = strrchr(errBuf, ':');
    assert_non_null(msg);
    //check error msg content
    assert_string_equal(msg, ": No such file or directory\n");
    //returned exit code not 0
    assert_true(result.exitInfo.isExited);
    assert_int_not_equal(result.exitInfo.exitStatus, 0);
}

static void s_runEventLoop(pid_t pid) {
    struct {
        uint32_t sigFd;
        int (* sigHandler)();
    } sigFds[2];
    //register handler for amx internal signals
    sigFds[0].sigFd = amxp_signal_fd();
    sigFds[0].sigHandler = amxp_signal_read;
    //register handler for system signals
    sigFds[1].sigFd = amxp_syssig_get_fd();
    sigFds[1].sigHandler = amxp_syssig_read;

    fd_set rfds;
    int fdMax;
    int res;
    //temporize reading
    struct timeval timeout = {0, 200000};
    while(amxp_subproc_find(pid) != NULL) {
        fdMax = -1;
        FD_ZERO(&rfds);
        for(uint32_t i = 0; i < SWL_ARRAY_SIZE(sigFds); i++) {
            FD_SET(sigFds[i].sigFd, &rfds);
            fdMax = SWL_MAX(fdMax, (int) sigFds[i].sigFd);
        }
        if(fdMax <= 0) {
            break;
        }
        res = select(fdMax + 1, &rfds, NULL, NULL, &timeout);
        if(res < 0) {
            if(errno == EINTR) {
                continue;
            }
            break;
        }
        if(!res) {
            continue;
        }
        for(uint32_t i = 0; i < SWL_ARRAY_SIZE(sigFds); i++) {
            if(FD_ISSET(sigFds[i].sigFd, &rfds)) {
                sigFds[i].sigHandler();
                break;
            }
        }
    }
}

static void bg_callback(amxp_subproc_t* subProc, const swl_exec_procExitInfo_t* const pExitInfo, void* const priv) {
    assert_non_null(pExitInfo);
    printf("Process %p terminated pid(%d) (e:%d/r:%d/k:%d/s:%d)\n",
           subProc, amxp_subproc_get_pid(subProc),
           pExitInfo->isExited, pExitInfo->exitStatus,
           pExitInfo->isSignaled, pExitInfo->termSignal);
    swl_exec_procExitInfo_t* pOut = (swl_exec_procExitInfo_t*) priv;
    assert_non_null(pOut);
    *pOut = *pExitInfo;
}

static void test_bg_execute(void** state _UNUSED) {
    swl_exec_procExitInfo_t procExitInfo;
    amxp_subproc_t* subProc = SWL_EXEC_CB(bg_callback, &procExitInfo, "sleep", "0.001");
    assert_non_null(subProc);
    assert_true(amxp_subproc_is_running(subProc));
    pid_t pid = amxp_subproc_get_pid(subProc);
    //run event loop until termination of process
    s_runEventLoop(pid);
    assert_true(procExitInfo.isExited);
    assert_int_equal(procExitInfo.exitStatus, 0);
}

static void test_bg_kill(void** state _UNUSED) {
    swl_exec_procExitInfo_t procExitInfo;
    amxp_subproc_t* subProc = SWL_EXEC_CB(bg_callback, &procExitInfo, "sleep", "0.5");
    assert_non_null(subProc);
    assert_true(amxp_subproc_is_running(subProc));
    pid_t pid = amxp_subproc_get_pid(subProc);
    //wait a lapse of time to let child process run before killing it
    usleep(100000);
    assert_int_equal(amxp_subproc_kill(subProc, SIGTERM), 0);
    //run event loop until termination of process
    s_runEventLoop(pid);
    assert_true(procExitInfo.isSignaled);
    assert_int_equal(procExitInfo.termSignal, SIGTERM);
}

static void s_test_bg_output_cb(amxp_subproc_t* subProc _UNUSED, const swl_exec_result_t* const result, void* const priv _UNUSED) {
    assert_true(result->exitInfo.isExited);
    assert_int_equal(result->exitInfo.exitStatus, 0);
    assert_string_equal(result->outBuf, "a b c\n");
    assert_string_equal(result->errBuf, "");
}

static void test_bg_output(void** state _UNUSED) {
    swl_exec_procExitInfo_t procExitInfo;
    amxp_subproc_t* subProc = SWL_EXEC_BUF_CB(s_test_bg_output_cb, &procExitInfo, "echo", "a b c");
    assert_non_null(subProc);
    assert_true(amxp_subproc_is_running(subProc));
    pid_t pid = amxp_subproc_get_pid(subProc);
    //run event loop until termination of process
    s_runEventLoop(pid);
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_script_path_check),
        cmocka_unit_test(test_execution),
        cmocka_unit_test(test_stdout_check),
        cmocka_unit_test(test_big_output_check),
        cmocka_unit_test(test_stderr_check),
        cmocka_unit_test(test_result_check),
        cmocka_unit_test(test_bg_execute),
        cmocka_unit_test(test_bg_kill),
        cmocka_unit_test(test_bg_output),
    };
    int rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);
    sahTraceClose();
    return rc;
}
