/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include "swla/swla_commonLib.h"

#include <debug/sahtrace.h>
#include "swla/swla_histogram.h"
#include "swl/swl_common.h"
#include "swla/swla_conversion.h"
#include "test-toolbox/ttb_mockClock.h"
#include "test-toolbox/ttb.h"
#include "test-toolbox/ttb_variant.h"

#define NR_BUCKETS 4
#define NR_WINDOWS 5
#define NR_MEAS_PER_WINDOW 30
#define HISTORY_SIZE 8

static uint8_t bucketSizes[NR_BUCKETS] = {5, 10, 20, 4};

#define MONOTIME_BASE 100000
#define REALTIME_BASE 1588169613
#define REALTIME_STR "2020-04-29T14:13:33Z"

struct timespec myMonoTime = {.tv_sec = MONOTIME_BASE, .tv_nsec = 0};
struct timespec myRealTime = {.tv_sec = REALTIME_BASE, .tv_nsec = 0};

static void s_resetTime() {
    ttb_mockClock_init(&myMonoTime, &myRealTime);
}

static void s_incTime() {
    ttb_mockClock_addTime(1, 0);
}

static void test_swl_histogram_init(void** state _UNUSED) {
    swl_histogram_t hist = {0};
    swl_histogram_init(&hist, NR_BUCKETS, NR_MEAS_PER_WINDOW, NR_WINDOWS, bucketSizes, HISTORY_SIZE);

    assert_int_equal(hist.totalNrBuckets, 39);
    assert_non_null(hist.nrBucketsPerValue);
    assert_non_null(hist.offsetList);
    uint16_t offsetList[NR_BUCKETS] = {0, 5, 15, 35};
    assert_memory_equal(offsetList, hist.offsetList, NR_BUCKETS * sizeof(uint16_t));

    assert_non_null(hist.histogramData.currentWindow.buckets);
    assert_non_null(hist.histogramData.totalWindow.buckets);
    assert_non_null(hist.histogramData.lastWindows);
    for(uint32_t i = 0; i < NR_WINDOWS; i++) {
        assert_non_null(hist.histogramData.lastWindows[i].buckets);
    }

    swl_histogram_destroy(&hist);
}

static uint32_t curIndex = 0;
static uint8_t nrFourIndexTest[NR_WINDOWS + 1] = {0, 0, 0, 1, 1, 2};

static void s_addMeasurement(swl_histogram_t* hist) {
    s_incTime();

    uint8_t indexes[NR_BUCKETS] = {0};
    indexes[0] = curIndex % bucketSizes[0];
    indexes[1] = ((curIndex / 2) * 2) % bucketSizes[1];
    indexes[2] = (curIndex / 3 + (curIndex + 1) / 3) % bucketSizes[2];
    indexes[3] = nrFourIndexTest[ (curIndex / NR_MEAS_PER_WINDOW) % 6];


    swl_histogram_addValues(hist, indexes);

    curIndex += 1;
}

static void s_addMeasurements(swl_histogram_t* hist, uint32_t nrMeas) {
    for(uint32_t i = 0; i < nrMeas; i++) {
        s_addMeasurement(hist);
    }
}

static void s_printHistData(swl_histogram_t* hist) {
    amxc_var_t tmp;
    amxc_var_init(&tmp);
    amxc_var_set_type(&tmp, AMXC_VAR_ID_HTABLE);
    swl_histogram_getAllHistograms(hist, &tmp);

    amxc_var_t tmpString;
    amxc_var_init(&tmpString);
    amxc_var_convert(&tmpString, &tmp, AMXC_VAR_ID_CSTRING);

    printf("out ------\n%s-----\n\n", amxc_var_constcast(cstring_t, &tmpString));
    amxc_var_clean(&tmpString);

    amxc_var_clean(&tmp);
}


static void s_printHistoryData(swl_histogram_t* hist) {
    amxc_var_t tmp;
    amxc_var_init(&tmp);
    amxc_var_set_type(&tmp, AMXC_VAR_ID_HTABLE);
    swl_histogram_getHistory(hist, &tmp);

    amxc_var_t tmpString;
    amxc_var_init(&tmpString);
    amxc_var_convert(&tmpString, &tmp, AMXC_VAR_ID_CSTRING);

    printf("out ------\n%s-----\n\n", amxc_var_constcast(cstring_t, &tmpString));
    amxc_var_clean(&tmpString);

    amxc_var_clean(&tmp);
}

static char* nameStrings[4];

static void s_setDefaultNameStrings() {
    nameStrings[0] = "0";
    nameStrings[1] = "1";
    nameStrings[2] = "2";
    nameStrings[3] = "3";
}

static void s_checkStrings(amxc_var_t* map, char* subMapName, char** strings) {
    amxc_var_t* subMap = amxc_var_get_key(map, subMapName, AMXC_VAR_FLAG_DEFAULT);

    assert_string_equal(amxc_var_constcast(cstring_t, amxc_var_get_key(subMap, nameStrings[0], AMXC_VAR_FLAG_DEFAULT)), strings[0]);
    assert_string_equal(amxc_var_constcast(cstring_t, amxc_var_get_key(subMap, nameStrings[1], AMXC_VAR_FLAG_DEFAULT)), strings[1]);
    assert_string_equal(amxc_var_constcast(cstring_t, amxc_var_get_key(subMap, nameStrings[2], AMXC_VAR_FLAG_DEFAULT)), strings[2]);
    assert_string_equal(amxc_var_constcast(cstring_t, amxc_var_get_key(subMap, nameStrings[3], AMXC_VAR_FLAG_DEFAULT)), strings[3]);

    // Need to use findChar because times are stored as dateTime, and da_findChar will result in NULL
    char* startTime = amxc_var_dyncast(cstring_t, amxc_var_get_key(subMap, "StartTime", AMXC_VAR_FLAG_DEFAULT));
    assert_string_equal(startTime, strings[4]);
    char* endTime = amxc_var_dyncast(cstring_t, amxc_var_get_key(subMap, "EndTime", AMXC_VAR_FLAG_DEFAULT));
    assert_string_equal(endTime, strings[5]);
    free(startTime);
    free(endTime);

}


static char* nullCheckStrings[6] = {"0,0,0,0,0"
    , "0,0,0,0,0,0,0,0,0,0"
    , "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0"
    , "0,0,0,0"
    , "0001-01-01T00:00:00Z"
    , "0001-01-01T00:00:00Z"};

static char* curStrings[6];
static char* totalStrings[6];
static char* lastStrings[6];
static size_t expectedNrVal[SWL_HISTOGRAM_WINDOW_ID_MAX];


static void s_checkSimpleString(swl_histogram_t* hist, swl_histogram_windowId_e windowId, char** strings) {
    amxc_var_t tmp;
    amxc_var_init(&tmp);
    amxc_var_set_type(&tmp, AMXC_VAR_ID_HTABLE);

    swl_histogram_mapAddHistogramWindow(hist, &tmp, "TestName", windowId);
    s_checkStrings(&tmp, "TestName", strings);

    amxc_var_clean(&tmp);

    char buffer[128] = {0};

    swl_timeMono_t time = swl_histogram_getStartTime(hist, windowId);
    swl_typeTimeMono_toChar(buffer, sizeof(buffer), time);
    assert_string_equal(buffer, strings[4]);

    time = swl_histogram_getEndTime(hist, windowId);
    swl_typeTimeMono_toChar(buffer, sizeof(buffer), time);
    assert_string_equal(buffer, strings[5]);
}

static void s_checkAllStrings(swl_histogram_t* hist) {
    amxc_var_t tmp;
    amxc_var_init(&tmp);
    amxc_var_set_type(&tmp, AMXC_VAR_ID_HTABLE);

    swl_histogram_getAllHistograms(hist, &tmp);

    s_checkStrings(&tmp, "Current", curStrings);
    s_checkStrings(&tmp, "Total", totalStrings);
    s_checkStrings(&tmp, "Last", lastStrings);

    s_checkSimpleString(hist, SWL_HISTOGRAM_WINDOW_ID_CURRENT, curStrings);
    s_checkSimpleString(hist, SWL_HISTOGRAM_WINDOW_ID_TOTAL, totalStrings);
    s_checkSimpleString(hist, SWL_HISTOGRAM_WINDOW_ID_LAST, lastStrings);

    amxc_var_clean(&tmp);
}

static void s_checkNrValInWindow(swl_histogram_t* hist) {
    for(size_t i = 0; i < SWL_HISTOGRAM_WINDOW_ID_MAX; i++) {
        assert_int_equal(expectedNrVal[i], swl_histogram_getNrValuesInWindow(hist, i));
    }
}

static void test_swl_histogram_getMap(void** state _UNUSED) {
    swl_histogram_t hist = {0};
    s_resetTime();
    curIndex = 0;

    swl_histogram_init(&hist, NR_BUCKETS, NR_MEAS_PER_WINDOW, NR_WINDOWS, bucketSizes, HISTORY_SIZE);

    memcpy(curStrings, nullCheckStrings, sizeof(nullCheckStrings));
    memcpy(totalStrings, nullCheckStrings, sizeof(nullCheckStrings));
    memcpy(lastStrings, nullCheckStrings, sizeof(nullCheckStrings));
    memset(expectedNrVal, 0, sizeof(expectedNrVal));


    s_checkAllStrings(&hist);
    s_checkNrValInWindow(&hist);


    s_addMeasurements(&hist, 1);
    curStrings[0] = "1,0,0,0,0";
    curStrings[1] = "1,0,0,0,0,0,0,0,0,0";
    curStrings[2] = "1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0";
    curStrings[3] = "1,0,0,0";
    curStrings[4] = "2020-04-29T14:13:34Z";
    curStrings[5] = "2020-04-29T14:13:34Z";
    s_checkAllStrings(&hist);

    expectedNrVal[SWL_HISTOGRAM_WINDOW_ID_CURRENT] = 1;
    s_checkNrValInWindow(&hist);


    s_addMeasurements(&hist, NR_MEAS_PER_WINDOW - 2);

    curStrings[0] = "6,6,6,6,5";
    curStrings[1] = "6,0,6,0,6,0,6,0,5,0";
    curStrings[2] = "2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,0";
    curStrings[3] = "29,0,0,0";
    curStrings[4] = "2020-04-29T14:13:34Z";
    curStrings[5] = "2020-04-29T14:14:02Z";

    expectedNrVal[SWL_HISTOGRAM_WINDOW_ID_CURRENT] = NR_MEAS_PER_WINDOW - 1;
    s_checkNrValInWindow(&hist);

    s_checkAllStrings(&hist);
    s_addMeasurements(&hist, 1);

    expectedNrVal[SWL_HISTOGRAM_WINDOW_ID_CURRENT] = 0;
    expectedNrVal[SWL_HISTOGRAM_WINDOW_ID_TOTAL] = NR_MEAS_PER_WINDOW;
    expectedNrVal[SWL_HISTOGRAM_WINDOW_ID_LAST] = NR_MEAS_PER_WINDOW;
    s_checkNrValInWindow(&hist);

    curStrings[0] = "0,0,0,0,0";
    curStrings[1] = "0,0,0,0,0,0,0,0,0,0";
    curStrings[2] = "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0";
    curStrings[3] = "0,0,0,0";
    curStrings[4] = "0001-01-01T00:00:00Z";
    curStrings[5] = "0001-01-01T00:00:00Z";
    totalStrings[0] = "6,6,6,6,6";
    totalStrings[1] = "6,0,6,0,6,0,6,0,6,0";
    totalStrings[2] = "2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1";
    totalStrings[3] = "30,0,0,0";
    totalStrings[4] = "2020-04-29T14:13:34Z";
    totalStrings[5] = "2020-04-29T14:14:03Z";
    lastStrings[0] = "6,6,6,6,6";
    lastStrings[1] = "6,0,6,0,6,0,6,0,6,0";
    lastStrings[2] = "2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1";
    lastStrings[3] = "30,0,0,0";
    lastStrings[4] = "2020-04-29T14:13:34Z";
    lastStrings[5] = "2020-04-29T14:14:03Z";

    s_checkAllStrings(&hist);

    s_addMeasurements(&hist, (NR_WINDOWS - 1) * NR_MEAS_PER_WINDOW - 1);

    expectedNrVal[SWL_HISTOGRAM_WINDOW_ID_CURRENT] = NR_MEAS_PER_WINDOW - 1;
    expectedNrVal[SWL_HISTOGRAM_WINDOW_ID_TOTAL] = NR_MEAS_PER_WINDOW * (NR_WINDOWS - 1);
    s_checkNrValInWindow(&hist);

    curStrings[0] = "6,6,6,6,5";
    curStrings[1] = "6,0,6,0,6,0,6,0,5,0";
    curStrings[2] = "2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,0";
    curStrings[3] = "0,29,0,0";
    curStrings[4] = "2020-04-29T14:15:34Z";
    curStrings[5] = "2020-04-29T14:16:02Z";

    totalStrings[0] = "24,24,24,24,24";
    totalStrings[1] = "24,0,24,0,24,0,24,0,24,0";
    totalStrings[2] = "8,4,8,4,8,4,8,4,8,4,8,4,8,4,8,4,8,4,8,4";
    totalStrings[3] = "90,30,0,0";
    totalStrings[4] = "2020-04-29T14:13:34Z";
    totalStrings[5] = "2020-04-29T14:15:33Z";

    lastStrings[0] = "6,6,6,6,6";
    lastStrings[1] = "6,0,6,0,6,0,6,0,6,0";
    lastStrings[2] = "2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1";
    lastStrings[3] = "0,30,0,0";
    lastStrings[4] = "2020-04-29T14:15:04Z";
    lastStrings[5] = "2020-04-29T14:15:33Z";

    s_checkAllStrings(&hist);


    s_addMeasurements(&hist, 1);
    curStrings[0] = "0,0,0,0,0";
    curStrings[1] = "0,0,0,0,0,0,0,0,0,0";
    curStrings[2] = "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0";
    curStrings[3] = "0,0,0,0";
    curStrings[4] = "0001-01-01T00:00:00Z";
    curStrings[5] = "0001-01-01T00:00:00Z";

    totalStrings[0] = "30,30,30,30,30";
    totalStrings[1] = "30,0,30,0,30,0,30,0,30,0";
    totalStrings[2] = "10,5,10,5,10,5,10,5,10,5,10,5,10,5,10,5,10,5,10,5";
    totalStrings[3] = "90,60,0,0";
    totalStrings[4] = "2020-04-29T14:13:34Z";
    totalStrings[5] = "2020-04-29T14:16:03Z";

    lastStrings[0] = "6,6,6,6,6";
    lastStrings[1] = "6,0,6,0,6,0,6,0,6,0";
    lastStrings[2] = "2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1";
    lastStrings[3] = "0,30,0,0";
    lastStrings[4] = "2020-04-29T14:15:34Z";
    lastStrings[5] = "2020-04-29T14:16:03Z";

    s_checkAllStrings(&hist);

    expectedNrVal[SWL_HISTOGRAM_WINDOW_ID_CURRENT] = 0;
    expectedNrVal[SWL_HISTOGRAM_WINDOW_ID_TOTAL] = NR_MEAS_PER_WINDOW * (NR_WINDOWS);
    s_checkNrValInWindow(&hist);


    s_addMeasurements(&hist, 1);
    curStrings[0] = "1,0,0,0,0";
    curStrings[1] = "1,0,0,0,0,0,0,0,0,0";
    curStrings[2] = "1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0";
    curStrings[3] = "0,0,1,0";
    curStrings[4] = "2020-04-29T14:16:04Z";
    curStrings[5] = "2020-04-29T14:16:04Z";

    s_checkAllStrings(&hist);

    expectedNrVal[SWL_HISTOGRAM_WINDOW_ID_CURRENT] = 1;
    s_checkNrValInWindow(&hist);

    //need to cycle nrWindows + 1 as we have a 6 window cycle, and only 5 windows in histogram
    s_addMeasurements(&hist, (NR_WINDOWS + 1) * NR_MEAS_PER_WINDOW);
    curStrings[4] = "2020-04-29T14:19:04Z";
    curStrings[5] = "2020-04-29T14:19:04Z";
    totalStrings[4] = "2020-04-29T14:16:34Z";
    totalStrings[5] = "2020-04-29T14:19:03Z";
    lastStrings[4] = "2020-04-29T14:18:34Z";
    lastStrings[5] = "2020-04-29T14:19:03Z";


    s_checkAllStrings(&hist);
    s_checkNrValInWindow(&hist);


    s_addMeasurements(&hist, NR_MEAS_PER_WINDOW - 1);
    expectedNrVal[SWL_HISTOGRAM_WINDOW_ID_CURRENT] = 0;
    s_checkNrValInWindow(&hist);


    swl_histogram_reset(&hist);

    memcpy(curStrings, nullCheckStrings, sizeof(nullCheckStrings));
    memcpy(totalStrings, nullCheckStrings, sizeof(nullCheckStrings));
    memcpy(lastStrings, nullCheckStrings, sizeof(nullCheckStrings));

    s_checkAllStrings(&hist);
    expectedNrVal[SWL_HISTOGRAM_WINDOW_ID_CURRENT] = 0;
    expectedNrVal[SWL_HISTOGRAM_WINDOW_ID_LAST] = 0;
    expectedNrVal[SWL_HISTOGRAM_WINDOW_ID_TOTAL] = 0;
    s_checkNrValInWindow(&hist);

    s_addMeasurements(&hist, (NR_WINDOWS) *NR_MEAS_PER_WINDOW + 1);

    curStrings[0] = "1,0,0,0,0";
    curStrings[1] = "1,0,0,0,0,0,0,0,0,0";
    curStrings[2] = "1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0";
    curStrings[3] = "0,0,1,0";
    curStrings[4] = "2020-04-29T14:22:04Z";
    curStrings[5] = "2020-04-29T14:22:04Z";

    totalStrings[0] = "30,30,30,30,30";
    totalStrings[1] = "30,0,30,0,30,0,30,0,30,0";
    totalStrings[2] = "10,5,10,5,10,5,10,5,10,5,10,5,10,5,10,5,10,5,10,5";
    totalStrings[3] = "90,60,0,0";
    totalStrings[4] = "2020-04-29T14:19:34Z";
    totalStrings[5] = "2020-04-29T14:22:03Z";

    lastStrings[0] = "6,6,6,6,6";
    lastStrings[1] = "6,0,6,0,6,0,6,0,6,0";
    lastStrings[2] = "2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1";
    lastStrings[3] = "0,30,0,0";
    lastStrings[4] = "2020-04-29T14:21:34Z";
    lastStrings[5] = "2020-04-29T14:22:03Z";

    s_printHistData(&hist);
    s_checkAllStrings(&hist);

    expectedNrVal[SWL_HISTOGRAM_WINDOW_ID_CURRENT] = 1;
    expectedNrVal[SWL_HISTOGRAM_WINDOW_ID_LAST] = NR_MEAS_PER_WINDOW;
    expectedNrVal[SWL_HISTOGRAM_WINDOW_ID_TOTAL] = NR_WINDOWS * NR_MEAS_PER_WINDOW;
    s_checkNrValInWindow(&hist);

    swl_histogram_destroy(&hist);
}

static void s_checkHistory(swl_histogram_t* hist, char* string0, char* string1, char* string2, char* string3) {
    amxc_var_t tmp;
    amxc_var_init(&tmp);
    amxc_var_set_type(&tmp, AMXC_VAR_ID_HTABLE);

    swl_histogram_getHistory(hist, &tmp);

    amxc_var_t* subMap = amxc_var_get_key(&tmp, "History", AMXC_VAR_FLAG_DEFAULT);

    assert_string_equal(amxc_var_constcast(cstring_t, amxc_var_get_key(subMap, nameStrings[0], AMXC_VAR_FLAG_DEFAULT)), string0);
    assert_string_equal(amxc_var_constcast(cstring_t, amxc_var_get_key(subMap, nameStrings[1], AMXC_VAR_FLAG_DEFAULT)), string1);
    assert_string_equal(amxc_var_constcast(cstring_t, amxc_var_get_key(subMap, nameStrings[2], AMXC_VAR_FLAG_DEFAULT)), string2);
    assert_string_equal(amxc_var_constcast(cstring_t, amxc_var_get_key(subMap, nameStrings[3], AMXC_VAR_FLAG_DEFAULT)), string3);

    amxc_var_clean(&tmp);
}

static void test_swl_histogram_getHistory(void** state _UNUSED) {
    swl_histogram_t hist = {0};
    s_resetTime();
    curIndex = 0;

    swl_histogram_init(&hist, NR_BUCKETS, NR_MEAS_PER_WINDOW, NR_WINDOWS, bucketSizes, HISTORY_SIZE);


    for(int i = 0; i < 4; i++) {

        s_checkHistory(&hist, "", "", "", "");

        s_addMeasurements(&hist, HISTORY_SIZE / 2);
        s_checkHistory(&hist, "0,1,2,3", "0,0,2,2", "0,0,1,2", "0,0,0,0");

        s_addMeasurements(&hist, HISTORY_SIZE / 2);
        s_checkHistory(&hist, "0,1,2,3,4,0,1,2", "0,0,2,2,4,4,6,6", "0,0,1,2,2,3,4,4", "0,0,0,0,0,0,0,0");

        s_addMeasurements(&hist, HISTORY_SIZE / 2);

        s_checkHistory(&hist, "4,0,1,2,3,4,0,1", "4,4,6,6,8,8,0,0", "2,3,4,4,5,6,6,7", "0,0,0,0,0,0,0,0");

        swl_histogram_reset(&hist);
        curIndex = 0;
    }
    s_printHistoryData(&hist);

    swl_histogram_destroy(&hist);
}


static void test_swl_histogram_getHistoryZero(void** state _UNUSED) {
    swl_histogram_t hist = {0};
    s_resetTime();
    curIndex = 0;

    swl_histogram_init(&hist, NR_BUCKETS, NR_MEAS_PER_WINDOW, NR_WINDOWS, bucketSizes, 0);


    for(int i = 0; i < 4; i++) {

        s_checkHistory(&hist, "", "", "", "");

        s_addMeasurements(&hist, HISTORY_SIZE / 2);
        s_checkHistory(&hist, "", "", "", "");

        s_addMeasurements(&hist, HISTORY_SIZE / 2);
        s_checkHistory(&hist, "", "", "", "");

        s_addMeasurements(&hist, HISTORY_SIZE / 2);
        s_checkHistory(&hist, "", "", "", "");

        swl_histogram_reset(&hist);
        curIndex = 0;
    }
    s_printHistoryData(&hist);

    swl_histogram_destroy(&hist);
}

static void test_swl_histogram_setNames(void** state _UNUSED) {
    swl_histogram_t hist = {0};
    s_resetTime();
    curIndex = 0;

    swl_histogram_init(&hist, NR_BUCKETS, NR_MEAS_PER_WINDOW, NR_WINDOWS, bucketSizes, HISTORY_SIZE);

    char* newNames[NR_BUCKETS] = {"name1", "name2", NULL, "name4"};
    swl_histogram_setNames(&hist, newNames);
    memcpy(nameStrings, newNames, NR_BUCKETS * sizeof(char*));
    nameStrings[2] = "2";


    memcpy(curStrings, nullCheckStrings, sizeof(nullCheckStrings));
    memcpy(totalStrings, nullCheckStrings, sizeof(nullCheckStrings));
    memcpy(lastStrings, nullCheckStrings, sizeof(nullCheckStrings));

    s_checkAllStrings(&hist);
    s_checkHistory(&hist, "", "", "", "");


    s_addMeasurements(&hist, HISTORY_SIZE);
    s_checkHistory(&hist, "0,1,2,3,4,0,1,2", "0,0,2,2,4,4,6,6", "0,0,1,2,2,3,4,4", "0,0,0,0,0,0,0,0");


    s_addMeasurements(&hist, (NR_WINDOWS) *NR_MEAS_PER_WINDOW + 1 - HISTORY_SIZE);

    curStrings[0] = "1,0,0,0,0";
    curStrings[1] = "1,0,0,0,0,0,0,0,0,0";
    curStrings[2] = "1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0";
    curStrings[3] = "0,0,1,0";
    curStrings[4] = "2020-04-29T14:16:04Z";
    curStrings[5] = "2020-04-29T14:16:04Z";

    totalStrings[0] = "30,30,30,30,30";
    totalStrings[1] = "30,0,30,0,30,0,30,0,30,0";
    totalStrings[2] = "10,5,10,5,10,5,10,5,10,5,10,5,10,5,10,5,10,5,10,5";
    totalStrings[3] = "90,60,0,0";
    totalStrings[4] = "2020-04-29T14:13:34Z";
    totalStrings[5] = "2020-04-29T14:16:03Z";

    lastStrings[0] = "6,6,6,6,6";
    lastStrings[1] = "6,0,6,0,6,0,6,0,6,0";
    lastStrings[2] = "2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1";
    lastStrings[3] = "0,30,0,0";
    lastStrings[4] = "2020-04-29T14:15:34Z";
    lastStrings[5] = "2020-04-29T14:16:03Z";

    s_checkAllStrings(&hist);

    s_setDefaultNameStrings();

    swl_histogram_destroy(&hist);

}


static uint32_t calls[SWL_HISTOGRAM_EVENT_MAX] = {0};

void myHistogramEventHandler (swl_histogram_t* map, swl_histogram_event_e event, void* data) {
    assert_ptr_equal(calls, data);
    assert_non_null(map);
    assert_true(event < SWL_HISTOGRAM_EVENT_MAX);
    calls[event] += 1;
}

void checkCallValues(uint32_t nrWindowDone, uint32_t nrCycleDone) {
    assert_int_equal(nrWindowDone, calls[SWL_HISTOGRAM_EVENT_WINDOW_DONE]);
    assert_int_equal(nrCycleDone, calls[SWL_HISTOGRAM_EVENT_HISTORY_DONE]);
}

static void test_swl_histogram_setHandler(void** state _UNUSED) {
    swl_histogram_t hist = {0};
    s_resetTime();
    curIndex = 0;

    swl_histogram_init(&hist, NR_BUCKETS, NR_MEAS_PER_WINDOW, NR_WINDOWS, bucketSizes, HISTORY_SIZE);
    swl_histogram_setHandler(&hist, myHistogramEventHandler, calls);

    memset(calls, 0, SWL_HISTOGRAM_EVENT_MAX * SWL_ARRAY_SIZE(calls));
    checkCallValues(0, 0);

    for(uint32_t i = 0; i < 5 * NR_WINDOWS; i++) {
        s_addMeasurements(&hist, NR_MEAS_PER_WINDOW - 1);
        checkCallValues(i, i / NR_WINDOWS);
        s_addMeasurements(&hist, 1);
        checkCallValues(i + 1, (i + 1) / NR_WINDOWS);
    }

    swl_histogram_destroy(&hist);
}


static void test_swl_histogram_setNrObsPerWindow(void** state _UNUSED) {
    swl_histogram_t hist = {0};
    s_resetTime();
    curIndex = 0;

    swl_histogram_init(&hist, NR_BUCKETS, NR_MEAS_PER_WINDOW, NR_WINDOWS, bucketSizes, HISTORY_SIZE);

    memcpy(curStrings, nullCheckStrings, sizeof(nullCheckStrings));
    memcpy(totalStrings, nullCheckStrings, sizeof(nullCheckStrings));
    memcpy(lastStrings, nullCheckStrings, sizeof(nullCheckStrings));

    s_checkAllStrings(&hist);


    s_addMeasurements(&hist, (NR_WINDOWS) *NR_MEAS_PER_WINDOW - 1);

    curStrings[0] = "6,6,6,6,5";
    curStrings[1] = "6,0,6,0,6,0,6,0,5,0";
    curStrings[2] = "2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,0";
    curStrings[3] = "0,29,0,0";
    curStrings[4] = "2020-04-29T14:15:34Z";
    curStrings[5] = "2020-04-29T14:16:02Z";

    totalStrings[0] = "24,24,24,24,24";
    totalStrings[1] = "24,0,24,0,24,0,24,0,24,0";
    totalStrings[2] = "8,4,8,4,8,4,8,4,8,4,8,4,8,4,8,4,8,4,8,4";
    totalStrings[3] = "90,30,0,0";
    totalStrings[4] = "2020-04-29T14:13:34Z";
    totalStrings[5] = "2020-04-29T14:15:33Z";

    lastStrings[0] = "6,6,6,6,6";
    lastStrings[1] = "6,0,6,0,6,0,6,0,6,0";
    lastStrings[2] = "2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1";
    lastStrings[3] = "0,30,0,0";
    lastStrings[4] = "2020-04-29T14:15:04Z";
    lastStrings[5] = "2020-04-29T14:15:33Z";

    s_checkAllStrings(&hist);

    s_addMeasurements(&hist, 1);

    swl_histogram_setNrObsPerWindow(&hist, NR_MEAS_PER_WINDOW / 2);

    memcpy(curStrings, nullCheckStrings, sizeof(nullCheckStrings));
    memcpy(totalStrings, nullCheckStrings, sizeof(nullCheckStrings));
    memcpy(lastStrings, nullCheckStrings, sizeof(nullCheckStrings));

    s_checkAllStrings(&hist);


    s_addMeasurements(&hist, 2 * NR_MEAS_PER_WINDOW + 10);

    s_printHistData(&hist);


    curStrings[0] = "2,2,2,2,2";
    curStrings[1] = "2,0,2,0,2,0,2,0,2,0";
    curStrings[2] = "2,1,2,1,2,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0";
    curStrings[3] = "10,0,0,0";
    curStrings[4] = "2020-04-29T14:17:04Z";
    curStrings[5] = "2020-04-29T14:17:13Z";

    totalStrings[0] = "12,12,12,12,12";
    totalStrings[1] = "12,0,12,0,12,0,12,0,12,0";
    totalStrings[2] = "4,2,4,2,4,2,4,2,4,2,4,2,4,2,4,2,4,2,4,2";
    totalStrings[3] = "30,0,30,0";
    totalStrings[4] = "2020-04-29T14:16:04Z";
    totalStrings[5] = "2020-04-29T14:17:03Z";

    lastStrings[0] = "3,3,3,3,3";
    lastStrings[1] = "2,0,2,0,3,0,4,0,4,0"; // starts halfway, so other one will be 4,0,4,0,3,0,2,0,2,0
    lastStrings[2] = "0,0,0,0,0,0,0,0,0,0,2,1,2,1,2,1,2,1,2,1";
    lastStrings[3] = "15,0,0,0";
    lastStrings[4] = "2020-04-29T14:16:49Z";
    lastStrings[5] = "2020-04-29T14:17:03Z";

    s_printHistData(&hist);
    s_checkAllStrings(&hist);

    swl_histogram_destroy(&hist);
}

/**
 * Test setNrWindows and getNrFilledWindows
 */
static void test_swl_histogram_setNrWindows(void** state _UNUSED) {
    swl_histogram_t hist = {0};
    s_resetTime();
    curIndex = 0;

    swl_histogram_init(&hist, NR_BUCKETS, NR_MEAS_PER_WINDOW, NR_WINDOWS, bucketSizes, HISTORY_SIZE);
    assert_int_equal(0, swl_histogram_getNrFilledWindows(&hist));

    swl_histogram_setNrWindows(&hist, NR_WINDOWS * 2);
    assert_int_equal(0, swl_histogram_getNrFilledWindows(&hist));

    swl_histogram_setNrWindows(&hist, NR_WINDOWS);
    assert_int_equal(0, swl_histogram_getNrFilledWindows(&hist));

    memcpy(curStrings, nullCheckStrings, sizeof(nullCheckStrings));
    memcpy(totalStrings, nullCheckStrings, sizeof(nullCheckStrings));
    memcpy(lastStrings, nullCheckStrings, sizeof(nullCheckStrings));

    s_checkAllStrings(&hist);


    s_addMeasurements(&hist, (NR_WINDOWS) *NR_MEAS_PER_WINDOW + 1);

    curStrings[0] = "1,0,0,0,0";
    curStrings[1] = "1,0,0,0,0,0,0,0,0,0";
    curStrings[2] = "1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0";
    curStrings[3] = "0,0,1,0";
    curStrings[4] = "2020-04-29T14:16:04Z";
    curStrings[5] = "2020-04-29T14:16:04Z";

    totalStrings[0] = "30,30,30,30,30";
    totalStrings[1] = "30,0,30,0,30,0,30,0,30,0";
    totalStrings[2] = "10,5,10,5,10,5,10,5,10,5,10,5,10,5,10,5,10,5,10,5";
    totalStrings[3] = "90,60,0,0";
    totalStrings[4] = "2020-04-29T14:13:34Z";
    totalStrings[5] = "2020-04-29T14:16:03Z";

    lastStrings[0] = "6,6,6,6,6";
    lastStrings[1] = "6,0,6,0,6,0,6,0,6,0";
    lastStrings[2] = "2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1";
    lastStrings[3] = "0,30,0,0";
    lastStrings[4] = "2020-04-29T14:15:34Z";
    lastStrings[5] = "2020-04-29T14:16:03Z";

    s_checkAllStrings(&hist);

    assert_int_equal(NR_WINDOWS, swl_histogram_getNrFilledWindows(&hist));

    // Increase nr windows, no impact allowed

    swl_histogram_setNrWindows(&hist, NR_WINDOWS * 2);
    s_checkAllStrings(&hist);

    assert_int_equal(NR_WINDOWS, swl_histogram_getNrFilledWindows(&hist));

    s_addMeasurements(&hist, (NR_WINDOWS) *NR_MEAS_PER_WINDOW * 2);

    curStrings[3] = "0,1,0,0";
    curStrings[4] = "2020-04-29T14:21:04Z";
    curStrings[5] = "2020-04-29T14:21:04Z";

    totalStrings[0] = "60,60,60,60,60";
    totalStrings[1] = "60,0,60,0,60,0,60,0,60,0";
    totalStrings[2] = "20,10,20,10,20,10,20,10,20,10,20,10,20,10,20,10,20,10,20,10";
    totalStrings[3] = "180,60,60,0";
    totalStrings[4] = "2020-04-29T14:16:04Z";
    totalStrings[5] = "2020-04-29T14:21:03Z";

    lastStrings[0] = "6,6,6,6,6";
    lastStrings[1] = "6,0,6,0,6,0,6,0,6,0";
    lastStrings[2] = "2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1";
    lastStrings[3] = "30,0,0,0";
    lastStrings[4] = "2020-04-29T14:20:34Z";
    lastStrings[5] = "2020-04-29T14:21:03Z";

    s_checkAllStrings(&hist);

    swl_histogram_setNrWindows(&hist, NR_WINDOWS / 2);
    assert_int_equal(NR_WINDOWS / 2, swl_histogram_getNrFilledWindows(&hist));

    //reduce window size, only total should be impacted.
    totalStrings[0] = "12,12,12,12,12";
    totalStrings[1] = "12,0,12,0,12,0,12,0,12,0";
    totalStrings[2] = "4,2,4,2,4,2,4,2,4,2,4,2,4,2,4,2,4,2,4,2";
    totalStrings[3] = "60,0,0,0";
    totalStrings[4] = "2020-04-29T14:20:04Z";

    s_checkAllStrings(&hist);

    s_addMeasurements(&hist, (NR_WINDOWS) *NR_MEAS_PER_WINDOW);
    assert_int_equal(NR_WINDOWS / 2, swl_histogram_getNrFilledWindows(&hist));

    curStrings[3] = "1,0,0,0";
    curStrings[4] = "2020-04-29T14:23:34Z";
    curStrings[5] = "2020-04-29T14:23:34Z";


    totalStrings[4] = "2020-04-29T14:22:34Z";
    totalStrings[5] = "2020-04-29T14:23:33Z";

    lastStrings[4] = "2020-04-29T14:23:04Z";
    lastStrings[5] = "2020-04-29T14:23:33Z";

    s_checkAllStrings(&hist);
    assert_int_equal(NR_WINDOWS / 2, swl_histogram_getNrFilledWindows(&hist));

    swl_histogram_setNrWindows(&hist, NR_WINDOWS);
    s_checkAllStrings(&hist);
    assert_int_equal(NR_WINDOWS / 2, swl_histogram_getNrFilledWindows(&hist));

    swl_histogram_setNrWindows(&hist, NR_WINDOWS / 2);
    s_checkAllStrings(&hist);
    assert_int_equal(NR_WINDOWS / 2, swl_histogram_getNrFilledWindows(&hist));


    swl_histogram_destroy(&hist);
}

static void s_checkHistoryFromValue(swl_histogram_t* hist, uint8_t* testData, uint8_t testDataSizePerVal, size_t expectNrEl, bool done) {
    for(uint32_t i = 0; i < NR_BUCKETS; i++) {
        uint8_t data1[testDataSizePerVal];
        memset(data1, 0, testDataSizePerVal);
        size_t nrEl = 0;

        swl_rc_ne result = swl_histogram_getHistoryFromValue(hist, data1, testDataSizePerVal, i, &nrEl);
        if(done) {
            assert_int_equal(result, SWL_RC_OK);
        } else {
            assert_int_equal(result, SWL_RC_CONTINUE);
        }
        assert_int_equal(nrEl, expectNrEl);

        //Ensure NULL nrEl call produces same result
        uint8_t data2[testDataSizePerVal];
        memset(data2, 0, testDataSizePerVal);
        swl_histogram_getHistoryFromValue(hist, data2, testDataSizePerVal, i, NULL);
        if(done) {
            assert_int_equal(result, SWL_RC_OK);
        } else {
            assert_int_equal(result, SWL_RC_CONTINUE);
        }

        assert_memory_equal(data1, data2, testDataSizePerVal);

        /*
           char testbuffer1[128] = {0};
           char testbuffer2[128] = {0};
           swl_typeUInt8_arrayToChar(testbuffer1,128,data,testDataSizePerVal);
           swl_typeUInt8_arrayToChar(testbuffer2,128,&testData[i * testDataSizePerVal],testDataSizePerVal);
           printf("%u %s %s \n",i,testbuffer1,testbuffer2);
         */

        assert_memory_equal(&testData[i * testDataSizePerVal], data1, testDataSizePerVal);
    }

}

static void test_swl_histogram_getHistoryFromValue(void** state _UNUSED) {
    swl_histogram_t hist = {0};
    s_resetTime();
    curIndex = 0;

    swl_histogram_init(&hist, NR_BUCKETS, NR_MEAS_PER_WINDOW, NR_WINDOWS, bucketSizes, HISTORY_SIZE);

    s_addMeasurements(&hist, HISTORY_SIZE / 2);

    uint8_t historyData[NR_BUCKETS * HISTORY_SIZE] = {
        0, 1, 2, 3, 0, 0, 0, 0,
        0, 0, 2, 2, 0, 0, 0, 0,
        0, 0, 1, 2, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0
    };

    s_checkHistoryFromValue(&hist, historyData, HISTORY_SIZE, HISTORY_SIZE / 2, true);

    s_addMeasurements(&hist, HISTORY_SIZE / 2);

    uint8_t historyData2[NR_BUCKETS * HISTORY_SIZE] = {
        0, 1, 2, 3, 4, 0, 1, 2,
        0, 0, 2, 2, 4, 4, 6, 6,
        0, 0, 1, 2, 2, 3, 4, 4,
        0, 0, 0, 0, 0, 0, 0, 0
    };

    s_checkHistoryFromValue(&hist, historyData2, HISTORY_SIZE, HISTORY_SIZE, true);

    s_addMeasurements(&hist, HISTORY_SIZE / 2);

    uint8_t historyData3[NR_BUCKETS * HISTORY_SIZE] = {
        4, 0, 1, 2, 3, 4, 0, 1,
        4, 4, 6, 6, 8, 8, 0, 0,
        2, 3, 4, 4, 5, 6, 6, 7,
        0, 0, 0, 0, 0, 0, 0, 0
    };
    s_checkHistoryFromValue(&hist, historyData3, HISTORY_SIZE, HISTORY_SIZE, true);

    //test smaller len
    uint8_t historyData4[NR_BUCKETS * HISTORY_SIZE / 2] = {
        3, 4, 0, 1,
        8, 8, 0, 0,
        5, 6, 6, 7,
        0, 0, 0, 0,
    };
    s_checkHistoryFromValue(&hist, historyData4, HISTORY_SIZE / 2, HISTORY_SIZE / 2, false);

    uint8_t historyData5[NR_BUCKETS * (HISTORY_SIZE + 2)] = {
        4, 0, 1, 2, 3, 4, 0, 1, 0, 0,
        4, 4, 6, 6, 8, 8, 0, 0, 0, 0,
        2, 3, 4, 4, 5, 6, 6, 7, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    };
    s_checkHistoryFromValue(&hist, historyData5, HISTORY_SIZE + 2, HISTORY_SIZE, true);

    swl_histogram_destroy(&hist);
}


static void test_swl_histogram_setHistoryLen(void** state _UNUSED) {
    swl_histogram_t hist = {0};
    s_resetTime();
    curIndex = 0;

    swl_histogram_init(&hist, NR_BUCKETS, NR_MEAS_PER_WINDOW, NR_WINDOWS, bucketSizes, HISTORY_SIZE);

    s_addMeasurements(&hist, HISTORY_SIZE);

    s_checkHistory(&hist, "0,1,2,3,4,0,1,2", "0,0,2,2,4,4,6,6", "0,0,1,2,2,3,4,4", "0,0,0,0,0,0,0,0");

    swl_histogram_setHistoryLen(&hist, HISTORY_SIZE / 2);
    s_checkHistory(&hist, "4,0,1,2", "4,4,6,6", "2,3,4,4", "0,0,0,0");

    s_addMeasurements(&hist, HISTORY_SIZE / 2);
    s_checkHistory(&hist, "3,4,0,1", "8,8,0,0", "5,6,6,7", "0,0,0,0");


    s_addMeasurements(&hist, HISTORY_SIZE / 2 + 2);
    s_checkHistory(&hist, "4,0,1,2", "4,4,6,6", "9,10,10,11", "0,0,0,0");

    swl_histogram_setHistoryLen(&hist, HISTORY_SIZE + 2);

    s_checkHistory(&hist, "4,0,1,2", "4,4,6,6", "9,10,10,11", "0,0,0,0");

    s_addMeasurements(&hist, HISTORY_SIZE - 2);


    s_checkHistory(&hist, "4,0,1,2,3,4,0,1,2,3", "4,4,6,6,8,8,0,0,2,2", "9,10,10,11,12,12,13,14,14,15", "0,0,0,0,0,0,0,0,0,0");

    s_addMeasurements(&hist, HISTORY_SIZE + 2);
    s_checkHistory(&hist, "4,0,1,2,3,4,0,1,2,3", "4,4,6,6,8,8,0,0,2,2", "16,16,17,18,18,19,0,0,1,2", "0,0,0,0,0,0,0,0,0,0");

    s_addMeasurements(&hist, HISTORY_SIZE);
    s_checkHistory(&hist, "2,3,4,0,1,2,3,4,0,1", "2,2,4,4,6,6,8,8,0,0", "1,2,2,3,4,4,5,6,6,7", "0,0,0,0,0,0,0,0,0,0");

    swl_histogram_setHistoryLen(&hist, HISTORY_SIZE);
    s_checkHistory(&hist, "4,0,1,2,3,4,0,1", "4,4,6,6,8,8,0,0", "2,3,4,4,5,6,6,7", "0,0,0,0,0,0,0,0");

    swl_histogram_setHistoryLen(&hist, 0);
    s_checkHistory(&hist, "", "", "", "");
    s_addMeasurements(&hist, HISTORY_SIZE * 2);
    s_checkHistory(&hist, "", "", "", "");

    swl_histogram_setHistoryLen(&hist, HISTORY_SIZE);

    s_addMeasurements(&hist, HISTORY_SIZE * 2);
    s_checkHistory(&hist, "1,2,3,4,0,1,2,3", "6,6,8,8,0,0,2,2", "4,4,5,6,6,7,8,8", "0,0,0,0,0,0,0,0");


    swl_histogram_destroy(&hist);
}

static void test_swl_histogram_checkAddError(void** state _UNUSED) {
    swl_histogram_t hist = {0};
    s_resetTime();
    curIndex = 0;

    swl_histogram_init(&hist, NR_BUCKETS, NR_MEAS_PER_WINDOW, NR_WINDOWS, bucketSizes, HISTORY_SIZE);

    memcpy(curStrings, nullCheckStrings, sizeof(nullCheckStrings));
    memcpy(totalStrings, nullCheckStrings, sizeof(nullCheckStrings));
    memcpy(lastStrings, nullCheckStrings, sizeof(nullCheckStrings));

    s_checkAllStrings(&hist);

    uint8_t indexes[NR_BUCKETS] = {100, 100, 100, 100};
    swl_histogram_addValues(&hist, indexes);

    curStrings[4] = "2020-04-29T14:13:33Z";
    curStrings[5] = "2020-04-29T14:13:33Z";


    s_checkAllStrings(&hist);


    uint8_t indexes2[NR_BUCKETS] = {0, 0, 100, 100};
    swl_histogram_addValues(&hist, indexes2);


    curStrings[0] = "1,0,0,0,0";
    curStrings[1] = "1,0,0,0,0,0,0,0,0,0";
    curStrings[4] = "2020-04-29T14:13:33Z";
    curStrings[5] = "2020-04-29T14:13:34Z";

    swl_histogram_destroy(&hist);
}

static void test_swl_histogram_checkUninitHist(void** state _UNUSED) {
    swl_histogram_t hist = {0};
    s_resetTime();
    curIndex = 0;

    uint8_t indexes[NR_BUCKETS] = {100, 100, 100, 100};
    swl_histogram_addValues(&hist, indexes);

    amxc_var_t tmp;
    amxc_var_init(&tmp);
    amxc_var_set_type(&tmp, AMXC_VAR_ID_HTABLE);

    swl_histogram_getHistory(&hist, &tmp);

    assert_int_equal(amxc_htable_size(amxc_var_get_const_amxc_htable_t(&tmp)), 0);

    swl_histogram_getAllHistograms(&hist, &tmp);
    assert_int_equal(amxc_htable_size(amxc_var_get_const_amxc_htable_t(&tmp)), 0);

    amxc_var_clean(&tmp);
}

static uint8_t nrFourIndexTest2[NR_WINDOWS] = {1, 2, 2, 2, 3};

static void s_addMeasurement2(swl_histogram_t* hist) {
    s_incTime();

    uint8_t indexes[NR_BUCKETS] = {0};
    indexes[0] = nrFourIndexTest2[ curIndex % 5];
    indexes[1] = ( 2 * (curIndex % 5)) % bucketSizes[1];
    indexes[2] = 5 + curIndex % 10;
    indexes[3] = curIndex % bucketSizes[3];


    swl_histogram_addValues(hist, indexes);

    curIndex += 1;
}

static void s_addMeasurements2(swl_histogram_t* hist, uint32_t nrMeas) {
    for(uint32_t i = 0; i < nrMeas; i++) {
        s_addMeasurement2(hist);
    }
}


#define NR_BCKT_VAL 7

static void s_checkBuckets(swl_histogram_t* hist, swl_histogram_windowId_e id, uint16_t* offsets, char** strings) {
    char buffer[100];
    uint16_t indices[NR_BCKT_VAL] = {0};
    for(int i = 0; i < 4; i++) {
        swl_histogram_getBucketOffsets(hist, id, i, indices, offsets, NR_BCKT_VAL);

        swl_typeUInt16_arrayToChar(buffer, sizeof(buffer), indices, NR_BCKT_VAL);
        assert_string_equal(buffer, strings[i]);
    }
}

static void test_swl_histogram_getBucketOffsets(void** state _UNUSED) {
    swl_histogram_t hist = {0};
    s_resetTime();
    curIndex = 0;

    swl_histogram_init(&hist, NR_BUCKETS, 40, 4, bucketSizes, HISTORY_SIZE);



    s_addMeasurements2(&hist, (4 + 1) * 40 - 1);
    s_printHistData(&hist);


    uint16_t offsets[NR_BCKT_VAL] = {0, 10, 20, 50, 80, 90, 100};

    char* expPctStr[4] = {"100,150,200,250,300,350,400",
        "0,50,200,450,800,850,900",
        "500,600,700,1000,1300,1400,1500",
        "0,40,80,200,320,360,400"};

    s_checkBuckets(&hist, SWL_HISTOGRAM_WINDOW_ID_TOTAL, offsets, expPctStr);
    s_checkBuckets(&hist, SWL_HISTOGRAM_WINDOW_ID_LAST, offsets, expPctStr);

    //current strings are a bit off due to last measurement not being available
    char* curExpPctStr[4] = {"100,150,200,248,296,344,400",
        "0,50,200,445,690,844,900",
        "500,600,700,990,1281,1380,1500",
        "0,40,80,196,316,358,400"};
    s_checkBuckets(&hist, SWL_HISTOGRAM_WINDOW_ID_CURRENT, offsets, curExpPctStr);

    swl_histogram_destroy(&hist);
}




static void s_checkGetHistory(swl_histogram_t* hist, char* fileName) {
    amxc_var_t var;
    amxc_var_init(&var);
    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);

    swl_histogram_getHistory(hist, &var);

    ttb_variant_assertPrintEqFile(&var, fileName);

    amxc_var_clean(&var);
}

static void s_checkGetAllMaps(swl_histogram_t* hist, char* fileName) {
    amxc_var_t var;
    amxc_var_init(&var);
    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);

    swl_histogram_getAllHistograms(hist, &var);

    ttb_variant_assertPrintEqFile(&var, fileName);

    amxc_var_clean(&var);
}

static void s_checkMapAddHistogramWindow(swl_histogram_t* hist, char* fileName, swl_histogram_windowId_e histogramIndex) {
    amxc_var_t var;
    amxc_var_init(&var);
    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);

    swl_histogram_mapAddHistogramWindow(hist, &var, NULL, histogramIndex);

    ttb_variant_assertPrintEqFile(&var, fileName);

    amxc_var_clean(&var);
}

static void s_checkMapAddFlatWindow(swl_histogram_t* hist, char* fileName, swl_histogram_windowId_e histogramIndex) {
    amxc_var_t var;
    amxc_var_init(&var);
    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);

    swl_histogram_addFlatWindowToMap(hist, &var, histogramIndex, "MyTest");

    ttb_variant_assertPrintEqFile(&var, fileName);

    amxc_var_clean(&var);
}

static void s_checkGetMaps(swl_histogram_t* hist, char* fileName) {
    char buffer[64];
    snprintf(buffer, sizeof(buffer), "getAllMaps/%s%s", fileName, ".txt");
    s_checkGetAllMaps(hist, buffer);

    snprintf(buffer, sizeof(buffer), "getHistory/%s%s", fileName, ".txt");
    s_checkGetHistory(hist, buffer);

    for(size_t i = 0; i < SWL_HISTOGRAM_WINDOW_ID_MAX; i++) {
        snprintf(buffer, sizeof(buffer), "getWindow/%s_%s%s", fileName, swl_histogram_tableNames_str[i], ".txt");

        s_checkMapAddHistogramWindow(hist, buffer, i);
        snprintf(buffer, sizeof(buffer), "getFlatWindow/%s_%s%s", fileName, swl_histogram_tableNames_str[i], ".txt");
        s_checkMapAddFlatWindow(hist, buffer, i);
    }
}


static void test_swl_histogram_getMaps(void** state _UNUSED) {
    swl_histogram_t hist = {0};
    s_resetTime();
    curIndex = 0;

    swl_histogram_init(&hist, NR_BUCKETS, NR_MEAS_PER_WINDOW, NR_WINDOWS, bucketSizes, HISTORY_SIZE);

    char* newNames[NR_BUCKETS] = {"name1", "name2", "name3", "name4"};
    swl_histogram_setNames(&hist, newNames);

    s_checkGetMaps(&hist, "empty");


    s_addMeasurements(&hist, NR_MEAS_PER_WINDOW / 2);
    s_checkGetMaps(&hist, "half");


    s_addMeasurements(&hist, NR_MEAS_PER_WINDOW / 2);
    s_checkGetMaps(&hist, "full");


    s_addMeasurements(&hist, NR_MEAS_PER_WINDOW / 2);
    s_checkGetMaps(&hist, "fullAndHalf");

    swl_histogram_destroy(&hist);
}


static int s_setupSuite(void** state _UNUSED) {
    ttb_mockClock_init(&myMonoTime, &myRealTime);
    s_setDefaultNameStrings();
    return 0;
}

static int s_teardownSuite(void** state _UNUSED) {
    return 0;
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlConv");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_histogram_init),
        cmocka_unit_test(test_swl_histogram_getMap),
        cmocka_unit_test(test_swl_histogram_getHistory),
        cmocka_unit_test(test_swl_histogram_getHistoryZero),
        cmocka_unit_test(test_swl_histogram_setNames),
        cmocka_unit_test(test_swl_histogram_setHandler),
        cmocka_unit_test(test_swl_histogram_setNrObsPerWindow),
        cmocka_unit_test(test_swl_histogram_setNrWindows),
        cmocka_unit_test(test_swl_histogram_getHistoryFromValue),
        cmocka_unit_test(test_swl_histogram_setHistoryLen),
        cmocka_unit_test(test_swl_histogram_checkAddError),
        cmocka_unit_test(test_swl_histogram_checkUninitHist),
        cmocka_unit_test(test_swl_histogram_getBucketOffsets),
        cmocka_unit_test(test_swl_histogram_getMaps),
    };
    ttb_util_setFilter();
    int rc = 0;
    rc = cmocka_run_group_tests(tests, s_setupSuite, s_teardownSuite);
    sahTraceClose();
    return rc;
}
