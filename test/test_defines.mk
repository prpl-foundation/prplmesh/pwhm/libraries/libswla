ifndef STAGINGDIR
	$(error "STAGINGDIR not defined")
endif
ifndef SUT_DIR
	$(error "SUT_DIR not defined")
endif

TYPE_DIR=$(SUT_DIR)/test/swla_type

INCDIRS = $(INCDIR_PRIV) $(if $(STAGINGDIR), $(STAGINGDIR)/include) $(if $(STAGINGDIR), $(STAGINGDIR)/usr/include) $(if $(STAGINGDIR), $(STAGINGDIR)/usr/include/libnl3)


CFLAGS += -I$(SUT_DIR)/include \
		  $(addprefix -I ,$(INCDIRS)) \
		  $(shell PKG_CONFIG_PATH=$(STAGINGDIR)/usr/lib/pkgconfig pkg-config --cflags sahtrace cmocka swlc test-toolbox)
LDFLAGS += -L$(STAGINGDIR)/lib \
           -L$(STAGINGDIR)/usr/lib \
           -L$(SUT_DIR)/src \
           -Wl,-rpath,$(STAGINGDIR)/lib \
           -Wl,-rpath,$(STAGINGDIR)/usr/lib \
		   $(shell PKG_CONFIG_PATH=$(STAGINGDIR)/usr/lib/pkgconfig pkg-config --libs sahtrace cmocka swlc test-toolbox)\
		   -lamxc -lamxd -lamxp -lamxb -lamxo
