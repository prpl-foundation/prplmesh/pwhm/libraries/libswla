/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "swla/swla_commonLib.h"
#include "swl/swl_common.h"
#include "swla/swla_trilean.h"

#include <amxd/amxd_dm.h>


static amxd_dm_t myDm;
static amxd_object_t* myObj;

static int s_setupSuite(void** state _UNUSED) {
    amxd_param_t* param = NULL;

    assert_int_equal(amxd_dm_init(&myDm), 0);

    assert_int_equal(amxd_object_new(&myObj, amxd_object_singleton, "parent"), 0);
    assert_int_equal(amxd_dm_add_root_object(&myDm, myObj), 0);

    assert_non_null(myObj);


    assert_int_equal(amxd_param_new(&param, "StringValue", AMXC_VAR_ID_CSTRING), 0);
    assert_int_equal(amxd_object_add_param(myObj, param), amxd_status_ok);
    assert_int_equal(amxd_param_new(&param, "Int8Value", AMXC_VAR_ID_INT8), 0);
    assert_int_equal(amxd_object_add_param(myObj, param), amxd_status_ok);
    return 0;
}

static int s_teardownSuite(void** state _UNUSED) {
    amxd_dm_clean(&myDm);
    return 0;
}

static void s_testConvertBackForthNormalCase(const char* expectedChar, swl_trl_e expectedTrl,
                                             swl_trl_format_e format) {

    //printf("debugprint: char %s trl %d format %d\n", expectedChar, expectedTrl, format);

    swl_trl_e actualTrl = 123;
    assert_true(swl_trl_fromChar(&actualTrl, expectedChar, format));
    assert_int_equal(expectedTrl, actualTrl);

    const char* actualChar = NULL;
    assert_true(swl_trl_toChar(&actualChar, expectedTrl, format));
    assert_non_null(actualChar);
    assert_string_equal(expectedChar, actualChar);
}

static void test_swl_trl_fromChar_toChar_normalCases(void** state _UNUSED) {
    s_testConvertBackForthNormalCase("Off", SWL_TRL_FALSE, SWL_TRL_FORMAT_AUTO);
    s_testConvertBackForthNormalCase("On", SWL_TRL_TRUE, SWL_TRL_FORMAT_AUTO);
    s_testConvertBackForthNormalCase("Auto", SWL_TRL_UNKNOWN, SWL_TRL_FORMAT_AUTO);
    s_testConvertBackForthNormalCase("Auto", SWL_TRL_AUTO, SWL_TRL_FORMAT_AUTO);

    s_testConvertBackForthNormalCase("False", SWL_TRL_FALSE, SWL_TRL_FORMAT_UNKNOWN);
    s_testConvertBackForthNormalCase("True", SWL_TRL_TRUE, SWL_TRL_FORMAT_UNKNOWN);
    s_testConvertBackForthNormalCase("Unknown", SWL_TRL_UNKNOWN, SWL_TRL_FORMAT_UNKNOWN);
    s_testConvertBackForthNormalCase("Unknown", SWL_TRL_AUTO, SWL_TRL_FORMAT_UNKNOWN);

    s_testConvertBackForthNormalCase("0", SWL_TRL_FALSE, SWL_TRL_FORMAT_NUMERIC);
    s_testConvertBackForthNormalCase("1", SWL_TRL_TRUE, SWL_TRL_FORMAT_NUMERIC);
    s_testConvertBackForthNormalCase("-1", SWL_TRL_UNKNOWN, SWL_TRL_FORMAT_NUMERIC);
    s_testConvertBackForthNormalCase("-1", SWL_TRL_AUTO, SWL_TRL_FORMAT_NUMERIC);
}

static void test_swl_trl_fromChar_errorCases(void** state _UNUSED) {
    swl_trl_e trl = 123;

    // case: Different notation
    assert_false(swl_trl_fromChar(&trl, "Unknown", SWL_TRL_FORMAT_AUTO));
    assert_false(swl_trl_fromChar(&trl, "Auto", SWL_TRL_FORMAT_UNKNOWN));
    assert_false(swl_trl_fromChar(&trl, "True", SWL_TRL_FORMAT_AUTO));
    assert_false(swl_trl_fromChar(&trl, "Yes", SWL_TRL_FORMAT_AUTO));

    // case: Empty string
    assert_false(swl_trl_fromChar(&trl, "", SWL_TRL_FORMAT_AUTO));

    // case: NULL string
    assert_false(swl_trl_fromChar(&trl, NULL, SWL_TRL_FORMAT_AUTO));

    // case: string contains extras
    assert_false(swl_trl_fromChar(&trl, "On ", SWL_TRL_FORMAT_AUTO));

    // case: null target
    assert_false(swl_trl_fromChar(NULL, "Off", SWL_TRL_FORMAT_AUTO));

    // case: invalid format
    assert_false(swl_trl_fromChar(NULL, "Off", SWL_TRL_FORMAT_MAX));

    // On error we don't want it to write.
    assert_int_equal(123, trl);
}

static void test_swl_trl_toChar_errorCases(void** state _UNUSED) {
    const char* text = NULL;

    // case: not a trilean value
    assert_false(swl_trl_toChar(&text, SWL_TRL_MAX, SWL_TRL_FORMAT_AUTO));
    assert_false(swl_trl_toChar(&text, -1, SWL_TRL_FORMAT_AUTO));

    // case: null target
    assert_false(swl_trl_toChar(NULL, SWL_TRL_TRUE, SWL_TRL_FORMAT_AUTO));

    // case: not a valid format
    assert_false(swl_trl_toChar(NULL, SWL_TRL_TRUE, SWL_TRL_FORMAT_MAX));
    assert_false(swl_trl_toChar(NULL, SWL_TRL_TRUE, -1));
}

static void test_swl_trl_isValid(void** state _UNUSED) {
    assert_true(swl_trl_isValid(SWL_TRL_FALSE));
    assert_true(swl_trl_isValid(SWL_TRL_TRUE));
    assert_true(swl_trl_isValid(SWL_TRL_UNKNOWN));

    assert_false(swl_trl_isValid(SWL_TRL_MAX));
    assert_false(swl_trl_isValid(64));
    assert_false(swl_trl_isValid((swl_trl_e) - 1));
}

void test_swl_trl_objectParamSet(void** state _UNUSED) {
    // GIVEN an object with a string parameter.
    amxd_object_t* object = myObj;

    // WHEN writing a trilean to that parameter
    bool ok = swl_trl_objectParamSet(object, "StringValue", SWL_TRL_AUTO, SWL_TRL_FORMAT_AUTO);
    assert_true(ok);

    // THEN the parameter contains the correct encoding of the trilean
    const char* actualChar = amxc_var_constcast(cstring_t, amxd_object_get_param_value(object, "StringValue"));
    assert_string_equal("Auto", actualChar);
}

void test_swl_trl_objectParamSet_int(void** state _UNUSED) {
    // GIVEN an object with a integer parameter.
    amxd_object_t* object = myObj;

    // WHEN writing a trilean to that integer parameter
    bool ok = swl_trl_objectParamSet(object, "Int8Value", SWL_TRL_AUTO, SWL_TRL_FORMAT_NUMERIC);
    assert_true(ok);

    // THEN the parameter contains the correct encoding of the trilean
    int8_t actualI8 = amxd_object_get_value(int8_t, object, "Int8Value", NULL);
    assert_int_equal(-1, actualI8);
    const amxc_var_t* actualValue = amxd_object_get_param_value(object, "Int8Value");
    assert_int_equal(amxc_var_type_of(actualValue), AMXC_VAR_ID_INT8);
}

void test_swl_trl_objectParamSet_invalid(void** state _UNUSED) {
    amxd_object_t* object = myObj;

    // case: parameter does not exist
    assert_false(swl_trl_objectParamSet(object, "DoesNotExist", SWL_TRL_AUTO, SWL_TRL_FORMAT_NUMERIC));

    // case: NULL object
    assert_false(swl_trl_objectParamSet(NULL, "Int8Value", SWL_TRL_AUTO, SWL_TRL_FORMAT_NUMERIC));

    // case: invalid trilean
    assert_false(swl_trl_objectParamSet(object, "Int8Value", SWL_TRL_MAX + 1, SWL_TRL_FORMAT_NUMERIC));

    // case: invalid format
    assert_false(swl_trl_objectParamSet(object, "Int8Value", SWL_TRL_AUTO, SWL_TRL_FORMAT_MAX));

    // case: NULL parameter
    assert_false(swl_trl_objectParamSet(object, NULL, SWL_TRL_AUTO, SWL_TRL_FORMAT_MAX));
}

void test_swl_trl_objectParamGet(void** state _UNUSED) {
    amxd_object_t* object = myObj;

    swl_trl_e trl = -1;

    // WHEN reading that parameter and converting to trilean
    amxd_object_set_cstring_t(object, "StringValue", "Auto");

    bool ok = swl_trl_objectParamGet(&trl, object, "StringValue", SWL_TRL_FORMAT_AUTO);
    // THEN the trilean has the expected value.
    assert_true(ok);
    assert_int_equal(SWL_TRL_AUTO, trl);

    amxd_object_set_cstring_t(object, "StringValue", "Off");
    ok = swl_trl_objectParamGet(&trl, object, "StringValue", SWL_TRL_FORMAT_AUTO);
    assert_true(ok);
    assert_int_equal(SWL_TRL_FALSE, trl);


    amxd_object_set_int8_t(object, "Int8Value", -1);
    ok = swl_trl_objectParamGet(&trl, object, "Int8Value", SWL_TRL_FORMAT_NUMERIC);
    assert_true(ok);
    assert_int_equal(SWL_TRL_AUTO, trl);


    amxd_object_set_int8_t(object, "Int8Value", 1);
    ok = swl_trl_objectParamGet(&trl, object, "Int8Value", SWL_TRL_FORMAT_NUMERIC);
    assert_true(ok);
    assert_int_equal(SWL_TRL_TRUE, trl);


    amxd_object_set_int8_t(object, "Int8Value", 0);
    ok = swl_trl_objectParamGet(&trl, object, "Int8Value", SWL_TRL_FORMAT_NUMERIC);
    assert_true(ok);
    assert_int_equal(SWL_TRL_FALSE, trl);
}

static void test_swl_trl_objectParamGet_invalid(void** state _UNUSED) {
    amxd_object_t* object = myObj;

    swl_trl_e trl = 5000;

    // Case: NULL target
    assert_false(swl_trl_objectParamGet(NULL, object, "StringValue", SWL_TRL_FORMAT_AUTO));

    // Case: NULL object
    assert_false(swl_trl_objectParamGet(&trl, NULL, "StringValue", SWL_TRL_FORMAT_AUTO));

    // Case: NULL parameter
    assert_false(swl_trl_objectParamGet(&trl, object, NULL, SWL_TRL_FORMAT_AUTO));

    // Case: parameter does not exist
    assert_false(swl_trl_objectParamGet(&trl, object, "OopsTypo", SWL_TRL_FORMAT_AUTO));

    // Case: incorrect format
    assert_false(swl_trl_objectParamGet(&trl, object, "StringValue", 9001));

    // Case: value empty

    amxd_object_set_cstring_t(object, "StringValue", "");
    assert_false(swl_trl_objectParamGet(&trl, object, "StringValue", SWL_TRL_FORMAT_AUTO));

    // Case: existing but different format
    amxd_object_set_cstring_t(object, "StringValue", "Typo");
    assert_false(swl_trl_objectParamGet(&trl, object, "StringValue", SWL_TRL_FORMAT_UNKNOWN));

    // Errors do not modify stuff:
    assert_int_equal(5000, trl);
}

static void test_swl_trl_fromBool(void** state _UNUSED) {
    assert_true(SWL_TRL_FALSE == swl_trl_fromBool(false));
    assert_true(SWL_TRL_TRUE == swl_trl_fromBool(true));
}

static void test_swl_trl_toBool(void** state _UNUSED) {
    // "true==x" instead of "assert_true(x)" because we don't want it to return 5 or -1.
    assert_true(true == swl_trl_toBool(SWL_TRL_TRUE, false));
    assert_true(true == swl_trl_toBool(SWL_TRL_TRUE, true));
    assert_true(false == swl_trl_toBool(SWL_TRL_FALSE, false));
    assert_true(false == swl_trl_toBool(SWL_TRL_FALSE, true));
    assert_true(false == swl_trl_toBool(SWL_TRL_UNKNOWN, false));
    assert_true(true == swl_trl_toBool(SWL_TRL_UNKNOWN, true));

    // case: not a trilean value
    assert_true(false == swl_trl_toBool(-1, true));
    assert_true(false == swl_trl_toBool(-1, false));
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlTrl");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_trl_fromChar_toChar_normalCases),
        cmocka_unit_test(test_swl_trl_fromChar_errorCases),
        cmocka_unit_test(test_swl_trl_toChar_errorCases),
        cmocka_unit_test(test_swl_trl_isValid),
        cmocka_unit_test(test_swl_trl_objectParamSet),
        cmocka_unit_test(test_swl_trl_objectParamSet_int),
        cmocka_unit_test(test_swl_trl_objectParamSet_invalid),
        cmocka_unit_test(test_swl_trl_objectParamGet),
        cmocka_unit_test(test_swl_trl_objectParamGet_invalid),
        cmocka_unit_test(test_swl_trl_fromBool),
        cmocka_unit_test(test_swl_trl_toBool),
    };
    int rc = cmocka_run_group_tests(tests, s_setupSuite, s_teardownSuite);
    sahTraceClose();
    return rc;
}
