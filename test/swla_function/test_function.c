/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>
#include <libgen.h>
#include <debug/sahtrace.h>
#include "test-toolbox/ttb_amx.h"
#include "test-toolbox/ttb_mockClock.h"
#include "test-toolbox/ttb_mockTimer.h"
#include "test-toolbox/ttb_timespec.h"
#include "test-toolbox/ttb_object.h"

#include "swla/swla_common_amx.h"
#include "swla/swla_namedTupleType.h"
#include "swl/ttb/swl_ttb.h"
#include "swla/swla_object.h"
#include "swla/swla_function.h"


/**
 * @file Test for testing that we can call a amx function from a unittest.
 *
 * Can also be used as an example of a unittest that calls a amx function.
 */

#ifndef _UNUSED
#define _UNUSED __attribute__((unused))
#endif

static bool s_calledFun = false;
static bool s_deferDone = false;
static bool s_cancelled = false;

static swl_function_deferredInfo_t s_deferInfo;
static swl_function_deferredInfo_t* s_allocDeferInfo;

static bool s_myPriv = false;

static bool s_withCancelHandler = false;
static bool s_errorDefer = false;
static bool s_allocFree = false;

#define MY_TEST_TABLE_VAR(X, Y) \
    X(Y, gtSwl_type_int32, index, "SmallInt") \
    X(Y, gtSwl_type_int64, key, "BigInt") \
    X(Y, gtSwl_type_charPtr, val, "String") \
    X(Y, gtSwl_type_charBuf32, buf, "Name")

SWL_NTT(myTestNTTType, swl_myTestNTT_t, MY_TEST_TABLE_VAR, );

swl_myTestNTT_t s_input = {.index = 123, .key = 456, .val = "fooBar", .buf = "abc"};
swl_myTestNTT_t s_output = {.index = 321, .key = 654, .val = "barFoo", .buf = ""};

/**
 * Implementation of amx/odl function - normally this is not in your test
 * (except if you want to mock them).
 */


static void s_exampleCancelCbFun(swl_function_deferredInfo_t* info, void* const priv) {
    assert_true(priv == &s_myPriv);
    assert_true(info = &s_deferInfo);
    s_cancelled = true;
}

static void s_freeCancelCbFun(swl_function_deferredInfo_t* info, void* const priv) {
    assert_true(priv == &s_myPriv);
    assert_true(info == s_allocDeferInfo);
    s_cancelled = true;
    s_allocDeferInfo = NULL;
    printf("FREE CANCEL\n");
    free(info);
}

static amxd_status_t s_exampleFunction_cbf(_UNUSED amxd_object_t* object,
                                           _UNUSED amxd_function_t* func,
                                           _UNUSED amxc_var_t* args,
                                           _UNUSED amxc_var_t* ret) {
    s_calledFun = true;

    swl_myTestNTT_t inVar;
    swl_ntt_fromVariant(&myTestNTTType, &inVar, args);
    assert_true(swl_type_equals((swl_type_t*) &myTestNTTType, &inVar, &s_input));


    printf("In %s\n", swl_type_toBuf128((swl_type_t*) &myTestNTTType, &inVar).buf);
    swl_type_cleanup((swl_type_t*) &myTestNTTType, &inVar);

    swl_type_toVariant((swl_type_t*) &myTestNTTType.type, ret, &s_output);

    printf("INVOKE \n");
    if(s_allocFree) {
        s_allocDeferInfo = calloc(1, sizeof(swl_function_deferredInfo_t));
        return swl_function_deferCb(s_allocDeferInfo, func, ret, s_freeCancelCbFun, &s_myPriv);
    } else if(s_errorDefer) {
        return swl_function_deferCb(&s_deferInfo, func, NULL, s_exampleCancelCbFun, &s_myPriv);
    } else if(s_withCancelHandler) {
        return swl_function_deferCb(&s_deferInfo, func, ret, s_exampleCancelCbFun, &s_myPriv);
    } else {
        return swl_function_defer(&s_deferInfo, func, ret);
    }
}




static int s_setupSuite(void** state) {
    ttb_amx_t* dm = ttb_amx_init();
    *state = dm;
    assert_int_equal(amxo_resolver_ftab_add(&dm->parser, "exampleFunction", AMXO_FUNC(s_exampleFunction_cbf)), 0);

    ttb_amx_loadDm(dm, "datamodel.odl", "Example");

    ttb_mockClock_initNow();
    assert_non_null(*state);
    return 0;
}

static int s_teardownSuite(void** state) {
    bool ok = ttb_amx_cleanup(*state);
    *state = NULL;
    assert_true(ok);

    return 0;
}

typedef struct {
    bool isDone;
    bool errorSeen;
} funResult_t;


static void s_initEnv() {

    s_calledFun = false;
    s_deferDone = false;
    s_cancelled = false;
}

static void s_deferredFunDone_cb(const amxc_var_t* const data, void* const priv) {
    assert_true(priv == &s_myPriv);
    assert_true(data->type_id == AMXC_VAR_ID_HTABLE);

    amxc_var_t* retVar = amxc_var_get_key(data, "retval", AMXC_VAR_FLAG_DEFAULT);
    assert_true(retVar->type_id == AMXC_VAR_ID_HTABLE);

    swl_myTestNTT_t outVar;
    memset(&outVar, 0, sizeof(swl_myTestNTT_t));
    swl_type_fromVariant((swl_type_t*) &myTestNTTType.type, &outVar, retVar);
    printf("Out %s\n", swl_type_toBuf128((swl_type_t*) &myTestNTTType.type, &outVar).buf);


    assert_true(swl_type_equals((swl_type_t*) &myTestNTTType.type, &outVar, &s_output));
    swl_type_cleanup((swl_type_t*) &myTestNTTType.type, &outVar);

    s_deferDone = true;
}

/**
 * Attempt to do defer. If ok, status goes from started to done
 * Otherwise status should not be started
 */
static void s_doDeferDone(bool ok) {
    swl_function_deferredStatus_e oldStatus = s_deferInfo.status;


    amxc_var_t baseOutput;
    amxc_var_init(&baseOutput);
    amxc_var_set_type(&baseOutput, AMXC_VAR_ID_HTABLE);
    swl_type_toVariant((swl_type_t*) &myTestNTTType.type, &baseOutput, &s_output);
    amxd_status_t retVal = swl_function_deferDone(&s_deferInfo, amxd_status_ok, NULL, &baseOutput);
    if(ok) {
        assert_true(oldStatus == SWL_FUNCTION_DEFERRED_STATUS_STARTED);
        assert_true(amxd_status_ok == retVal);
        assert_true(s_deferInfo.status == SWL_FUNCTION_DEFERRED_STATUS_DONE);
    } else {
        assert_true(amxd_status_invalid_action == retVal);
        assert_true(oldStatus == s_deferInfo.status);
        assert_true(s_deferInfo.status != SWL_FUNCTION_DEFERRED_STATUS_STARTED);
    }

    amxc_var_clean(&baseOutput);

}


static void test_simpleFunCall(void** state) {
    ttb_amx_t* ttbBus = *state;
    ttb_object_t* obj = ttbBus->rootObj;

    ttb_var_t* reqVar = ttb_object_createArgs();
    assert_non_null(reqVar);

    s_initEnv();

    swl_type_toVariant((swl_type_t*) &myTestNTTType.type, reqVar, &s_input);
    ttb_var_t* replyVar = NULL;
    ttb_reply_t* reply = ttb_object_callFun(ttbBus, obj, "exampleFunction", &reqVar, &replyVar);
    assert_true(s_calledFun);
    amxd_status_t* status = (amxd_status_t*) reply;
    assert_true(*status == amxd_status_deferred);

    assert_non_null(replyVar);
    assert_true(replyVar->type_id == AMXC_VAR_ID_UINT64);
    assert_true(s_deferInfo.status == SWL_FUNCTION_DEFERRED_STATUS_STARTED);
    assert_true(amxc_var_get_const_uint64_t(replyVar) == s_deferInfo.callId);

    amxd_function_set_deferred_cb(amxc_var_get_const_uint64_t(replyVar), s_deferredFunDone_cb, &s_myPriv);

    ttb_mockTimer_goToFutureSec(1);


    s_doDeferDone(true);

    ttb_mockTimer_goToFutureSec(1);
    assert_true(s_deferInfo.status == SWL_FUNCTION_DEFERRED_STATUS_DONE);

    assert_true(s_deferDone);

    ttb_object_cleanReply(&reply, &replyVar);
}


static void test_cancelFunCallWithoutCB(void** state) {
    ttb_amx_t* ttbBus = *state;
    ttb_object_t* obj = ttbBus->rootObj;

    ttb_var_t* reqVar = ttb_object_createArgs();
    assert_non_null(reqVar);

    s_initEnv();

    swl_type_toVariant((swl_type_t*) &myTestNTTType.type, reqVar, &s_input);
    ttb_var_t* replyVar = NULL;
    ttb_reply_t* reply = ttb_object_callFun(ttbBus, obj, "exampleFunction", &reqVar, &replyVar);
    assert_true(s_calledFun);
    amxd_status_t* status = (amxd_status_t*) reply;
    assert_true(*status == amxd_status_deferred);

    ttb_mockTimer_goToFutureSec(1);
    assert_true(s_deferInfo.status == SWL_FUNCTION_DEFERRED_STATUS_STARTED);


    amxd_function_deferred_remove(s_deferInfo.callId);

    ttb_mockTimer_goToFutureSec(1);

    assert_true(s_deferInfo.status == SWL_FUNCTION_DEFERRED_STATUS_CANCELLED);

    s_doDeferDone(false);
    ttb_mockTimer_goToFutureSec(1);
    assert_false(s_cancelled);


    assert_false(s_deferDone);

    ttb_object_cleanReply(&reply, &replyVar);
}

static void test_cancelFunCallWithCB(void** state) {
    s_withCancelHandler = true;

    ttb_amx_t* ttbBus = *state;
    ttb_object_t* obj = ttbBus->rootObj;

    ttb_var_t* reqVar = ttb_object_createArgs();
    assert_non_null(reqVar);

    s_initEnv();

    swl_type_toVariant((swl_type_t*) &myTestNTTType.type, reqVar, &s_input);
    ttb_var_t* replyVar = NULL;
    ttb_reply_t* reply = ttb_object_callFun(ttbBus, obj, "exampleFunction", &reqVar, &replyVar);
    assert_true(s_calledFun);
    amxd_status_t* status = (amxd_status_t*) reply;
    assert_true(*status == amxd_status_deferred);

    ttb_mockTimer_goToFutureSec(1);
    assert_true(s_deferInfo.status == SWL_FUNCTION_DEFERRED_STATUS_STARTED);


    amxd_function_deferred_remove(s_deferInfo.callId);

    ttb_mockTimer_goToFutureSec(1);

    assert_true(s_deferInfo.status == SWL_FUNCTION_DEFERRED_STATUS_CANCELLED);

    s_doDeferDone(false);
    ttb_mockTimer_goToFutureSec(1);


    assert_false(s_deferDone);

    ttb_object_cleanReply(&reply, &replyVar);

    assert_true(s_cancelled);
    s_withCancelHandler = false;
}



static void test_cancelFunCallWithCBAllocFree(void** state) {
    s_allocFree = true;

    ttb_amx_t* ttbBus = *state;
    ttb_object_t* obj = ttbBus->rootObj;

    ttb_var_t* reqVar = ttb_object_createArgs();
    assert_non_null(reqVar);
    assert_null(s_allocDeferInfo);

    s_initEnv();

    swl_type_toVariant((swl_type_t*) &myTestNTTType.type, reqVar, &s_input);
    ttb_var_t* replyVar = NULL;
    ttb_reply_t* reply = ttb_object_callFun(ttbBus, obj, "exampleFunction", &reqVar, &replyVar);
    assert_true(s_calledFun);
    amxd_status_t* status = (amxd_status_t*) reply;
    assert_true(*status == amxd_status_deferred);

    ttb_mockTimer_goToFutureSec(1);
    assert_non_null(s_allocDeferInfo);
    assert_true(s_allocDeferInfo->status == SWL_FUNCTION_DEFERRED_STATUS_STARTED);


    amxd_function_deferred_remove(s_allocDeferInfo->callId);

    ttb_mockTimer_goToFutureSec(1);

    assert_null(s_allocDeferInfo);

    assert_false(s_deferDone);

    ttb_object_cleanReply(&reply, &replyVar);

    assert_true(s_cancelled);
    s_allocFree = false;
}



static void test_errorDefer(void** state) {
    s_errorDefer = true;

    ttb_amx_t* ttbBus = *state;
    ttb_object_t* obj = ttbBus->rootObj;

    ttb_var_t* reqVar = ttb_object_createArgs();
    assert_non_null(reqVar);

    s_initEnv();

    swl_type_toVariant((swl_type_t*) &myTestNTTType.type, reqVar, &s_input);
    ttb_var_t* replyVar = NULL;
    ttb_reply_t* reply = ttb_object_callFun(ttbBus, obj, "exampleFunction", &reqVar, &replyVar);
    assert_true(s_calledFun);
    amxd_status_t* status = (amxd_status_t*) reply;
    assert_true(*status == amxd_status_unknown_error);

    s_doDeferDone(false);

    ttb_object_cleanReply(&reply, &replyVar);
    s_errorDefer = false;
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {

    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_simpleFunCall),
        cmocka_unit_test(test_cancelFunCallWithoutCB),
        cmocka_unit_test(test_cancelFunCallWithCB),
        cmocka_unit_test(test_cancelFunCallWithCBAllocFree),
        cmocka_unit_test(test_errorDefer),
    };
    ttb_util_setFilter();
    return cmocka_run_group_tests(tests, s_setupSuite, s_teardownSuite);
}
