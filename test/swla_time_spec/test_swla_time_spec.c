/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cmocka.h>
#include <time.h>
#include "test-toolbox/ttb_mockClock.h"
#include "test-toolbox/ttb_assert.h"

#include <debug/sahtrace.h>

#include "swla/swla_commonLib.h"
#include "swla/swla_time_spec.h"
#include "test_swla_type_testCommon.h"

#include <amxd/amxd_dm.h>

#define MONOTIME_TV_SEC 100000
#define MONOTIME_TV_NSEC 343343000
#define REALTIME_TV_SEC 1588169613
#define REALTIME_TV_NSEC 787787000

// assume system up for 100k seconds
#define REALTIME_STR "2020-04-29T14:13:33.787787Z"


#define TEST1_NULLTIME_STR "0001-01-01T00:00:00Z"

//test 1 old time still in monotime
#define TEST1_MONOTIME 64653
#define TEST1_MONOTIME_NSEC 678679000
#define TEST1_REALTIME 1588134267
#define TEST1_REALTIME_NSEC 123123000
#define TEST1_REALTIME_STR "2020-04-29T04:24:27.123123Z"

//time in future
#define TEST2_MONOTIME 33804636
#define TEST2_MONOTIME_NSEC 545545000
#define TEST2_REALTIME 1621874249
#define TEST2_REALTIME_NSEC 989989000
#define TEST2_REALTIME_STR "2021-05-24T16:37:29.989989Z"

//very old time, before start of mono
#define TEST3_REALTIME 1564365271
#define TEST3_REALTIME_STR "2019-07-29T01:54:31Z"

typedef struct testVal {
    swl_timeSpecMono_t monoTime;
    swl_timeSpecReal_t realTime;
    const char* str;
    char* localStr;
    amxc_ts_t timeStamp;
    bool monoBefore;
} testVal_t;


testVal_t vals[] = {
    {.monoTime = {.tv_sec = 0, .tv_nsec = 0},
        .realTime = {.tv_sec = 0, .tv_nsec = 0},
        .str = TEST1_NULLTIME_STR},
    {.monoTime = {.tv_sec = MONOTIME_TV_SEC, .tv_nsec = MONOTIME_TV_NSEC},
        .realTime = {.tv_sec = REALTIME_TV_SEC, .tv_nsec = REALTIME_TV_NSEC},
        .str = REALTIME_STR},
    {.monoTime = {.tv_sec = TEST1_MONOTIME, .tv_nsec = TEST1_MONOTIME_NSEC},
        .realTime = {.tv_sec = TEST1_REALTIME, .tv_nsec = TEST1_REALTIME_NSEC},
        .str = TEST1_REALTIME_STR},
    {.monoTime = {.tv_sec = TEST2_MONOTIME, .tv_nsec = TEST2_MONOTIME_NSEC},
        .realTime = {.tv_sec = TEST2_REALTIME, .tv_nsec = TEST2_REALTIME_NSEC},
        .str = TEST2_REALTIME_STR },
    {.monoTime = {.tv_sec = 0, .tv_nsec = 1},
        .realTime = {.tv_sec = REALTIME_TV_SEC - MONOTIME_TV_SEC, .tv_nsec = REALTIME_TV_NSEC - MONOTIME_TV_NSEC + 1},
        .str = "2020-04-28T10:26:53.444444001Z",
        .monoBefore = true},
};

static amxd_dm_t myDm;
static amxd_object_t* myObj;

struct timespec myMonoTime = {.tv_sec = MONOTIME_TV_SEC, .tv_nsec = MONOTIME_TV_NSEC};
struct timespec myRealTime = {.tv_sec = REALTIME_TV_SEC, .tv_nsec = REALTIME_TV_NSEC};

static int s_setupSuite(void** state) {
    (void) state;
    for(size_t i = 0; i < SWL_ARRAY_SIZE(vals); i++) {
        amxc_ts_parse(&vals[i].timeStamp, vals[i].str, strlen(vals[i].str));
        vals[i].localStr = strdup(vals[i].str);
    }

    amxd_param_t* param = NULL;

    assert_int_equal(amxd_dm_init(&myDm), 0);

    assert_int_equal(amxd_object_new(&myObj, amxd_object_singleton, "parent"), 0);
    assert_int_equal(amxd_dm_add_root_object(&myDm, myObj), 0);

    assert_non_null(myObj);

    assert_int_equal(amxd_param_new(&param, "MyTime", AMXC_VAR_ID_TIMESTAMP), 0);
    assert_int_equal(amxd_object_add_param(myObj, param), amxd_status_ok);


    ttb_mockClock_init(&myMonoTime, &myRealTime);

    return 0;
}

static int s_teardownSuite(void** state) {
    (void) state;

    for(size_t i = 0; i < SWL_ARRAY_SIZE(vals); i++) {
        free(vals[i].localStr);
    }
    amxd_dm_clean(&myDm);

    return 0;
}


static void test_swl_typetimeSpec_mapAddMono(void** state _UNUSED) {
    for(size_t i = 0; i < SWL_ARRAY_SIZE(vals); i++) {
        amxc_var_t myMap;
        amxc_var_init(&myMap);
        amxc_var_set_type(&myMap, AMXC_VAR_ID_HTABLE);
        swl_typeTimeSpecMono_addToMap(&myMap, "MyTime", vals[i].monoTime);

        char* timeStr = amxc_var_get_cstring_t(amxc_var_get_key(&myMap, "MyTime", AMXC_VAR_FLAG_DEFAULT));
        ttb_assert_str_eq(timeStr, vals[i].str);
        free(timeStr);


        const amxc_ts_t* time = amxc_var_constcast(amxc_ts_t, amxc_var_get_key(&myMap, "MyTime", AMXC_VAR_FLAG_DEFAULT));
        assert_memory_equal(time, &vals[i].timeStamp, sizeof(amxc_ts_t));
        amxc_var_clean(&myMap);
    }
}

static void test_swl_typetimeSpec_mapAddReal(void** state _UNUSED) {
    for(size_t i = 0; i < SWL_ARRAY_SIZE(vals); i++) {
        amxc_var_t myMap;
        amxc_var_init(&myMap);
        amxc_var_set_type(&myMap, AMXC_VAR_ID_HTABLE);
        swl_typeTimeSpecReal_addToMap(&myMap, "MyTime", vals[i].realTime);

        char* timeStr = amxc_var_get_cstring_t(amxc_var_get_key(&myMap, "MyTime", AMXC_VAR_FLAG_DEFAULT));
        ttb_assert_str_eq(timeStr, vals[i].str);
        free(timeStr);


        const amxc_ts_t* time = amxc_var_constcast(amxc_ts_t, amxc_var_get_key(&myMap, "MyTime", AMXC_VAR_FLAG_DEFAULT));
        assert_memory_equal(time, &vals[i].timeStamp, sizeof(amxc_ts_t));
        amxc_var_clean(&myMap);
    }
}

#define NR_MONO_TEST_VALUES 3
swl_timeSpecMono_t monoBase[NR_MONO_TEST_VALUES] = {
    {.tv_sec = TEST1_MONOTIME, .tv_nsec = TEST1_MONOTIME_NSEC},
    {.tv_sec = TEST2_MONOTIME, .tv_nsec = TEST2_MONOTIME_NSEC},
    {.tv_sec = TEST1_MONOTIME, .tv_nsec = TEST1_MONOTIME_NSEC},
};
size_t monoCount[NR_MONO_TEST_VALUES] = {2, 1, 2};
swl_timeSpecMono_t monoNotContains[NR_MONO_TEST_VALUES] = {
    {.tv_sec = TEST1_MONOTIME, .tv_nsec = TEST1_MONOTIME_NSEC + 1},
    {.tv_sec = TEST2_MONOTIME + 1, .tv_nsec = TEST2_MONOTIME_NSEC},
    {.tv_sec = TEST1_MONOTIME, .tv_nsec = TEST1_MONOTIME_NSEC - 1},
};
#define monoStrChar TEST1_REALTIME_STR ","TEST2_REALTIME_STR ","TEST1_REALTIME_STR
#define monoStr2Char TEST1_REALTIME_STR ";;"TEST2_REALTIME_STR ";;"TEST1_REALTIME_STR
const char* monotestStrValuesChar[NR_MONO_TEST_VALUES] = {TEST1_REALTIME_STR, TEST2_REALTIME_STR, TEST1_REALTIME_STR};

#define NR_REAL_TEST_VALUES 3
swl_timeSpecReal_t realBase[NR_REAL_TEST_VALUES] = {
    {.tv_sec = TEST1_REALTIME, .tv_nsec = TEST1_REALTIME_NSEC},
    {.tv_sec = TEST2_REALTIME, .tv_nsec = TEST2_REALTIME_NSEC},
    {.tv_sec = TEST1_REALTIME, .tv_nsec = TEST1_REALTIME_NSEC},
};
size_t realCount[NR_REAL_TEST_VALUES] = {2, 1, 2};
swl_timeSpecReal_t realNotContains[NR_REAL_TEST_VALUES] = {
    {.tv_sec = TEST1_REALTIME, .tv_nsec = TEST1_REALTIME_NSEC + 1},
    {.tv_sec = TEST2_REALTIME + 1, .tv_nsec = TEST2_REALTIME_NSEC},
    {.tv_sec = TEST1_REALTIME, .tv_nsec = TEST1_REALTIME_NSEC - 1},
};
#define realStrChar TEST1_REALTIME_STR ","TEST2_REALTIME_STR ","TEST1_REALTIME_STR
#define realStr2Char TEST1_REALTIME_STR ";;"TEST2_REALTIME_STR ";;"TEST1_REALTIME_STR
const char* realtestStrValuesChar[NR_REAL_TEST_VALUES] = {TEST1_REALTIME_STR, TEST2_REALTIME_STR, TEST1_REALTIME_STR};

void* s_varToMonoData(amxc_var_t* variant) {
    swl_datetime_t time;
    amxc_ts_t* tm = amxc_var_dyncast(amxc_ts_t, variant);
    if(amxc_ts_to_tm_utc(tm, &time.datetime) < 0) {
        free(tm);
        return NULL;
    }
    time.nanoseconds = tm->nsec;
    printf("PRINT %u %u\n", tm->nsec, variant->type_id);

    swl_timeSpecMono_t* tgtData = calloc(1, sizeof(swl_timeSpecMono_t));
    swl_timespec_datetimeToMono(tgtData, &time);
    free(tm);
    return tgtData;
}

void* s_varToRealData(amxc_var_t* variant) {
    swl_datetime_t time;
    amxc_ts_t* tm = amxc_var_dyncast(amxc_ts_t, variant);
    if(amxc_ts_to_tm_utc(tm, &time.datetime) < 0) {
        free(tm);
        return NULL;
    }
    time.nanoseconds = tm->nsec;

    swl_timeSpecReal_t* tgtData = calloc(1, sizeof(swl_timeSpecReal_t));
    swl_timespec_datetimeToReal(tgtData, &time);
    free(tm);
    return tgtData;
}

testData_t timeMonoData = {
    .name = "monoTime",
    .testType = swl_type_timeSpecMono,
    .serialStr = monoStrChar,
    .serialStr2 = monoStr2Char,
    .strValues = monotestStrValuesChar,
    .baseValues = &monoBase,
    .valueCount = monoCount,
    .notContains = &monoNotContains,
    .nrTestValues = NR_MONO_TEST_VALUES,
    .targetType = AMXC_VAR_ID_TIMESTAMP,
    .varToData = s_varToMonoData
};

testData_t timeRealData = {
    .name = "realTime",
    .testType = swl_type_timeSpecReal,
    .serialStr = realStrChar,
    .serialStr2 = realStr2Char,
    .strValues = realtestStrValuesChar,
    .baseValues = &realBase,
    .valueCount = realCount,
    .notContains = &realNotContains,
    .nrTestValues = NR_MONO_TEST_VALUES,
    .targetType = AMXC_VAR_ID_TIMESTAMP,
    .varToData = s_varToRealData
};

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlTimeSpec ");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_typetimeSpec_mapAddMono),
        cmocka_unit_test(test_swl_typetimeSpec_mapAddReal),
    };
    int rc = cmocka_run_group_tests(tests, s_setupSuite, s_teardownSuite);

    runCommonTypeTest(&timeMonoData);
    runCommonTypeTest(&timeRealData);

    sahTraceClose();
    return rc;
}
