/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>

#include "swla/swla_common_amx.h"

#include "swla/swla_commonLib.h"
#include "swla/swla_type.h"
#include "swla/swla_table.h"
#include "swla/swla_namedTupleType.h"

#define NR_TYPES 3
#define NR_VALUES 4


#define MY_TEST_TABLE_VAR(X, Y) \
    X(Y, gtSwl_type_int32, index, "SmallInt") \
    X(Y, gtSwl_type_int64, key, "BigInt") \
    X(Y, gtSwl_type_charPtr, val, "String")

SWL_NTT(myTestNTTType, swl_myTestNTT_t, MY_TEST_TABLE_VAR, )


swl_myTestNTT_t testVals[NR_VALUES] = {
    {1, 100, "test1"},
    {2, -200, "test2"},
    {-8852, 289870, "foobar"},
    {8851, -289869, "barfoo"},
};

swl_myTestNTT_t testValsInverse[NR_VALUES] = {
    {8851, -289869, "barfoo"},
    {-8852, 289870, "foobar"},
    {2, -200, "test2"},
    {1, 100, "test1"},
};


#define NR_OTHER_DATA 2

swl_myTestNTT_t otherData[NR_OTHER_DATA] = {
    {5, 500, "notHere"},
    {-5, -500, "alsoNotHere"},
};

char* names[NR_TYPES] = {"SmallInt", "BigInt", "String"};

static amxd_dm_t s_myDm;
amxd_object_t* s_myObj;

static int s_setupSuite(void** state _UNUSED) {
    amxd_param_t* param = NULL;

    assert_int_equal(amxd_dm_init(&s_myDm), 0);

    assert_int_equal(amxd_object_new(&s_myObj, amxd_object_singleton, "parent"), 0);
    assert_int_equal(amxd_dm_add_root_object(&s_myDm, s_myObj), 0);
    assert_non_null(s_myObj);

    assert_int_equal(amxd_param_new(&param, "SmallInt", AMXC_VAR_ID_INT32), 0);
    assert_int_equal(amxd_object_add_param(s_myObj, param), amxd_status_ok);
    assert_int_equal(amxd_param_new(&param, "BigInt", AMXC_VAR_ID_INT64), 0);
    assert_int_equal(amxd_object_add_param(s_myObj, param), amxd_status_ok);
    assert_int_equal(amxd_param_new(&param, "String", AMXC_VAR_ID_CSTRING), 0);
    assert_int_equal(amxd_object_add_param(s_myObj, param), amxd_status_ok);
    assert_true(amxd_object_get_param_value(s_myObj, "SmallInt") != NULL);
    return 0;
}

static int s_teardownSuite(void** state _UNUSED) {

    amxd_dm_clean(&s_myDm);
    return 0;
}

static void test_swl_ntt_toFromMap(void** state _UNUSED) {

    for(size_t i = 0; i < NR_VALUES; i++) {
        amxc_var_t tmpMap;
        amxc_var_init(&tmpMap);
        amxc_var_set_type(&tmpMap, AMXC_VAR_ID_HTABLE);

        swl_ntt_toMap(&tmpMap, &myTestNTTType, &testVals[i]);

        for(size_t j = 0; j < NR_TYPES; j++) {
            amxc_var_t tmpVar;
            amxc_var_init(&tmpVar);
            swl_type_t* tmpType = myTestNTTType.type.types[j];
            swl_type_toVariant(tmpType, &tmpVar, swl_ntt_getValByIndex(&myTestNTTType, &testVals[i], j));
            int diff = 0;

            amxc_var_t* mapVar = amxc_var_get_key(&tmpMap, names[j], AMXC_VAR_FLAG_DEFAULT);
            assert_int_equal(amxc_var_compare(&tmpVar, mapVar, &diff), 0);
            amxc_var_clean(&tmpVar);

        }

        swl_myTestNTT_t val;
        memset(&val, 0, sizeof(swl_myTestNTT_t));

        swl_ntt_fromMap(&tmpMap, &myTestNTTType, &val);

        assert_true(swl_ntt_equals(&myTestNTTType, &val, &testVals[i]));

        swl_tupleType_cleanup(&myTestNTTType.type, &val);
        amxc_var_clean(&tmpMap);
    }
}


static void test_swl_ntt_toFromObject(void** state _UNUSED) {
    for(size_t i = 0; i < NR_VALUES; i++) {

        assert_true(swl_ntt_toObject(s_myObj, &myTestNTTType, &testVals[i]));

        for(size_t j = 0; j < NR_TYPES; j++) {
            amxc_var_t tmpVar;
            amxc_var_init(&tmpVar);
            swl_type_t* tmpType = myTestNTTType.type.types[j];
            swl_type_toVariant(tmpType, &tmpVar, swl_ntt_getValByIndex(&myTestNTTType, &testVals[i], j));
            int diff = 0;

            amxc_var_t mapVar;
            amxc_var_init(&mapVar);
            amxd_status_t status = amxd_object_get_param(s_myObj, names[j], &mapVar);
            assert_int_equal(status, amxd_status_ok);


            assert_int_equal(amxc_var_compare(&tmpVar, &mapVar, &diff), 0);
            assert_true(diff == 0);


            amxc_var_clean(&mapVar);
            amxc_var_clean(&tmpVar);
        }

        swl_myTestNTT_t val;
        memset(&val, 0, sizeof(swl_myTestNTT_t));

        swl_ntt_fromObject(s_myObj, &myTestNTTType, &val);

        assert_true(swl_ntt_equals(&myTestNTTType, &val, &testVals[i]));

        swl_tupleType_cleanup(&myTestNTTType.type, &val);
    }
}



static void test_swl_ntt_toFromObjectChanged(void** state _UNUSED) {
    for(size_t i = 0; i < (1 << NR_TYPES); i++) {
        assert_true(swl_ntt_toObject(s_myObj, &myTestNTTType, &testVals[0]));

        swl_myTestNTT_t tmpData;
        memset(&tmpData, 0, sizeof(swl_myTestNTT_t));
        swl_tupleType_copyByMask(&tmpData, &myTestNTTType.type, &testVals[0], ~i);
        swl_tupleType_copyByMask(&tmpData, &myTestNTTType.type, &otherData[0], i);


        swl_mask64_m tmpMask = 0;
        assert_true(swl_ntt_toObjectUpdate(s_myObj, &myTestNTTType, &tmpData, &tmpMask));
        assert_int_equal(tmpMask, i);

        assert_true(swl_ntt_toObject(s_myObj, &myTestNTTType, &testVals[0]));
        tmpMask = 0;
        assert_true(swl_ntt_fromObjectUpdate(s_myObj, &myTestNTTType, &tmpData, &tmpMask));
        assert_int_equal(tmpMask, i);


        swl_tupleType_cleanup(&myTestNTTType.type, &tmpData);
    }
}

static void test_swl_ntt_toFromArgs(void** state _UNUSED) {
    for(size_t i = 0; i < NR_VALUES; i++) {
        amxc_var_t argList;
        amxc_var_init(&argList);
        amxc_var_set_type(&argList, AMXC_VAR_ID_HTABLE);

        for(size_t j = 0; j < NR_TYPES; j++) {
            amxc_var_t* tmpVar = amxc_var_add_new_key(&argList, names[j]);
            swl_type_t* tmpType = myTestNTTType.type.types[j];
            swl_type_toVariant(tmpType, tmpVar, swl_ntt_getValByIndex(&myTestNTTType, &testVals[i], j));
        }

        const amxc_htable_t* myTable = amxc_var_get_const_amxc_htable_t(&argList);

        assert_true(amxc_htable_size(myTable) == NR_TYPES);

        swl_myTestNTT_t val;
        memset(&val, 0, sizeof(swl_myTestNTT_t));

        swl_ntt_fromArgs(&argList, &myTestNTTType, &val, NULL);

        assert_true(swl_ntt_equals(&myTestNTTType, &val, &testVals[i]));

        swl_tupleType_cleanup(&myTestNTTType.type, &val);

        amxc_var_clean(&argList);
    }
}



static void test_swl_ntt_toFromVar(void** state _UNUSED) {
    for(size_t i = 0; i < NR_VALUES; i++) {
        amxc_var_t var;
        amxc_var_init(&var);

        assert_true(swl_ntt_toVariant(&myTestNTTType, &var, &testVals[i]));

        assert_true(var.type_id == AMXC_VAR_ID_HTABLE);
        const amxc_htable_t* varMap = amxc_var_get_const_amxc_htable_t(&var);

        for(size_t j = 0; j < NR_TYPES; j++) {
            amxc_var_t tmpVar;
            amxc_var_init(&tmpVar);

            swl_type_t* tmpType = myTestNTTType.type.types[j];
            swl_type_toVariant(tmpType, &tmpVar, swl_ntt_getValByName(&myTestNTTType, &testVals[i], names[j]));
            int diff = 0;


            amxc_htable_it_t* it = amxc_htable_get(varMap, names[j]);
            amxc_var_t* mapVar = amxc_htable_it_get_data(it, amxc_var_t, hit);

            assert_int_equal(amxc_var_compare(&tmpVar, mapVar, &diff), 0);
            assert_true(diff == 0);

            amxc_var_clean(&tmpVar);
        }

        swl_myTestNTT_t val;
        memset(&val, 0, sizeof(swl_myTestNTT_t));

        swl_ntt_fromVariant(&myTestNTTType, &val, &var);

        assert_true(swl_ntt_equals(&myTestNTTType, &val, &testVals[i]));

        swl_ntt_cleanup(&myTestNTTType, &val);
        amxc_var_clean(&var);
    }
}


int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlConv");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_ntt_toFromMap),
        cmocka_unit_test(test_swl_ntt_toFromObject),
        cmocka_unit_test(test_swl_ntt_toFromArgs),
        cmocka_unit_test(test_swl_ntt_toFromObjectChanged),
        cmocka_unit_test(test_swl_ntt_toFromVar),

    };
    int rc = 0;
    rc = cmocka_run_group_tests(tests, s_setupSuite, s_teardownSuite);
    sahTraceClose();
    return rc;
}
