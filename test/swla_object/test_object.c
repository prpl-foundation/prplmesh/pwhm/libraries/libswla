/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>
#include <libgen.h>
#include <debug/sahtrace.h>
#include "test-toolbox/ttb_amx.h"
#include "test-toolbox/ttb_mockClock.h"
#include "test-toolbox/ttb_mockTimer.h"
#include "test-toolbox/ttb_timespec.h"
#include "test-toolbox/ttb_object.h"

#include "swla/swla_common_amx.h"
#include "swla/swla_namedTupleType.h"
#include "swl/ttb/swl_ttb.h"
#include "swla/swla_object.h"


/**
 * @file Test for testing that we can call a amx function from a unittest.
 *
 * Can also be used as an example of a unittest that calls a amx function.
 */

#ifndef _UNUSED
#define _UNUSED __attribute__((unused))
#endif

static bool s_calledFun = false;

#define MY_TEST_TABLE_VAR(X, Y) \
    X(Y, gtSwl_type_int32, index, "SmallInt") \
    X(Y, gtSwl_type_int64, key, "BigInt") \
    X(Y, gtSwl_type_charPtr, val, "String") \
    X(Y, gtSwl_type_charBuf32, buf, "Name")

SWL_NTT(myTestNTTType, swl_myTestNTT_t, MY_TEST_TABLE_VAR, );

swl_myTestNTT_t s_input = {.index = 123, .key = 456, .val = "fooBar", .buf = "abc"};
swl_myTestNTT_t s_output = {.index = 321, .key = 654, .val = "barFoo", .buf = ""};

/**
 * Implementation of amx/odl function - normally this is not in your test
 * (except if you want to mock them).
 */

static amxd_status_t s_exampleFunction_cbf(_UNUSED amxd_object_t* object,
                                           _UNUSED amxd_function_t* func,
                                           _UNUSED amxc_var_t* args,
                                           _UNUSED amxc_var_t* ret) {
    s_calledFun = true;

    swl_myTestNTT_t inVar;
    swl_ntt_fromVariant(&myTestNTTType, &inVar, args);
    assert_true(swl_type_equals((swl_type_t*) &myTestNTTType, &inVar, &s_input));

    char buf[128] = {0};
    swl_type_toChar((swl_type_t*) &myTestNTTType, buf, sizeof(buf), &inVar);
    printf("In %s\n", buf);

    swl_type_cleanup((swl_type_t*) &myTestNTTType, &inVar);

    swl_type_toVariant((swl_type_t*) &myTestNTTType.type, ret, &s_output);

    printf("INVOKE \n");
    return amxd_status_ok;
}




static int s_setupSuite(void** state) {
    ttb_amx_t* dm = ttb_amx_init();
    *state = dm;
    assert_int_equal(amxo_resolver_ftab_add(&dm->parser, "exampleFunction", AMXO_FUNC(s_exampleFunction_cbf)), 0);

    ttb_amx_loadDm(dm, "datamodel.odl", "Example");

    ttb_mockClock_initNow();
    assert_non_null(*state);
    return 0;
}

static int s_teardownSuite(void** state) {
    bool ok = ttb_amx_cleanup(*state);
    *state = NULL;
    assert_true(ok);

    return 0;
}

typedef struct {
    bool isDone;
    bool errorSeen;
} funResult_t;



static void test_simpleFunCall(void** state) {
    ttb_amx_t* ttbBus = *state;
    ttb_object_t* obj = ttbBus->rootObj;

    ttb_var_t* reqVar = ttb_object_createArgs();
    assert_non_null(reqVar);

    swl_type_toVariant((swl_type_t*) &myTestNTTType.type, reqVar, &s_input);
    s_calledFun = false;

    ttb_var_t* replyVar = NULL;
    ttb_reply_t* reply = ttb_object_callFun(ttbBus, obj, "exampleFunction", &reqVar, &replyVar);
    assert_true(ttb_object_replySuccess(reply));


    swl_myTestNTT_t outVar;
    swl_type_fromVariant((swl_type_t*) &myTestNTTType.type, &outVar, replyVar);
    assert_true(swl_type_equals((swl_type_t*) &myTestNTTType.type, &outVar, &s_output));

    char buf[128] = {0};
    swl_type_toChar((swl_type_t*) &myTestNTTType, buf, sizeof(buf), &outVar);
    printf("Out %s\n", buf);

    swl_type_cleanup((swl_type_t*) &myTestNTTType.type, &outVar);
    assert_true(s_calledFun);

    ttb_object_cleanReply(&reply, &replyVar);
}

static void test_transaction(void** state) {

    ttb_amx_t* ttbBus = *state;
    ttb_object_t* obj = ttbBus->rootObj;
    ttb_object_t* subObj = ttb_object_getChildObject(obj, "SubObject");

    ttb_object_t* tempObj = ttb_object_getChildObject(obj, "MultiObj");

    amxd_trans_t trans;
    assert_int_equal(swl_object_prepareTransaction(&trans, subObj), amxd_status_ok);
    assert_int_equal(swl_type_toTransParamString((swl_type_t*) &myTestNTTType, &trans, "Test", &s_input), 1);
    assert_int_equal(swl_object_finalizeTransaction(&trans, &ttbBus->dm), amxd_status_ok);

    swl_myTestNTT_t outVar;
    memset(&outVar, 0, sizeof(swl_myTestNTT_t));
    assert_int_equal(swl_type_fromObjectParamString((swl_type_t*) &myTestNTTType, subObj, "Test", &outVar), 1);
    swl_ttb_assertTypeValEquals((swl_type_t*) &myTestNTTType, s_input, outVar);

    swl_type_cleanup((swl_type_t*) &myTestNTTType, &outVar);

    amxd_object_t* object = NULL;
    amxd_object_new_instance(&object, tempObj, "Test1", 0, NULL);

    assert_int_equal(swl_object_prepareTransaction(&trans, object), amxd_status_ok);
    assert_int_equal(swl_type_toTransParamString((swl_type_t*) &myTestNTTType, &trans, "TestKey", &s_input), 1);
    assert_int_equal(swl_object_finalizeTransaction(&trans, &ttbBus->dm), amxd_status_ok);


    assert_int_equal(swl_type_fromObjectParamString((swl_type_t*) &myTestNTTType, subObj, "Test", &outVar), 1);
    swl_ttb_assertTypeValEquals((swl_type_t*) &myTestNTTType, s_input, outVar);
    swl_type_cleanup((swl_type_t*) &myTestNTTType, &outVar);


}

static void test_referenceObj(void** state) {
    ttb_amx_t* ttbBus = *state;
    ttb_object_t* obj = ttbBus->rootObj;
    ttb_object_t* subObj = ttb_object_getChildObject(obj, "SubObject");

    ttb_object_t* tempObj = ttb_object_getChildObject(obj, "MultiObj");

    amxd_object_t* object = NULL;
    amxd_object_new_instance(&object, tempObj, "Referrer1", 0, NULL);
    //success case: reference path uses common root object name
    amxd_object_t* referenceObject = swla_object_getReferenceObject(object, "Example.SubObject");
    assert_ptr_equal(referenceObject, subObj);
    //success case: reference path has root prefix, but includes common root object name
    referenceObject = swla_object_getReferenceObject(object, "AnyPrefix.Example.SubObject");
    assert_ptr_equal(referenceObject, subObj);
    //error case: missing the common root object in the reference path
    referenceObject = swla_object_getReferenceObject(object, "AnyPrefix.SubObject");
    assert_null(referenceObject);
    //error case: totally unmatching reference path
    referenceObject = swla_object_getReferenceObject(object, "AnyPrefix.AnyObject");
    assert_null(referenceObject);
}

static void test_referenceObjPriv(void** state) {
    ttb_amx_t* ttbBus = *state;
    ttb_object_t* obj = ttbBus->rootObj;
    ttb_object_t* subObj = ttb_object_getChildObject(obj, "SubObject");
    void* oldPrivData = subObj->priv;

    //setting object private data
    subObj->priv = subObj;

    ttb_object_t* tempObj = ttb_object_getChildObject(obj, "MultiObj");

    amxd_object_t* object = NULL;
    amxd_object_new_instance(&object, tempObj, "Referrer2", 0, NULL);
    //success case: reference path uses common root object name
    void* refPrivData = swla_object_getReferenceObjectPriv(object, "Example.SubObject");
    assert_ptr_equal(refPrivData, subObj);
    //success case: reference path has root prefix, but includes common root object name
    refPrivData = swla_object_getReferenceObjectPriv(object, "AnyPrefix.Example.SubObject");
    assert_ptr_equal(refPrivData, subObj);
    //error case: missing the common root object in the reference path
    refPrivData = swla_object_getReferenceObjectPriv(object, "AnyPrefix.SubObject");
    assert_null(refPrivData);
    //error case: totally unmatching reference path
    refPrivData = swla_object_getReferenceObjectPriv(object, "AnyPrefix.AnyObject");
    assert_null(refPrivData);

    //restore old private data
    subObj->priv = oldPrivData;
}

static void test_dumpObjToMap(void** state) {
    ttb_amx_t* ttbBus = *state;
    ttb_object_t* obj = ttbBus->rootObj;
    ttb_object_t* subObj = ttb_object_getChildObject(obj, "SubObject");
    assert_non_null(subObj);
    amxc_var_t retval;
    amxc_var_init(&retval);
    amxc_var_set_type(&retval, AMXC_VAR_ID_HTABLE);
    amxd_status_t status = swla_object_paramsToMap(&retval, subObj);
    assert_int_equal(status, amxd_status_ok);
    assert_string_equal(GET_CHAR(&retval, "Test"), "ABC");
    amxc_var_clean(&retval);
}

static void test_addChildObjToMap(void** state) {
    ttb_amx_t* ttbBus = *state;
    ttb_object_t* obj = ttbBus->rootObj;
    ttb_object_t* subObj = ttb_object_getChildObject(obj, "SubObject");
    amxc_var_t retval;
    amxc_var_init(&retval);
    amxc_var_set_type(&retval, AMXC_VAR_ID_HTABLE);
    amxd_status_t status = swla_object_addChildParamsToMap(&retval, obj, "SubObject");
    assert_int_equal(status, amxd_status_ok);
    assert_string_equal(GETP_CHAR(&retval, "SubObject.Test"), "ABC");
    SWLA_OBJECT_SET_PARAM_CSTRING(subObj, "Test", "CBA");
    amxc_var_set_type(&retval, AMXC_VAR_ID_HTABLE);
    status = swla_object_addChildParamsToMap(&retval, obj, "SubObject");
    assert_int_equal(status, amxd_status_ok);
    assert_string_equal(GETP_CHAR(&retval, "SubObject.Test"), "CBA");
    amxc_var_clean(&retval);
}

static void test_getFirstAvailableIndex(void** state) {
    ttb_amx_t* ttbBus = *state;
    ttb_object_t* obj = ttbBus->rootObj;
    ttb_object_t* tempObj = ttb_object_getChildObject(obj, "MultiObj");

    int index = swla_object_getFirstAvailableIndex(tempObj);
    assert_int_equal(index, 4);

    amxd_object_t* instObj1 = NULL;
    amxd_object_new_instance(&instObj1, tempObj, "Instance1", index, NULL);

    assert_int_equal(instObj1->index, 4);
    index = swla_object_getFirstAvailableIndex(tempObj);
    assert_int_equal(index, 5);

    amxd_object_t* instObj2 = NULL;
    amxd_object_new_instance(&instObj2, tempObj, "Instance2", index, NULL);

    assert_int_equal(instObj2->index, 5);
    assert_int_equal(swla_object_getFirstAvailableIndex(tempObj), 6);

    amxd_object_delete(&instObj1);

    index = swla_object_getFirstAvailableIndex(tempObj);
    assert_int_equal(index, 4);

    amxd_object_t* instObj3 = NULL;
    amxd_object_new_instance(&instObj3, tempObj, "Instance3", index, NULL);

    assert_int_equal(instObj3->index, 4);
    assert_int_equal(swla_object_getFirstAvailableIndex(tempObj), 6);
    assert_int_equal(swla_object_getFirstAvailableIndex(instObj3), 0);

    amxd_object_delete(&instObj2);
    amxd_object_delete(&instObj3);

    assert_int_equal(swla_object_getFirstAvailableIndex(tempObj), 4);

    amxd_object_for_each(instance, it, tempObj) {
        amxd_object_t* instObj = amxc_container_of(it, amxd_object_t, it);
        amxd_object_delete(&instObj);
    }

    assert_int_equal(swla_object_getFirstAvailableIndex(tempObj), 1);
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {

    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_dumpObjToMap),
        cmocka_unit_test(test_addChildObjToMap),
        cmocka_unit_test(test_simpleFunCall),
        cmocka_unit_test(test_transaction),
        cmocka_unit_test(test_referenceObj),
        cmocka_unit_test(test_referenceObjPriv),
        cmocka_unit_test(test_getFirstAvailableIndex),
    };
    return cmocka_run_group_tests(tests, s_setupSuite, s_teardownSuite);
}
