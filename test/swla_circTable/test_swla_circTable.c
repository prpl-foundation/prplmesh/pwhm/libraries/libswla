/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>
#include <amxc/amxc.h>

#include <debug/sahtrace.h>
#include "swla/swla_tupleType.h"
#include "swla/swla_conversion.h"
#include "swl/swl_common.h"
#include "swla/swla_circTable.h"
#include "swl/swl_common.h"
#include "swla/swla_commonLib.h"
#include "swla/swla_type.h"
#include "test-toolbox/ttb_assert.h"

#define NR_TYPES 3
#define NR_VALUES 4

SWL_TABLE(myTestTable,
          ARR(int32_t index; int64_t key; char* val; ),
          ARR(swl_type_int32, swl_type_int64, swl_type_charPtr),
          ARR(
              {1, 100, "test1"},
              {2, -200, "test2"},
              {-8852, 289870, "foobar"},
              {8851, -289869, "barfoo"},
              )
          );
char* strings[NR_TYPES][NR_VALUES] = {{"1", "2", "-8852", "8851"},
    {"100", "-200", "289870", "-289869"},
    {"test1", "test2", "foobar", "barfoo"}};


char* names[NR_TYPES] = {"SmallInt", "BigInt", "String"};

static uint32_t s_nrElFilled = 0;
static bool s_filled = false;
static uint32_t s_nrListValues = 0;

static char** s_curTargetStringList[NR_TYPES];

static char s_curTargetString[NR_TYPES][256];

#define NR_OTHER_DATA 2

myTestTableStruct_t otherData[NR_OTHER_DATA] = {
    {5, 500, "notHere"},
    {-5, -500, "alsoNotHere"},
};


static void s_cleanupArrays() {
    for(int i = 0; i < NR_TYPES; i++) {
        swl_type_arrayCleanup(&swl_type_charPtr_impl, s_curTargetStringList[i], s_nrListValues);
        memset(s_curTargetString[i], 0, 256);
    }
    s_nrElFilled = 0;
    s_filled = false;
}

static void s_initTargetList(size_t nrValues) {
    for(size_t i = 0; i < NR_TYPES; i++) {
        s_curTargetStringList[i] = calloc(1, nrValues * sizeof(char*));
    }
    s_nrListValues = nrValues;
}


//
static void s_arraysResize(uint32_t newNrValues) {
    if(s_nrListValues == newNrValues) {
        return;
    }
    s_cleanupArrays();
    for(size_t i = 0; i < NR_TYPES; i++) {
        free(s_curTargetStringList[i]);
        s_curTargetStringList[i] = calloc(1, newNrValues * sizeof(char*));

    }
    s_nrListValues = newNrValues;
    s_nrElFilled = 0;
}

static void s_cleanupTargetList() {
    for(size_t i = 0; i < NR_TYPES; i++) {
        if(s_curTargetStringList[i] != NULL) {
            free(s_curTargetStringList[i]);
            s_curTargetStringList[i] = NULL;
        }
    }
    s_nrListValues = 0;
}

static void s_printCircTable(swl_circTable_t* table) {
    amxc_string_t string;
    amxc_string_init(&string, 0);

    swl_circTable_toString(table, &string);
    printf("%s\n", amxc_string_get(&string, 0));

    amxc_string_clean(&string);
}

static void s_printTable(swl_table_t* table) {
    amxc_string_t string;
    amxc_string_init(&string, 0);

    swl_table_toString(table, &string, NULL);
    printf("%s\n", amxc_string_get(&string, 0));

    amxc_string_clean(&string);
}

static void s_checkCircTableStr(swl_circTable_t* table) {
    amxc_var_t myMap;
    amxc_var_init(&myMap);
    amxc_var_set_type(&myMap, AMXC_VAR_ID_HTABLE);

    swl_circTable_mapAdd(&myMap, table);

    // Test getMap
    assert_int_equal(amxc_htable_size(amxc_var_get_const_amxc_htable_t(&myMap)), NR_TYPES);
    for(size_t i = 0; i < NR_TYPES; i++) {
        ttb_assert_str_eq(amxc_var_constcast(cstring_t, amxc_var_get_key(&myMap, names[i], AMXC_VAR_FLAG_DEFAULT)), s_curTargetString[i]);
    }

    // Test getMapColumn
    amxc_var_clean(&myMap);
    amxc_var_set_type(&myMap, AMXC_VAR_ID_HTABLE);
    for(size_t i = 0; i < NR_TYPES; i++) {
        swl_circTable_mapAddColumn(&myMap, table, i);
        amxc_var_t* var = amxc_var_get_key(&myMap, names[i], AMXC_VAR_FLAG_DEFAULT);
        char* data = amxc_var_get_cstring_t(var);

        assert_string_equal(data, s_curTargetString[i]);
        free(data);
        assert_int_equal(amxc_htable_size(amxc_var_get_const_amxc_htable_t(&myMap)), 1);
        amxc_var_clean(&myMap);
        amxc_var_set_type(&myMap, AMXC_VAR_ID_HTABLE);
    }

    // Test map getColumn
    for(size_t i = 0; i < NR_TYPES; i++) {
        size_t bufSize = myTestTableTypes[i]->size * s_nrListValues;
        char dataBuffer[bufSize];
        memset(dataBuffer, 0, bufSize);

        swl_circTable_columnToArray(dataBuffer, s_nrListValues, table, i);
        char buffer[256] = {0};
        swl_type_arrayToChar(myTestTableTypes[i], buffer, sizeof(buffer), dataBuffer, table->table.curNrTuples);
        assert_string_equal(buffer, s_curTargetString[i]);
    }

    amxc_var_clean(&myMap);
}



static void s_addValue(swl_circTable_t* table, size_t index) {
    swl_circTable_addValues(table, swl_table_getTuple(&myTestTable, index));

    // update debug array

    for(int i = 0; i < NR_TYPES; i++) {
        char* addStr = strings[i][index];
        if(s_filled) {
            swl_type_arrayAppend(&swl_type_charPtr_impl, s_curTargetStringList[i], s_nrListValues, addStr, -1);
            swl_typeCharPtr_arrayToChar(s_curTargetString[i], 256, s_curTargetStringList[i], s_nrElFilled);
        } else {
            swl_type_arraySet(&swl_type_charPtr_impl, s_curTargetStringList[i], s_nrListValues, addStr, s_nrElFilled);
            swl_typeCharPtr_arrayToChar(s_curTargetString[i], 256, s_curTargetStringList[i], s_nrElFilled + 1);
        }
    }

    s_checkCircTableStr(table);

    if(!s_filled) {
        s_nrElFilled += 1;
    }

    if(s_nrElFilled == s_nrListValues) {
        s_filled = true;
    }
    assert_int_equal(swl_circTable_isFilled(table), s_filled);
    assert_int_equal(swl_circTable_getNrFilled(table), s_nrElFilled);
}


static void test_swl_circTable_mapAddGet(void** state _UNUSED) {
    s_initTargetList(NR_VALUES);
    swl_circTable_t testTable = {0};
    s_nrElFilled = 0;
    swl_circTable_init(&testTable, myTestTable.tupleType, myTestTable.nrTuples);

    s_printTable(&myTestTable);

    swl_circTable_setNames(&testTable, names);

    s_addValue(&testTable, 0);
    s_addValue(&testTable, 1);
    s_addValue(&testTable, 2);
    s_addValue(&testTable, 3);
    assert_true(swl_table_equals(&testTable.table, &myTestTable));

    s_addValue(&testTable, 3);
    s_addValue(&testTable, 2);
    s_addValue(&testTable, 1);
    s_addValue(&testTable, 0);

    s_addValue(&testTable, 0);
    s_addValue(&testTable, 1);
    s_addValue(&testTable, 2);
    s_addValue(&testTable, 3);

    assert_true(swl_table_equals(&testTable.table, &myTestTable));


    swl_circTable_destroy(&testTable);
    s_cleanupArrays();
    s_cleanupTargetList();
}

static void test_swl_circTable_resize(void** state _UNUSED) {
    s_initTargetList(NR_VALUES);
    swl_circTable_t testTable = {0};
    s_nrElFilled = 0;
    swl_circTable_init(&testTable, myTestTable.tupleType, myTestTable.nrTuples);
    swl_circTable_setNames(&testTable, names);

    swl_circTable_resize(&testTable, 2);
    swl_circTable_resize(&testTable, 4);


    s_addValue(&testTable, 0);
    s_addValue(&testTable, 1);
    s_addValue(&testTable, 2);
    s_addValue(&testTable, 3);

    assert_true(swl_table_equals(&testTable.table, &myTestTable));

    swl_circTable_resize(&testTable, 6);
    s_arraysResize(6);
    s_checkCircTableStr(&testTable);

    s_addValue(&testTable, 0);
    s_addValue(&testTable, 1);
    s_addValue(&testTable, 2);
    s_addValue(&testTable, 3);
    s_addValue(&testTable, 0);
    s_addValue(&testTable, 1);
    s_addValue(&testTable, 2);
    s_addValue(&testTable, 3);



    swl_circTable_resize(&testTable, 4);
    s_arraysResize(4);
    s_checkCircTableStr(&testTable);
    s_addValue(&testTable, 0);
    s_addValue(&testTable, 1);
    s_addValue(&testTable, 2);
    s_addValue(&testTable, 3);
    s_addValue(&testTable, 0);
    s_addValue(&testTable, 1);
    s_addValue(&testTable, 2);
    s_addValue(&testTable, 3);

    swl_circTable_destroy(&testTable);
    s_cleanupArrays();
    s_cleanupTargetList();
}

static void test_swl_circTable_reset(void** state _UNUSED) {
    s_initTargetList(NR_VALUES);
    swl_circTable_t testTable = {0};
    s_nrElFilled = 0;
    swl_circTable_init(&testTable, myTestTable.tupleType, myTestTable.nrTuples);
    swl_circTable_setNames(&testTable, names);

    // check empty reset
    swl_circTable_reset(&testTable);

    for(int i = 0; i < 9; i++) {
        for(int j = 0; j < i; j++) {
            s_addValue(&testTable, j % 4);
        }
        swl_circTable_reset(&testTable);
        s_cleanupArrays();
        s_checkCircTableStr(&testTable);
    }



    swl_circTable_destroy(&testTable);
    s_cleanupArrays();
    s_cleanupTargetList();
}

static void test_swl_circTable_getElementFunctions(void** state _UNUSED) {
    s_initTargetList(NR_VALUES);
    swl_circTable_t testTable = {0};
    s_nrElFilled = 0;
    swl_circTable_init(&testTable, myTestTable.tupleType, myTestTable.nrTuples);
    swl_circTable_setNames(&testTable, names);

    for(ssize_t i = -2 * NR_VALUES; i < 2 * NR_VALUES; i++) {

        assert_null(swl_circTable_getTuple(&testTable, i));

        for(size_t j = 0; j < NR_TYPES; j++) {
            assert_null(swl_circTable_getValue(&testTable, i, j));
            assert_null(swl_circTable_getReference(&testTable, i, j));
        }
    }


    for(size_t fillIndex = 0; fillIndex < 2 * NR_VALUES; fillIndex++) {
        s_addValue(&testTable, fillIndex % NR_VALUES);

        for(ssize_t i = -2 * NR_VALUES; i < 0; i++) {

            size_t baseIndex = SWL_ARRAY_INDEX(i, SWL_MIN(fillIndex + 1, (size_t) NR_VALUES));
            if(-1 * (i + 1) > (ssize_t) SWL_MIN(fillIndex, (size_t) NR_VALUES - 1)) {
                assert_null(swl_circTable_getTuple(&testTable, i));

                for(size_t j = 0; j < NR_TYPES; j++) {
                    assert_null(swl_circTable_getValue(&testTable, i, j));
                    assert_null(swl_circTable_getReference(&testTable, i, j));
                }
            } else {
                if(fillIndex >= NR_VALUES) {
                    baseIndex = (baseIndex + fillIndex + 1) % NR_VALUES;
                }
                swl_tuple_t* t1 = swl_circTable_getTuple(&testTable, i);
                swl_tuple_t* t2 = swl_table_getTuple(&myTestTable, baseIndex);


                assert_true(swl_tupleType_equals(&myTestTableTupleType, t1, t2));
                for(size_t j = 0; j < NR_TYPES; j++) {

                    assert_true(swl_type_equals(myTestTableTypes[j],
                                                swl_circTable_getValue(&testTable, i, j),
                                                swl_table_getValue(&myTestTable, baseIndex, j)));

                    assert_true(swl_type_equals(myTestTableTypes[j],
                                                swl_type_toPtr(myTestTableTypes[j], swl_circTable_getReference(&testTable, i, j)),
                                                swl_type_toPtr(myTestTableTypes[j], swl_table_getReference(&myTestTable, baseIndex, j))));

                }
            }
        }


        for(ssize_t i = 0; i < 2 * NR_VALUES; i++) {
            size_t baseIndex = SWL_ARRAY_INDEX(i, SWL_MIN(fillIndex + 1, (size_t) NR_VALUES));

            if(i > (ssize_t) SWL_MIN(fillIndex, (size_t) NR_VALUES - 1)) {
                assert_null(swl_circTable_getTuple(&testTable, i));

                for(size_t j = 0; j < NR_TYPES; j++) {
                    assert_null(swl_circTable_getValue(&testTable, i, j));
                    assert_null(swl_circTable_getReference(&testTable, i, j));
                }
            } else {
                if(fillIndex >= NR_VALUES) {
                    baseIndex = (baseIndex + fillIndex + 1) % NR_VALUES;
                }
                swl_tuple_t* t1 = swl_circTable_getTuple(&testTable, i);

                swl_tuple_t* t2 = swl_table_getTuple(&myTestTable, baseIndex);

                assert_true(swl_tupleType_equals(&myTestTableTupleType, t1, t2));
                for(size_t j = 0; j < NR_TYPES; j++) {
                    char buf1[256];
                    char buf2[256];
                    swl_type_toChar(myTestTableTypes[j], buf1, sizeof(buf1),
                                    swl_circTable_getValue(&testTable, i, j));

                    swl_type_toChar(myTestTableTypes[j], buf2, sizeof(buf2),
                                    swl_table_getValue(&myTestTable, baseIndex, j));

                    assert_true(swl_type_equals(myTestTableTypes[j],
                                                swl_circTable_getValue(&testTable, i, j),
                                                swl_table_getValue(&myTestTable, baseIndex, j)));

                    assert_true(swl_type_equals(myTestTableTypes[j],
                                                swl_type_toPtr(myTestTableTypes[j], swl_circTable_getReference(&testTable, i, j)),
                                                swl_type_toPtr(myTestTableTypes[j], swl_table_getReference(&myTestTable, baseIndex, j))));

                }
            }
        }
    }




    swl_circTable_destroy(&testTable);
    s_cleanupArrays();
    s_cleanupTargetList();
}

static void s_testColToArr(swl_circTable_t* table, uint32_t arraySize, char* expectedOutput) {
    char buffer[64] = {0};

    int32_t testArray[arraySize];
    memset(testArray, 0, sizeof(testArray));

    swl_circTable_columnToArray(testArray, SWL_ARRAY_SIZE(testArray), table, 0);
    swl_typeInt32_arrayToChar(buffer, sizeof(buffer), testArray, SWL_ARRAY_SIZE(testArray));

    assert_string_equal(buffer, expectedOutput);
}

static void test_swl_circTable_columnToArray(void** state _UNUSED) {
    s_initTargetList(NR_VALUES);
    swl_circTable_t testTable = {0};
    s_nrElFilled = 0;
    swl_circTable_init(&testTable, myTestTable.tupleType, myTestTable.nrTuples);
    swl_circTable_setNames(&testTable, names);

    s_printCircTable(&testTable);
    s_testColToArr(&testTable, 2, "0,0");


    s_addValue(&testTable, 0);
    s_testColToArr(&testTable, 2, "1,0");
    s_testColToArr(&testTable, 4, "1,0,0,0");
    s_testColToArr(&testTable, 6, "1,0,0,0,0,0");


    s_addValue(&testTable, 1);
    s_testColToArr(&testTable, 2, "1,2");


    s_addValue(&testTable, 2);
    s_testColToArr(&testTable, 2, "2,-8852");

    s_addValue(&testTable, 3);
    s_testColToArr(&testTable, 2, "-8852,8851");

    s_addValue(&testTable, 0);
    s_testColToArr(&testTable, 2, "8851,1");

    s_addValue(&testTable, 1);
    s_testColToArr(&testTable, 2, "1,2");

    swl_circTable_reset(&testTable);
    s_cleanupArrays();

    s_addValue(&testTable, 2);
    s_testColToArr(&testTable, 2, "-8852,0");

    swl_circTable_destroy(&testTable);
    s_cleanupArrays();
    s_cleanupTargetList();
}


static int s_setupSuite(void** state _UNUSED) {
    return 0;
}

static int s_teardownSuite(void** state _UNUSED) {
    return 0;
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlConv");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_circTable_mapAddGet),
        cmocka_unit_test(test_swl_circTable_resize),
        cmocka_unit_test(test_swl_circTable_reset),
        cmocka_unit_test(test_swl_circTable_getElementFunctions),
        cmocka_unit_test(test_swl_circTable_columnToArray),
    };
    int rc = 0;
    rc = cmocka_run_group_tests(tests, s_setupSuite, s_teardownSuite);
    sahTraceClose();
    return rc;
}
