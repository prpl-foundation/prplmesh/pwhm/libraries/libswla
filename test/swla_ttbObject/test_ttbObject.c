/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>
#include <libgen.h>
#include <debug/sahtrace.h>
#include "test-toolbox/ttb_amx.h"
#include "test-toolbox/ttb_mockClock.h"
#include "test-toolbox/ttb_mockTimer.h"
#include "test-toolbox/ttb_timespec.h"
#include "test-toolbox/ttb_object.h"

#include "swla/swla_namedTupleType.h"
#include "swla/ttb/swla_ttbObject.h"


/**
 * @file Test for testing that we can call a pcb function from a unittest.
 *
 * Can also be used as an example of a unittest that calls a pcb function.
 */

#ifndef _UNUSED
#define _UNUSED __attribute__((unused))
#endif

static bool s_calledFun = false;

#define MY_TEST_TABLE_VAR(X, Y) \
    X(Y, gtSwl_type_int32, index, "SmallInt") \
    X(Y, gtSwl_type_int64, key, "BigInt") \
    X(Y, gtSwl_type_charPtr, val, "String") \
    X(Y, gtSwl_type_charBuf32, buf, "Name")

SWL_NTT(myTestNTTType, swl_myTestNTT_t, MY_TEST_TABLE_VAR, );
SWL_NTT_MASK(myTestNTTType, swl_myTestNTT_t, MY_TEST_TABLE_VAR)

swl_myTestNTT_t s_input = {.index = 123, .key = 456, .val = "fooBar", .buf = "abc"};
swl_myTestNTT_t s_output = {.index = 321, .key = 654, .val = "barFoo", .buf = ""};

/**
 * Implementation of pcb/odl function - normally this is not in your test
 * (except if you want to mock them).
 */

static amxd_status_t s_exampleFunction_cbf(_UNUSED amxd_object_t* obj,
                                           _UNUSED amxd_function_t* func,
                                           _UNUSED amxc_var_t* args,
                                           _UNUSED amxc_var_t* ret) {
    char* obj_path = amxd_object_get_path(obj, AMXD_OBJECT_TERMINATE);
    printf("Func called on %s\n", obj_path);
    free(obj_path);

    s_calledFun = true;

    swl_myTestNTT_t inVar;
    memset(&inVar, 0, sizeof(swl_myTestNTT_t));
    swl_mask64_m mask;
    swl_trl_e retTrl = swl_ntt_fromArgs(args, &myTestNTTType, &inVar, &mask);
    assert_int_equal(retTrl, SWL_TRL_TRUE);
    assert_int_equal(mask, m_myTestNTTType__all);
    assert_true(swl_type_equals((swl_type_t*) &myTestNTTType, &inVar, &s_input));

    char buf[128] = {0};
    swl_type_toChar((swl_type_t*) &myTestNTTType, buf, sizeof(buf), &inVar);
    printf("In %s\n", buf);

    swl_type_cleanup((swl_type_t*) &myTestNTTType, &inVar);


    swl_type_toVariant((swl_type_t*) &myTestNTTType.type, ret, &s_output);


    return amxd_status_ok;
}



static int setup_suite(void** state) {
    ttb_amx_t* ttbAmx = ttb_amx_init();
    *state = ttbAmx;
    assert_int_equal(amxo_resolver_ftab_add(&ttbAmx->parser, "exampleFunction", AMXO_FUNC(s_exampleFunction_cbf)), 0);

    ttb_amx_loadDm(ttbAmx, "datamodel.odl", "Example");



    ttb_mockClock_initNow();
    assert_non_null(*state);
    return 0;
}

static int teardown_suite(void** state) {
    bool ok = ttb_amx_cleanup(*state);
    *state = NULL;
    assert_true(ok);

    return 0;
}

typedef struct {
    bool isDone;
    bool errorSeen;
} funResult_t;




static void test_simpleFunCall(void** state) {
    ttb_amx_t* ttb_amx = *state;
    ttb_object_t* obj = ttb_amx->rootObj;


    ttb_var_t* reqVar = ttb_object_createArgs();
    assert_non_null(reqVar);

    swl_type_toVariant((swl_type_t*) &myTestNTTType.type, reqVar, &s_input);
    s_calledFun = false;

    ttb_var_t* replyVar = NULL;
    ttb_reply_t* reply = ttb_object_callFun(ttb_amx, obj, "exampleFunction", &reqVar, &replyVar);
    assert_true(ttb_object_replySuccess(reply));


    swl_myTestNTT_t outVar;
    swl_type_fromVariant((swl_type_t*) &myTestNTTType.type, &outVar, replyVar);
    assert_true(swl_type_equals((swl_type_t*) &myTestNTTType.type, &outVar, &s_output));

    char buf[128] = {0};
    swl_type_toChar((swl_type_t*) &myTestNTTType, buf, sizeof(buf), &outVar);
    printf("Out %s\n", buf);

    swl_type_cleanup((swl_type_t*) &myTestNTTType.type, &outVar);
    assert_true(s_calledFun);

    ttb_object_cleanReply(&reply, &replyVar);
}



static void test_swla_ttbObject_callTypeFun(void** state) {
    ttb_amx_t* ttb_amx = *state;
    ttb_object_t* obj = ttb_amx->rootObj;


    s_calledFun = false;

    swla_ttbObject_callTypeFun(ttb_amx, obj, "exampleFunction", &myTestNTTType.type, &s_input);
}


static void test_swla_ttbObject_callTypeFunRep(void** state) {
    ttb_amx_t* ttb_amx = *state;
    ttb_object_t* obj = ttb_amx->rootObj;

    s_calledFun = false;
    swl_myTestNTT_t outVar;

    swla_ttbObject_callTypeFunRep(ttb_amx, obj, "exampleFunction",
                                  &myTestNTTType.type, &s_input,
                                  &myTestNTTType.type, &outVar);


    assert_true(s_calledFun);
    swl_type_cleanup((swl_type_t*) &myTestNTTType.type, &outVar);
}

static void test_swla_ttbObject_callTypeFunRepToFile(void** state) {
    ttb_amx_t* ttb_amx = *state;
    ttb_object_t* obj = ttb_amx->rootObj;

    s_calledFun = false;
    swl_myTestNTT_t outVar;
    swla_ttbObject_callTypeFunRepToFile(ttb_amx, obj, "exampleFunction",
                                        &myTestNTTType.type, &s_input,
                                        &myTestNTTType.type, &outVar, "tmp.txt", NULL);
    swl_type_cleanup((swl_type_t*) &myTestNTTType.type, &outVar);
    assert_true(s_calledFun);
    swl_ttb_assertFileEquals("tmp.txt", "TestType.txt");
    remove("tmp.txt");
}

static void test_swla_ttbObject_assertCallTypeFunToFile(void** state) {
    ttb_amx_t* ttb_amx = *state;
    ttb_object_t* obj = ttb_amx->rootObj;

    s_calledFun = false;
    swla_ttbObject_assertCallTypeFunEqFile(ttb_amx, obj, "exampleFunction",
                                           &myTestNTTType.type, &s_input,
                                           "TestTypeNoRep.txt");
    assert_true(s_calledFun);
}

static void test_swla_ttbObject_assertCallTypeFunRepToFile(void** state) {
    ttb_amx_t* ttb_amx = *state;
    ttb_object_t* obj = ttb_amx->rootObj;

    s_calledFun = false;
    swl_myTestNTT_t outVar;
    swla_ttbObject_assertCallTypeFunRepEqFile(ttb_amx, obj, "exampleFunction",
                                              &myTestNTTType.type, &s_input,
                                              &myTestNTTType.type, &outVar, "TestType.txt", NULL);
    swl_type_cleanup((swl_type_t*) &myTestNTTType.type, &outVar);
    assert_true(s_calledFun);
}

static void test_swla_ttbObject_assertCallFunEqFile(void** state) {
    ttb_amx_t* ttb_amx = *state;
    ttb_object_t* obj = ttb_amx->rootObj;


    ttb_var_t* reqVar = ttb_object_createArgs();
    assert_non_null(reqVar);

    swl_type_toVariant((swl_type_t*) &myTestNTTType.type, reqVar, &s_input);
    s_calledFun = false;

    swla_ttbObject_assertCallFunEqFile(ttb_amx, obj, "exampleFunction", &reqVar, "Test.txt");
}

static void test_swla_ttbObject_assertCallFunEqFileExt(void** state) {
    ttb_amx_t* ttb_amx = *state;
    ttb_object_t* obj = ttb_amx->rootObj;

    ttb_var_t* reqVar = ttb_object_createArgs();
    assert_non_null(reqVar);

    swl_type_toVariant((swl_type_t*) &myTestNTTType.type, reqVar, &s_input);
    s_calledFun = false;
    ttb_var_t* replyVar = NULL;
    ttb_reply_t* reply = NULL;

    swla_ttbObject_assertCallFunEqFileExt(ttb_amx, obj, "exampleFunction", &reqVar, "Test.txt", reply, replyVar);
    ttb_object_cleanReply(&reply, &replyVar);
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {

    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_simpleFunCall),
        cmocka_unit_test(test_swla_ttbObject_callTypeFun),
        cmocka_unit_test(test_swla_ttbObject_callTypeFunRep),
        cmocka_unit_test(test_swla_ttbObject_callTypeFunRepToFile),
        cmocka_unit_test(test_swla_ttbObject_assertCallTypeFunToFile),
        cmocka_unit_test(test_swla_ttbObject_assertCallTypeFunRepToFile),
        cmocka_unit_test(test_swla_ttbObject_assertCallFunEqFile),
        cmocka_unit_test(test_swla_ttbObject_assertCallFunEqFileExt),
    };
    return cmocka_run_group_tests(tests, setup_suite, teardown_suite);
}
