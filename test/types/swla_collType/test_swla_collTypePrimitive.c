/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "swl/swl_common.h"
#include "swla/swla_type.h"
#include "swl/fileOps/swl_fileUtils.h"
#include "swl/types/swl_arrayType.h"
#include "swl/types/swl_vectorType.h"
#include "swl/types/swl_arrayListType.h"
#include "swl/types/swl_arrayBufType.h"
#include "swla/swla_tupleType.h"

#define NR_TYPES 6



SWL_ARRAY_TYPE(tMyTestArrayType, gtSwl_type_int32, 4, false, false);
SWL_ARRAY_TYPE(tMyTestArrayTypeNull, gtSwl_type_int32, 4, true, false);
SWL_ARRAY_BUF_TYPE(tMyTestArrayBufType, gtSwl_type_int32, 4, false);
SWL_ARRAY_LIST_TYPE(tMyTestArrayListType, gtSwl_type_int32, false);
SWL_VECTOR(tMyTestVectorType, gtSwl_type_int32, false);

SWL_ARRAY_TYPE(tMyTestArrayCharPtrType, gtSwl_type_charPtr, 4, false, false);
SWL_ARRAY_TYPE(tMyTestArrayCharPtrTypeNull, gtSwl_type_charPtr, 4, true, false);
SWL_ARRAY_BUF_TYPE(tMyTestArrayBufCharPtrType, gtSwl_type_charPtr, 4, false);
SWL_ARRAY_LIST_TYPE(tMyTestArrayListCharPtrType, gtSwl_type_charPtr, false);
SWL_VECTOR(tMyTestVectorCharPtrType, gtSwl_type_charPtr, false);

/**
 * Defining a tuple type using a table
 *
 * These values are different from any in the test data
 * Note to ensure list exceeds 10, to ensure multi block vector, and exceed default arraylist capacity.
 */
static int32_t myTestValues[] = {
    1,
    2,
    200,
    -50,
    99,
    -2002,
    102012,
    0,
    99182,
    22,
    51,
    108,
    99,
    -1,
    INT32_MAX,
    INT32_MIN,
    0,
    1,
    -1
};

static char* myTestCharPtrValues[] = {
    "1foa",
    "asdfasdf2",
    "200sdfasd",
    "-50jkasvb",
    "99abc",
    "-2002def",
    "102012",
    "",
    "qwerty",
    "22",
    "51",
    "108",
    "99",
    "-1",
    "INT32_MAX",
    "INT32_MIN",
    "0",
    "1",
    "-1"
};

#define DEFAULT_NR_VALUES 4

typedef struct {
    swl_collTypeFun_t* fun; //Collection Under Test
    swl_collType_t* type;   //Collection Under Test
    size_t maxSize;
    size_t colSize;
    bool allowEmpty;
} swl_colTest_t;

static swl_colTest_t* s_ct;
static swl_type_t* s_elType;


typedef struct testData {
    swl_colTest_t* testArray;
    swl_coll_t* srcCollection;
    swl_type_t* type;
    size_t srcColSize;
} testData_t;

testData_t* curTestData = NULL;


static void test_swl_toFromVariant(void** state _UNUSED) {
    swl_bit8_t buf[s_ct->colSize];
    memset(buf, 0, s_ct->colSize);
    swl_coll_t* col = &buf[0];

    size_t targetSize = (s_ct->maxSize == 0 ? curTestData->srcColSize : DEFAULT_NR_VALUES);
    assert_int_equal(s_ct->fun->initFromArray(&s_ct->type->type, col, curTestData->srcCollection, targetSize), SWL_RC_OK);
    assert_int_equal(s_ct->fun->size(&s_ct->type->type, col), targetSize);


    amxc_var_t myVar;
    amxc_var_init(&myVar);

    assert_true(swl_type_toVariant((swl_type_t*) s_ct->type, &myVar, col));

    assert_true(amxc_var_type_of(&myVar) == AMXC_VAR_ID_LIST);
    size_t index = 0;
    amxc_var_for_each(it, &myVar) {
        char elBuf[s_elType->size];
        memset(elBuf, 0, s_elType->size);
        swl_typeEl_t* tmpData = &elBuf[0];
        assert_true(swl_type_fromVariant(s_elType, tmpData, it));

        assert_true(swl_type_equals(s_elType, SWL_TYPE_EL_TO_DATA(s_elType, tmpData), swl_type_arrayGetValue(s_elType, curTestData->srcCollection,
                                                                                                             curTestData->srcColSize, index)));
        swl_type_cleanup(s_elType, tmpData);
        index++;
    }
    assert_int_equal(index, targetSize);

    swl_bit8_t buf2[s_ct->colSize];
    memset(buf2, 0, s_ct->colSize);
    swl_coll_t* col2 = &buf2[0];
    s_ct->fun->init(&s_ct->type->type, col2);
    swl_type_fromVariant((swl_type_t*) s_ct->type, col2, &myVar);
    assert_true(swl_type_equals((swl_type_t*) s_ct->type, col, col2));

    s_ct->fun->cleanup(&s_ct->type->type, col);
    s_ct->fun->cleanup(&s_ct->type->type, col2);

    amxc_var_clean(&myVar);
}



static void test_swl_toFromVariantString(void** state _UNUSED) {
    swl_bit8_t buf[s_ct->colSize];
    memset(buf, 0, s_ct->colSize);
    swl_coll_t* col = &buf[0];

    size_t targetSize = (s_ct->maxSize == 0 ? curTestData->srcColSize : DEFAULT_NR_VALUES);
    assert_int_equal(s_ct->fun->initFromArray(&s_ct->type->type, col, curTestData->srcCollection, targetSize), SWL_RC_OK);
    assert_int_equal(s_ct->fun->size(&s_ct->type->type, col), targetSize);

    amxc_var_t myVar;
    amxc_var_init(&myVar);

    assert_true(swl_type_toVariantString((swl_type_t*) s_ct->type, &myVar, col));

    assert_true(amxc_var_type_of(&myVar) == AMXC_VAR_ID_CSTRING);
    const char* srcStr = amxc_var_get_const_cstring_t(&myVar);
    char testBuf[256] = {0};
    swl_type_arrayToChar(s_elType, testBuf, sizeof(testBuf), curTestData->srcCollection, targetSize);

    assert_string_equal(srcStr, testBuf);

    swl_bit8_t buf2[s_ct->colSize];
    memset(buf2, 0, s_ct->colSize);
    swl_coll_t* col2 = &buf2[0];
    s_ct->fun->init(&s_ct->type->type, col2);
    swl_type_fromVariantString((swl_type_t*) s_ct->type, col2, &myVar);
    swl_type_arrayToChar(s_elType, testBuf, sizeof(testBuf), curTestData->srcCollection, targetSize);

    assert_true(swl_type_equals((swl_type_t*) s_ct->type, col, col2));

    s_ct->fun->cleanup(&s_ct->type->type, col);
    s_ct->fun->cleanup(&s_ct->type->type, col2);

    amxc_var_clean(&myVar);
}

static void test_swl_toFromVariantSetString(void** state _UNUSED) {
    s_ct->type->toStrVar = true;

    swl_bit8_t buf[s_ct->colSize];
    memset(buf, 0, s_ct->colSize);
    swl_coll_t* col = &buf[0];

    size_t targetSize = (s_ct->maxSize == 0 ? curTestData->srcColSize : DEFAULT_NR_VALUES);
    assert_int_equal(s_ct->fun->initFromArray(&s_ct->type->type, col, curTestData->srcCollection, targetSize), SWL_RC_OK);
    assert_int_equal(s_ct->fun->size(&s_ct->type->type, col), targetSize);


    amxc_var_t myVar;

    amxc_var_init(&myVar);
    assert_true(swl_type_toVariant((swl_type_t*) s_ct->type, &myVar, col));

    assert_true(amxc_var_type_of(&myVar) == AMXC_VAR_ID_CSTRING);
    const char* srcStr = amxc_var_get_const_cstring_t(&myVar);
    char testBuf[256] = {0};
    swl_type_arrayToChar(s_elType, testBuf, sizeof(testBuf), curTestData->srcCollection, targetSize);
    assert_string_equal(srcStr, testBuf);

    swl_bit8_t buf2[s_ct->colSize];
    memset(buf2, 0, s_ct->colSize);
    swl_coll_t* col2 = &buf2[0];
    s_ct->fun->init(&s_ct->type->type, col2);
    swl_type_fromVariant((swl_type_t*) s_ct->type, col2, &myVar);

    swl_type_arrayToChar(s_elType, testBuf, sizeof(testBuf), curTestData->srcCollection, targetSize);

    assert_true(swl_type_equals((swl_type_t*) s_ct->type, col, col2));



    s_ct->fun->cleanup(&s_ct->type->type, col);
    s_ct->fun->cleanup(&s_ct->type->type, col2);

    amxc_var_clean(&myVar);
    s_ct->type->toStrVar = false;
}

static int s_setupSuite(void** state _UNUSED) {
    printf("\n\n Test collection : -%s- \n", s_ct->fun->name);
    s_ct->colSize = s_ct->type->type.size;
    return 0;
}

static int s_teardownSuite(void** state _UNUSED) {
    return 0;
}

swl_colTest_t testColInt32[] = {
    {.fun = &swl_arrayCollectionType_fun,
        .maxSize = 4,
        .type = &tMyTestArrayType.type, },
    {.fun = &swl_arrayCollectionType_fun,
        .maxSize = 4,
        .type = &tMyTestArrayTypeNull.type,
        .allowEmpty = true, },
    {.fun = &swl_arrayBufCollectionType_fun,
        .maxSize = 4,
        .type = &tMyTestArrayBufType.type, },
    {.fun = &swl_arrayListCollType_fun,
        .maxSize = 0,
        .type = &tMyTestArrayListType.type, },
    {.fun = &swl_vectorCollectionType_fun,
        .maxSize = 0,
        .type = &tMyTestVectorType.type, },
};

swl_colTest_t testColCharPtr[] = {
    {.fun = &swl_arrayCollectionType_fun,
        .maxSize = 4,
        .type = &tMyTestArrayCharPtrType.type, },
    {.fun = &swl_arrayCollectionType_fun,
        .maxSize = 4,
        .type = &tMyTestArrayCharPtrTypeNull.type,
        .allowEmpty = true, },
    {.fun = &swl_arrayBufCollectionType_fun,
        .maxSize = 4,
        .type = &tMyTestArrayBufCharPtrType.type, },
    {.fun = &swl_arrayListCollType_fun,
        .maxSize = 0,
        .type = &tMyTestArrayListCharPtrType.type, },
    {.fun = &swl_vectorCollectionType_fun,
        .maxSize = 0,
        .type = &tMyTestVectorCharPtrType.type, },
};

testData_t testDataList[] = {
    { .testArray = testColInt32,
        .srcCollection = myTestValues,
        .srcColSize = SWL_ARRAY_SIZE(myTestValues),
        .type = &gtSwl_type_int32, },
    { .testArray = testColCharPtr,
        .srcCollection = myTestCharPtrValues,
        .srcColSize = SWL_ARRAY_SIZE(myTestCharPtrValues),
        .type = &gtSwl_type_charPtr, },
};

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlConv");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_toFromVariant),
        cmocka_unit_test(test_swl_toFromVariantString),
        cmocka_unit_test(test_swl_toFromVariantSetString),
    };



    int rc = 0;
    size_t start = 0;
    size_t end = SWL_ARRAY_SIZE(testColCharPtr) - 1;

    for(size_t j = 0; j < 2; j++) {
        curTestData = &testDataList[j];
        s_elType = testDataList[j].type;
        for(size_t i = start; i <= end; i++) {
            s_ct = &testDataList[j].testArray[i];
            rc += cmocka_run_group_tests(tests, s_setupSuite, s_teardownSuite);
        }
    }

    sahTraceClose();
    return rc;
}
