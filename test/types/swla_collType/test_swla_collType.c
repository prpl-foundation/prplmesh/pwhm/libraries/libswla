/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "swl/swl_common.h"
#include "swla/swla_type.h"
#include "swl/fileOps/swl_fileUtils.h"
#include "swl/types/swl_arrayType.h"
#include "swl/types/swl_vectorType.h"
#include "swl/types/swl_arrayListType.h"
#include "swl/types/swl_arrayBufType.h"
#include "swla/swla_tupleType.h"

#define NR_TYPES 6


#define MY_TEST_TABLE_VAR(X, Y) \
    X(Y, gtSwl_type_int8, test2) \
    X(Y, gtSwl_type_int64, index) \
    X(Y, gtSwl_type_charPtr, val)

SWL_TT(tMyTestTupleType, swl_myTestTT_t, MY_TEST_TABLE_VAR, )

SWL_ARRAY_TYPE(tMyTestArrayType, tMyTestTupleType, 4, false, false);
SWL_ARRAY_TYPE(tMyTestArrayTypeNull, tMyTestTupleType, 4, true, false);
SWL_ARRAY_BUF_TYPE(tMyTestArrayBufType, tMyTestTupleType, 4, false);
SWL_ARRAY_LIST_TYPE(tMyTestArrayListType, tMyTestTupleType, false);
SWL_VECTOR(tMyTestVectorType, tMyTestTupleType, false);

/**
 * Defining a tuple type using a table
 *
 * These values are different from any in the test data
 * Note to ensure list exceeds 10, to ensure multi block vector, and exceed default arraylist capacity.
 */
static swl_myTestTT_t myTestValues[] = {
    {1, 100, "atest1"},
    {2, -200, "atest2"},
    {-88, 289870, "afoobar"},
    {88, -28989, "abarfoo"},
    {99, -2869, "atest5"},
    {101, -2899, "a1234"},
    {-2, -289869, "!&*/?\"`"},
    {-88, -69, ""},
    {11, -2861, "ztest1"},
    {12, -2862, "ztest2"},
    {13, -2863, "ztest3"},
    {14, -2864, "ztest4"},
    {15, -2865, "ztest5"},
    {16, -2866, "ztest6"},
    {17, -2867, "ztest7"},
    {18, -2868, "ztest8"},
    {19, -2869, "ztest9"},
    {10, -2860, "ztest0"},
};

#define DEFAULT_NR_VALUES 4

tMyTestArrayBufType_type testData[] = {
    {.data = {
            {1, 100, "test1"},
            {2, -200, "test2"},
            {-88, 289870, "foobar"},
            {88, -289869, "barfoo"},
        }, .size = 4 },
    {.data = {
            {2, 100, "test1"}, // 2 changed here
            {2, -200, "test2"},
            {-88, 289870, "foobar"},
            {88, -289869, "barfoo"},
        }, .size = 4},
    {.data = {
            {2, 100, "test1"},
            {2, -202, "test2"}, // -202 changed here
            {-88, 289870, "foobar"},
            {88, -289869, "barfoo"},
        }, .size = 4},
    {.data = {
            {2, 100, "test1"},
            {2, -200, "test2"},
            {-88, 289870, "foobars"}, // fobars changed here
            {88, -289869, "barfoo"},
        }, .size = 4},
    {.data = {
            {1, 100, "test1"},
            {2, -200, "test2"},
            {-88, 289870, "foobar"},
            {0, 0, NULL},
        }, .size = 3},
    {.data = {
            {0, 0, NULL},
            {0, 0, NULL},
            {0, 0, NULL},
            {0, 0, NULL},
        }, .size = 0},
};

typedef struct {
    swl_collTypeFun_t* fun; //Collection Under Test
    swl_collType_t* type;   //Collection Under Test
    size_t maxSize;
    size_t colSize;
    bool allowEmpty;
} swl_colTest_t;

static swl_colTest_t* s_ct;
static swl_type_t* s_elType;

static void test_swl_toFromVariant(void** state _UNUSED) {
    //Simple init
    swl_bit8_t buf[s_ct->colSize];
    memset(buf, 0, s_ct->colSize);
    swl_coll_t* col = &buf[0];

    size_t targetSize = (s_ct->maxSize == 0 ? SWL_ARRAY_SIZE(myTestValues) : DEFAULT_NR_VALUES);
    assert_int_equal(s_ct->fun->initFromArray(&s_ct->type->type, col, &myTestValues, targetSize), SWL_RC_OK);
    assert_int_equal(s_ct->fun->size(&s_ct->type->type, col), targetSize);

    amxc_var_t myVar;
    amxc_var_init(&myVar);

    assert_true(swl_type_toVariant((swl_type_t*) s_ct->type, &myVar, col));

    assert_true(amxc_var_type_of(&myVar) == AMXC_VAR_ID_LIST);
    size_t index = 0;
    amxc_var_for_each(it, &myVar) {
        swl_myTestTT_t tmpData;
        memset(&tmpData, 0, sizeof(swl_myTestTT_t));
        assert_true(swl_type_fromVariant(s_elType, &tmpData, it));
        assert_true(swl_type_equals(s_elType, &tmpData, &myTestValues[index]));
        swl_type_cleanup(s_elType, &tmpData);
        index++;
    }
    assert_int_equal(index, targetSize);

    swl_bit8_t buf2[s_ct->colSize];
    memset(buf2, 0, s_ct->colSize);
    swl_coll_t* col2 = &buf2[0];
    s_ct->fun->init(&s_ct->type->type, col2);
    swl_type_fromVariant((swl_type_t*) s_ct->type, col2, &myVar);
    assert_true(swl_type_equals((swl_type_t*) s_ct->type, col, col2));

    s_ct->fun->cleanup(&s_ct->type->type, col);
    s_ct->fun->cleanup(&s_ct->type->type, col2);

    amxc_var_clean(&myVar);
}

static int s_setupSuite(void** state _UNUSED) {
    printf("\n\n Test collection : -%s- \n", s_ct->fun->name);
    s_ct->colSize = s_ct->type->type.size;
    return 0;
}

static int s_teardownSuite(void** state _UNUSED) {
    return 0;
}

swl_colTest_t testCol[] = {
    {.fun = &swl_arrayCollectionType_fun,
        .maxSize = 4,
        .type = &tMyTestArrayType.type, },
    {.fun = &swl_arrayCollectionType_fun,
        .maxSize = 4,
        .type = &tMyTestArrayTypeNull.type,
        .allowEmpty = true, },
    {.fun = &swl_arrayBufCollectionType_fun,
        .maxSize = 4,
        .type = &tMyTestArrayBufType.type, },
    {.fun = &swl_arrayListCollType_fun,
        .maxSize = 0,
        .type = &tMyTestArrayListType.type, },
    {.fun = &swl_vectorCollectionType_fun,
        .maxSize = 0,
        .type = &tMyTestVectorType.type, },
};



int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlConv");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_toFromVariant),
    };

    s_elType = (swl_type_t*) &tMyTestTupleType;

    int rc = 0;
    size_t start = 0;
    size_t end = SWL_ARRAY_SIZE(testCol) - 1;

    for(size_t i = start; i <= end; i++) {
        s_ct = &testCol[i];
        rc += cmocka_run_group_tests(tests, s_setupSuite, s_teardownSuite);
    }

    sahTraceClose();
    return rc;
}
