/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>

#include "swla/swla_commonLib.h"

#include "swl/swl_common.h"
#include "swla/swla_unLiTable.h"
#include "swla/swla_tupleType.h"


typedef struct {
    uint32_t a;
    uint8_t b;
    char buf[18];
} test_unLiListData_t;

test_unLiListData_t emptyData = {0};

#define NR_TYPES 3
#define NR_VALUES 12
SWL_TUPLE_TYPE(unLiListTest,
               ARR(swl_type_uint32, swl_type_uint8, swl_type_charBuf18));

static const test_unLiListData_t testData[NR_VALUES] = {
    {0, 1, "abc"},
    {10, 2, "cde"},
    {20, 4, "fg"},
    {30, 8, "h"},
    {40, 16, "ijk"},
    {50, 32, "l"},
    {60, 64, "mno"},
    {70, 2, "pq"},
    {80, 3, "rst"},
    {90, 5, "uvw"},
    {1000, 9, "xyz"},
    {500, 100, "abcklmxyz"},
};

static void test_swl_unLiTable_toListOfMaps(void** state _UNUSED) {

    amxc_var_t myVar;
    amxc_var_init(&myVar);
    amxc_var_set_type(&myVar, AMXC_VAR_ID_LIST);

    swl_unLiTable_t unLiTable;
    swl_unLiList_t* unLiList = &unLiTable.list;
    uint32_t nrElToCreate = 12;
    uint32_t blockSize = 3;

    swl_unLiTable_initExt(&unLiTable, &unLiListTestTupleType, blockSize);

    uint32_t i = 0;
    for(i = 0; i < nrElToCreate; i++) {
        swl_unLiList_add(unLiList, &testData[i]);
    }

    char* names[NR_TYPES] = {"BigInt", "SmallInt", "Buf"};

    swl_unLiTable_toListOfMaps(&unLiTable, &myVar, names);

    const amxc_llist_t* myList = amxc_var_get_const_amxc_llist_t(&myVar);
    assert_int_equal(amxc_llist_size(myList), NR_VALUES);
    for(size_t i = 0; i < NR_VALUES; i++) {
        amxc_var_t* tmpVar = amxc_var_from_llist_it(amxc_llist_get_at(myList, i));
        assert_non_null(tmpVar);
        assert_int_equal(amxc_var_type_of(tmpVar), AMXC_VAR_ID_HTABLE);
        for(size_t j = 0; j < NR_TYPES; j++) {
            amxc_var_t* subMap = amxc_var_get_key(tmpVar, names[j], AMXC_VAR_FLAG_DEFAULT);
            char data[unLiListTestTupleType.types[j]->size];
            swl_typeEl_t* typeEl = data;
            swl_type_fromVariant(unLiListTestTupleType.types[j], typeEl, subMap);
            swl_typeData_t* typeData = swl_type_toPtr(unLiListTestTupleType.types[j], typeEl);
            void* localData = swl_unLiList_get(unLiList, i);
            void* val = swl_tupleType_getValue(&unLiListTestTupleType, localData, j);
            assert_true(swl_type_equals(unLiListTestTupleType.types[j], typeData, val));
            if(!unLiListTestTupleType.types[j]->typeFun->isValue) {
                free(typeData);
            }
        }
    }

    amxc_var_clean(&myVar);
    swl_unLiTable_destroy(&unLiTable);
}


static int s_setupSuite(void** state _UNUSED) {
    return 0;
}

static int s_teardownSuite(void** state _UNUSED) {
    return 0;
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlConv");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_unLiTable_toListOfMaps),
    };
    int rc = cmocka_run_group_tests(tests, s_setupSuite, s_teardownSuite);
    sahTraceClose();
    return rc;
}
