/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>
#include <libgen.h>
#include <debug/sahtrace.h>
#include "test-toolbox/ttb_amx.h"
#include "test-toolbox/ttb_mockClock.h"
#include "test-toolbox/ttb_mockTimer.h"
#include "test-toolbox/ttb_timespec.h"
#include "test-toolbox/ttb_object.h"

#include "swla/swla_common_amx.h"
#include "swl/ttb/swl_ttb.h"
#include "swl/swl_common_table.h"
#include "swla/swla_lib.h"
#include "swla/swla_object.h"
#include "swla/swla_type.h"
#include "swla/swla_dm.h"
#include "swla/swla_trans.h"
#include "amxb/amxb_subscription.h"

static void s_multiObj_setConf_ocf(const char* const sig_name, const amxc_var_t* const data, void* const priv);
amxd_status_t s_multiObj_destroyInst_odf(amxd_object_t* object, amxd_param_t* param, amxd_action_t reason, const amxc_var_t* const args, amxc_var_t* const retval, void* priv);
static amxd_status_t s_readObj_orf(amxd_object_t* const object, amxd_param_t* const param, amxd_action_t reason, const amxc_var_t* const args, amxc_var_t* const retval, void* priv);

static int s_setupSuite(void** state) {
    ttb_amx_t* ttbAmx = ttb_amx_init();
    *state = ttbAmx;

    assert_int_equal(amxo_resolver_ftab_add(&ttbAmx->parser, "multiObj_setConf", AMXO_FUNC(s_multiObj_setConf_ocf)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&ttbAmx->parser, "multiObj_destroyInst", AMXO_FUNC(s_multiObj_destroyInst_odf)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&ttbAmx->parser, "readObj", AMXO_FUNC(s_readObj_orf)), 0);
    assert_true(ttb_amx_loadDm(ttbAmx, "datamodel.odl", "Example"));

    ttb_mockClock_initNow();
    assert_non_null(*state);
    swl_lib_initialize(ttbAmx->bus_ctx);
    return 0;
}

static int s_teardownSuite(void** state) {
    swl_lib_cleanup();
    bool ok = ttb_amx_cleanup(*state);
    *state = NULL;
    assert_true(ok);
    return 0;
}

typedef struct {
    swl_table_t resNotifs;
    ttb_amx_t* ttbAmx;
    amxb_subscription_t* topCtx;
    amxb_subscription_t* subsCtx;
} testInfo_t;

SWL_TUPLE_TYPE_NEW(notifDataEntry, ARR(swl_type_charPtr, swl_type_charPtr))
typedef struct SWL_PACKED {
    char* path;
    char* value;
} notifDataEntry_t;
typedef notifDataEntry_t notifDataEntry_type;

static void s_setParam_pwf(void* priv, amxd_object_t* object, amxd_param_t* param, const amxc_var_t* const newValue) {
    testInfo_t* pTestInfo = (testInfo_t*) priv;
    swl_table_t* pNotifs = &pTestInfo->resNotifs;
    assert_ptr_equal(object, amxd_param_get_owner(param));
    char* ppath = swl_dm_getParamPath(param, false);
    assert_non_null(ppath);
    char* pvalue = amxc_var_dyncast(cstring_t, (amxc_var_t*) newValue);
    printf(">>> %s = %s\n", ppath, pvalue);
    notifDataEntry_t entry = {ppath, pvalue};
    swl_table_add(pNotifs, &entry);
    free(ppath);
    free(pvalue);
}

static void s_setTopObject_ocf(const char* const sig_name _UNUSED,
                               const amxc_var_t* const data,
                               void* const priv) {
    /* defined loading order regardless amx htable tuples order. */
    SWLA_DM_HDLRS(sTopObjectDmHdlrs,
                  ARR(SWLA_DM_PARAM_HDLR("TopParam1", s_setParam_pwf),
                      SWLA_DM_PARAM_HDLR("TopParam2", s_setParam_pwf)));
    testInfo_t* pTestInfo = (testInfo_t*) priv;
    assert_int_equal(swla_dm_procObjEvt(&pTestInfo->ttbAmx->dm, &sTopObjectDmHdlrs, sig_name, data, priv), SWL_RC_OK);
}

static void s_setSubObject_ocf(const char* const sig_name _UNUSED,
                               const amxc_var_t* const data,
                               void* const priv) {
    /* defined loading order regardless amx htable tuples order. */
    SWLA_DM_HDLRS(sSubObjectDmHdlrs,
                  ARR(SWLA_DM_PARAM_HDLR("Param1", s_setParam_pwf),
                      SWLA_DM_PARAM_HDLR("Param2", s_setParam_pwf),
                      SWLA_DM_PARAM_HDLR("Param3", s_setParam_pwf),
                      SWLA_DM_PARAM_HDLR("Param4", s_setParam_pwf),
                      SWLA_DM_PARAM_HDLR("Param5", s_setParam_pwf), ));
    testInfo_t* pTestInfo = (testInfo_t*) priv;
    assert_int_equal(swla_dm_procObjEvt(&pTestInfo->ttbAmx->dm, &sSubObjectDmHdlrs, sig_name, data, priv), SWL_RC_OK);
}

testInfo_t testInfo;
static int s_test_setup(void** state _UNUSED) {
    return 0;
}
static int s_test_teardown(void** state _UNUSED) {
    swl_table_destroy(&testInfo.resNotifs);
    amxb_subscription_delete(&testInfo.subsCtx);
    amxb_subscription_delete(&testInfo.topCtx);
    return 0;
}

static void s_checkNotifs(swl_table_t* pExpNotifs, swl_table_t* pResNotifs) {
    uint32_t expSize = swl_table_getSize(pExpNotifs);
    assert_int_equal(expSize, swl_table_getSize(pResNotifs));
    uint32_t i;
    swl_tuple_t* expTuple;
    swl_tuple_t* resTuple;
    for(i = 0, expTuple = swl_table_getTuple(pExpNotifs, i), resTuple = swl_table_getTuple(pResNotifs, i);
        i < expSize && expTuple != NULL && resTuple != NULL;
        i++, expTuple = swl_table_getTuple(pExpNotifs, i), resTuple = swl_table_getTuple(pResNotifs, i)) {
        assert_string_equal(swl_tupleType_getValue(&notifDataEntry, expTuple, 0), swl_tupleType_getValue(&notifDataEntry, resTuple, 0));
        assert_string_equal(swl_tupleType_getValue(&notifDataEntry, expTuple, 1), swl_tupleType_getValue(&notifDataEntry, resTuple, 1));
    }
}

static void test_events_onLoadingDefaults(void** state) {
    ttb_amx_t* ttbAmx = *state;
    testInfo.ttbAmx = ttbAmx;
    assert_int_equal(swl_table_initMax(&testInfo.resNotifs, &notifDataEntry, 0), SWL_RC_OK);
    assert_int_equal(amxb_subscription_new(&testInfo.subsCtx, ttbAmx->bus_ctx, "Example.SubObject.", NULL, s_setSubObject_ocf, &testInfo), 0);
    ttb_mockTimer_goToFutureMs(100);
    assert_int_equal(amxo_parser_parse_file(&ttbAmx->parser, "00_datamodel_defaults.odl", amxd_dm_get_root(&ttbAmx->dm)), 0);
    ttb_mockTimer_goToFutureMs(100);

    SWL_TABLE_FIXED(expNotifs, notifDataEntry,
                    ARR(
                        /*read default conf values from definition: except passive: volatile,read-only,initial empty */
                        {"Example.SubObject.Param1", "AAA"},
                        {"Example.SubObject.Param3", "CCC"},
                        /*read user default values with same defined proc order */
                        {"Example.SubObject.Param1", "111"},
                        {"Example.SubObject.Param3", "333"},
                        {"Example.SubObject.Param4", "444"},
                        ));
    s_checkNotifs(&expNotifs, &testInfo.resNotifs);
}

static void s_setObj_owf(void* priv, amxd_object_t* object, const amxc_var_t* const paramValues) {
    testInfo_t* pTestInfo = (testInfo_t*) priv;
    swl_table_t* pNotifs = &pTestInfo->resNotifs;
    char* opath = amxd_object_get_path(object, AMXD_OBJECT_NAMED);
    char* ovalue = swl_typeCharPtr_fromVariantDef((amxc_var_t*) paramValues, "");
    printf(">>> %s = {%s}\n", opath, ovalue);
    notifDataEntry_t entry = {opath, ovalue};
    swl_table_add(pNotifs, &entry);
    free(opath);
    free(ovalue);
}

static void s_multiObj_setConf_ocf(const char* const sig_name _UNUSED,
                                   const amxc_var_t* const data,
                                   void* const priv _UNUSED) {
    /* defined loading order regardless amx htable tuples order. */
    SWLA_DM_HDLRS(sMultiObjDmHdlrs,
                  ARR(SWLA_DM_PARAM_HDLR("TestKey2", s_setParam_pwf),
                      SWLA_DM_PARAM_HDLR("TestKey1", s_setParam_pwf), ),
                  .instAddedCb = s_setObj_owf,
                  .objChangedCb = s_setObj_owf,
                  );
    assert_int_equal(swla_dm_procObjEvtOfLocalDm(&sMultiObjDmHdlrs, sig_name, data, &testInfo), SWL_RC_OK);
}

static void test_events_onLoadingInstance(void** state) {
    ttb_amx_t* ttbAmx = *state;
    testInfo.ttbAmx = ttbAmx;
    assert_int_equal(swl_table_initMax(&testInfo.resNotifs, &notifDataEntry, 0), SWL_RC_OK);
    assert_int_equal(amxo_parser_parse_file(&ttbAmx->parser, "10_datamodel_defaults.odl", amxd_dm_get_root(&ttbAmx->dm)), 0);
    ttb_mockTimer_goToFutureMs(100);

    SWL_TABLE_FIXED(expNotifs, notifDataEntry,
                    ARR(
                        /*read instance defaults (combined between those from definition and from conf */
                        {"Example.MultiObj.1", "TestKey1:AAA,TestKey2:123"}, /* instance addition */
                        {"Example.MultiObj.1", "TestKey1:AAA,TestKey2:123"}, /* instance update */
                        {"Example.MultiObj.1.TestKey2", "123"},              /* param set (latest value from conf) as in monitoring order*/
                        {"Example.MultiObj.1.TestKey1", "AAA"},              /* monitored param set (value from definition) as in monitoring order*/
                        ));
    s_checkNotifs(&expNotifs, &testInfo.resNotifs);
}

static void test_events_addInstance(void** state) {
    ttb_amx_t* ttbAmx = *state;
    testInfo.ttbAmx = ttbAmx;
    assert_int_equal(swl_table_initMax(&testInfo.resNotifs, &notifDataEntry, 0), SWL_RC_OK);

    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Example.MultiObj.");
    amxd_trans_add_inst(&trans, 0, "inst2");
    amxd_trans_set_value(cstring_t, &trans, "TestKey1", "111");
    amxd_trans_set_value(cstring_t, &trans, "TestKey3", "333");
    amxd_trans_apply(&trans, &ttbAmx->dm);
    amxd_trans_clean(&trans);
    ttb_mockTimer_goToFutureMs(100);

    SWL_TABLE_FIXED(expNotifs, notifDataEntry,
                    ARR(
                        /*read instance conf (combined between those from definition and from trans conf */
                        {"Example.MultiObj.inst2", "TestKey1:111,TestKey2:999"}, /* instance addition */
                        {"Example.MultiObj.inst2", "TestKey1:111,TestKey2:999"}, /* instance update */
                        {"Example.MultiObj.inst2.TestKey2", "999"},              /* param set (latest value from conf) as in monitoring order*/
                        {"Example.MultiObj.inst2.TestKey1", "111"},              /* monitored param set (value from definition) as in monitoring order*/
                        ));
    s_checkNotifs(&expNotifs, &testInfo.resNotifs);
}

static void test_events_setParams(void** state) {
    ttb_amx_t* ttbAmx = *state;
    testInfo.ttbAmx = ttbAmx;
    assert_int_equal(swl_table_initMax(&testInfo.resNotifs, &notifDataEntry, 0), SWL_RC_OK);

    amxd_object_t* obj = swla_object_getReferenceObject(ttbAmx->rootObj, "Example.MultiObj.inst2.");
    assert_non_null(obj);
    assert_int_equal(swl_typeUInt32_commitObjectParam(obj, "TestUnknown", 222), 0);
    assert_int_equal(swl_typeCharPtr_commitObjectParam(obj, "TestKey2", "zzz"), 0);
    assert_int_equal(swl_typeUInt32_commitObjectParam(obj, "TestKey2", 222), 1);
    assert_int_equal(swl_typeCharPtr_commitObjectParam(obj, "TestKey3", "333"), 1);
    ttb_mockTimer_goToFutureMs(100);

    SWL_TABLE_FIXED(expNotifs, notifDataEntry,
                    ARR(
                        {"Example.MultiObj.inst2", "TestKey2:222"}, /* instance update */
                        {"Example.MultiObj.inst2.TestKey2", "222"}, /* param set (latest value from conf) as in monitoring order*/
                        ));
    s_checkNotifs(&expNotifs, &testInfo.resNotifs);
}

amxd_status_t s_multiObj_destroyInst_odf(amxd_object_t* object, amxd_param_t* param, amxd_action_t reason, const amxc_var_t* const args, amxc_var_t* const retval, void* priv) {
    amxd_status_t status = amxd_action_object_destroy(object, param, reason, args, retval, priv);
    assert_int_equal(status, amxd_status_ok);
    //no param value changes when instance is being destroyed
    s_setObj_owf(&testInfo, object, args);
    return status;
}

static void test_events_delInstance(void** state) {
    ttb_amx_t* ttbAmx = *state;
    testInfo.ttbAmx = ttbAmx;
    assert_int_equal(swl_table_initMax(&testInfo.resNotifs, &notifDataEntry, 0), SWL_RC_OK);

    amxd_object_t* tmpl = amxd_object_get(ttbAmx->rootObj, "MultiObj");
    assert_non_null(tmpl);
    assert_int_equal(swl_object_delInstWithTrans(amxd_object_get_instance(tmpl, NULL, 1), &ttbAmx->dm), amxd_status_ok);
    assert_int_equal(swl_object_delInstWithTransOnLocalDm(amxd_object_get_instance(tmpl, "inst2", 0)), amxd_status_ok);
    assert_int_equal(swl_object_delInstWithTrans(amxd_object_get_instance(tmpl, NULL, 2), &ttbAmx->dm), amxd_status_invalid_value);
    ttb_mockTimer_goToFutureMs(1000);

    SWL_TABLE_FIXED(expNotifs, notifDataEntry,
                    ARR(
                        {"Example.MultiObj.1", ""},     /* instance deletion by index */
                        {"Example.MultiObj.inst2", ""}, /* instance deletion by name*/
                        ));
    s_checkNotifs(&expNotifs, &testInfo.resNotifs);
}

static void test_transaction_exists(void** state) {
    ttb_amx_t* ttbAmx = *state;
    testInfo.ttbAmx = ttbAmx;
    assert_int_equal(swl_table_initMax(&testInfo.resNotifs, &notifDataEntry, 0), SWL_RC_OK);

    amxd_object_t* obj = swla_object_getReferenceObject(ttbAmx->rootObj, "Example.");
    amxd_object_t* subObj = swla_object_getReferenceObject(ttbAmx->rootObj, "Example.SubObject");
    assert_int_equal(amxb_subscription_new(&testInfo.subsCtx, ttbAmx->bus_ctx, "Example.SubObject.", NULL, s_setSubObject_ocf, &testInfo), 0);
    assert_int_equal(amxb_subscription_new(&testInfo.topCtx, ttbAmx->bus_ctx, "Example.", NULL, s_setTopObject_ocf, &testInfo), 0);



    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Example.");

    amxd_trans_set_value(cstring_t, &trans, "TopParam1", "C3");

    swla_trans_t subTrans;
    amxd_trans_t* trans2 = swla_trans_init(&subTrans, &trans, subObj);
    assert_non_null(trans2);
    assert_ptr_equal(&trans, trans2);

    amxd_trans_set_value(cstring_t, trans2, "Param1", "E5");

    assert_ptr_equal(swla_trans_finalize(&subTrans, NULL), SWL_RC_CONTINUE);

    amxd_trans_select_object(&trans, obj);

    amxd_trans_set_value(cstring_t, &trans, "TopParam2", "D4");

    amxd_trans_apply(&trans, &ttbAmx->dm);
    amxd_trans_clean(&trans);
    ttb_mockTimer_goToFutureMs(100);
    ttb_mockTimer_goToFutureMs(100);


    char* val1 = amxd_object_get_cstring_t(obj, "TopParam1", NULL);
    ttb_assert_str_eq(val1, "C3");
    free(val1);
    char* val2 = amxd_object_get_cstring_t(obj, "TopParam2", NULL);
    ttb_assert_str_eq(val2, "D4");
    free(val2);

    char* val3 = amxd_object_get_cstring_t(subObj, "Param1", NULL);
    ttb_assert_str_eq(val3, "E5");
    free(val3);

    SWL_TABLE_FIXED(expNotifs, notifDataEntry,
                    ARR(
                        {"Example.TopParam1", "C3"},
                        {"Example.TopParam2", "D4"},
                        {"Example.SubObject.Param1", "E5"},
                        ));
    s_checkNotifs(&expNotifs, &testInfo.resNotifs);
}


static void test_transaction_notExists(void** state) {
    ttb_amx_t* ttbAmx = *state;
    testInfo.ttbAmx = ttbAmx;
    assert_int_equal(swl_table_initMax(&testInfo.resNotifs, &notifDataEntry, 0), SWL_RC_OK);

    amxd_object_t* subObj = swla_object_getReferenceObject(ttbAmx->rootObj, "Example.SubObject");
    assert_int_equal(amxb_subscription_new(&testInfo.subsCtx, ttbAmx->bus_ctx, "Example.SubObject.", NULL, s_setSubObject_ocf, &testInfo), 0);



    swla_trans_t subTrans;
    amxd_trans_t* trans2 = swla_trans_init(&subTrans, NULL, subObj);
    assert_non_null(trans2);

    amxd_trans_set_value(cstring_t, trans2, "Param1", "F6");

    assert_ptr_equal(swla_trans_finalize(&subTrans, NULL), SWL_RC_OK);

    ttb_mockTimer_goToFutureMs(100);
    ttb_mockTimer_goToFutureMs(100);


    char* val3 = amxd_object_get_cstring_t(subObj, "Param1", NULL);
    ttb_assert_str_eq(val3, "F6");
    free(val3);

    SWL_TABLE_FIXED(expNotifs, notifDataEntry,
                    ARR(
                        {"Example.SubObject.Param1", "F6"},
                        ));
    s_checkNotifs(&expNotifs, &testInfo.resNotifs);
}

typedef struct {
    u_int16_t readHdlrCallCounter;
    swla_dm_objActionReadCtx_t onActionReadCtx;
} testReadObjectData_t;

static swl_rc_ne sObjReadHdlr(amxd_object_t* const object) {
    testReadObjectData_t* readObjData = (testReadObjectData_t*) object->priv;
    if(readObjData != NULL) {
        readObjData->readHdlrCallCounter++;
        char* obj_path = amxd_object_get_path(object, AMXD_OBJECT_TERMINATE);
        printf(">>> [%s] Read Hdlr call Count %u\n", obj_path, readObjData->readHdlrCallCounter);
        free(obj_path);
    }
    return SWL_RC_OK;
}

static amxd_status_t s_readObj_orf(amxd_object_t* const object, amxd_param_t* const param, amxd_action_t reason, const amxc_var_t* const args, amxc_var_t* const retval, void* priv) {
    testReadObjectData_t* readObjData = (testReadObjectData_t*) object->priv;
    swla_dm_objActionReadCtx_t* onActionReadCtx = (readObjData == NULL) ? NULL :  &readObjData->onActionReadCtx;
    return swla_dm_procObjActionRead(object, param, reason, args, retval, priv, onActionReadCtx, sObjReadHdlr);
}

static amxd_object_t* s_createAndGetReadObjInstance(amxd_object_t* rootObject, uint32_t index) {
    assert_non_null(rootObject);
    amxd_object_t* templateObject = amxd_object_get(rootObject, "ReadObject");
    assert_non_null(templateObject);

    amxd_trans_t trans;
    swl_object_prepareTransaction(&trans, templateObject);
    amxd_trans_add_inst(&trans, index, NULL);
    swl_object_finalizeTransactionOnLocalDm(&trans);
    ttb_mockTimer_goToFutureMs(100);
    amxd_object_t* readObjectInstance = amxd_object_get_instance(templateObject, NULL, index);

    return readObjectInstance;
}

static void s_setReadObjParamsTansaction(amxd_object_t* objInstance, const char* str) {
    assert_non_null(objInstance);
    amxd_trans_t trans;
    swl_object_prepareTransaction(&trans, objInstance);
    amxd_trans_set_value(cstring_t, &trans, "Param1", str);
    amxd_trans_set_value(cstring_t, &trans, "Param2", str);
    swl_object_finalizeTransactionOnLocalDm(&trans);
}

static void test_action_read(void** state) {
    ttb_amx_t* ttbAmx = *state;
    testInfo.ttbAmx = ttbAmx;

    amxd_object_t* rootObject = swla_object_getReferenceObject(ttbAmx->rootObj, "Example.");
    assert_non_null(rootObject);

    amxd_object_t* readObjectInstance1 = s_createAndGetReadObjInstance(rootObject, 1);
    assert_non_null(readObjectInstance1);
    testReadObjectData_t* readObjData1 = calloc(1, sizeof(testReadObjectData_t));
    assert_non_null(readObjData1);
    readObjectInstance1->priv = readObjData1;

    amxd_object_t* readObjectInstance2 = s_createAndGetReadObjInstance(rootObject, 2);
    assert_non_null(readObjectInstance2);
    testReadObjectData_t* readObjData2 = calloc(1, sizeof(testReadObjectData_t));
    assert_non_null(readObjData2);
    readObjectInstance2->priv = readObjData2;

    // Read handler is called upon setting object parameters via transaction
    s_setReadObjParamsTansaction(readObjectInstance1, "trans1");
    ttb_assert_uint_eq(readObjData1->readHdlrCallCounter, 1);
    ttb_mockTimer_goToFutureMs(100);

    // Read handler is not triggered as the last invocation occured within the DEFAULT_REFRESH_PERIOD_MS
    s_setReadObjParamsTansaction(readObjectInstance2, "trans2");
    ttb_assert_uint_eq(readObjData1->readHdlrCallCounter, 1);

    // If (minRefreshPeriod == 0) then wait at least DEFAULT_REFRESH_PERIOD_MS between two transactions
    // The read handler gets triggered after each transaction
    s_setReadObjParamsTansaction(readObjectInstance2, "trans1");
    ttb_assert_uint_eq(readObjData2->readHdlrCallCounter, 1);
    ttb_mockTimer_goToFutureMs(DEFAULT_REFRESH_PERIOD_MS);
    ttb_mockTimer_goToFutureMs(100);
    s_setReadObjParamsTansaction(readObjectInstance2, "trans2");
    ttb_assert_uint_eq(readObjData2->readHdlrCallCounter, 2);
    // If (minRefreshPeriod > 0) then wait at least minRefreshPeriod between two transactions
    swla_dm_objActionReadCtx_t* onActionReadCtx1 = &readObjData2->onActionReadCtx;
    onActionReadCtx1->minRefreshPeriodMs = 2000;
    ttb_mockTimer_goToFutureMs(DEFAULT_REFRESH_PERIOD_MS);
    ttb_mockTimer_goToFutureMs(100);
    s_setReadObjParamsTansaction(readObjectInstance2, "trans3");
    ttb_assert_uint_eq(readObjData2->readHdlrCallCounter, 2);
    ttb_mockTimer_goToFutureMs(DEFAULT_REFRESH_PERIOD_MS);
    s_setReadObjParamsTansaction(readObjectInstance2, "trans4");
    ttb_assert_uint_eq(readObjData2->readHdlrCallCounter, 3);
    onActionReadCtx1->minRefreshPeriodMs = 0;

    // Block read handler call
    ttb_mockTimer_goToFutureMs(DEFAULT_REFRESH_PERIOD_MS);
    ttb_mockTimer_goToFutureMs(100);
    swla_dm_objActionReadCtx_t* onActionReadCtx2 = &readObjData2->onActionReadCtx;
    SWLA_DM_OBJ_BLOCK_READ_HDLR_CALL(onActionReadCtx2);
    s_setReadObjParamsTansaction(readObjectInstance2, "trans3");
    ttb_assert_uint_eq(readObjData2->readHdlrCallCounter, 3);
    SWLA_DM_OBJ_ALLOW_READ_HDLR_CALL(onActionReadCtx2);

    // Multi read handling:
    /* First object get ==> read handler is invoked
     * Second object get ==> read handler is not invoked
     * wait at least DEFAULT_REFRESH_PERIOD_MS
     * Third object get ==> read handler is invoked
     * */
    amxc_var_t tmpVar;
    amxc_var_init(&tmpVar);
    amxd_object_get_params(readObjectInstance1, &tmpVar, amxd_dm_access_private);
    amxc_var_clean(&tmpVar);
    ttb_assert_uint_eq(readObjData1->readHdlrCallCounter, 2);
    ttb_mockTimer_goToFutureMs(100);
    amxc_var_init(&tmpVar);
    amxd_object_get_params(readObjectInstance1, &tmpVar, amxd_dm_access_private);
    amxc_var_clean(&tmpVar);
    ttb_assert_uint_eq(readObjData1->readHdlrCallCounter, 2);
    ttb_mockTimer_goToFutureMs(DEFAULT_REFRESH_PERIOD_MS);
    ttb_mockTimer_goToFutureMs(100);
    amxc_var_init(&tmpVar);
    amxd_object_get_params(readObjectInstance1, &tmpVar, amxd_dm_access_private);
    amxc_var_clean(&tmpVar);
    ttb_assert_uint_eq(readObjData1->readHdlrCallCounter, 3);

    // swla_dm_getObjectParams always block read handler call
    amxc_var_init(&tmpVar);
    swla_dm_getObjectParams(readObjectInstance2, &tmpVar, onActionReadCtx2);
    amxc_var_clean(&tmpVar);
    ttb_assert_uint_eq(readObjData2->readHdlrCallCounter, 3);
    ttb_mockTimer_goToFutureMs(DEFAULT_REFRESH_PERIOD_MS);
    ttb_mockTimer_goToFutureMs(100);
    amxc_var_init(&tmpVar);
    swla_dm_getObjectParams(readObjectInstance2, &tmpVar, onActionReadCtx2);
    amxc_var_clean(&tmpVar);
    ttb_assert_uint_eq(readObjData2->readHdlrCallCounter, 3);


    free(readObjData1);
    readObjectInstance1->priv = NULL;

    free(readObjData2);
    readObjectInstance2->priv = NULL;
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    sahTraceSetLevel(TRACE_LEVEL_INFO);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlDmEv");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup_teardown(test_events_onLoadingDefaults, s_test_setup, s_test_teardown),
        cmocka_unit_test_setup_teardown(test_events_onLoadingInstance, s_test_setup, s_test_teardown),
        cmocka_unit_test_setup_teardown(test_events_addInstance, s_test_setup, s_test_teardown),
        cmocka_unit_test_setup_teardown(test_events_setParams, s_test_setup, s_test_teardown),
        cmocka_unit_test_setup_teardown(test_events_delInstance, s_test_setup, s_test_teardown),
        cmocka_unit_test_setup_teardown(test_transaction_exists, s_test_setup, s_test_teardown),
        cmocka_unit_test_setup_teardown(test_transaction_notExists, s_test_setup, s_test_teardown),
        cmocka_unit_test_setup_teardown(test_action_read, s_test_setup, s_test_teardown),
    };
    ttb_util_setFilter();
    return cmocka_run_group_tests(tests, s_setupSuite, s_teardownSuite);
}
