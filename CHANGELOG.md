# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v3.13.2 - 2024-11-18(10:01:21 +0000)

### Other

- : Add debug info

## Release v3.13.1 - 2024-10-23(07:49:43 +0000)

### Other

- - Fix fd leak in swl_exec_common_buf_bg

## Release v3.13.0 - 2024-10-14(13:08:49 +0000)

### Other

- - Add SWL_EXEC_BUF_CB

## Release v3.12.9 - 2024-10-14(07:32:43 +0000)

### Other

- - fix implicit declaration basename()

## Release v3.12.8 - 2024-10-11(15:38:44 +0000)

### Other

- - fix zero time

## Release v3.12.7 - 2024-10-10(14:30:20 +0000)

### Other

- timespec real to variant conversion is incorrectly affected by time zone

## Release v3.12.6 - 2024-09-11(10:20:52 +0000)

## Release v3.12.5 - 2024-08-23(14:58:16 +0000)

### Other

- : 2.4GHz radio is reconfiguring itself in loop

## Release v3.12.4 - 2024-08-13(07:52:37 +0000)

### Other

- [UNIT-TEST] Coverage report generation issue
- -- No more than one device can be white-listed

## Release v3.12.3 - 2024-07-17(16:31:02 +0000)

### Other

- - fix CI

## Release v3.12.2 - 2024-07-03(13:54:27 +0000)

### Other

- -- wld is not responding

## Release v3.12.1 - 2024-03-18(06:53:07 +0000)

### Other

- - Timestamps are not in UTC

## Release v3.12.0 - 2024-02-08(13:18:34 +0000)

### Other

- - add abstraction for function delaying

## Release v3.11.0 - 2024-01-05(14:31:51 +0000)

### Other

- - Add api to handle object read action

## Release v3.10.0 - 2023-11-16(15:12:52 +0000)

### Other

- - add new read*FromMap functions

## Release v3.9.0 - 2023-11-16(11:56:10 +0000)

### Other

- - port existing PCB functions to AMX

## Release v3.8.0 - 2023-10-19(09:41:23 +0000)

### Other

- Add transaction functions

## Release v3.7.0 - 2023-10-12(08:32:17 +0000)

### Other

- - add api to fetch the first available index in template object

## Release v3.6.0 - 2023-07-17(08:49:23 +0000)

### Other

- add api to delete obj instance with one transaction

## Release v3.5.0 - 2023-06-13(10:06:41 +0000)

### Other

- add wrapper apis to commit typed obj params with one transaction

## Release v3.4.0 - 2023-06-08(13:51:06 +0000)

### Other

- add api to register handlers for local datamodel object events

## Release v3.3.0 - 2023-06-08(07:51:24 +0000)

### Other

- add api to register datamodel object events

## Release v3.2.0 - 2023-06-07(13:47:43 +0000)

### Other

- add type Ref functions for value types

## Release v3.1.1 - 2023-05-04(14:32:13 +0000)

### Other

- - Fix pipeline

## Release v3.1.0 - 2023-03-22(11:00:17 +0000)

### Other

- - add a delay execute function

## Release v3.0.0 - 2023-03-20(16:45:37 +0000)

### Other

- - Fix compile issues due to subtype update

## Release v2.6.0 - 2023-03-14(14:05:37 +0000)

### Other

- add apis to dump amx obj params into variant htable

## Release v2.5.0 - 2023-02-07(08:06:28 +0000)

### Other

- restore swla exec apis

## Release v2.4.1 - 2023-01-16(14:23:47 +0000)

### Other

- - Header files not exported
- - Fix variant printing

## Release v2.4.0 - 2023-01-02(09:23:25 +0000)

### Other

- - add Tuple Type array map functions

## Release v2.3.1 - 2022-12-21(10:24:49 +0000)

### Other

- - fix circ table

## Release v2.3.0 - 2022-12-19(16:11:36 +0000)

### Other

- - add File compare functions
- remove squash commits as no needed anymore

## Release v2.2.0 - 2022-11-29(09:49:22 +0000)

### Other

- Add getReferenceObject api

## Release v2.1.0 - 2022-11-09(16:26:15 +0000)

### Other

- squash commits before opensourcing them to make sahbot principal author for SoftAtHome deliveries
- - Add collection type

## Release v2.0.3 - 2022-10-07(08:59:06 +0000)

### Other

- build with -Wformat -Wformat-security

## Release v2.0.2 - 2022-10-05(13:08:04 +0000)

### Other

- - Add tupleType type functions and test example
- - update type and obj tests

## Release v2.0.1 - 2022-09-28(07:44:46 +0000)

### Other

- - Fix testing
- - Add named tuple type

## Release v2.0.0 - 2022-09-27(07:22:29 +0000)

### Other

- [prpl] clean swl swla headers install

## Release v1.1.1 - 2022-06-17(14:00:41 +0000)

### Other

- - move swl_circTable_getMatchingTuple function to libswlc

## Release v1.1.0 - 2022-06-17(09:10:18 +0000)

### Other

- - [prplMesh_WHM] provide STA capabilities for STA connected BWL Event
- [amx] ambiorix swlc awla and wld/pwhm

## Release v1.0.3 - 2022-06-09(09:40:16 +0000)

### Other

- revert ambiorix template

## Release v1.0.2 - 2022-06-08(08:24:31 +0000)

### Other

- disable complexity check

## Release v1.0.1 - 2022-06-07(07:15:30 +0000)

### Other

- include ambiorix template for ambiorix components
- [prpl] opensource pwhm swla and swlc

