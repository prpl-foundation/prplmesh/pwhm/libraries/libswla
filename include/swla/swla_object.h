/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef INCLUDE_SWLA_SWLA_OBJECT_H_
#define INCLUDE_SWLA_SWLA_OBJECT_H_

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_object.h>

/**
 * @brief fetch a reference object, with provided reference path
 *
 * @param callerObj (in): caller object, used to get common local root object name
 * @param referencePath (in): datamodel path to referenced object
 * @return obj (out): reference object if found, NULL otherwise
 */
amxd_object_t* swla_object_getReferenceObject(amxd_object_t* callerObj, const char* referencePath);

/**
 * @brief fetch a reference object, with provided reference path
 * and return included private data context
 *
 * @param callerObj (in): caller object, used to get common local root object name
 * @param referencePath (in): datamodel path to referenced object
 * @return void* (out): reference object's private data
 */
void* swla_object_getReferenceObjectPriv(amxd_object_t* callerObj, const char* referencePath);

/**
 * @brief dump object's parameters into variant map
 *
 * @param map (out): output variant map, will include object parameters
 * @param obj (in): source object
 * @return amxd_status_ok if dump is successful, error code otherwise
 */
amxd_status_t swla_object_paramsToMap(amxc_var_t* map, amxd_object_t* obj);

/**
 * @brief add child object's parameters into new sub map section, named with child object name
 *
 * @param map (out): output variant map, will include new sub map with child object parameters
 * @param parentObj (in): source parent object
 * @param childObjName (in): child object name
 * @return amxd_status_ok if dump is successful, error code otherwise
 */
amxd_status_t swla_object_addChildParamsToMap(amxc_var_t* map, amxd_object_t* parentObj, const char* childObjName);

/**
 * @brief fetch in template object, the first available index to be used
 * when creating new instance
 *
 * @param templateObj (in): template object in which the new instance will be added
 * @return the found available index, 0 if templateObj is not a template object
 */
uint32_t swla_object_getFirstAvailableIndex(amxd_object_t* templateObj);

/*
 * @brief generic utility macro to set object's typed parameter value
 */
#define SWLA_OBJECT_SET_PARAM_TYPED(type, object, paramName, val) { \
        amxd_param_t* param = amxd_object_get_param_def(object, paramName); \
        if(param != NULL) {amxc_var_set_ ## type(&param->value, val);}}

/*
 * @brief generic utility macro to set object's int32 parameter value
 */
#define SWLA_OBJECT_SET_PARAM_INT32(object, paramName, val) \
    SWLA_OBJECT_SET_PARAM_TYPED(int32_t, object, paramName, val)

/*
 * @brief generic utility macro to set object's uint32 parameter value
 */
#define SWLA_OBJECT_SET_PARAM_UINT32(object, paramName, val) \
    SWLA_OBJECT_SET_PARAM_TYPED(uint32_t, object, paramName, val)

/*
 * @brief generic utility macro to set object's uint64 parameter value
 */
#define SWLA_OBJECT_SET_PARAM_UINT64(object, paramName, val) \
    SWLA_OBJECT_SET_PARAM_TYPED(uint64_t, object, paramName, val)

/*
 * @brief generic utility macro to set object's bool parameter value
 */
#define SWLA_OBJECT_SET_PARAM_BOOL(object, paramName, val) \
    SWLA_OBJECT_SET_PARAM_TYPED(bool, object, paramName, val)

/*
 * @brief generic utility macro to set object's char string parameter value
 */
#define SWLA_OBJECT_SET_PARAM_CSTRING(object, paramName, val) \
    SWLA_OBJECT_SET_PARAM_TYPED(cstring_t, object, paramName, val)

/*
 * @brief prepare transaction to set parameters of selected object
 * (including read-only parameters)
 *
 * @param trans pointer to transaction context to be initialized
 * @param object object to be altered and committed
 *
 * @return amxd_status_ok in case of success, error code otherwise
 */
amxd_status_t swl_object_prepareTransaction(amxd_trans_t* const trans, const amxd_object_t* object);

/*
 * @brief finalize transaction (commit and clean it up)
 *
 * @param trans transaction context to be applied and cleaned up
 * @param dm datamodel context on which transaction must be applied
 *
 * @return amxd_status_ok on success, error code otherwise
 */
amxd_status_t swl_object_finalizeTransaction(amxd_trans_t* trans, amxd_dm_t* dm);

/*
 * @brief finalize transaction (commit and clean it up) on local dm context
 * (dm retrieved from registered amxb_bus_ctx_t set with swl_lib_initialize)
 *
 * @param trans transaction context to be applied and cleaned up
 *
 * @return amxd_status_ok on success, error code otherwise
 */
amxd_status_t swl_object_finalizeTransactionOnLocalDm(amxd_trans_t* trans);

/*
 * @brief delete object instance using transaction
 *
 * @param object object instance to be deleted
 * @param dm datamodel context on which transaction must be applied
 *
 * @return amxd_status_ok on success, error code otherwise
 */
amxd_status_t swl_object_delInstWithTrans(const amxd_object_t* instance, amxd_dm_t* dm);

/*
 * @brief delete object instance using transaction on local dm context
 * (dm retrieved from registered amxb_bus_ctx_t set with swl_lib_initialize)
 *
 * @param instance object instance to be deleted
 *
 * @return amxd_status_ok on success, error code otherwise
 */
amxd_status_t swl_object_delInstWithTransOnLocalDm(const amxd_object_t* instance);

#endif /* INCLUDE_SWLA_SWLA_OBJECT_H_ */
