/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef INCLUDE_SWLA_SWLA_DM_H_
#define INCLUDE_SWLA_SWLA_DM_H_

#include <stdint.h>
#include "swl/swl_common.h"
#include "swl/swl_common_time_spec.h"
#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_action.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>

/*
 * @brief proto handler of datamodel parameter change
 */
typedef void (* paramChangedHandler_f)(void* priv, amxd_object_t* object, amxd_param_t* param, const amxc_var_t* const newValue);

/*
 * @brief proto handler of datamodel object change
 */
typedef void (* objChangedHandler_f)(void* priv, amxd_object_t* object, const amxc_var_t* const newParamValues);

/*
 * @brief proto handler of datamodel object instance addition
 */
typedef void (* instAddedHandler_f)(void* priv, amxd_object_t* object, const amxc_var_t* const intialParamValues);

/*
 * @brief proto handler of datamodel object read action
 */
typedef swl_rc_ne (* objActionReadHandler_f)(amxd_object_t* const object);

/*
 * @brief struct describing handler of datamodel parameter change
 * (parent object is known when handler is called)
 */
typedef struct {
    // parameter name (no issue with multi params having same name, but located in different objects)
    const char* paramName;
    // handler for parameter value change
    paramChangedHandler_f paramChangedCb;
} swla_dm_paramEvtHdlr_t;

#define SWLA_DM_PARAM_HDLR(name, cb) ARR(.paramName = name, .paramChangedCb = cb, )

typedef struct {
    swla_dm_paramEvtHdlr_t* paramHdlrs; //list of param handlers
    uint32_t nrParamHdlrs;              //number of param handlers
    objChangedHandler_f objChangedCb;   //object change handler
    instAddedHandler_f instAddedCb;     //instance added handler
} swla_dm_objEvtHdlrs_t;

#define SWLA_DM_HDLRS(tableName, _paramHdlrs, ...) \
    swla_dm_paramEvtHdlr_t tableName ## ParamHdlrs[] = _paramHdlrs; \
    swla_dm_objEvtHdlrs_t tableName = { \
        .paramHdlrs = tableName ## ParamHdlrs, \
        .nrParamHdlrs = SWL_ARRAY_SIZE(tableName ## ParamHdlrs), \
        ## __VA_ARGS__ \
    }

#define DEFAULT_REFRESH_PERIOD_MS 1000   //Default value of the minimum interval between read handler calls
typedef struct {
    bool blockReadHdlrCall;
    swl_timeSpecMono_t lastReadHdlrCallTime;
    u_int32_t minRefreshPeriodMs;
} swla_dm_objActionReadCtx_t;

#define SWLA_DM_OBJ_BLOCK_READ_HDLR_CALL(pOnActionReadCtx) \
    if(pOnActionReadCtx) { \
        pOnActionReadCtx->blockReadHdlrCall = true; \
    }

#define SWLA_DM_OBJ_ALLOW_READ_HDLR_CALL(pOnActionReadCtx) \
    if(pOnActionReadCtx) { \
        pOnActionReadCtx->blockReadHdlrCall = false; \
    }

/*
 * @brief process object related received event:
 * - call instance addition handler with all configured parameters
 * - call object addition handler with all configured parameters
 * - fetch watched params in list of those changed, and call their relative handlers
 *
 * @param dm amx datamodel context
 * @param hdlrs struct including the event handlers
 * @param sig_name signal name
 * @param eventData event data including the altered object (and eventually list of changed parameters)
 * @param priv private user data
 *
 * @return SWL_RC_OK on success, error code otherwise
 */
swl_rc_ne swla_dm_procObjEvt(amxd_dm_t* dm, swla_dm_objEvtHdlrs_t* hdlrs, const char* const sig_name, const amxc_var_t* const eventData, void* priv);

/*
 * @brief process object related received events, of local dm context
 * (dm retrieved from registered amxb_bus_ctx_t set with swl_lib_initialize)
 *
 * @param hdlrs struct including the event handlers
 * @param sig_name signal name
 * @param eventData event data including the altered object (and eventually list of changed parameters)
 * @param priv private user data
 *
 * @return SWL_RC_OK on success, error code otherwise
 */
swl_rc_ne swla_dm_procObjEvtOfLocalDm(swla_dm_objEvtHdlrs_t* hdlrs, const char* const sig_name, const amxc_var_t* const eventData, void* priv);

/*
 * @brief get all parameter values of an object.
 * If the objUpdate parameter is not NULL, the object's parameters value update,
 * triggered by the read handler, will be blocked:
 * the current values of the object's parameters will be returned.
 *
 * @param object Pointer to a data model object.
 * @param retval An initialized variant which contains the values of all parameters.
 * @param objUpdate Pointer to a data structure to Blok/Allow the call of the object's read handler. This pointer can be NULL.
 *
 * return amxd_status_ok on success.
 */
amxd_status_t swla_dm_getObjectParams(amxd_object_t* const object, amxc_var_t* const retval, swla_dm_objActionReadCtx_t* onActionReadCtx);

/*
 * @brief process object read action.
 * when objUpdate is not null, verify whether invoking the readHdlr function
 * is necessary to update the values of the object's.
 * if the updte is not necessary, then get only the current values of the object's parameters.
 *
 * @param object The data model object on which the action is invoked.
 * @param param This is always NULL for the object read action.
 * @param reason The action reason: action_object_read.
 * @param args A variant containing the action arguments.
 * @param retval A variant that must be filled with the action return data.
 * @param priv An opaque pointer (user data), that is added when the action callback was set.
 * @param objUpdate Pointer to a data structure containing the object's read handler last call
 *                  and indicating whether the call of the read handler should be blocked. This pointer can be NULL.
 * @param readHdlr Handler of the object read action.
 *
 * return amxd_status_ok if successful.
 */
amxd_status_t swla_dm_procObjActionRead(amxd_object_t* const object,
                                        amxd_param_t* const param,
                                        amxd_action_t reason,
                                        const amxc_var_t* const args,
                                        amxc_var_t* const retval,
                                        void* priv,
                                        swla_dm_objActionReadCtx_t* onActionReadCtx, objActionReadHandler_f readHdlr);

/*
 * @brief return full datamodel parameter path string
 *
 * @param param amx parameter context
 * @param indexed wether returned path is in indexed format
 *
 * @return string with full path (to be freed by caller) or NULL in case of error
 *
 */
char* swl_dm_getParamPath(amxd_param_t* param, bool indexed);

#endif /* INCLUDE_SWLA_SWLA_DMEVTS_H_ */
