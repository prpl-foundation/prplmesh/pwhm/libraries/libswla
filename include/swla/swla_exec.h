/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
/**
 * Helper functions to streamline executing other programs or scripts.
 *
 * @note requires swl_lib to be initialized.
 */
#ifndef SRC_INCLUDE_SWL_EXEC_H_
#define SRC_INCLUDE_SWL_EXEC_H_

#include "swla/swla_commonLib.h"

#define SWL_EXEC_BUF_MAX_ALLOC_SIZE 1024

typedef struct {
    bool isExited;      // process exited
    int32_t exitStatus; // exit code
    bool isSignaled;    // process ended with signal
    int32_t termSignal; // received signal
} swl_exec_procExitInfo_t;

typedef void (* swl_exec_doneHandler_f) (amxp_subproc_t* subProc, const swl_exec_procExitInfo_t* const exitInfo, void* const priv);

amxp_subproc_t* swl_exec_common_bg(const char* file, uint32_t line,
                                   swl_exec_doneHandler_f doneHandler, void* userData,
                                   const char* prg, char* format, ...);

swl_rc_ne swl_exec_common_fd(const char* file, uint32_t line,
                             int* pFdOut, int* pFdErr,
                             const char* prg, char* format, ...);

swl_rc_ne swl_exec_common_buf(const char* file, uint32_t line,
                              char* buf, size_t bufSize,
                              const char* prg, char* format, ...);

typedef struct {
    char* outBuf;                     /* std output buffer (not filled if null). */
    size_t outBufSize;                /* std output buffer size (not filled if null). */
    char* errBuf;                     /* std error buffer (not filled if null). */
    size_t errBufSize;                /* std error buffer size (not filled if null). */
    swl_exec_procExitInfo_t exitInfo; /* child termination info. */
} swl_exec_result_t;

swl_rc_ne swl_exec_common_buf_ext(const char* file, uint32_t line,
                                  swl_exec_result_t* pResult,
                                  const char* prg, char* format, ...);

typedef void (* swl_exec_doneBufHandler_f) (amxp_subproc_t* subProc, const swl_exec_result_t* const result, void* const priv);

amxp_subproc_t* swl_exec_common_buf_bg(const char* file, uint32_t line,
                                       swl_exec_doneBufHandler_f doneHandler, void* userData,
                                       uint32_t maxStdOutBufSize, uint32_t maxStdErrBufSize,
                                       const char* prg, const char* format, ...);

/**
 * @brief execute a program and wait for it to finish.
 *
 * @param prg: the program to be called to execute
 * @param format: the arguements to be added, as list of arguements
 * @return SWL_RC_OK if program correctly started, SWL_RC_ERROR otherwise
 */
#define SWL_EXEC(prg, ...) \
    swl_exec_common_fd(__FILE__, __LINE__, NULL, NULL, prg, ## __VA_ARGS__)


/**
 * @brief execute a program in background
 *
 * @param prg: the program to be called to execute
 * @param format: the arguements to be added, as list of arguements
 * @return subProc context if program correctly started, NULL otherwise
 */
#define SWL_EXEC_BG(prg, ...) \
    swl_exec_common_bg(__FILE__, __LINE__, NULL, NULL, NULL, NULL, prg, ## __VA_ARGS__)

/**
 * @brief execute a program in background, and call doneHandler when done
 *
 * @param doneHandler: the callback handler. Shall be called when task finishes. Can be NULL
 * @param userData: the userdata to be passed to the callback handler
 * @param prg: the program to be called to execute
 * @param format: the arguements to be added, as list of arguements
 * @return subProc context if program correctly started, NULL otherwise
 */
#define SWL_EXEC_CB(doneHandler, userData, prg, ...) \
    swl_exec_common_bg(__FILE__, __LINE__, doneHandler, userData, prg, ## __VA_ARGS__)

/**
 * @brief execute a program and put output in provided file descriptors. Returns when done.
 *
 * @param pFdOut: the output file descriptor
 * @param pFdErr: the error file descriptor
 * @param prg: the program to be called to execute
 * @param format: the arguements to be added, as list of arguements
 * @return SWL_RC_OK if program correctly started, SWL_RC_ERROR otherwise. Note that
 *   it is possible the program failed during run, but this shall not be reflected in return value.
 */
#define SWL_EXEC_FD(fdOut, fdErr, prg, ...) \
    swl_exec_common_fd(__FILE__, __LINE__, fdOut, fdErr, prg, ## __VA_ARGS__)

/**
 * @brief execute a program and write stdOut into given buffer
 *
 * @param buf: the buffer to write output to
 * @param bufSize: bufSize
 * @param prg: the program to be called to execute
 * @param format: the arguements to be added, as list of arguements
 * @return SWL_RC_OK if program correctly started, SWL_RC_ERROR otherwise. Note that
 *   it is possible the program failed during run, but this shall not be reflected in return value.
 */
#define SWL_EXEC_BUF(buf, bufSize, prg, ...) \
    swl_exec_common_buf(__FILE__, __LINE__, buf, bufSize, prg, ## __VA_ARGS__)

/**
 * @brief execute a program and write stdOut stdErr (buffers), exitCode and exitSignal into given structure.
 *
 * @param retResult: pointer to struct to be filled with all execution results
 * @param prg: the program to be called to execute
 * @param format: the arguements to be added, as list of arguements
 * @return SWL_RC_OK if program correctly started, SWL_RC_ERROR otherwise. Note that
 *   it is possible the program failed during run, but this shall not be reflected in return value.
 *   Output and error buffers are filled only if not null and sized.
 */
#define SWL_EXEC_BUF_EXT(result, prg, ...) \
    swl_exec_common_buf_ext(__FILE__, __LINE__, result, prg, ## __VA_ARGS__)

/**
 * @brief execute a program in background, and call doneHandler with stdout and stderr when done
 *
 * @param doneHandler: the callback handler. Shall be called when task finishes. Can be NULL.
 *   stdout and stderr will be passed each up to SWL_EXEC_BUF_MAX_ALLOC_SIZE - 1 bytes. Beyond that,
 *   the output of the program is truncated.
 * @param userData: the userdata to be passed to the callback handler
 * @param prg: the program to be called to execute
 * @param format: the arguments to be added, as list of arguments
 * @return subProc context if program correctly started, NULL otherwise
 */

#define SWL_EXEC_BUF_CB(doneHandler, userData, prg, ...) \
    swl_exec_common_buf_bg(__FILE__, __LINE__, doneHandler, userData, \
                           SWL_EXEC_BUF_MAX_ALLOC_SIZE, SWL_EXEC_BUF_MAX_ALLOC_SIZE, \
                           prg, ## __VA_ARGS__)

#endif /* SRC_INCLUDE_SWL_EXEC_H_ */
