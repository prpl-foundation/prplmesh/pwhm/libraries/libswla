/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef SRC_INCLUDE_SWL_TTB_SWL_TTBOBJECT_H_
#define SRC_INCLUDE_SWL_TTB_SWL_TTBOBJECT_H_

#include "swl/ttb/swl_ttb.h"
#include "swla/swla_type.h"
#include "test-toolbox/ttb_util.h"


/**
 * Write the output of calling a given amx function with given reqVar to file
 * ReqVar can be NULL
 */
#define swla_ttbObject_callFunToFile(ttb_amx, obj, functionName, reqVar, fileName) \
    { \
        assert_non_null(obj); \
        ttb_var_t* replyVar = NULL; \
        ttb_reply_t* reply = ttb_object_callFun(ttb_amx, obj, functionName, reqVar, &replyVar); \
        ttb_assert_non_null(reply); \
        ttb_assert_non_null(replyVar); \
        ttb_util_printVariantToJsonFileName(replyVar, fileName, NULL); \
        ttb_object_cleanReply(&reply, &replyVar); \
    }

/**
 * Assert calling function with given request var equals file name.
 * Write file with swla_ttbObject_callFunToFile
 */
#define swla_ttbObject_assertCallFunEqFile(ttb_amx, obj, functionName, reqVar, fileName) \
    { \
        char tmpNameBuffer[L_tmpnam] = {0}; \
        tmpnam(tmpNameBuffer); \
        swla_ttbObject_callFunToFile(ttb_amx, obj, functionName, reqVar, !gTtb_assert_printMode ? tmpNameBuffer : fileName); \
        if(!gTtb_assert_printMode) { \
            swl_ttb_assertFileEquals(tmpNameBuffer, fileName); \
            remove(tmpNameBuffer); \
        } \
    }

/**
 * Write the output of calling a given amx function with given reqVar to file
 * ReqVar can be NULL
 * Also puts the output reply in reply, and reply variant in myReplyVar.
 * These must be cleaned when done.
 */
#define swla_ttbObject_callFunToFileExt(ttb_amx, obj, functionName, reqVar, fileName, reply, myReplyVar) \
    { \
        assert_non_null(obj); \
        reply = ttb_object_callFun(ttb_amx, obj, functionName, reqVar, &myReplyVar); \
        ttb_assert_non_null(reply); \
        ttb_assert_non_null(myReplyVar); \
        ttb_util_printVariantToJsonFileName(myReplyVar, fileName, NULL); \
    }

/**
 * Assert calling function with given request var equals file name.
 * Also puts the output reply in reply, and reply variant in myReplyVar.
 * These must be cleaned when done.
 */
#define swla_ttbObject_assertCallFunEqFileExt(ttb_amx, obj, functionName, reqVar, fileName, reply, myReplyVar) \
    { \
        char tmpNameBuffer[L_tmpnam] = {0}; \
        tmpnam(tmpNameBuffer); \
        swla_ttbObject_callFunToFileExt(ttb_amx, obj, functionName, reqVar, !gTtb_assert_printMode ? tmpNameBuffer : fileName, reply, myReplyVar); \
        if(!gTtb_assert_printMode) { \
            swl_ttb_assertFileEquals(tmpNameBuffer, fileName); \
            remove(tmpNameBuffer); \
        } \
    }

/**
 * Call a function with given input type and data that will be converted to input variant based on type to variant functions.
 */
#define swla_ttbObject_callTypeFun(ttb_amx, obj, functionName, inType, inTypeData) \
    { \
        ttb_var_t* reqVar = ttb_object_createArgs(); \
        assert_non_null(reqVar); \
        swl_type_toVariant((swl_type_t*) inType, reqVar, inTypeData); \
        ttb_reply_t* reply = ttb_object_callFun(ttb_amx, obj, functionName, &reqVar, NULL); \
        ttb_object_cleanReply(&reply, NULL); \
    }

/**
 * Call a function with given input type and data, and will write to output type data.
 */
#define swla_ttbObject_callTypeFunRep(ttb_amx, obj, functionName, inType, inTypeData, repType, repTypeData) \
    { \
        ttb_var_t* reqVar = ttb_object_createArgs(); \
        assert_non_null(reqVar); \
        ttb_var_t* replyVar = NULL; \
        swl_type_toVariant((swl_type_t*) inType, reqVar, inTypeData); \
        ttb_reply_t* reply = ttb_object_callFun(ttb_amx, obj, functionName, &reqVar, &replyVar); \
        assert_non_null(reply); \
        assert_non_null(replyVar); \
        swl_type_fromVariant((swl_type_t*) repType, repTypeData, replyVar); \
        ttb_object_cleanReply(&reply, &replyVar); \
    }

/**
 * Call a function with given input type and data, write to given file based on ttb print.
 */
#define swla_ttbObject_callTypeFunToFile(ttb_amx, obj, functionName, inType, inTypeData, fileName) \
    { \
        assert_non_null(obj); \
        assert_non_null(ttb_amx); \
        ttb_var_t* reqVar = ttb_object_createArgs(); \
        assert_non_null(reqVar); \
        swl_type_toVariant((swl_type_t*) inType, reqVar, inTypeData); \
        swla_ttbObject_callFunToFile(ttb_amx, obj, functionName, &reqVar, fileName); \
    }

/**
 * Assert calling function with given input type and data, write to output type data, and also to given file, using output type,
 * and assert that written file matches fileName.
 */
#define swla_ttbObject_assertCallTypeFunEqFile(ttb_amx, obj, functionName, inType, inTypeData, fileName) \
    { \
        char tmpNameBuffer[L_tmpnam] = {0}; \
        tmpnam(tmpNameBuffer); \
        swla_ttbObject_callTypeFunToFile(ttb_amx, obj, functionName, inType, inTypeData, !gTtb_assert_printMode ? tmpNameBuffer : fileName); \
        if(!gTtb_assert_printMode) { \
            swl_ttb_assertFileEquals(tmpNameBuffer, fileName); \
            remove(tmpNameBuffer); \
        } \
    }


/**
 * Call a function with given input type and data, write to output type data, and also to given file, using output type.
 */
#define swla_ttbObject_callTypeFunRepToFile(ttb_amx, obj, functionName, inType, inTypeData, outType, outTypeData, fileName, printArgs) \
    { \
        ttb_var_t* reqVar = ttb_object_createArgs(); \
        assert_non_null(reqVar); \
        swl_type_toVariant((swl_type_t*) inType, reqVar, inTypeData); \
        assert_non_null(obj); \
        ttb_var_t* replyVar = NULL; \
        ttb_reply_t* reply = ttb_object_callFun(ttb_amx, obj, functionName, &reqVar, &replyVar); \
        ttb_assert_non_null(reply); \
        ttb_assert_non_null(replyVar); \
        swl_type_fromVariant((swl_type_t*) outType, outTypeData, replyVar); \
        ttb_object_cleanReply(&reply, &replyVar); \
        swl_type_toFileName((swl_type_t*) outType, fileName, SWL_TYPE_TO_PTR(((swl_type_t*) outType), outTypeData), printArgs, false); \
    }

/**
 * Call a function with given input type and data, write to output type data, and also to given file, using output type.
 */
#define swla_ttbObject_assertCallTypeFunRepEqFile(ttb_amx, obj, functionName, inType, inTypeData, outType, outTypeData, fileName, printArgs) \
    { \
        char tmpNameBuffer[L_tmpnam] = {0}; \
        tmpnam(tmpNameBuffer); \
        swla_ttbObject_callTypeFunRepToFile(ttb_amx, obj, functionName, inType, inTypeData, outType, outTypeData, !gTtb_assert_printMode ? tmpNameBuffer : fileName, printArgs); \
        if(!gTtb_assert_printMode) { \
            swl_ttb_assertFileEquals(tmpNameBuffer, fileName); \
            remove(tmpNameBuffer); \
        } \
    }

#endif /* SRC_INCLUDE_SWL_TTB_SWL_TTBOBJECT_H_ */
