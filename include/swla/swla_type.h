/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef SRC_INCLUDE_SWL_TYPE_H_
#define SRC_INCLUDE_SWL_TYPE_H_

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include "swl/swl_common_type.h"

typedef bool (* swl_type_toAmxVariant_cb) (swl_type_t* type, amxc_var_t* tgt, const swl_typeData_t* srcData);
typedef bool (* swl_type_fromAmxVariant_cb) (swl_type_t* type, swl_typeEl_t* tgtData, const amxc_var_t* src);


bool swl_type_toVariant(swl_type_t* type, amxc_var_t* tgt, const swl_typeData_t* srcData);
bool swl_type_fromVariant(swl_type_t* type, swl_typeEl_t* tgtData, const amxc_var_t* src);

bool swl_type_toObjectParam(swl_type_t* type, amxd_object_t* object, const char* param, swl_typeData_t* srcData);
bool swl_type_toTransParam(swl_type_t* type, amxd_trans_t* const trans, const char* param, swl_typeData_t* srcData);
bool swl_type_commitObjectParam(swl_type_t* type, amxd_object_t* const object, const char* param, swl_typeData_t* srcData);
bool swl_type_fromObjectParam(swl_type_t* type, amxd_object_t* object, const char* param, swl_typeEl_t* tgtData);
bool swl_type_addToMap(swl_type_t* type, amxc_var_t* map, const char* param, swl_typeData_t* srcData);
bool swl_type_findInMap(swl_type_t* type, const amxc_var_t* map, const char* param, swl_typeEl_t* tgtData);

// Use these functions to force the srcData to be converted to a string variant.
bool swl_type_toVariantString(swl_type_t* type, amxc_var_t* tgt, const swl_typeData_t* srcData);
bool swl_type_fromVariantString(swl_type_t* type, swl_typeEl_t* tgtData, const amxc_var_t* src);
bool swl_type_toObjectParamString(swl_type_t* type, amxd_object_t* object, const char* param, swl_typeData_t* srcData);
bool swl_type_toTransParamString(swl_type_t* type, amxd_trans_t* const trans, const char* param, swl_typeData_t* srcData);
bool swl_type_commitObjectParamString(swl_type_t* type, amxd_object_t* object, const char* param, swl_typeData_t* srcData);
bool swl_type_fromObjectParamString(swl_type_t* type, amxd_object_t* object, const char* param, swl_typeEl_t* tgtData);
bool swl_type_addToMapString(swl_type_t* type, amxc_var_t* map, const char* param, swl_typeData_t* srcData);
bool swl_type_findInMapString(swl_type_t* type, const amxc_var_t* map, const char* param, swl_typeEl_t* tgtData);

bool swl_type_arrayObjectParamSetChar(amxd_object_t* object, const char* name, swl_type_t* type, swl_typeEl_t* array, size_t arraySize);
bool swl_type_arrayTransParamSetChar(amxd_trans_t* trans, const char* name, swl_type_t* type, swl_typeEl_t* array, size_t arraySize);
size_t swl_type_arrayObjectParamChar(amxd_object_t* object, const char* name, swl_type_t* type, swl_typeEl_t* tgtArray, size_t tgtArraySize);
bool swl_type_arrayMapAddChar(amxc_var_t* map, const char* name, swl_type_t* type, swl_typeEl_t* array, size_t arraySize);
size_t swl_type_arrayMapFindChar(amxc_var_t* map, const char* name, swl_type_t* type, swl_typeEl_t* tgtArray, size_t tgtArraySize);

bool swl_type_arrayToVariantList(swl_type_t* type, amxc_var_t* tgtVarList, swl_typeEl_t* srcArray, size_t srcArraySize);
bool swl_type_arrayFromVariantList(swl_type_t* type, swl_typeEl_t* tgtArray, size_t tgtArraySize, amxc_var_t* srcVarList, size_t* tgtElementsWritten);


#define SWL_AMX_TYPE_DEFINE_FUNCTIONS_GENERIC(name, swlType, cType) \
    static inline size_t swl_type ## name ## _arrayObjectParamSetChar(amxd_object_t * object, const char* name, cType * array, size_t arraySize) { \
        return swl_type_arrayObjectParamSetChar(object, name, swlType, array, arraySize);} \
    static inline size_t swl_type ## name ## _arrayTransParamSetChar(amxd_trans_t * trans, const char* name, cType * array, size_t arraySize) { \
        return swl_type_arrayTransParamSetChar(trans, name, swlType, array, arraySize);} \
    static inline size_t swl_type ## name ## _arrayObjectParamChar(amxd_object_t * object, const char* name, cType * tgtArray, size_t tgtArraySize) { \
        return swl_type_arrayObjectParamChar(object, name, swlType, tgtArray, tgtArraySize);} \
    static inline size_t swl_type ## name ## _arrayMapAddChar(amxc_var_t * map, const char* name, cType * array, size_t arraySize) { \
        return swl_type_arrayMapAddChar(map, name, swlType, array, arraySize);} \
    static inline size_t swl_type ## name ## _arrayMapFindChar(amxc_var_t * map, const char* name, cType * tgtArray, size_t tgtArraySize) { \
        return swl_type_arrayMapFindChar(map, name, swlType, tgtArray, tgtArraySize);} \

#define SWL_AMX_TYPE_DEFINE_FUNCTIONS_VAL(name, swlType, cType) \
    static inline bool swl_type ## name ## _toVariant(amxc_var_t * tgt, cType srcData) { \
        return swl_type_toVariant(swlType, tgt, &srcData);} \
    static inline bool swl_type ## name ## _toVariantRef(amxc_var_t * tgt, cType * srcData) { \
        return swl_type_toVariant(swlType, tgt, srcData);} \
    static inline bool swl_type ## name ## _fromVariant(cType * tgtData, const amxc_var_t * src) { \
        return swl_type_fromVariant(swlType, tgtData, src);} \
    static inline cType swl_type ## name ## _fromVariantDef(const amxc_var_t * src, cType defaultVal) { \
        cType tmpVal; \
        if(swl_type_fromVariant(swlType, &tmpVal, src)) { \
            return tmpVal; \
        } \
        return defaultVal;} \
    static inline bool swl_type ## name ## _toObjectParam(amxd_object_t * object, const char* param, cType srcData) { \
        return swl_type_toObjectParam(swlType, object, param, &srcData);} \
    static inline bool swl_type ## name ## _commitObjectParam(amxd_object_t * object, const char* param, cType srcData) { \
        return swl_type_commitObjectParam(swlType, object, param, &srcData);} \
    static inline bool swl_type ## name ## _toTransParam(amxd_trans_t* const trans, const char* param, cType srcData) { \
        return swl_type_toTransParam(swlType, trans, param, &srcData);} \
    static inline bool swl_type ## name ## _toObjectParamRef(amxd_object_t * object, const char* param, cType * srcData) { \
        return swl_type_toObjectParam(swlType, object, param, srcData);} \
    static inline bool swl_type ## name ## _commitObjectParamRef(amxd_object_t * object, const char* param, cType * srcData) { \
        return swl_type_commitObjectParam(swlType, object, param, srcData);} \
    static inline bool swl_type ## name ## _toTransParamRef(amxd_trans_t* const trans, const char* param, cType * srcData) { \
        return swl_type_toTransParam(swlType, trans, param, srcData);} \
    static inline bool swl_type ## name ## _fromObjectParam(amxd_object_t * object, const char* param, cType * tgtData) { \
        return swl_type_fromObjectParam(swlType, object, param, tgtData);} \
    static inline cType swl_type ## name ## _fromObjectParamDef(amxd_object_t * object, const char* param, cType defaultVal) { \
        cType tmpVal; \
        if(swl_type_fromObjectParam(swlType, object, param, &tmpVal)) { \
            return tmpVal; \
        } \
        return defaultVal;} \
    static inline bool swl_type ## name ## _toObjectParamString(amxd_object_t * object, const char* param, cType srcData) { \
        return swl_type_toObjectParamString(swlType, object, param, &srcData);} \
    static inline bool swl_type ## name ## _commitObjectParamString(amxd_object_t * object, const char* param, cType srcData) { \
        return swl_type_commitObjectParamString(swlType, object, param, &srcData);} \
    static inline bool swl_type ## name ## _toTransParamString(amxd_trans_t* const trans, const char* param, cType srcData) { \
        return swl_type_toTransParamString(swlType, trans, param, &srcData);} \
    static inline bool swl_type ## name ## _toObjectParamStringRef(amxd_object_t * object, const char* param, cType * srcData) { \
        return swl_type_toObjectParamString(swlType, object, param, srcData);} \
    static inline bool swl_type ## name ## _commitObjectParamStringRef(amxd_object_t * object, const char* param, cType * srcData) { \
        return swl_type_commitObjectParamString(swlType, object, param, srcData);} \
    static inline bool swl_type ## name ## _toTransParamStringRef(amxd_trans_t* const trans, const char* param, cType * srcData) { \
        return swl_type_toTransParamString(swlType, trans, param, srcData);} \
    static inline bool swl_type ## name ## _fromObjectParamString(amxd_object_t * object, const char* param, cType * tgtData) { \
        return swl_type_fromObjectParamString(swlType, object, param, tgtData);} \
    static inline cType swl_type ## name ## _fromObjectParamStringDef(amxd_object_t * object, const char* param, cType defaultVal) { \
        cType tmpVal; \
        if(swl_type_fromObjectParam(swlType, object, param, &tmpVal)) { \
            return tmpVal; \
        } \
        return defaultVal;} \
    static inline bool swl_type ## name ## _addToMap(amxc_var_t * map, const char* param, cType srcData) { \
        return swl_type_addToMap(swlType, map, param, &srcData);} \
    static inline bool swl_type ## name ## _addToMapRef(amxc_var_t * map, const char* param, cType * srcData) { \
        return swl_type_addToMap(swlType, map, param, srcData);} \
    static inline bool swl_type ## name ## _findInMap(const amxc_var_t * map, const char* param, cType * tgtData) { \
        return swl_type_findInMap(swlType, map, param, tgtData);} \
    static inline cType swl_type ## name ## _findInMapDef(const amxc_var_t * map, const char* param, cType defaultVal) { \
        cType tmpVal; \
        if(swl_type_findInMap(swlType, map, param, &tmpVal)) { \
            return tmpVal; \
        } \
        return defaultVal;} \
    SWL_AMX_TYPE_DEFINE_FUNCTIONS_GENERIC(name, swlType, cType)

#define SWL_TYPE_DEFINE_FUNCTIONS_VAL(name, swlType, cType) \
    SWL_COMMON_TYPE_DEFINE_FUNCTIONS_VAL(name, swlType, cType) \
    SWL_AMX_TYPE_DEFINE_FUNCTIONS_VAL(name, swlType, cType)

#define SWL_AMX_TYPE_DEFINE_FUNCTIONS_REF(name, swlType, cType) \
    static inline bool swl_type ## name ## _toVariant(amxc_var_t * tgt, cType srcData) { \
        return swl_type_toVariant(swlType, tgt, srcData);} \
    static inline bool swl_type ## name ## _fromVariant(cType * tgtData, const amxc_var_t * src) { \
        return swl_type_fromVariant(swlType, tgtData, src);} \
    static inline cType swl_type ## name ## _fromVariantDef(const amxc_var_t * src, cType defaultVal) { \
        cType tmpVal; \
        if(swl_type_fromVariant(swlType, &tmpVal, src)) { \
            return tmpVal; \
        } \
        if(defaultVal) { \
            return swl_type_copy(swlType, defaultVal); \
        } \
        return NULL;} \
    static inline bool swl_type ## name ## _toObjectParam(amxd_object_t * object, const char* param, cType srcData) { \
        return swl_type_toObjectParam(swlType, object, param, srcData);} \
    static inline bool swl_type ## name ## _commitObjectParam(amxd_object_t * object, const char* param, cType srcData) { \
        return swl_type_commitObjectParam(swlType, object, param, srcData);} \
    static inline bool swl_type ## name ## _fromObjectParam(amxd_object_t * object, const char* param, cType * tgtData) { \
        return swl_type_fromObjectParam(swlType, object, param, tgtData);} \
    static inline cType swl_type ## name ## _fromObjectParamDef(amxd_object_t * object, const char* param, cType defaultVal) { \
        cType tmpVal; \
        if(swl_type_fromObjectParam(swlType, object, param, &tmpVal)) { \
            return tmpVal; \
        } \
        if(defaultVal) { \
            return swl_type_copy(swlType, defaultVal); \
        } \
        return NULL;} \
    static inline bool swl_type ## name ## _addToMap(amxc_var_t * map, const char* param, cType srcData) { \
        return swl_type_addToMap(swlType, map, param, srcData);} \
    static inline bool swl_type ## name ## _findInMap(amxc_var_t * map, const char* param, cType * tgtData) { \
        return swl_type_findInMap(swlType, map, param, tgtData);} \
    static inline cType swl_type ## name ## _findInMapDef(amxc_var_t * map, const char* param, cType defaultVal) { \
        cType tmpVal; \
        if(swl_type_findInMap(swlType, map, param, &tmpVal)) { \
            return tmpVal; \
        } \
        if(defaultVal) { \
            return swl_type_copy(swlType, defaultVal); \
        } \
        return NULL;} \
    SWL_AMX_TYPE_DEFINE_FUNCTIONS_GENERIC(name, swlType, cType)

#define SWL_TYPE_DEFINE_FUNCTIONS_REF(name, swlType, cType) \
    SWL_COMMON_TYPE_DEFINE_FUNCTIONS_REF(name, swlType, cType) \
    SWL_AMX_TYPE_DEFINE_FUNCTIONS_REF(name, swlType, cType)

SWL_AMX_TYPE_DEFINE_FUNCTIONS_VAL(Bool, swl_type_bool, bool);
SWL_AMX_TYPE_DEFINE_FUNCTIONS_VAL(Int8, swl_type_int8, int8_t)
SWL_AMX_TYPE_DEFINE_FUNCTIONS_VAL(Int16, swl_type_int16, int16_t)
SWL_AMX_TYPE_DEFINE_FUNCTIONS_VAL(Int32, swl_type_int32, int32_t)
SWL_AMX_TYPE_DEFINE_FUNCTIONS_VAL(Int64, swl_type_int64, int64_t)
SWL_AMX_TYPE_DEFINE_FUNCTIONS_VAL(UInt8, swl_type_uint8, uint8_t)
SWL_AMX_TYPE_DEFINE_FUNCTIONS_VAL(UInt16, swl_type_uint16, uint16_t)
SWL_AMX_TYPE_DEFINE_FUNCTIONS_VAL(UInt32, swl_type_uint32, uint32_t)
SWL_AMX_TYPE_DEFINE_FUNCTIONS_VAL(UInt64, swl_type_uint64, uint64_t)
SWL_AMX_TYPE_DEFINE_FUNCTIONS_VAL(Double, swl_type_double, double)
SWL_AMX_TYPE_DEFINE_FUNCTIONS_REF(CharPtr, swl_type_charPtr, char*)
//Note, void pointers are compared on their own values, not the memory pointing to.
SWL_AMX_TYPE_DEFINE_FUNCTIONS_VAL(VoidPtr, swl_type_voidPtr, void*)

#define SWL_AMX_TYPE_CHARBUF_EXT(size) \
    SWL_AMX_TYPE_DEFINE_FUNCTIONS_VAL(CharBuf ## size, swl_type_charBuf ## size, char*)

SWL_AMX_TYPE_CHARBUF_EXT(4);
SWL_AMX_TYPE_CHARBUF_EXT(8);
SWL_AMX_TYPE_CHARBUF_EXT(16);
SWL_AMX_TYPE_CHARBUF_EXT(18);
SWL_AMX_TYPE_CHARBUF_EXT(32);
SWL_AMX_TYPE_CHARBUF_EXT(64);
SWL_AMX_TYPE_CHARBUF_EXT(128);


#endif /* SRC_INCLUDE_SWL_TYPE_H_ */
